<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Actividades extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('noticias_model');
        $this->load->model('gcrecimiento_model'); 
        $this->load->model('actividades_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        if (isset($_GET['id'])) {
            $data['actividadesEditar']=  $this->actividades_model->get_Actividades($_GET['id'],$this->session->userdata('id_usuario'));
        }else{
           $data['actividades']=  $this->actividades_model->get_Actividades(null,$this->session->userdata('id_usuario')); 
        }        
        $data['dias'] = $this->gcrecimiento_model->get_Dias();
        $this->load->view('templatesBackend/header');
        $this->load->view('actividades_backend_view',$data);
        $this->load->view('templatesBackend/footer');
    }

    public function agregarActividad($value='')
    {
        $return = Array('ok'=>TRUE);

        $id = $this->input->post('id');
        $fecha = $this->input->post('fechaActividad');
        $hora = $this->input->post('horaActividad');
        $nombre = $this->input->post('nombreActividad');
        $dia = $this->input->post('diaActividad');
        $descripcion = $this->input->post('descripcion_actividad');

               
        $this->actividades_model->save_actividad($id,$this->session->userdata('id_usuario'),$dia,$fecha,$nombre,$hora,$descripcion);
        
        echo json_encode($return);  
        
    } 

   public function eliminarActividad($value='')
    {
        $return = Array('ok'=>1);
        $id = $this->input->post('id');
        if($id != null){             
             $this->actividades_model->removeActividad($this->session->userdata('id_usuario'),$id);
        }else{
            $return = Array('ok'=>2);
        }

        echo json_encode($return); 

    }

    public function ver_actividad_irregular($id)
    {
        $data['actividad'] = $this->actividades_model->get_Actividades($id,$this->session->userdata('id_usuario'));

        $this->load->view('sigle_actividad_irregular_view',$data);
        //var_dump($data);
    }
    
}