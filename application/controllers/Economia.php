<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Economia extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('gcrecimiento_model');
        $this->load->model('estadistica_model');
        $this->load->model('finanzas_model');
        $this->load->model('economia_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        $data['titulo']="Economia";
        $data['actividades'] = $this->economia_model->get_Conceptos();
        $data['valores'] = $this->economia_model->get_Valores();
        $this->load->view('templatesBackend/header',$data);
        $this->load->view('economia_view',$data);
        $this->load->view('templatesBackend/footer',$data);
    }

    public function agregarDatos($value='')
    {
        $id = $this->input->post('id');
        $concepto = $this->input->post('concepto');
        $dato2010 = $this->input->post('2010');
        $dato2011 = $this->input->post('2011');
        $dato2012 = $this->input->post('2012');
        $dato2013 = $this->input->post('2013');
        $dato2014 = $this->input->post('2014');


        if($this->session->userdata('is_logued_in'))
            {
                if(
                    $concepto != null &&
                    $dato2010 != null &&
                    $dato2011 != null &&
                    $dato2012 != null &&
                    $dato2013 != null &&
                    $dato2014  != null
                    )
                {
                    $this->economia_model->save_Datos($concepto,$dato2010,$dato2011,$dato2012,$dato2013,$dato2014);               
                       
                    $validado = 1;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);

                } 
                else{
                    $validado = 2;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);
                }  
                
            }else{
                redirect(base_url().'index.php/login');
            }
        
    }

    
}