<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Estadistica extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('estadistica_model');
        $this->load->model('gcrecimiento_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        $data['titulo'] = 'Estadistica';
        $data['actividadesRegulares'] = $this->estadistica_model->get_ActividadesRegulares();
    	$this->load->view('templatesBackend/header',$data);
    	$this->load->view('estadistica_view',$data);
    	$this->load->view('templatesBackend/footer',$data);
       // var_dump($data['miembros']);
    }

    public function getEstadisticaAsistencia()
    {
        
        $sexo= $this->input->post('sexo');
        $ministerio = $this->input->post('ministerio');
        $fechaInicial= $this->input->post('fechaInicial');
        $fechaFinal = $this->input->post('fechaFinal');
        $actividad = $this->input->post('actividad');

        if($sexo > 1 && $ministerio == 0 && $actividad == 9 && $fechaInicial == "" && $fechaFinal ==""){
            if($sexo==2){
                $sexo = 'M';
                $data['porcentajeEstadistica'] = $this->estadistica_model->get_PorcentajeAsistenciaEstadistico3($sexo,$ministerio,$actividad,null,null);
                $data['grafica']= 1;
            }
            if($sexo==3){
                $sexo = 'F';
                $data['grafica']= 2;
                $data['porcentajeEstadistica'] = $this->estadistica_model->get_PorcentajeAsistenciaEstadistico3($sexo,$ministerio,$actividad,null,null);
            }
        } 

        if($sexo > 1 && $ministerio == 0 && $actividad == 9 && $fechaInicial != "" && $fechaFinal !=""){
            if($sexo==2){
                $sexo = 'M';
                $data['porcentajeEstadistica'] = $this->estadistica_model->get_PorcentajeAsistenciaEstadistico3($sexo,$ministerio,$actividad,$fechaInicial,$fechaFinal);
                $data['grafica']= 1;
            }
            if($sexo==3){
                $sexo = 'F';
                $data['grafica']= 2;
                $data['porcentajeEstadistica'] = $this->estadistica_model->get_PorcentajeAsistenciaEstadistico3($sexo,$ministerio,$actividad,$fechaInicial,$fechaFinal);
            }
        } 

        if($sexo == 1 && $ministerio == 0 && $fechaInicial == "" && $actividad == 9){

            $dato['porcentajeEstadisticaHombres'] = $this->estadistica_model->get_PorcentajeAsistenciaEstadistico3('M',$ministerio,$actividad,null,null);
            $dato['porcentajeEstadisticaMujeres'] = $this->estadistica_model->get_PorcentajeAsistenciaEstadistico3('F',$ministerio,$actividad,null,null);
            $data['porcentajeEstadistica'] = $dato;
            $data['grafica'] =3;
        }

        if($sexo == 1 && $ministerio == 0 && $fechaInicial != "" && $fechaFinal != "" && $actividad == 9){

            $dato['porcentajeEstadisticaHombres'] = $this->estadistica_model->get_PorcentajeAsistenciaEstadistico3('M',$ministerio,$actividad,$fechaInicial,$fechaFinal);
            $dato['porcentajeEstadisticaMujeres'] = $this->estadistica_model->get_PorcentajeAsistenciaEstadistico3('F',$ministerio,$actividad,$fechaInicial,$fechaFinal);
            $data['porcentajeEstadistica'] = $dato;
            $data['grafica'] =3;
        }

        //Cuando el sexo es Todos, la Actividad Sea Toda

        if($sexo == 1 && $ministerio == 0 && $fechaInicial == "" && $actividad == 8){
            $data1['asistenciaPresente']= $this->estadistica_model->get_Total_Asistencias_Presentes_Agrupadas_Por_Actividad(2,null,null);
            $data1['totalAsistencia'] = $this->estadistica_model->get_Total_Asistencias_Agrupadas_Por_Actividad(2,null,null);

            $data2['asistenciaPresente']= $this->estadistica_model->get_Total_Asistencias_Presentes_Agrupadas_Por_Actividad(3,null,null);
            $data2['totalAsistencia'] = $this->estadistica_model->get_Total_Asistencias_Agrupadas_Por_Actividad(3,null,null);

            for ($i=0; $i < sizeof($data1['asistenciaPresente']); $i++) { 
                $dato[$i]=($data1['asistenciaPresente'][$i]['TotalAsistenciaPresente']/$data1['totalAsistencia'][$i]['TotalAsistencia'])*100;
                $dato2[$i] = $data1['asistenciaPresente'][$i]['nombreActividadRegular'];
            }

            for ($i=0; $i < sizeof($data2['asistenciaPresente']); $i++) { 
                $dato3[$i]=($data2['asistenciaPresente'][$i]['TotalAsistenciaPresente']/$data2['totalAsistencia'][$i]['TotalAsistencia'])*100;
                $dato4[$i] = $data2['asistenciaPresente'][$i]['nombreActividadRegular'];
            }

            $data['nombreActividad'] = $dato2;
            $data['porcentajeActividad'] = $dato;
            $data['nombreActividadF'] = $dato4;
            $data['porcentajeActividadF'] = $dato3;
            $data['grafica']= 4;
        }

        if($sexo == 1 && $ministerio == 0 && $fechaInicial != "" && $fechaFinal != "" && $actividad == 8){
            $data1['asistenciaPresente']= $this->estadistica_model->get_Total_Asistencias_Presentes_Agrupadas_Por_Actividad(2,$fechaInicial,$fechaFinal);
            $data1['totalAsistencia'] = $this->estadistica_model->get_Total_Asistencias_Agrupadas_Por_Actividad(2,$fechaInicial,$fechaFinal);

            $data2['asistenciaPresente']= $this->estadistica_model->get_Total_Asistencias_Presentes_Agrupadas_Por_Actividad(3,$fechaInicial,$fechaFinal);
            $data2['totalAsistencia'] = $this->estadistica_model->get_Total_Asistencias_Agrupadas_Por_Actividad(3,$fechaInicial,$fechaFinal);

            for ($i=0; $i < sizeof($data1['asistenciaPresente']); $i++) { 
                $dato[$i]=($data1['asistenciaPresente'][$i]['TotalAsistenciaPresente']/$data1['totalAsistencia'][$i]['TotalAsistencia'])*100;
                $dato2[$i] = $data1['asistenciaPresente'][$i]['nombreActividadRegular'];
            }

            for ($i=0; $i < sizeof($data2['asistenciaPresente']); $i++) { 
                $dato3[$i]=($data2['asistenciaPresente'][$i]['TotalAsistenciaPresente']/$data2['totalAsistencia'][$i]['TotalAsistencia'])*100;
                $dato4[$i] = $data2['asistenciaPresente'][$i]['nombreActividadRegular'];
            }

            $data['nombreActividad'] = $dato2;
            $data['porcentajeActividad'] = $dato;
            $data['nombreActividadF'] = $dato4;
            $data['porcentajeActividadF'] = $dato3;
            $data['grafica']= 4;
        }

        if($sexo > 1 && $ministerio == 0 && $fechaInicial == "" && $actividad == 8){
            $data1['asistenciaPresente']= $this->estadistica_model->get_Total_Asistencias_Presentes_Agrupadas_Por_Actividad($sexo,null,null);
            $data1['totalAsistencia'] = $this->estadistica_model->get_Total_Asistencias_Agrupadas_Por_Actividad($sexo,null,null);

            for ($i=0; $i < sizeof($data1['asistenciaPresente']); $i++) { 
                $dato[$i]=($data1['asistenciaPresente'][$i]['TotalAsistenciaPresente']/$data1['totalAsistencia'][$i]['TotalAsistencia'])*100;
                $dato2[$i] = $data1['asistenciaPresente'][$i]['nombreActividadRegular'];
            }

            $data['nombreActividad'] = $dato2;
            $data['porcentajeActividad'] = $dato;
            $data['grafica']= 5;
            $data['sexo']=$sexo;
        }

        if($sexo > 1 && $ministerio == 0 && $fechaInicial != "" && $fechaFinal != "" && $actividad == 8){
            $data1['asistenciaPresente']= $this->estadistica_model->get_Total_Asistencias_Presentes_Agrupadas_Por_Actividad($sexo,$fechaInicial,$fechaFinal);
            $data1['totalAsistencia'] = $this->estadistica_model->get_Total_Asistencias_Agrupadas_Por_Actividad($sexo,$fechaInicial,$fechaFinal);

            for ($i=0; $i < sizeof($data1['asistenciaPresente']); $i++) { 
                $dato[$i]=($data1['asistenciaPresente'][$i]['TotalAsistenciaPresente']/$data1['totalAsistencia'][$i]['TotalAsistencia'])*100;
                $dato2[$i] = $data1['asistenciaPresente'][$i]['nombreActividadRegular'];
            }

            $data['nombreActividad'] = $dato2;
            $data['porcentajeActividad'] = $dato;
            $data['grafica']= 5;
            $data['sexo']=$sexo;
        }

        if($sexo == 0  && $ministerio == 0 && $fechaInicial == "" && $actividad == 8){
            $data1['asistenciaPresente']= $this->estadistica_model->get_Total_Asistencias_Presentes_Agrupadas_Por_Actividad(null,null,null);
            $data1['totalAsistencia'] = $this->estadistica_model->get_Total_Asistencias_Agrupadas_Por_Actividad(null,null,null);

            for ($i=0; $i < sizeof($data1['asistenciaPresente']); $i++) { 
                $dato[$i]=($data1['asistenciaPresente'][$i]['TotalAsistenciaPresente']/$data1['totalAsistencia'][$i]['TotalAsistencia'])*100;
                $dato2[$i] = $data1['asistenciaPresente'][$i]['nombreActividadRegular'];
            }

            $data['nombreActividad'] = $dato2;
            $data['porcentajeActividad'] = $dato;
            $data['grafica']= 6;
            $data['sexo']=$sexo;
        }

        if($sexo == 0  && $ministerio == 0 && $fechaInicial != "" && $fechaFinal != "" && $actividad == 8){
            $data1['asistenciaPresente']= $this->estadistica_model->get_Total_Asistencias_Presentes_Agrupadas_Por_Actividad(null,$fechaInicial,$fechaFinal);
            $data1['totalAsistencia'] = $this->estadistica_model->get_Total_Asistencias_Agrupadas_Por_Actividad(null,$fechaInicial,$fechaFinal);

            for ($i=0; $i < sizeof($data1['asistenciaPresente']); $i++) { 
                $dato[$i]=($data1['asistenciaPresente'][$i]['TotalAsistenciaPresente']/$data1['totalAsistencia'][$i]['TotalAsistencia'])*100;
                $dato2[$i] = $data1['asistenciaPresente'][$i]['nombreActividadRegular'];
            }

            $data['nombreActividad'] = $dato2;
            $data['porcentajeActividad'] = $dato;
            $data['grafica']= 6;
            $data['sexo']=$sexo;
        }


        if($sexo > 1 && $ministerio == 0 && $fechaInicial == "" && $actividad >= 1 && $actividad != 9 && $actividad != 8){
            $data1['asistenciaPresente']= $this->estadistica_model->get_Total_Asistencias_Presentes_Por_Actividad($sexo,$actividad,null,null);
            $data1['totalAsistencia'] = $this->estadistica_model->get_Total_Asistencias_Por_Actividad($sexo,$actividad,null,null);
            $data1['porcentaje'] = ($data1['asistenciaPresente'][0]['TotalAsistencia']/$data1['totalAsistencia'][0]['TotalAsistencia'])*100;

            $data['nombreActividad'] = $data1['asistenciaPresente'][0]['nombreActividadRegular'];
            $data['porcentajeActividad'] = $data1['porcentaje'];
            $data['grafica']= 7;
            $data['sexo']=$sexo;
        }

        if($sexo > 1 && $ministerio == 0 && $fechaInicial != "" && $fechaFinal != "" && $actividad >= 1 && $actividad != 9 && $actividad != 8){
            $data1['asistenciaPresente']= $this->estadistica_model->get_Total_Asistencias_Presentes_Por_Actividad($sexo,$actividad,$fechaInicial,$fechaFinal);
            $data1['totalAsistencia'] = $this->estadistica_model->get_Total_Asistencias_Por_Actividad($sexo,$actividad,$fechaInicial,$fechaFinal);
            $data1['porcentaje'] = ($data1['asistenciaPresente'][0]['TotalAsistencia']/$data1['totalAsistencia'][0]['TotalAsistencia'])*100;

            $data['nombreActividad'] = $data1['asistenciaPresente'][0]['nombreActividadRegular'];
            $data['porcentajeActividad'] = $data1['porcentaje'];
            $data['grafica']= 7;
            $data['sexo']=$sexo;
        }

        if($sexo == 0 && $ministerio == 0 && $fechaInicial == "" && $actividad >= 1 && $actividad != 9 && $actividad != 8){
            $data1['asistenciaPresente']= $this->estadistica_model->get_Total_Asistencias_Presentes_Por_Actividad(null,$actividad,null,null);
            $data1['totalAsistencia'] = $this->estadistica_model->get_Total_Asistencias_Por_Actividad(null,$actividad,null,null);
            $data1['porcentaje'] = ($data1['asistenciaPresente'][0]['TotalAsistencia']/$data1['totalAsistencia'][0]['TotalAsistencia'])*100;

            $data['nombreActividad'] = $data1['asistenciaPresente'][0]['nombreActividadRegular'];
            $data['porcentajeActividad'] = $data1['porcentaje'];
            $data['grafica']= 8;
            $data['sexo']=$sexo;
        }

        if($sexo == 0 && $ministerio == 0 && $fechaInicial != "" && $fechaFinal != "" && $actividad >= 1 && $actividad != 9 && $actividad != 8){
            $data1['asistenciaPresente']= $this->estadistica_model->get_Total_Asistencias_Presentes_Por_Actividad(null,$actividad,$fechaInicial,$fechaFinal);
            $data1['totalAsistencia'] = $this->estadistica_model->get_Total_Asistencias_Por_Actividad(null,$actividad,$fechaInicial,$fechaFinal);
            $data1['porcentaje'] = ($data1['asistenciaPresente'][0]['TotalAsistencia']/$data1['totalAsistencia'][0]['TotalAsistencia'])*100;

            $data['nombreActividad'] = $data1['asistenciaPresente'][0]['nombreActividadRegular'];
            $data['porcentajeActividad'] = $data1['porcentaje'];
            $data['grafica']= 8;
            $data['sexo']=$sexo;
        }

        if($sexo == 1 && $ministerio == 0 && $fechaInicial == "" && $actividad >= 1 && $actividad != 9 && $actividad != 8){
            $data1['asistenciaPresenteHombre']= $this->estadistica_model->get_Total_Asistencias_Presentes_Por_Actividad(2,$actividad,null,null);
            $data1['totalAsistenciaHombre'] = $this->estadistica_model->get_Total_Asistencias_Por_Actividad(2,$actividad,null,null);
            $data1['porcentajeHombre'] = ($data1['asistenciaPresenteHombre'][0]['TotalAsistencia']/$data1['totalAsistenciaHombre'][0]['TotalAsistencia'])*100;

            $data2['asistenciaPresenteMujer'] = $this->estadistica_model->get_Total_Asistencias_Presentes_Por_Actividad(3,$actividad,$fechaInicial,$fechaFinal);
            $data2['totalAsistenciaMujer'] = $this->estadistica_model->get_Total_Asistencias_Por_Actividad(3,$actividad,$fechaInicial,$fechaFinal);
            $data2['porcentajeMujer'] = ($data2['asistenciaPresenteMujer'][0]['TotalAsistencia']/$data2['totalAsistenciaMujer'][0]['TotalAsistencia'])*100;

            $data['nombreActividad'] = $data2['asistenciaPresenteMujer'][0]['nombreActividadRegular'];
            $data['porcentajeActividad'] = $data1['porcentajeHombre'];
            $data['porcentajeActividadF'] = $data2['porcentajeMujer'];
            $data['grafica']= 9;
            $data['sexo']=$sexo;

        }

        if($sexo == 1 && $ministerio == 0 && $fechaInicial != "" && $fechaFinal != "" && $actividad >= 1 && $actividad != 9 && $actividad != 8){
            $data1['asistenciaPresenteHombre']= $this->estadistica_model->get_Total_Asistencias_Presentes_Por_Actividad(2,$actividad,$fechaInicial,$fechaFinal);
            $data1['totalAsistenciaHombre'] = $this->estadistica_model->get_Total_Asistencias_Por_Actividad(2,$actividad,$fechaInicial,$fechaFinal);
            $data1['porcentajeHombre'] = ($data1['asistenciaPresenteHombre'][0]['TotalAsistencia']/$data1['totalAsistenciaHombre'][0]['TotalAsistencia'])*100;

            $data2['asistenciaPresenteMujer']= $this->estadistica_model->get_Total_Asistencias_Presentes_Por_Actividad(3,$actividad);
            $data2['totalAsistenciaMujer'] = $this->estadistica_model->get_Total_Asistencias_Por_Actividad(3,$actividad);
            $data2['porcentajeMujer'] = ($data2['asistenciaPresenteMujer'][0]['TotalAsistencia']/$data2['totalAsistenciaMujer'][0]['TotalAsistencia'])*100;

            $data['nombreActividad'] = $data2['asistenciaPresenteMujer'][0]['nombreActividadRegular'];
            $data['porcentajeActividad'] = $data1['porcentajeHombre'];
            $data['porcentajeActividadF'] = $data2['porcentajeMujer'];
            $data['grafica']= 9;
            $data['sexo']=$sexo;

        }

        echo json_encode($data);

    }

    public function prueba()
    {
        $data1['asistenciaPresenteHombre']= $this->estadistica_model->get_Total_Asistencias_Presentes_Por_Actividad(2,1);
        $data1['totalAsistenciaHombre'] = $this->estadistica_model->get_Total_Asistencias_Por_Actividad(2,1);
        $data1['porcentajeHombre'] = ($data1['asistenciaPresenteHombre'][0]['TotalAsistencia']/$data1['totalAsistenciaHombre'][0]['TotalAsistencia'])*100;

        $data2['asistenciaPresenteMujer']= $this->estadistica_model->get_Total_Asistencias_Presentes_Por_Actividad(3,1);
        $data2['totalAsistenciaMujer'] = $this->estadistica_model->get_Total_Asistencias_Por_Actividad(3,1);
        $data2['porcentajeMujer'] = ($data2['asistenciaPresenteMujer'][0]['TotalAsistencia']/$data2['totalAsistenciaMujer'][0]['TotalAsistencia'])*100;

        $data['nombreActividad'] = $data2['asistenciaPresenteMujer'][0]['nombreActividadRegular'];
        $data['porcentajeActividad'] = $data1['porcentajeHombre'];
        $data['porcentajeActividadF'] = $data2['porcentajeMujer'];
        $data['grafica']= 6;


        var_dump($data);
    }
}