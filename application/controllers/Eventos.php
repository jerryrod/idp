<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Eventos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('gcrecimiento_model');
        $this->load->model('evento_model');
        $this->load->model('estadistica_model');
        $this->load->model('ministerios_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        if (isset($_GET['id'])) {
            $data['miembro'] = $this->miembros_model->get_Miembro_Editar($this->session->userdata('id_usuario'),$_GET['id']);
        }

        if(isset($_GET['a'])==6 && isset($_GET['']))

    	$data['titulo'] = "Pagina de Inicio";
        $data['estados'] = $this->miembros_model->get_Estados();
        $data['carreras'] = $this->miembros_model->get_Carreras();
        $data['actividadesRegulares'] = $this->estadistica_model->get_ActividadesRegulares();;
        $data['celulas'] = $this->gcrecimiento_model->get_Celulas_Add_Miembro($this->session->userdata('id_usuario'));
        $data['responsable'] = $this->miembros_model->get_Responsables($this->session->userdata('id_usuario'));
        $data['miembros'] = $this->miembros_model->get_Miembros($this->session->userdata('id_usuario'),null);
        $data['actividades'] =  $this->miembros_model->get_Actividades($this->session->userdata('id_usuario'));
        $data['eventos'] = $this->evento_model->get_Eventos($this->session->userdata('id_usuario'),null);
        $data['miembrosActivos'] = $this->miembros_model->get_MiembrosActivos($this->session->userdata('id_usuario'));
        $data['miembrosInactivos'] = $this->miembros_model->get_MiembrosInactivos($this->session->userdata('id_usuario'));
        $data['miembrosDescarriados'] = $this->miembros_model->get_MiembrosDescarriados($this->session->userdata('id_usuario'));
        $data['miembrosTrasladados'] = $this->miembros_model->get_MiembrosTrasladados($this->session->userdata('id_usuario'));
        $data['miembrosExterior'] = $this->miembros_model->get_MiembrosExterior($this->session->userdata('id_usuario'));
        $data['miembrosFallecidos'] = $this->miembros_model->get_MiembrosFallecidos($this->session->userdata('id_usuario'));
        $data['miembrosTotal'] = $this->miembros_model->get_MiembrosCount($this->session->userdata('id_usuario'));
        $data['miembrosTotalHombre'] = $this->miembros_model->get_MiembrosCountHombre($this->session->userdata('id_usuario'));
        $data['miembrosTotalMujer'] = $this->miembros_model->get_MiembrosCountMujer($this->session->userdata('id_usuario'));
        $data['ministerios'] = $this->ministerios_model->get_Nombres_Ministerios($this->session->userdata('id_usuario'));
    	$this->load->view('templatesBackend/header',$data);
    	$this->load->view('eventos_view',$data);
    	$this->load->view('templatesBackend/footer',$data);
       // var_dump($data['miembros']);
    }   

 

    public function agregarEvento($value='')
    {
        $validado = false;
        $nombreEvento= $this->input->post('nombre_evento');
        $responsable = $this->input->post('responsable_evento');
        $ministerio = $this->input->post('ministerio');
        $fechaEvento = $this->input->post('fecha_evento');
        $descripcion = $this->input->post('descripcion_evento');

        //var_dump($id);

        if($this->session->userdata('is_logued_in'))
            {
                 if(
                    $nombreEvento != null &&
                    $responsable != null &&
                    $fechaEvento != null && 
                    $descripcion != null  ){
                   
                    
                    $this->evento_model->save_Evento(null,$nombreEvento,$fechaEvento,null,$descripcion,$responsable,$this->session->userdata('id_usuario'),$ministerio);

                    $validado = 1;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);
                 }
                 else{
                    $validado = 2;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);
                }  
            }
            else{
                redirect(base_url().'index.php/login');
            }
    }

   

    public function ver_Evento($id)
    {
        $data['evento'] = $this->miembros_model->get_Evento( $this->session->userdata('id_usuario'),$id);
        $this->load->view('sigle_evento_view_evento',$data);
        //var_dump($data['miembro']);
    }

    public function ver_Estadistica($id)
    {
        $data['cantidadAsistenciaPresente'] = $this->miembros_model->get_Estadistica_Evento_Presentes( $this->session->userdata('id_usuario'),$id);
        $data['cantidadAsistencia'] = $this->miembros_model->get_Estadistica_Evento( $this->session->userdata('id_usuario'),$id);
        $data['estadisticaAsistecia'] = ($data['cantidadAsistenciaPresente']->cantidadTotal/$data['cantidadAsistencia']->cantidadTotal)*100;
        $data['miembrosTotal'] = $this->miembros_model->get_miembros_Por_Evento($this->session->userdata('id_usuario'),$id);
        $this->load->view('sigle_estadistica_evento_view',$data);
        //var_dump($data['miembro']);
    }



   
}