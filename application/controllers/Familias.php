<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Familias extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('familias_model');
        $this->load->model('miembros_model');
        $this->load->model('gcrecimiento_model');
        $this->load->model('estadistica_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
    	
    	$this->load->view('familiaForm_view',$data);    	
       // var_dump($data['miembros']);
    }

    public function formularioFamilia($value='')
    {
        $this->load->view('templatesBackend/headerFancy');
        $this->load->view('familiaForm_view');
    }


    public function agregarFamilia()
    {    
        $validado = false;
       
        $id= $this->input->post('id');
        $apellidoPaterno = $this->input->post('apellidoPaterno');
        $apellidoMaterno = $this->input->post('apellidoMaterno');
        $descripcion = $this->input->post('descripcion');
        

       if($this->session->userdata('is_logued_in')){
                if($apellidoPaterno != null &&
                    $apellidoMaterno != null)
                {
                    if($id==null){
                        $this->familias_model->save_Familia(null,$apellidoPaterno,$apellidoMaterno,$descripcion);
                    }

                    if($id>0){
                        $this->familias_model->save_Familia($id,$apellidoPaterno,$apellidoMaterno,$descripcion);                       
                       // $this->miembros_model->save_Descripcion_Cambio($id,$descripcionCambioEstado,$id_usuario,$antiguoEstado,$estado);
                    }

                    $validado = 1;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);

                } 
                else{
                    $validado = 2;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);
                }  
            
        }else{
            redirect(base_url().'index.php/login');
        }
        
    }

    public function verFamilia($id)
    {
        
        $data['familia'] = $this->familias_model->get_Familias($id);
        //var_dump($id);
        $this->load->view('templatesBackend/headerFancy');
        $this->load->view('familia_single_view',$data);
    }


    //Me permite Crear un Token unico para el nombre de las fotos y los album 
    function generateGuid($include_braces = false) {
        if (function_exists('com_create_guid')) {
            if ($include_braces === true) {
                return com_create_guid();
            } else {
                return substr(com_create_guid(), 1, 36);
            }
        } else {
            mt_srand((double) microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));

            $guid = substr($charid,  0, 8) . '-' .
                    substr($charid,  8, 4) . '-' .
                    substr($charid, 12, 4) . '-' .
                    substr($charid, 16, 4) . '-' .
                    substr($charid, 20, 12);

            if ($include_braces) {
                $guid = '{' . $guid . '}';
            }

            return $guid;
        }
    } 



    public function eliminarImagenMiembro()
    {
        $return = Array('ok'=>TRUE);
        $id = $this->input->post('id');
        $nombreimagen = $this->input->post('imagen');

        $path = $_SERVER['DOCUMENT_ROOT'].'IDPGlobal/IPDNacionalRD/centralStoDgo/application/miembros/'.$nombreimagen;
        unlink($path);
        
        $this->miembros_model->eliminar_Imagen($id,'sin_imagen.gif');
     
        echo json_encode($return);          
        
    }

    public function agregarAsistencia($value='')
    {
        $validado = false;
        $idMiembro= $this->input->post('id');
        $nombre = $this->input->post('nombre');
        $valor = $this->input->post('valor');
        $fecha = $this->input->post('fecha');

        //var_dump($id);

        if($this->session->userdata('is_logued_in'))
            {
                 if(
                    $nombre != null && 
                    $fecha != null  ){
                    $arrayId=[];
                    $arrayValor=[];
                    $contador = 0;

                    foreach ($idMiembro as $key => $value) {
                       $arrayId[$key] = $value;
                       $contador++;
                    }

                    foreach ($valor as $key => $value) {
                       $arrayValor[$key] = $value;
                    }

                    $idEvento = $this->miembros_model->save_Actividad(null,$this->session->userdata('id_usuario'),$nombre,$fecha);


                    for($i=0;$i<$contador;$i++){
                        $this->miembros_model->save_Asistencia(null,$arrayId[$i],$this->session->userdata('id_usuario'),$arrayValor[$i],$idEvento,$fecha,$nombre,null);
                    }

                   // $this->miembros_model->save_Actividad(null,$nombre,$fecha);



                    $validado = 1;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);
                 }
                 else{
                    $validado = 2;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);
                }  
            }
            else{
                redirect(base_url().'index.php/login');
            }
    }

   
    public function ver_Miembro($id)
    {
        $data['miembro'] = $this->miembros_model->get_Miembro($this->session->userdata('id_usuario'),$id);

        $existeAsistencia = $this->miembros_model->existeAsistencia($id);
        if($existeAsistencia > 0){
             $data['porcentajeAsistencia'] = $this->miembros_model->get_PorcentajeAsistencia( $this->session->userdata('id_usuario'),$id);
        }else{
             $data['porcentajeAsistencia'] = 0;
        }
       
        $data['lider'] = $this->gcrecimiento_model->get_Lider($this->session->userdata('id_usuario'),$data['miembro']->celula);
        $this->load->view('sigle_miembro_view',$data);
        //var_dump($data['miembro']->celula);
    }

    public function ver_MiembroBienbenido()
    {
        $id = $this->input->post('id');
        $data['miembro'] = $this->miembros_model->get_Miembro($this->session->userdata('id_usuario'),$id);

        $existeAsistencia = $this->miembros_model->existeAsistencia($id);
        if($existeAsistencia > 0){
             $data['porcentajeAsistencia'] = $this->miembros_model->get_PorcentajeAsistencia($this->session->userdata('id_usuario'),$id);
        }else{
             $data['porcentajeAsistencia'] = 0;
        }
       
        $data['lider'] = $this->gcrecimiento_model->get_Lider($this->session->userdata('id_usuario'),$data['miembro']->celula);
        //$this->load->view('sigle_miembro_view',$data);
        echo json_encode($data);
        //var_dump($data['miembro']->celula);
    }

    public function getMiembroPorNombre(){
        if($_GET['nombre'] != null){
            $nombre = $_GET['nombre'];
            $data['miembrosN'] = $this->miembros_model->get_Miembro_Por_Nombre($this->session->userdata('id_usuario'),$nombre);        
        }else{
            $data['miembrosN'] =  $this->miembros_model->get_Miembros($this->session->userdata('id_usuario'));
        }
        echo json_encode($data);
    }

    public function porcentajeIndividualPorMiembrosColor($id)
    {
    	$existeAsistencia = $this->miembros_model->existeAsistencia($id);
        if($existeAsistencia > 0){
            $data['porcentajeAsistencia'] = $this->miembros_model->get_PorcentajeAsistencia($this->session->userdata('id_usuario'),$id);
            return $data['porcentajeAsistencia'];  
        }else{
            return $existeAsistencia;
        }       
            
    }

    public function ver_Actividad($id)
    {
        $data['actividad'] = $this->miembros_model->get_Actividad( $this->session->userdata('id_usuario'),$id);
        $this->load->view('sigle_evento_view',$data);
        //var_dump($data['miembro']);
    }


    public function ver_Estadistica($id)
    {
        $data['cantidadAsistenciaPresente'] = $this->miembros_model->get_Estadistica_Evento_Presentes( $this->session->userdata('id_usuario'),$id);
        $data['cantidadAsistencia'] = $this->miembros_model->get_Estadistica_Evento( $this->session->userdata('id_usuario'),$id);
        $data['estadisticaAsistecia'] = ($data['cantidadAsistenciaPresente']->cantidadTotal/$data['cantidadAsistencia']->cantidadTotal)*100;
        $data['miembrosTotal'] = $this->miembros_model->get_miembros_Por_Evento($this->session->userdata('id_usuario'),$id);
        $this->load->view('sigle_estadistica_evento_view',$data);
        //var_dump($data['miembro']);
    }



    public function actualizarCelulaMiembro($id)
    {
        //$celula = $this->input->post('celula');

        if(isset($id)){
                    
            $this->miembros_model->update_Miembro_Celula($this->session->userdata('id_usuario'),$id);
            $validado = 1;
            $va = '{"validado": '.$validado.'}';
            echo json_encode($va);
         }
         else{
            $validado = 2;
            $va = '{"validado": '.$validado.'}';
            echo json_encode($va);
            //$data['miembrosIntegrantes'] = $this->gcrecimiento_model->get_Miembros_Por_Celula_model($this->session->userdata('id_usuario'),$celula);
            //var_dump($data['miembrosIntegrantes']);
            //echo json_encode($data['miembrosIntegrantes']) ; 
            //var_dump($id);
        }
    }
}