<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Finanzas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('gcrecimiento_model');
        $this->load->model('estadistica_model');
        $this->load->model('finanzas_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        $data['titulo']="Finanzas";
        $data['actividades'] = $this->finanzas_model->get_Actividades();
        $data['ofrenda'] = $this->finanzas_model->get_Ofrendas($this->session->userdata('id_usuario'),null);
        $this->load->view('templatesBackend/header',$data);
        $this->load->view('finanzas_view',$data);
        $this->load->view('templatesBackend/footer',$data);
        //var_dump($data['actividades']);
    }

    public function agregarOfrenda($value='')
    {
        $id = $this->input->post('id');
        $actividad = $this->input->post('actividad');
        $cantidadBruta = $this->input->post('cantidadBruta');
        $fecha = $this->input->post('fecha');
        $gastos = $this->input->post('gastos');
        $descripcionGastos = $this->input->post('descripcionGastos');
        $cantidadNeta = $this->input->post('cantidadNeta');
        $gastosTotal = $this->input->post('gastosTotal');


        if($this->session->userdata('is_logued_in'))
            {
                if(
                    $actividad != null &&
                    $cantidadBruta != null &&
                    $fecha != null &&
                    $cantidadNeta != null &&
                    $gastosTotal  != null
                    )
                {
                	if($gastos != null){
                		$contador = 0;
	                    $arrayValorGasto = [];
	                    $arrayDescripcionGasto = [];
	                    foreach ($gastos as $key => $value) {
	                       $arrayValorGasto[$key] = $value;
	                       $contador++;
	                    }

	                    foreach ($descripcionGastos as $key => $value) {
	                       $arrayDescripcionGasto[$key] = $value;
	                    }
                	}
                    

                    if($id==null){
                       $idOfrenda = $this->finanzas_model->save_Ofrenda(null,$cantidadNeta,$cantidadBruta,$actividad,$this->session->userdata('id_usuario'),$fecha,$gastosTotal);

                       	if($gastos != null){
                       		for($i=0;$i<$contador;$i++){
                           		$this->finanzas_model->save_Gastos($idOfrenda,$this->session->userdata('id_usuario'),$arrayDescripcionGasto[$i],$arrayValorGasto[$i],$fecha);
                        	} 
                       	}
                                               
                    }

                    if($id>0){
                        $this->finanzas_model->save_Ofrenda($id,$nombre,$apellido,$email,$telefono,$estado,$estadoCivil,$estadoEducativo,$nivelEducativo,$estadoLaboral,$sexo,$fechaNacimiento,$fechaConversion,$carrera, $id_usuario,$celula);
                        $this->miembros_model->save_Descripcion_Cambio($id,$descripcionCambioEstado,$id_usuario,$antiguoEstado,$estado);
                    }

                    $validado = 1;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);

                } 
                else{
                    $validado = 2;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);
                }  
                
            }else{
                redirect(base_url().'index.php/login');
            }
        
    }

    public function ver_gastos($id)
    {
        $data['id'] = $id;
        $data['ofrenda'] = $this->finanzas_model->get_Ofrendas($this->session->userdata('id_usuario'),$id);
        $data['gastos'] =  $this->finanzas_model->get_Gastos($this->session->userdata('id_usuario'),$id);
        $this->load->view('finanzas_gastos_sigle',$data);
    }
}