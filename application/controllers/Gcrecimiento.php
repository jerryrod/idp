<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Gcrecimiento extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('gcrecimiento_model');        
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
    	if (isset($_GET['id'])) {
            $data['celula'] = $this->gcrecimiento_model->get_Celula_Editar($this->session->userdata('id_usuario'),$_GET['id']);
        }
        $data['celulaAddMiembro'] = $this->gcrecimiento_model->get_Celulas_Add_Miembro($this->session->userdata('id_usuario'));
    	$data['dias'] = $this->gcrecimiento_model->get_Dias();
    	$data['celulas'] = $this->gcrecimiento_model->get_Celulas($this->session->userdata('id_usuario'));
    	$data['miembros'] = $this->miembros_model->get_Responsables($this->session->userdata('id_usuario'));
    	$this->load->view('templatesBackend/header',$data);
    	$this->load->view('gcrecimiento_view',$data);
    	$this->load->view('templatesBackend/footer',$data);
    }

    public function get_Miembros_Por_Celula($id_celula)
    {
        $data['miembrosIntegrantes'] = $this->gcrecimiento_model->get_Miembros_Por_Celula_model($this->session->userdata('id_usuario'),$id_celula);
        //var_dump($data['miembrosIntegrantes']);
        return $data['miembrosIntegrantes']; 
    }


    public function agregarCelula($value='')
    {
        $validado = false;
        $id = $this->input->post('id');
        $lider= $this->input->post('lider_celula');
        $anfitrion = $this->input->post('anfitrion');
        $direccion = $this->input->post('direccion');
        $dia = $this->input->post('dia');
        $fecha_inicio = $this->input->post('fecha_inicio');
        $descripcion_cambio_lider = $this->input->post('descripcion_cambio_lider');
        $id_celula; 

       // var_dump($lider);

        if($this->session->userdata('is_logued_in'))
            {
                 if(
                    $lider != null &&
                    $anfitrion != null &&
                    $direccion != null && 
                    $dia != null  &&
                    $fecha_inicio != null){

                 	if($id==null){
                        $id_Celula = $this->gcrecimiento_model->save_Celula(null,$lider,$anfitrion,$direccion,$dia,$fecha_inicio,$this->session->userdata('id_usuario'));
                        $this->gcrecimiento_model->save_lider_celula($lider,$this->session->userdata('id_usuario'),$id_Celula,null);
                    }

                    if($id>0){
                        $dataCelula = $this->gcrecimiento_model->get_Celula_Single($this->session->userdata('id_usuario'),$id);
                        $this->gcrecimiento_model->save_Celula($id,$lider,$anfitrion,$direccion,$dia,$fecha_inicio,$this->session->userdata('id_usuario')); 
                        if($dataCelula->lider != $lider){
                            $this->gcrecimiento_model->save_Cambio_de_lider($dataCelula->lider, $descripcion_cambio_lider, $this->session->userdata('id_usuario'), $id, $lider);
                            $this->gcrecimiento_model->save_lider_celula($lider,$this->session->userdata('id_usuario'),$id);
                        }
                                            
                    }                   

                    $validado = 1;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);
                 }
                 else{
                    $validado = 2;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);
                }  
            }
            else{
                redirect(base_url().'index.php/login');
            }
    }

    public function prueba($id)
    {
         $dataCelula = $this->gcrecimiento_model->get_Celula_Single($this->session->userdata('id_usuario'),$id);
         if($dataCelula->lider == 19){
            var_dump($dataCelula->lider);
         }
         
    }

    public function agregarMiembroCelula($idCelula)
    {
        $data['id_celula'] = $idCelula;
        $data['miembros'] = $this->miembros_model->get_Miembros($this->session->userdata('id_usuario'));
        $this->load->view('sigle_addMiembroCelula_view',$data); 
        $this->load->view('templatesBackend/footer',$data);
    }

    public function actualizarMiembroCelula($value='')
    {
        $id_celula = $this->input->post('celula');
        $valor= $this->input->post('valor');
        $contador =0;

        //Cada checkbox seleccionado me lo agrega al array llamado arrayValor el indice sera el id del miembro a agregar. 
        //En la variable contador voy capturando la cantidad de checkbox seleccionado para tomarlo como parametro para 
        //el for que me enviara los datos al modelo. 
        foreach ($valor as $key => $value) {
                $arrayValor[$key] = $value;
                $contador++;
            }

        for($i=0;$i<$contador;$i++){
            $this->miembros_model->update_Miembro_Celula2($this->session->userdata('id_usuario'),$arrayValor[$i],$id_celula);
        }

        $validado = 1;
        $va = '{"validado": '.$validado.'}';
        echo json_encode($va);
                 
    }

    public function ver_Celula($id_celula)
    {
        $data['celula'] = $this->gcrecimiento_model->get_Celula_Single($this->session->userdata('id_usuario'),$id_celula);
        $this->load->view('single_celula_view',$data);
    }
}