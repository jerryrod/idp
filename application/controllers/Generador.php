<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Generador extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('gcrecimiento_model');
        $this->load->model('estadistica_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        if(!$this->session->userdata('is_logued_in')){
            redirect(base_url().'index.php/login');
         } 

        $data['data2'] = "Hola mundo";
        $data['miembros'] = $this->miembros_model->get_Miembro($this->session->userdata('id_usuario'),null);
        //$data['miembros'] = $this->miembros_model->get_Miembros($this->session->userdata('id_usuario'),null);
    	$this->load->view('templatesBackend/header',$data);
    	$this->load->view('generador_view',$data);
    	$this->load->view('templatesBackend/footer',$data);
        //var_dump($data['miembros']);
    }
}