<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
       
    	$this->load->model('Login_model');
        $this->load->model('noticias_model');
        $this->load->model('actividades_model');
    	$this->load->library(array('session','form_validation'));
    	$this->load->helper(array('url','form'));
    	$this->load->database('default');
	
    }

    public function index($value='')
    {
        $data['noticias'] = $this->noticias_model->get_Noticias(null,$this->session->userdata('id_usuario'));
        $data['actividades']=  $this->actividades_model->get_Actividades(null,$this->session->userdata('id_usuario'));
    	$data['titulo'] = "Pagina de Inicio";
        $data['active'] = 'homeActive';
    	$this->load->view('templates/headerSite',$data);
    	$this->load->view('home_view_site',$data);
    	$this->load->view('templates/footerSite');
    	
    	//$this->load->view('welcome_message');
        //var_dump($data['noticias']);
    }
}