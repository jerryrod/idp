<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

   

 
class HomeBackend extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('Homebackend_model');
        $this->load->model('miembros_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }


    public function index($value='')
    {
        if(!$this->session->userdata('is_logued_in')){
         redirect(base_url().'index.php/login');
         } 

    	$data['titulo'] = "Pagina de Inicio";
        //Escuela Biblica
        $data['ultimoEscuelaBiblicaSemana'] = $this->homeBackend_model->get_UltimaActividad($this->session->userdata('id_usuario'),3);

        //-----Adultos
        $data['AdultosPresentes'] = $this->homeBackend_model->get_AdultosPresentes($this->session->userdata('id_usuario'),$data['ultimoEscuelaBiblicaSemana']->id,'a');
        $data['cantidadMiembrosAdultos'] = $this->homeBackend_model->getCantidadMiembroPorGrupo( $this->session->userdata('id_usuario'),$data['ultimoEscuelaBiblicaSemana']->id,'a');

        //-----Jovenes
        $data['JovenesPresentes'] = $this->homeBackend_model->get_AdultosPresentes($this->session->userdata('id_usuario'),$data['ultimoEscuelaBiblicaSemana']->id,'j');        
        $data['cantidadMiembrosJovenes'] = $this->homeBackend_model->getCantidadMiembroPorGrupo( $this->session->userdata('id_usuario'),$data['ultimoEscuelaBiblicaSemana']->id,'j');


        //Culto Viernes
        $data['ultimoCultoViernes'] = $this->homeBackend_model->get_UltimaActividad($this->session->userdata('id_usuario'),2);
        $data['totalPresentesViernes'] = $this->homeBackend_model->get_AdultosPresentes($this->session->userdata('id_usuario'),$data['ultimoCultoViernes']->id,null);
        $data['cantidadMiembros'] = $this->homeBackend_model->getCantidadMiembroPorGrupo( $this->session->userdata('id_usuario'),$data['ultimoCultoViernes']->id,null);

        //Culto Domingo
        $data['ultimoCultoDomingo'] = $this->homeBackend_model->get_UltimaActividad($this->session->userdata('id_usuario'),1);
        $data['totalPresentesDomingo'] = $this->homeBackend_model->get_AdultosPresentes($this->session->userdata('id_usuario'),$data['ultimoCultoDomingo']->id,null);
        $data['cantidadMiembros2'] = $this->homeBackend_model->getCantidadMiembroPorGrupo( $this->session->userdata('id_usuario'),$data['ultimoCultoDomingo']->id,null);

        //Actividades Semanales
        $data['actividadesSemanales'] = $this->homeBackend_model->get_ActividadesSemanal();

        //Actividades Semanales 2
        $data['actividadesSemanales2'] = $this->homeBackend_model->get_ActividadesSemanal2();

        if($data['actividadesSemanales'] == null){
            $data['actividadesSemanales'] = $data['actividadesSemanales2'];
            $data['actividadesSemanales2'] = $this->homeBackend_model->get_ActividadesSemanal3();
        }

        //$this->generarArchivo($data['actividadesSemanales2']);

       
    	$this->load->view('templatesBackend/header');
    	$this->load->view('homebakend_view',$data);
    	$this->load->view('templatesBackend/footer');

        var_dump($this->session->userdata('perfil'));
        
    }


    public function generarArchivo($value='')
    {
        $fichero = 'C:\Users\scatalino\Desktop\gente.txt';
        //var_dump($fichero);
        // Abre el fichero para obtener el contenido existente
        //$actual = file_get_contents($fichero);
        // Añade una nueva persona al fichero
        //$actual .= $value;
        // Escribe el contenido al fichero
        //file_put_contents($fichero, $actual);
        //$toBeSaved = serialize($value);
        $json_data = json_encode($value);
        file_put_contents($fichero, $json_data);
    }


}