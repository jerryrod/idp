<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Liderazgo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('liderazgo_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        $data['lideres']=  $this->liderazgo_model->get_Lideres_fijos(null,2);
        $data['active'] = 'liderazgoActive';
    	$this->load->view('templates/headerSite',$data);
    	$this->load->view('liderazgo_view_site',$data);
    	$this->load->view('templates/footerSite');
    }

    public function ver_Lider_Single($lider)
    {
        $data['lider']=  $this->liderazgo_model->get_Lider($lider,2);
        $this->load->view('templates/header');
        $this->load->view('ver_contenido_lider',$data);
        $this->load->view('templates/footer');
        //var_dump($data['lider']);
    }
}