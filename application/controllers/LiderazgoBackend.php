<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class LiderazgoBackend extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('liderazgo_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        if(isset($_GET['l']) && $_GET['l']==2){
            if (isset($_GET['id'])) {
                $data['lider'] = $this->liderazgo_model->get_Lider_Editar($this->session->userdata('id_usuario'),$_GET['id']);
            }
        }
        $data['responsable'] = $this->miembros_model->get_Responsables($this->session->userdata('id_usuario'));
        $data['lideres']=  $this->liderazgo_model->get_Lideres(null,$this->session->userdata('id_usuario'));
        $data['cargo'] = $this->liderazgo_model->get_Cargos();
    	$this->load->view('templatesBackend/header');
    	$this->load->view('liderazgo_backend_view',$data);
    	$this->load->view('templatesBackend/footer');
    }


    public function agregarLider($value='')
    {
        $return = Array('ok'=>1);
        $upload_folder =$_SERVER['DOCUMENT_ROOT'].'IDPGlobal/IPDNacionalRD/LosFrailes1/application/Lideres';
        $nombre_archivo = $_FILES['archivo']['name'];
        $tipo_archivo = $_FILES['archivo']['type'];
        $tamano_archivo = $_FILES['archivo']['size'];
        $tmp_archivo = $_FILES['archivo']['tmp_name'];

        $archivador = $upload_folder . '/' . $nombre_archivo;

        $id = $this->input->post('id');
        $fechaNombramiento = $this->input->post('fecha_nombramiento');
        $biografia = $this->input->post('biografia');
        $cargo = $this->input->post('cargo');
        $lider = $this->input->post('lider');
        $descripcion_cambio_lider = $this->input->post('descripcion_cambio_liderazgo');

        /*Si no cambio la imagen*/

        $existeCargo = $this->existeCargo($id_usuario,$cargo);

        
            if (!move_uploaded_file($tmp_archivo, $archivador)) {
                /*Obtengo los datos de ese nombramiento si es que existe.*/
                $dataLider = $this->liderazgo_model->get_Lider_Editar($this->session->userdata('id_usuario'),$id);

                /*Verifico que hay un cambio de lider en el mismo registro del nombramiento y si el objeto $dataLider no viene nulo*/
                if($dataLider->id_miembro != $lider && $dataLider->id_miembro != null){
                    $this->liderazgo_model->save_Lider(null,$lider,$cargo,$fechaNombramiento,$nombre_archivo,$this->session->userdata('id_usuario'),1,$biografia);
                    $this->liderazgo_model->save_Cambio_de_lider($dataLider->id_miembro, $descripcion_cambio_lider, $this->session->userdata('id_usuario'), $id, $lider,0);
                    $return = Array('ok' => 0, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
                }

                if($dataLider->id_miembro == $lider && $dataLider->id_miembro != null){
                    $this->liderazgo_model->save_Lider($id,$lider,$cargo,$fechaNombramiento,$nombre_archivo,$this->session->userdata('id_usuario'),1,$biografia);                   
                    $return = Array('ok' => 1, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
                }

                /*Si el objeto $dataLider es nulo es que es un nuevo cargo*/
                else{
                    /*Verifico que el cargo a insertar no este usado*/
                    if($existeCargo==0){
                        $this->liderazgo_model->save_Lider($id,$lider,$cargo,$fechaNombramiento,$nombre_archivo,$this->session->userdata('id_usuario'),1,$biografia);
                        $return = Array('ok' => 0, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
                    }else{
                        $return = Array('ok'=>0); 
                    }   
                }  
                
                

            }else{
                /*Si cambio la imagen*/
                $dataLider = $this->liderazgo_model->get_Lider_Editar($this->session->userdata('id_usuario'),$id);    
                
                /*Verifico que hay un cambio de lider en el mismo registro del nombramiento y si el objeto $dataLider no viene nulo*/
                if($dataLider->id_miembro != $lider && $dataLider->id_miembro != null){
                    $this->liderazgo_model->save_Lider(null,$lider,$cargo,$fechaNombramiento,$nombre_archivo,$this->session->userdata('id_usuario'),1,$biografia);
                    $this->liderazgo_model->save_Cambio_de_lider($dataLider->id_miembro, $descripcion_cambio_lider, $this->session->userdata('id_usuario'), $id, $lider,0);
                    $return = Array('ok' => 0, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
                }
                /*Si el objeto $dataLider es nulo es que es un nuevo cargo*/
                else{
                    /*Verifico que el cargo a insertar no este usado*/
                    if($existeCargo==0){
                        $this->liderazgo_model->save_Lider($id,$lider,$cargo,$fechaNombramiento,$nombre_archivo,$this->session->userdata('id_usuario'),1,$biografia);
                        $return = Array('ok' => 0, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
                    }else{
                        $return = Array('ok'=>0); 
                    }   
                }  
             
            }
         echo json_encode($return);  
           
    } 

    public function existeCargo($id_usuario,$cargo)
    {
        //$id_usuario = $_GET['usuario'];
        //$cargo = $_GET['cargo'];
        $existe = $this->liderazgo_model->get_Lider_Por_Cargo($this->session->userdata('id_usuario'),$cargo);
        //var_dump($existe);
        return $existe;
    }

    public function eliminarImagenNoticia($value='')
    {
        $return = Array('ok'=>TRUE);
        $upload_folder =$_SERVER['DOCUMENT_ROOT'].'IDPGlobal/IPDNacionalRD/LosFrailes1/application/noticias';
        $nombre_archivo = $_FILES['archivo']['name'];
        $tipo_archivo = $_FILES['archivo']['type'];
        $tamano_archivo = $_FILES['archivo']['size'];
        $tmp_archivo = $_FILES['archivo']['tmp_name'];

        $archivador = $upload_folder . '/' . $nombre_archivo;

        $id = $this->input->post('id');
        $fecha = $this->input->post('fecha_noticia');
        $descripcion = $this->input->post('descripcion_noticia');
        $titulo = $this->input->post('titulo');

        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            
            $this->noticias_model->eliminar_Imagen($id,$titulo,$descripcion,$fecha,$nombre_archivo,$this->session->userdata('id_usuario'));
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');

        }else{
           
            $this->noticias_model->eliminar_Imagen($id,$titulo,$descripcion,$fecha,$nombre_archivo,$this->session->userdata('id_usuario'));

            echo json_encode($return);  
        }
        
    }

    public function ver_Noticia($id_noticia)
    {
        $data['id_noticia'] = $id_noticia;
        $data['noticia'] = $this->noticias_model->get_Noticias($id_noticia,$this->session->userdata('id_usuario'));
        $this->load->view('sigle_noticia_view',$data);
    }
}