<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $GLOBALS['er'] = False;
        $GLOBALS['token'] = '';
		$this->load->model('login_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }
	
	public function index()
	{	
		if($this->session->userdata('is_logued_in') && $this->session->userdata('perfil') == 2){
			redirect(base_url().'index.php/bienvenido');
		} 
		
		switch ($this->session->userdata('perfil')) {
			case '':
				$data['token'] = $this->token();
				$this->load->view('login_view',$data);

				break;
			case 1:
				redirect(base_url().'index.php/homeBackend');
				break;
			case 2:
				redirect(base_url().'index.php/bienvenido');
				break;
			case 3:
				redirect(base_url().'index.php/home');
				break;
			default:		
				$data['titulo'] = 'Login con roles de usuario en codeigniter';
				//$this->load->view('templates/header',$data);
				//$this->load->view('login_view',$data);
				//$this->load->view('templates/footer',$data);
				break;		
		}
	}
 
	/*public function login_view(){
		$data['token'] = $this->token();
		//$this->load->view('templates/header',$data);
		$this->load->view('login_view',$data);
		//$this->load->view('templates/footer',$data);
		
	}*/

	public function token()
	{
		$token = md5(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		
		return $token;
	}


	public function new_user()
	{
		

		//if($this->input->post('token') == $this->session->userdata('token'))
		if(2 == 2)
		{
            //$this->form_validation->set_rules('username', 'nombre de usuario', 'required|trim|min_length[2]|max_length[150]|xss_clean');
            //$this->form_validation->set_rules('password', 'password', 'required|trim|min_length[5]|max_length[150]|xss_clean');

            $this->form_validation->set_rules('username', 'nombre de usuario', 'required|trim|min_length[2]|max_length[150]');
            $this->form_validation->set_rules('password', 'password', 'required|trim|min_length[5]|max_length[150]');

            //var_dump( $this->form_validation->run());
            //lanzamos mensajes de error si es que los hay
            
			if($this->form_validation->run() == FALSE)
			{
				//$this->index();
			}else{
				
				$username = $this->input->post('username');
				$password = sha1($this->input->post('password'));
				$check_user = $this->login_model->login_user($username,$password);
				//var_dump($check_user);

				if($check_user == TRUE)
				{
					$data = array(
	                'is_logued_in' 	=> 		TRUE,
	                'id_usuario' 	=> 		$check_user->id,
	                'perfil'		=>		$check_user->perfil,
	                'perfilNombre'	=>		$check_user->nombrePerfil,
	                'username' 		=> 		$check_user->username,
	                'nombre' 		=> 		$check_user->nombre,
	                'apellido' 		=> 		$check_user->apellido,
	                'iglesia'		=>		$check_user->iglesia
            		);		
					$this->session->set_userdata($data);
					var_dump($_SESSION);
					//$this->index();
				}
				else{
					echo 'else 4';
				}
			}
			//redirect(base_url().'index.php/homeBackend');
			
		}else{
			echo "else";
			//redirect(base_url().'index.php/login');
		}
			
	}

		
	public function logout_ci()
	{
		$this->session->sess_destroy();
		//var_dump($this->index());
		//header("Location: http://localhost/IDPGlobal/IPDNacionalRD/centralStoDgo/index.php/login");
		$this->index();
		Die();
	}
}

 ?>