<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Miembros extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('gcrecimiento_model');
        $this->load->model('estadistica_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        if (isset($_GET['id'])) {
            $data['miembro'] = $this->miembros_model->get_Miembro_Editar($this->session->userdata('id_usuario'),$_GET['id']);
        }

        if(isset($_GET['a'])==6 && isset($_GET['']))

    	$data['titulo'] = "Pagina de Inicio";
        $data['estados'] = $this->miembros_model->get_Estados();
        $data['carreras'] = $this->miembros_model->get_Carreras();
        $data['actividadesRegulares'] = $this->estadistica_model->get_ActividadesRegulares();;
        $data['celulas'] = $this->gcrecimiento_model->get_Celulas_Add_Miembro($this->session->userdata('id_usuario'));
        $data['responsable'] = $this->miembros_model->get_Responsables($this->session->userdata('id_usuario'));
        $data['miembros'] = $this->miembros_model->get_Miembros($this->session->userdata('id_usuario'),null);
        $data['actividades'] =  $this->miembros_model->get_Actividades($this->session->userdata('id_usuario'));
        $data['eventos'] = $this->miembros_model->get_Eventos($this->session->userdata('id_usuario'));
        $data['miembrosActivos'] = $this->miembros_model->get_MiembrosActivos($this->session->userdata('id_usuario'));
        $data['miembrosInactivos'] = $this->miembros_model->get_MiembrosInactivos($this->session->userdata('id_usuario'));
        $data['miembrosDescarriados'] = $this->miembros_model->get_MiembrosDescarriados($this->session->userdata('id_usuario'));
        $data['miembrosTrasladados'] = $this->miembros_model->get_MiembrosTrasladados($this->session->userdata('id_usuario'));
        $data['miembrosExterior'] = $this->miembros_model->get_MiembrosExterior($this->session->userdata('id_usuario'));
        $data['miembrosFallecidos'] = $this->miembros_model->get_MiembrosFallecidos($this->session->userdata('id_usuario'));
        $data['miembrosTotal'] = $this->miembros_model->get_MiembrosCount($this->session->userdata('id_usuario'));
        $data['miembrosTotalHombre'] = $this->miembros_model->get_MiembrosCountHombre($this->session->userdata('id_usuario'));
        $data['miembrosTotalMujer'] = $this->miembros_model->get_MiembrosCountMujer($this->session->userdata('id_usuario'));
    	$this->load->view('templatesBackend/header',$data);
    	$this->load->view('miembros_view',$data);
    	$this->load->view('templatesBackend/footer',$data);
       // var_dump($data['miembros']);
    }


    public function get_miembroEditar_Bienvenido($value='')
    {
        $id= $this->input->post('id');
        $data['miembro'] = $this->miembros_model->get_Miembro_Editar($this->session->userdata('id_usuario'),$id);

        echo json_encode($data);
    } 

    public function getEstatosMiembros($value='')
      {
          $data['estados'] = $this->miembros_model->get_Estados();  
          echo json_encode($data);
      }  

    //Me permite Crear un Token unico para el nombre de las fotos y los album 
    function generateGuid($include_braces = false) {
        if (function_exists('com_create_guid')) {
            if ($include_braces === true) {
                return com_create_guid();
            } else {
                return substr(com_create_guid(), 1, 36);
            }
        } else {
            mt_srand((double) microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));

            $guid = substr($charid,  0, 8) . '-' .
                    substr($charid,  8, 4) . '-' .
                    substr($charid, 12, 4) . '-' .
                    substr($charid, 16, 4) . '-' .
                    substr($charid, 20, 12);

            if ($include_braces) {
                $guid = '{' . $guid . '}';
            }

            return $guid;
        }
    } 

    public function agregarMiembro()
    {    
        $validado = false;

        if(isset($_FILES['archivo'])){
        
            $upload_folder =$_SERVER['DOCUMENT_ROOT'].'IDPGlobal/IPDNacionalRD/centralStoDgo/application/miembros';
            $nombre_archivo = $_FILES['archivo']['name'];
            $tipo_archivo = $_FILES['archivo']['type'];
            $tamano_archivo = $_FILES['archivo']['size'];
            $tmp_archivo = $_FILES['archivo']['tmp_name'];

            $nombre_archivo1 = explode('.',$nombre_archivo); 
            $nombreGUID = $this->generateGuid();
            $nombreGUID = $nombreGUID.'.'.$nombre_archivo1[1];

            $archivador = $upload_folder . '/' . $nombreGUID;
            
                    

            $id= $this->input->post('id');
            $nombre = $this->input->post('nombre');
            $apellido = $this->input->post('apellido');
            $email = $this->input->post('email');
            $telefono = $this->input->post('telefono');
            $calle = $this->input->post('calle');
            $casa = $this->input->post('casa');
            $urbanizacion = $this->input->post('urbanizacion');
            $Municipio = $this->input->post('Municipio');
            $codigoPostal = $this->input->post('codigoPostal');
            $estado = $this->input->post('estado');
            $ministerio = $this->input->post('ministerio');
            $estadoCivil = $this->input->post('estadoCivil');
            $estadoEducativo = $this->input->post('estadoEducativo');
            $nivelEducativo = $this->input->post('nivelEducativo');
            $estadoLaboral = $this->input->post('estadoLaboral');
            $sexo = $this->input->post('sexo');
            $celula = $this->input->post('celula');
            $fechaNacimiento = $this->input->post('fechaNacimiento');
            $fechaConversion = $this->input->post('fechaConversion');
            $carrera = $this->input->post('carrera');
            $id_usuario = $this->session->userdata('id_usuario');
            $descripcionCambioEstado = $this->input->post('descripcionCambioEstado');
            $antiguoEstado = $this->input->post('valueAntiguo');


           if($this->session->userdata('is_logued_in')){
                if (!move_uploaded_file($tmp_archivo, $archivador)) {   
                     $return = Array('ok' => FALSE, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
                     redirect(base_url().'index.php/bienvenido');
                }else{
                    if(
                        $nombre != null && 
                        $apellido != null && 
                        $estadoCivil!= null &&
                        $estadoEducativo  != null &&
                        $nivelEducativo != null &&
                        $estadoLaboral != null &&
                        $sexo != null &&
                        $fechaNacimiento  != null &&
                        $fechaConversion != null
                        )
                    {
                        if($id==null){
                            $this->miembros_model->save_Miembro(null,$nombre,$apellido,$email,$telefono,$nombreGUID,$calle,$casa,$urbanizacion,$Municipio,$codigoPostal,$estado,$ministerio, $estadoCivil,$estadoEducativo,$nivelEducativo,$estadoLaboral,$sexo,$fechaNacimiento,$fechaConversion,$carrera, $id_usuario,$celula);                    }

                        if($id>0){
                            $this->miembros_model->save_Miembro($id,$nombre,$apellido,$email,$telefono,$nombreGUID,$calle,$casa,$urbanizacion,$Municipio,$codigoPostal,$estado,$ministerio, $estadoCivil,$estadoEducativo,$nivelEducativo,$estadoLaboral,$sexo,$fechaNacimiento,$fechaConversion,$carrera, $id_usuario,$celula);
                            $this->miembros_model->save_Descripcion_Cambio($id,$descripcionCambioEstado,$id_usuario,$antiguoEstado,$estado);
                        }

                        $validado = 1;
                        $va = '{"validado": '.$validado.'}';
                        echo json_encode($va);

                    } 
                    else{
                        $validado = 2;
                        $va = '{"validado": '.$validado.'}';
                        echo json_encode($va);
                    }  
                }
            }else{
                redirect(base_url().'index.php/login');
            }
        }else{
            $id= $this->input->post('id');
            $nombre = $this->input->post('nombre');
            $apellido = $this->input->post('apellido');
            $email = $this->input->post('email');
            $telefono = $this->input->post('telefono');
            $calle = $this->input->post('calle');
            $casa = $this->input->post('casa');
            $urbanizacion = $this->input->post('urbanizacion');
            $Municipio = $this->input->post('Municipio');
            $codigoPostal = $this->input->post('codigoPostal');
            $estado = $this->input->post('estado');
            $ministerio = $this->input->post('ministerio');
            $estadoCivil = $this->input->post('estadoCivil');
            $estadoEducativo = $this->input->post('estadoEducativo');
            $nivelEducativo = $this->input->post('nivelEducativo');
            $estadoLaboral = $this->input->post('estadoLaboral');
            $sexo = $this->input->post('sexo');
            $celula = $this->input->post('celula');
            $fechaNacimiento = $this->input->post('fechaNacimiento');
            $fechaConversion = $this->input->post('fechaConversion');
            $carrera = $this->input->post('carrera');
            $id_usuario = $this->session->userdata('id_usuario');
            $descripcionCambioEstado = $this->input->post('descripcionCambioEstado');
            $antiguoEstado = $this->input->post('valueAntiguo');


           if($this->session->userdata('is_logued_in')){
                    if(
                        $nombre != null && 
                        $apellido != null && 
                        $estadoCivil!= null &&
                        $estadoEducativo  != null &&
                        $nivelEducativo != null &&
                        $estadoLaboral != null &&
                        $sexo != null &&
                        $fechaNacimiento  != null &&
                        $fechaConversion != null
                        )
                    {
                        if($id==null){
                            $this->miembros_model->save_Miembro(null,$nombre,$apellido,$email,$telefono,'sin_imagen.gif',$calle,$casa,$urbanizacion,$Municipio,$codigoPostal,$estado,$ministerio, $estadoCivil,$estadoEducativo,$nivelEducativo,$estadoLaboral,$sexo,$fechaNacimiento,$fechaConversion,$carrera, $id_usuario,$celula);                    }

                        if($id>0){
                            $imagenContenida = $this->miembros_model->get_nombreImagenContenida($id);
                            //var_dump($imagenContenida->imagen);
                            if($imagenContenida != null){
                                $this->miembros_model->save_Miembro($id,$nombre,$apellido,$email,$telefono,$imagenContenida->imagen,$calle,$casa,$urbanizacion,$Municipio,$codigoPostal,$estado,$ministerio, $estadoCivil,$estadoEducativo,$nivelEducativo,$estadoLaboral,$sexo,$fechaNacimiento,$fechaConversion,$carrera, $id_usuario,$celula);
                            }else{
                              $this->miembros_model->save_Miembro($id,$nombre,$apellido,$email,$telefono,'sin_imagen.gif',$calle,$casa,$urbanizacion,$Municipio,$codigoPostal,$estado,$ministerio, $estadoCivil,$estadoEducativo,$nivelEducativo,$estadoLaboral,$sexo,$fechaNacimiento,$fechaConversion,$carrera, $id_usuario,$celula);
                            }
                            $this->miembros_model->save_Descripcion_Cambio($id,$descripcionCambioEstado,$id_usuario,$antiguoEstado,$estado);
                        }

                        $validado = 1;
                        $va = '{"validado": '.$validado.'}';
                        echo json_encode($va);

                    } 
                    else{
                        $validado = 2;
                        $va = '{"validado": '.$validado.'}';
                        echo json_encode($va);
                    }  
                
            }else{
                redirect(base_url().'index.php/login');
            }
        }
    }

    public function eliminarImagenMiembro()
    {
        $return = Array('ok'=>TRUE);
        $id = $this->input->post('id');
        $nombreimagen = $this->input->post('imagen');

        $path = $_SERVER['DOCUMENT_ROOT'].'IDPGlobal/IPDNacionalRD/centralStoDgo/application/miembros/'.$nombreimagen;
        unlink($path);
        
        $this->miembros_model->eliminar_Imagen($id,'sin_imagen.gif');
     
        echo json_encode($return);          
        
    }

    public function agregarAsistencia($value='')
    {
        $validado = false;
        $idMiembro= $this->input->post('id');
        $nombre = $this->input->post('nombre');
        $valor = $this->input->post('valor');
        $fecha = $this->input->post('fecha');

        //var_dump($id);

        if($this->session->userdata('is_logued_in'))
            {
                 if(
                    $nombre != null && 
                    $fecha != null  ){
                    $arrayId=[];
                    $arrayValor=[];
                    $contador = 0;

                    foreach ($idMiembro as $key => $value) {
                       $arrayId[$key] = $value;
                       $contador++;
                    }

                    foreach ($valor as $key => $value) {
                       $arrayValor[$key] = $value;
                    }

                    $idEvento = $this->miembros_model->save_Actividad(null,$this->session->userdata('id_usuario'),$nombre,$fecha);


                    for($i=0;$i<$contador;$i++){
                        $this->miembros_model->save_Asistencia(null,$arrayId[$i],$this->session->userdata('id_usuario'),$arrayValor[$i],$idEvento,$fecha,$nombre,null);
                    }

                   // $this->miembros_model->save_Actividad(null,$nombre,$fecha);



                    $validado = 1;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);
                 }
                 else{
                    $validado = 2;
                    $va = '{"validado": '.$validado.'}';
                    echo json_encode($va);
                }  
            }
            else{
                redirect(base_url().'index.php/login');
            }
    }

   
    public function ver_Miembro($id)
    {
        $data['miembro'] = $this->miembros_model->get_Miembro($this->session->userdata('id_usuario'),$id);

        $existeAsistencia = $this->miembros_model->existeAsistencia($id);
        if($existeAsistencia > 0){
             $data['porcentajeAsistencia'] = $this->miembros_model->get_PorcentajeAsistencia( $this->session->userdata('id_usuario'),$id);
        }else{
             $data['porcentajeAsistencia'] = 0;
        }
       
        $data['lider'] = $this->gcrecimiento_model->get_Lider($this->session->userdata('id_usuario'),$data['miembro']->celula);
        $this->load->view('sigle_miembro_view',$data);
        //var_dump($data['miembro']->celula);
    }

    public function ver_MiembroBienbenido()
    {
        $id = $this->input->post('id');
        $data['miembro'] = $this->miembros_model->get_Miembro($this->session->userdata('id_usuario'),$id);

        $existeAsistencia = $this->miembros_model->existeAsistencia($id);
        if($existeAsistencia > 0){
             $data['porcentajeAsistencia'] = $this->miembros_model->get_PorcentajeAsistencia($this->session->userdata('id_usuario'),$id);
        }else{
             $data['porcentajeAsistencia'] = 0;
        }
       
        $data['lider'] = $this->gcrecimiento_model->get_Lider($this->session->userdata('id_usuario'),$data['miembro']->celula);
        //$this->load->view('sigle_miembro_view',$data);
        echo json_encode($data);
        //var_dump($data['miembro']->celula);
    }

    public function getMiembroPorNombre(){
        if($_GET['nombre'] != null){
            $nombre = $_GET['nombre'];
            $data['miembrosN'] = $this->miembros_model->get_Miembro_Por_Nombre($this->session->userdata('id_usuario'),$nombre);        
        }else{
            $data['miembrosN'] =  $this->miembros_model->get_Miembros($this->session->userdata('id_usuario'));
        }
        echo json_encode($data);
    }

    public function porcentajeIndividualPorMiembrosColor($id)
    {
    	$existeAsistencia = $this->miembros_model->existeAsistencia($id);
        if($existeAsistencia > 0){
            $data['porcentajeAsistencia'] = $this->miembros_model->get_PorcentajeAsistencia($this->session->userdata('id_usuario'),$id);
            return $data['porcentajeAsistencia'];  
        }else{
            return $existeAsistencia;
        }       
            
    }

    public function ver_Actividad($id)
    {
        $data['actividad'] = $this->miembros_model->get_Actividad( $this->session->userdata('id_usuario'),$id);
        $this->load->view('sigle_evento_view',$data);
        //var_dump($data['miembro']);
    }


    public function ver_Estadistica($id)
    {
        $data['cantidadAsistenciaPresente'] = $this->miembros_model->get_Estadistica_Evento_Presentes( $this->session->userdata('id_usuario'),$id);
        $data['cantidadAsistencia'] = $this->miembros_model->get_Estadistica_Evento( $this->session->userdata('id_usuario'),$id);
        $data['estadisticaAsistecia'] = ($data['cantidadAsistenciaPresente']->cantidadTotal/$data['cantidadAsistencia']->cantidadTotal)*100;
        $data['miembrosTotal'] = $this->miembros_model->get_miembros_Por_Evento($this->session->userdata('id_usuario'),$id);
        $this->load->view('sigle_estadistica_evento_view',$data);
        //var_dump($data['miembro']);
    }



    public function actualizarCelulaMiembro($id)
    {
        //$celula = $this->input->post('celula');

        if(isset($id)){
                    
            $this->miembros_model->update_Miembro_Celula($this->session->userdata('id_usuario'),$id);
            $validado = 1;
            $va = '{"validado": '.$validado.'}';
            echo json_encode($va);
         }
         else{
            $validado = 2;
            $va = '{"validado": '.$validado.'}';
            echo json_encode($va);
            //$data['miembrosIntegrantes'] = $this->gcrecimiento_model->get_Miembros_Por_Celula_model($this->session->userdata('id_usuario'),$celula);
            //var_dump($data['miembrosIntegrantes']);
            //echo json_encode($data['miembrosIntegrantes']) ; 
            //var_dump($id);
        }
    }
}