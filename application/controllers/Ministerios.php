<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Ministerios extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('gcrecimiento_model');
        $this->load->model('ministerios_model');
        $this->load->model('evento_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        $data['reponsables'] = $this->miembros_model->get_Responsables($this->session->userdata('id_usuario'));
        $data['miembrosVarones'] = $this->miembros_model->get_Miembros($this->session->userdata('id_usuario'),'M');
        $data['miembrosMujeres'] = $this->miembros_model->get_Miembros($this->session->userdata('id_usuario'),'F');
        $miembros = $this->miembros_model->get_Miembros($this->session->userdata('id_usuario'),null);
        $data['miembrosTotal'] = $this->miembros_model->get_MiembrosCount($this->session->userdata('id_usuario'));
        $data['miembrosTotalHombre'] = $this->miembros_model->get_MiembrosCountHombre($this->session->userdata('id_usuario'));
        $data['ministerios'] = $this->ministerios_model->get_Ministerios($this->session->userdata('id_usuario'));
        $data['eventosVarones'] = $this->evento_model->get_Eventos($this->session->userdata('id_usuario'),5);
        $data['eventosMujeres'] = $this->evento_model->get_Eventos($this->session->userdata('id_usuario'),6);


        $data['jovenes']  =  $miembros;








        $this->load->view('templatesBackend/header',$data);
        $this->load->view('ministerios_view',$data);
        $this->load->view('templatesBackend/footer',$data);
    }

    public function agregarMinisterio($value='')
    {
        $return = Array('ok'=>TRUE);
        $upload_folder =$_SERVER['DOCUMENT_ROOT'].'IDPGlobal/IPDNacionalRD/LosFrailes1/application/imagesMinisterio';
        $nombre_archivo = $_FILES['archivo']['name'];
        $tipo_archivo = $_FILES['archivo']['type'];
        $tamano_archivo = $_FILES['archivo']['size'];
        $tmp_archivo = $_FILES['archivo']['tmp_name'];

        $archivador = $upload_folder . '/' . $nombre_archivo;

        $id = $this->input->post('id');
        $nombre = $this->input->post('nombreMinisterio');
        $lider = $this->input->post('lider');

        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{  
            if($id != null){
                $this->ministerios_model->save_Ministerio($id,$this->session->userdata('id_usuario'),$nombre,$lider,$nombre_archivo);

            }else{
                $this->ministerios_model->save_Ministerio(null,$this->session->userdata('id_usuario'),$nombre,$lider,$nombre_archivo);
            }             
         
        } 
        
        echo json_encode($return);      
       
    }

    public function porcentajeIndividualPorMiembrosColor($id)
    {
        $existeAsistencia = $this->miembros_model->existeAsistencia($id);
        if($existeAsistencia > 0){
            $data['porcentajeAsistencia'] = $this->miembros_model->get_PorcentajeAsistencia($this->session->userdata('id_usuario'),$id);
            return $data['porcentajeAsistencia'];  
        }else{
            return $existeAsistencia;
        }       
            
    }

    public function ver_estadisticas_asistencia_caballeros($id='')
    {
        $id=$_GET['id'];
        $data['actividades'] = $this->ministerios_model->get_Actividades_asistencia($this->session->userdata('id_usuario'),$id);
        $this->load->view('sigle_actividades_asistencia_view',$data);
    }
}