<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Multimedia extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('multimedia_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
    	if(isset($_GET['s'])){
    		$data['seccion'] = $_GET['s'];
    	}

        $data['audios'] = $this->multimedia_model->get_Audios(2,null,null);
        $data['gruposAudio'] = $this->multimedia_model->get_Grupos(2);
        $data['mensajesDominicales'] = $this->multimedia_model->get_Audios_por_fecha(2);
        $data['albumsFotos'] = $this->multimedia_model->get_AlbumsFotos(2);

        $data['active'] = 'multimediaActive';

    	$this->load->view('templates/headerSite',$data);
    	$this->load->view('multimedia_view_site',$data);
    	$this->load->view('templates/footerSite_m');
    }

    public function ver_grupo_single($idCarpeta)
    {
    	$data['active'] = 'multimediaActive';

        $data['audios'] = $this->multimedia_model->get_Audios(2,$idCarpeta,null);
        $this->load->view('templates/headerSite',$data);
        $this->load->view('single_grupos_view_site',$data);
        $this->load->view('templates/footerSite_m');
    }

    public function ver_album_single($idCarpeta)
    {
    	$data['active'] = 'multimediaActive';
        $data['fotos'] = $this->multimedia_model->get_Fotos(2,$idCarpeta,null);
        $this->load->view('templates/headerSite',$data);
        $this->load->view('single_albums_view_fotos_site',$data);
        $this->load->view('templates/footerSite_m');
    }

    public function ver_audio_single($id)
    {
    	$data['active'] = 'multimediaActive';
        $data['audio'] = $this->multimedia_model->get_Audios(2,null,$id);
        $this->load->view('templates/headerSite',$data);
        $this->load->view('single_audio_view',$data);
        $this->load->view('templates/footerSite_m');
    }
    
    public function getSerie($value='')
    {
    	$_POST = json_decode(file_get_contents('php://input'), true);
    	
    	$id = $this->input->post('id');
    	
    	
    	$data['gruposAudio'] = $this->multimedia_model->get_Grupos(2,$id);
    	echo json_encode($data);
    }

    public function getAudiosSerie()
    {
    	$_POST = json_decode(file_get_contents('php://input'), true);
    	//var_dump($this->input->post(null,true));
        $return = Array('ok'=>1);
        $id = $this->input->post('id');
        
        $data['data'] = $this->multimedia_model->get_Audios(2, $id, null);
        
        
        echo json_encode($data);   
       // echo json_encode(file_get_contents("php://input"));
        //echo json_encode(phpinfo());  
        //echo json_encode($_POST);     
        //echo json_encode($_REQUEST);  
        //var_dump($_POST);
    }
}