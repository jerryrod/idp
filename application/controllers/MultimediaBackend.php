<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class MultimediaBackend extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('liderazgo_model');
        $this->load->model('multimedia_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {

        if (isset($_GET['id'])) {
            $data['audio'] = $this->multimedia_model->get_Audio_Editar($this->session->userdata('id_usuario'),$_GET['id']);
            if(isset($_GET['m']) && $_GET['m'] ==5){
               $data['fotosBD'] = $this->multimedia_model->get_Fotos(2,$_GET['id'],null);
               var_dump($data['fotosBD']);
            }
        }

        $data['albunFotos'] = $this->multimedia_model->get_Album_Fotos($this->session->userdata('id_usuario'));
        $data['lideres']=  $this->liderazgo_model->get_Lideres(null,$this->session->userdata('id_usuario'));
        $data['audios'] = $this->multimedia_model->get_Audios($this->session->userdata('id_usuario'));
        $data['responsable'] = $this->miembros_model->get_Responsables($this->session->userdata('id_usuario'));
        $data['cargo'] = $this->liderazgo_model->get_Cargos();
        $data['carpetaAudio'] = $this->multimedia_model->get_Carpetas_Audio();
        $data['carpetaFoto'] = $this->multimedia_model->get_Carpetas_Foto();
    	$this->load->view('templatesBackend/header');
    	$this->load->view('multimediaBackend_view',$data);
    	$this->load->view('templatesBackend/footer');
    }

    public function agregarCarpetaAudio($value='')
    {
    	$_POST = json_decode(file_get_contents('php://input'), true);
    	//Return 1 es bueno, return 0 es malo.
        $result = array('ok' => 1);        
        $nombre = $this->input->post('nombreCarpeta');
        $idSerie = $this->input->post('id');
        
        //var_dump( $nombre );
        
	if($idSerie > 0){
	        if($nombre != null){
	            $this->multimedia_model->add_Carpeta_Audio($idSerie,$this->session->userdata('id_usuario'),$nombre,null);
	        }else{
	            $result = array('ok' => 0);
	        }
	 }else{
	 	if($nombre != null){
	            $this->multimedia_model->add_Carpeta_Audio(null,$this->session->userdata('id_usuario'),$nombre,null);
	        }else{
	            $result = array('ok' => 0);
	        }
	 }       
        return $result;

    }

     public function agregarCarpetaFoto($value='')
    {
        $result = array('ok' => 1);        
        $nombre = $this->input->post('nombreCarpetaFoto');
        $tipoActividad = $this->input->post('tipoActividad');
        $diaServicio = $this->input->post('diaServicio');

        $nombreCarpetaGenerado = $this->generateGuid();

        mkdir($_SERVER['DOCUMENT_ROOT'].'IDPGlobal/IPDNacionalRD/centralStoDgo/application/imagenesAlbum/'.$nombreCarpetaGenerado, 0700);

        if( $nombre != null){
             $this->multimedia_model->add_Carpeta_Foto($nombre,$tipoActividad,$diaServicio,$this->session->userdata('id_usuario'),$nombreCarpetaGenerado);
        }else{
            $result = array('ok' => 0);
        }
        return $result;

    }
    

    public function agregarAudio($value='')
    {
    	$_POST = json_decode(file_get_contents('php://input'), true);
        $return = Array('ok'=>1);
        $return2 = Array('ok'=>1,'id'=> $_POST);
        
        $upload_folder = $_SERVER['DOCUMENT_ROOT'].'application/audio';
        
        /*$nombre_archivo = $_FILES['archivo']['name'];
        $tipo_archivo = $_FILES['archivo']['type'];
        $tamano_archivo = $_FILES['archivo']['size'];
        $tmp_archivo = $_FILES['archivo']['tmp_name'];*/

        /*$nombre_archivo1 = explode('.',$nombre_archivo);
        $existe = $this->existeNombreAudio($nombre_archivo);
        $contador =1;
        if($existe != 0){
            while($existe != 0){
                $nombre_archivo = $nombre_archivo1[0].$contador.'.'.$nombre_archivo1[1];
                $existe = $this->existeNombreAudio($nombre_archivo);
                $contador++;
            }
        }
        $archivador = $upload_folder . '/' . $nombre_archivo1vo;

        $id = $this->input->post('id');
        $nombreAudio = $this->input->post('nombreAudio');
        $carpetaAudio = $this->input->post('carpetaAudio');
        $fechaAudio = $this->input->post('fechaAudio');
        
        $return2 = Array('ok'=>1,'id'=>$id);
        
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => 0, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $this->multimedia_model->save_Audio($id,$nombreAudio,$carpetaAudio,$fechaAudio,$this->session->userdata('id_usuario'),$nombre_archivo);                       
        }*/
        
        echo json_encode($return2); 
           
    } 

    public function existeNombreAudio($nombre_audio)
    {
        //$id_usuario = $_GET['usuario'];
        //$cargo = $_GET['cargo'];
        $existe = $this->multimedia_model->get_audio_por_nombre($this->session->userdata('id_usuario'),$nombre_audio);
        //var_dump($existe);
        return $existe;
    }


     public function agregarAudio3($value='')
    {
        $return = Array('ok'=>1);
        $upload_folder =$_SERVER['DOCUMENT_ROOT'].'application/audio';
        $nombre_archivo = $_FILES['archivo']['name'];
        $tipo_archivo = $_FILES['archivo']['type'];
        $tamano_archivo = $_FILES['archivo']['size'];
        $tmp_archivo = $_FILES['archivo']['tmp_name'];
        $nombreArchivoConcatenado = '';

        $nombre_archivo1 = explode('.',$nombre_archivo);
        

        $separarNombreDeArchivoPorEspacios = preg_split("/[\s,-?¿]+/", $nombre_archivo1[0]);
        for ($i=0; $i < count($separarNombreDeArchivoPorEspacios) ; $i++) { 
            $nombreArchivoConcatenado = $nombreArchivoConcatenado.$separarNombreDeArchivoPorEspacios[$i];
        }

        $existe = $this->existeNombreAudio($nombreArchivoConcatenado.$nombre_archivo1[1]);
        $contador =1;

        if($existe != 0){
            while($existe != 0){
                $nombre_archivo = $nombreArchivoConcatenado.$contador.'.'.$nombre_archivo1[1];
                $existe = $this->existeNombreAudio($nombre_archivo);
                $contador++;
            }
        }
        $nombre_archivo = $nombreArchivoConcatenado.'.'.$nombre_archivo1[1];
        $archivador = $upload_folder . '/' . $nombre_archivo;

        $id = $this->input->post('id');
        $nombreAudio = $this->input->post('nombreAudio');
        $carpetaAudio = $this->input->post('carpetaAudio');
        $fechaAudio = $this->input->post('fechaAudio');
        
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => 0, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $this->multimedia_model->save_Audio($id,$nombreAudio,$carpetaAudio,$fechaAudio,$this->session->userdata('id_usuario'),$nombre_archivo);                       
        }
        
        echo json_encode($return); 

    }

    public function eliminarAudio($value='')
    {
        $return = Array('ok'=>1);
        $id = $this->input->post('id');
        if($id != null){             
             $this->multimedia_model->removeAudio($this->session->userdata('id_usuario'),$id);
        }else{
            $return = Array('ok'=>2);
        }


        echo json_encode($return); 

    }

    public function agregarFotos($value='')
    {
        $id = $this->input->post('id');
        $carpetaFoto = $this->input->post('carpetaFoto');

        $nombreFolderGuardarFotos = $this->multimedia_model->get_nombre_folder_carpeta_foto($this->session->userdata('id_usuario'),$carpetaFoto);

        $max = sizeof($_FILES['archivo']['name']);
        $return = Array('ok'=> 1,'msg' => 'Exito desde JSON','nombre' => $_FILES['archivo']['name'][0],'nombreCarpeta' => $nombreFolderGuardarFotos);
       $upload_folder = $_SERVER['DOCUMENT_ROOT'].'IDPGlobal/IPDNacionalRD/centralStoDgo/application/imagenesAlbum/'.$nombreFolderGuardarFotos;

      
       for($i=0; $i < $max; $i++) {
            $nombre_archivo = $_FILES['archivo']['name'][$i];
            $tipo_archivo = $_FILES['archivo']['type'][$i];
            $tamano_archivo = $_FILES['archivo']['size'][$i];
            $tmp_archivo = $_FILES['archivo']['tmp_name'][$i];
            $nombreArchivoConcatenado = '';

            $nombre_archivo1 = explode('.',$nombre_archivo);        

            /* $separarNombreDeArchivoPorEspacios = preg_split("/[\s,-?¿]+/", $nombre_archivo1[0]);
            for ($r=0; $r < count($separarNombreDeArchivoPorEspacios) ; $r++) { 
                $nombreArchivoConcatenado = $nombreArchivoConcatenado.$separarNombreDeArchivoPorEspacios[$r];
            }*/

            $nombreGUID = $this->generateGuid();

            //$nombre_archivo = $nombreArchivoConcatenado.'.'.$nombre_archivo1[1];
            $nombreGUID = $nombreGUID.'.'.$nombre_archivo1[1];

            $archivador = $upload_folder . '/' . $nombreGUID;

            if (!move_uploaded_file($tmp_archivo, $archivador)) {
                $return = Array('ok' => 0, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
            }else{
                $this->multimedia_model->save_Foto($id,$carpetaFoto,$this->session->userdata('id_usuario'),$nombreGUID);                       
            }
        }  
               
        echo json_encode($return); 

    }



    public function existeNombreFoto($nombre_foto)
    {        
        $existe = $this->multimedia_model->get_foto_por_nombre($this->session->userdata('id_usuario'),$nombre_foto);
        return $existe;
    }

    public function eliminarAlbumFoto($value='')
    {
        $return = Array('ok'=>1);
        $ruta = $this->delTree($_SERVER['DOCUMENT_ROOT'].'IDPGlobal/IPDNacionalRD/centralStoDgo/application/imagenesAlbum/');
        $nombreCarpeta = '';
        $id = $this->input->post('id');
        if($id != null){      
            //Removemos el album de la bd. 
            $nombreCarpeta = $this->multimedia_model->get_nombre_folder_carpeta_foto($this->session->userdata('id_usuario'),$id);
            $this->multimedia_model->removeAlbumFoto($this->session->userdata('id_usuario'),$id);
            $this->multimedia_model->removeFotosDelAlbum($this->session->userdata('id_usuario'),$id);
        }else{
            $return = Array('ok'=>2);
        }
        //Removemos el album de nuestro servidor local.
        $ruta = $this->delTree($_SERVER['DOCUMENT_ROOT'].'IDPGlobal/IPDNacionalRD/centralStoDgo/application/imagenesAlbum/'.$nombreCarpeta);

        echo json_encode($return); 
    }

    public function eliminarFoto($value='')
    {
         $id = $this->input->post('id');
    }

    /*public function delTree($dir) {
        $files =scandir($dir);
        foreach ($files as $file) { 
           unlink("$dir/$file"); 
        } 
        return rmdir($dir); 
    }*/

     public function delTree($dir) {  
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') continue;
            unlink($dir.DIRECTORY_SEPARATOR.$item);
        }
        rmdir($dir);
    }



    //Me permite Crear un Token unico para el nombre de las fotos y los album 
    function generateGuid($include_braces = false) {
        if (function_exists('com_create_guid')) {
            if ($include_braces === true) {
                return com_create_guid();
            } else {
                return substr(com_create_guid(), 1, 36);
            }
        } else {
            mt_srand((double) microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));

            $guid = substr($charid,  0, 8) . '-' .
                    substr($charid,  8, 4) . '-' .
                    substr($charid, 12, 4) . '-' .
                    substr($charid, 16, 4) . '-' .
                    substr($charid, 20, 12);

            if ($include_braces) {
                $guid = '{' . $guid . '}';
            }

            return $guid;
        }
    }

}