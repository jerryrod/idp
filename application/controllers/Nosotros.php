<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Nosotros extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        $data['active'] = 'nosotrosActive';
    	$this->load->view('templates/headerSite',$data);
    	$this->load->view('nosotros_view_site');
    	$this->load->view('templates/footerSite');
    }
}