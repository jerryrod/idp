<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Noticias extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('noticias_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        $id_usuario = 2;
        $data['active'] = 'noticiaActive';
        $data['noticias']=  $this->noticias_model->get_Noticias(null,$id_usuario);
        $this->load->view('templates/headerSite',$data);
        $this->load->view('noticias_view',$data);
        $this->load->view('templates/footerSite_m');
    }

    public function ver_noticia($id_noticia)
    {
        $data['active'] = 'noticiaActive';
        $data['noticia']=  $this->noticias_model->get_Noticias($id_noticia,2);
        $this->load->view('templates/headerSite',$data);
        $this->load->view('ver_contenido_noticia.php',$data);
        $this->load->view('templates/footerSite_m');
    }
}