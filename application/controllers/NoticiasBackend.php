<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class NoticiasBackend extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('noticias_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        if (isset($_GET['id'])) {
            $data['noticia'] = $this->noticias_model->get_Noticias($_GET['id'],$this->session->userdata('id_usuario'));
        }

        $data['columnas'] = array('1'=>1,'2'=>2, '3'=>3);
        $data['noticias']=  $this->noticias_model->get_Noticias(null,$this->session->userdata('id_usuario'));
        $this->load->view('templatesBackend/header');
        $this->load->view('noticias_backend_view',$data);
        $this->load->view('templatesBackend/footer');
    }

    public function agregarNoticia($value='')
    {
        $return = Array('ok'=>TRUE);
        $upload_folder =$_SERVER['DOCUMENT_ROOT'].'IDPGlobal/IPDNacionalRD/LosFrailes1/application/noticias';
        $nombre_archivo = $_FILES['archivo']['name'];
        $tipo_archivo = $_FILES['archivo']['type'];
        $tamano_archivo = $_FILES['archivo']['size'];
        $tmp_archivo = $_FILES['archivo']['tmp_name'];

        $archivador = $upload_folder . '/' . $nombre_archivo;

        $id = $this->input->post('id');
        $fecha = $this->input->post('fecha_noticia');
        $descripcion = $this->input->post('descripcion_noticia');
        $titulo = $this->input->post('titulo');
        $columna = $this->input->post('columna');

        if (!move_uploaded_file($tmp_archivo, $archivador)) {            
            $this->noticias_model->save_Noticia($id,$titulo,$descripcion,$fecha,$nombre_archivo,$this->session->userdata('id_usuario'),$columna );
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{           
            $this->noticias_model->save_Noticia($id,$titulo,$descripcion,$fecha,$nombre_archivo,$this->session->userdata('id_usuario'),$columna );
            echo json_encode($return);  
        }       
           
    } 

    public function eliminarImagenNoticia($value='')
    {
        $return = Array('ok'=>TRUE);
        $upload_folder =$_SERVER['DOCUMENT_ROOT'].'IDPGlobal/IPDNacionalRD/LosFrailes1/application/noticias';
        $nombre_archivo = $_FILES['archivo']['name'];
        $tipo_archivo = $_FILES['archivo']['type'];
        $tamano_archivo = $_FILES['archivo']['size'];
        $tmp_archivo = $_FILES['archivo']['tmp_name'];

        $archivador = $upload_folder . '/' . $nombre_archivo;

        $id = $this->input->post('id');
        $fecha = $this->input->post('fecha_noticia');
        $descripcion = $this->input->post('descripcion_noticia');
        $titulo = $this->input->post('titulo');

        if (!move_uploaded_file($tmp_archivo, $archivador)) {            
            $this->noticias_model->eliminar_Imagen($id,$titulo,$descripcion,$fecha,$nombre_archivo,$this->session->userdata('id_usuario'));
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrio un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{           
            $this->noticias_model->eliminar_Imagen($id,$titulo,$descripcion,$fecha,$nombre_archivo,$this->session->userdata('id_usuario'));

            echo json_encode($return);  
        }
        
    }

    public function ver_Noticia($id_noticia='')
    {
        $data['id_noticia'] = $id_noticia;
        $data['noticia'] = $this->noticias_model->get_Noticias($id_noticia,$this->session->userdata('id_usuario'));
        $this->load->view('sigle_noticia_view',$data);
    }

    public function ver_NoticiaSingle()
    {
        $id = $this->input->post('id');
        $data['noticia'] = $this->noticias_model->get_Noticias($id,$this->session->userdata('id_usuario'));
        echo json_encode($data);
    }

    public function get_noticiaEditar_Bienvenido($value='')
    {
        $id= $this->input->post('id');
        $data['noticia'] = $this->noticias_model->get_Noticia_Editar($this->session->userdata('id_usuario'),$id);

        echo json_encode($data);
    }   

    //Me permite Crear un Token unico para el nombre de las fotos y los album 
    function generateGuid($include_braces = false) {
        if (function_exists('com_create_guid')) {
            if ($include_braces === true) {
                return com_create_guid();
            } else {
                return substr(com_create_guid(), 1, 36);
            }
        } else {
            mt_srand((double) microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));

            $guid = substr($charid,  0, 8) . '-' .
                    substr($charid,  8, 4) . '-' .
                    substr($charid, 12, 4) . '-' .
                    substr($charid, 16, 4) . '-' .
                    substr($charid, 20, 12);

            if ($include_braces) {
                $guid = '{' . $guid . '}';
            }

            return $guid;
        }
    } 
}