<?php

class Upload extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

	function index()
	{
		$this->load->view('upload_form', array('error' => ' ' ));
	}

	 public function file_upload_demo()
    {
        try
        {
            if($this->input->post("submit")){        
                $this->load->library("uploader");
                $this->uploader->do_upload();
            }
            return $this->view();
        }
        catch(Exception $err)
        {
            log_message("error",$err->getMessage());
            return show_error($err->getMessage());
        }
    }
}
?>