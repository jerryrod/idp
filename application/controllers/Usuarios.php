<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**
 * 
 */
class Usuarios extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('usuarios_model');
		$this->load->model('iglesia_model');
	}
	
	public function index()
	{
		if($this->session->userdata('perfil') == FALSE)
		{
			redirect(base_url().'login');
		}
		if(isset($_GET['u']) && isset($_GET['id'])){
			$data['usuario'] = $this->usuarios_model->get_Usuario($_GET['id']);
		}
		$data['titulo'] = 'Bienvenido a la web '.$this->session->userdata('perfil');
		$data['usuarios'] = $this->usuarios_model->get_usuarios();
		$data['rol'] = $this->usuarios_model->get_roles();
		$this->load->view('templatesBackend/header',$data);
		$this->load->view('usuarios_view',$data);
		$this->load->view('templatesBackend/footer',$data);
	}

	public function formularioUsuarios($value='')
	{

		$data['provincias'] = $this->usuarios_model->get_provincias();
		$data['iglesias'] = $this->usuarios_model->get_Iglesias();
		$data['pais'] = $this->usuarios_model->get_paises();
		$data['rol'] = $this->usuarios_model->get_roles();
		//$this->load->view('templatesBackend/headerFancy');
        //$this->load->view('usuariosForm_view',$data);
	}

	public function guardarUsuario($value='')
	{
		/*
			Leyenda Validado

			0 = No esta logueado.
			--1 = Exito.
			2 = Se guardo el registro por primera vez
			3 = se actualizo el registro en la base de datos.


		*/
		$validado = 1;
		$opcionIglesia =  $this->input->post('opcionIglesia');

		$id = $this->input->post('id');
		$nombre = $this->input->post('nombre');
		$apellido = $this->input->post('apellido');
		$email = $this->input->post('email');
		$usuario = $this->input->post('usuario');
		$pass = $this->input->post('pass');
		$rol = $this->input->post('rol');

		if($opcionIglesia == 1) {
			$nombreIglesia = $this->input->post('nombreIglesia');
			$sectorIglesia = $this->input->post('sectorIglesia');
			$calleIglesia = $this->input->post('calleIglesia');
			$numeroLocalIglesia = $this->input->post('numeroLocalIglesia');
			$paisIglesia = $this->input->post('paisIglesia');
			$provincia = $this->input->post('provincia');

			if($this->session->userdata('is_logued_in'))
		    {
		       if(
		                $nombre != null && 
		                $apellido != null && 
		                $email!= null &&
		                $usuario  != null &&
		                $rol != null && 
		                $nombreIglesia != null && 
		                $sectorIglesia != null && 
		                $calleIglesia != null && 
		                $numeroLocalIglesia != null && 
		                $paisIglesia != null && 
		                $provincia != null  	
		                )
		            {
		                if($id==null){
		                	$id_Iglesia = $this->iglesia_model->add_church(null,$nombreIglesia,$calleIglesia,$numeroLocalIglesia,$paisIglesia,$provincia,$sectorIglesia);
		                    $this->usuarios_model->add_user(null,$nombre,$apellido,$email,$usuario,$pass,$rol,$this->session->userdata('perfil'),$id_Iglesia);
		                    $validado = 2;
		                }

		                if($id>0){
		                    $this->usuarios_model->add_user($id,$nombre,$apellido,$email,$usuario,$pass,$rol,$this->session->userdata('perfil'));
		                    $validado = 3;
		                }
		            }
		     }else{
		     	$validado = 0;
		     }

		}

		if($opcionIglesia == 2){
			$iglesiaAgregada = $this->input->post('iglesiaAgregada');

			if($this->session->userdata('is_logued_in'))
		    {
		            if(
		                $nombre != null && 
		                $apellido != null && 
		                $email!= null &&
		                $usuario  != null &&
		                $rol != null &&
		                $iglesiaAgregada != null
		                )
		            {
		                if($id==null){
		                    $this->usuarios_model->add_user(null,$nombre,$apellido,$email,$usuario,$pass,$rol,$this->session->userdata('perfil'),$iglesiaAgregada);
		                    $validado = 2;
		                }

		                if($id>0){
		                    $this->usuarios_model->add_user($id,$nombre,$apellido,$email,$usuario,$pass,$rol,$this->session->userdata('perfil'),$iglesiaAgregada);
		                    $validado = 3;
		                }
		            }
		     }else{
		     	$validado = 0;
		     }
		}

		if($opcionIglesia == 3){
			if($this->session->userdata('is_logued_in'))
		    {
		            if(
		                $nombre != null && 
		                $apellido != null && 
		                $email!= null &&
		                $usuario  != null &&
		                $rol != null 
		                )
		            {
		                if($id==null){
		                    $this->usuarios_model->add_user(null,$nombre,$apellido,$email,$usuario,$pass,$rol,$this->session->userdata('perfil'),null);
		                    $validado = 2;
		                }

		                if($id>0){
		                    $this->usuarios_model->add_user($id,$nombre,$apellido,$email,$usuario,$pass,$rol,$this->session->userdata('perfil'),null);
		                    $validado = 3;
		                }
		            }
		     }else{
		     	$validado = 0;
		     }
		}
		
        $va = '{"validado": '.$validado.'}';
        echo json_encode($va);
	}

	public function verificarPassword()
	{
		$validado;
		$id = $this->input->post('id');
		$passAnterior = $this->input->post('passAnterior');

		$validado = $this->usuarios_model->verificarPasswordModel($id,$passAnterior);
				
		$va = '{"validado": '.$validado.'}';
        echo json_encode($va);

	}

	public function ver_Usuario($id)
	{
        $data['usuario'] = $this->usuarios_model->get_Usuario($id);
        $this->load->view('sigle_usuario_view',$data);
        //var_dump($data['miembro']->celula);
    }

    public function ver_UsuarioBienvenido()
	{
		$id = $this->input->post('id');
        $data['usuario'] = $this->usuarios_model->get_Usuario($id);

        echo json_encode($data);
    }

    public function eliminarUsuario($value='')
    {
    	$validado = 1;
    	$id = $this->input->post('id');

    	if($this->session->userdata('is_logued_in') && $this->session->userdata('perfil') == 1)
	    {
	    	$this->usuarios_model->delete_user($id);
	    	$validado = $id;
	    }else{
	    	$validado = 0;
	    }

	    $va = '{"validado": '.$validado.'}';
        echo json_encode($va);

    }   


}