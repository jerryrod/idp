<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Webservices extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
        $this->load->model('miembros_model');
        $this->load->model('gcrecimiento_model');
        $this->load->model('estadistica_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
    }

    public function index($value='')
    {
        $servicio="http://172.22.42.111/Fiserv/Contracts2.0/InterTrade/BackEndService.asmx?WSDL"; //url del servicio
        //$servicio="http://10.246.246.26:7810/aplicaciones/Contracts2.0/InterTrade/BackEndService.asmx?WSDL"; //url del servicio
        $parametros=array(); //parametros de la llamada
        //$parametros['idioma']="es";
        //$parametros['usuario']="manolo";
        //$parametros['clave']="tuclave";
        $client = new SoapClient($servicio, $parametros);
        //$result = $this->obj2array($client);
        //$result = $client->AplicaPagoPrestamo($parametros);//llamamos al métdo que nos interesa con los parámetros
        //$result = $client->AplicaPagoTarjeta($parametros);//llamamos al métdo que nos interesa con los parámetros
        //$result = $client->ConsultaGeneralProducto($parametros);//llamamos al métdo que nos interesa con los parámetros
        //$result = $client->CuentaRelacionados($parametros);//llamamos al métdo que nos interesa con los parámetros
        //$result = $client->CuentasCliente($parametros);//llamamos al métdo que nos interesa con los parámetros
        //$result = $client->EjecutarTransaccionInternacional($parametros);//llamamos al métdo que nos interesa con los parámetros
        //$result = $client->VerificarTransaccionInternacional($parametros);//llamamos al métdo que nos interesa con los parámetros
        //$result = $client->ConsultarDatosGeneralesCliente($parametros);//llamamos al métdo que nos interesa con los parámetros
        $result = $client->ClienteBanreservas($parametros);//llamamos al métdo que nos interesa con los parámetros
        $data['cliente'] = $result;

        $data['titulo'] = 'Hola mundo';
    	$this->load->view('templatesBackend/header',$data);
    	$this->load->view('webservice_view',$data);
    	$this->load->view('templatesBackend/footer',$data);
       // var_dump($data['miembros']);
    }

    public function obj2array($obj) {
          $out = array();
          foreach ($obj as $key => $val) {
            switch(true) {
                case is_object($val):
                 $out[$key] = obj2array($val);
                 break;
              case is_array($val):
                 $out[$key] = obj2array($val);
                 break;
              default:
                $out[$key] = $val;
            }
          }
          return $out;
    }

}