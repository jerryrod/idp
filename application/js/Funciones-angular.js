var AlbergueApp = angular.module('AlbergueApp', []);

var BienvenidoApp = angular.module('BienvenidoApp', []);


AlbergueApp.controller('Multimedia', function ($scope,$http,$timeout) {
    $scope.Coleccion = [];
    $scope.ColeccionPersonas = [];
    $scope.RepresentanteCedula = "";
    
    var rutaRaiz =  location.href;

    $scope.Nuevo = {
        Cedula : 0,
        Nombre : "",
        Apellido: "",
        Edad: "",
        Parentesco: "",
        Nacionalidad: "",
        FechaIngreso: (new Date()),
        Vigencia: "",
        FamiliaId: ""
    };

     $scope.resetNewObjetc = () => {        
        $scope.Nuevo = {
            Cedula: 0,
            Nombre: "",
            Apellido: "",
            Edad: "",
            Parentesco: "",
            Nacionalidad: "",
            FechaIngreso: (new Date()),
            Vigencia: "",
            FamiliaId: ""
        }
    }

    $scope.nuevaFamilia = {
        Nombre: "",
        ResponsableId: 0,
        Personas: []
     };

    $scope.Representante = false; 
    
    $scope.parametroAtras = 0;
    $scope.parametroCategoriaAudio = 0;
    $scope.estados = { series: true, Audio: true, RepeatSerie: false, buttonAtras: false, RepeatAudio: false, formulario: false, encabezado: false, messajeSuccess: false, messajeError: false };

    //Mostrar Formulario
    $scope.showForm = function () {
        $scope.estado.formulario = true;
    }

    

    
    //Envia la familia para ser insertada en la base de datos.
    $scope.addFamilia = () => {

        //console.log($scope.ColeccionPersonas);
        $scope.nuevaFamilia.Personas = $scope.ColeccionPersonas;
        $scope.nuevaFamilia.ResponsableId = parseInt($scope.RepresentanteCedula);

        console.log("Llegada");
        console.log($scope.RepresentanteCedula);
        console.log("Recibido");
        console.log($scope.nuevaFamilia.ResponsableId);
               
        $http.post("/api/Familias", $scope.nuevaFamilia).then(function (response) {
            //console.log(response);
            if (response.statusText === "OK") {
                $scope.estado.messajeSuccess = true;
                $timeout(ocultarSucces, 2000);
            } else {
                $scope.estado.messajeError = true;
                $timeout(ocultarSucces, 2000);
            }
            
        });

    }


    $scope.getSeries = () => {
        $http.get(rutaRaiz+"/getSerie").then(function (data) {
            $scope.series = data;
           //console.log($scope.series);
        });
    }
    
    $scope.getAudiosSerie= (idSerie) => {
   
    	$scope.estados.RepeatSerie = false;
    	$scope.estados.RepeatAudio = true;
    	$scope.estados.buttonAtras = true;
    	$scope.parametroAtras = 2;
    	//$scope.data= {id:idSerie}
    	
    	//console.log(idSerie);
    
        $http.post(rutaRaiz+"/getAudiosSerie",{"id":idSerie }).then(function (data) {
            $scope.audios = data;
            //console.log($scope.audios.data.data);
           // console.log($scope.audios);
        });
    }
    
    $scope.btnAtras = (param) => {
    	
    	switch (param){
    		//Categorias a Series
    		case 1:
    			$scope.estados.Audio = true;
    			$scope.estados.RepeatSerie = false;
    			$scope.estados.RepeatAudio = false;
    			$scope.estados.buttonAtras = false;
    			$scope.parametroAtras = 0;
    		break;
    		
    		//Audios a Series
    		case 2:
    			$scope.estados.RepeatSerie = true;
    			$scope.estados.RepeatAudio = false;
    			$scope.parametroAtras = 1;
    		break;
    		default:
    			console.log("Nada que hacer");
    		break;
    	}
    
    }
    
    $scope.getCategoriaAudio = (param) => {
    	
    	switch (param){
    		//Series
    		case 1:
    			$scope.estados.Audio = false;
    			$scope.estados.RepeatSerie = true;
    			$scope.estados.buttonAtras = true;
    			$scope.parametroAtras = 1;
    			
    		break;
    		default:
    			console.log("Nada que hacer");
    		break;
    	}
    
    }
    
    
    
    
    
    $scope.changeImageAudio= () => {
    	console.log("aki");
    }
    
    //-------------------Inicializar funciones---------------------------- 
    //Inicializar series
    
    $scope.getSeries();
    
});




//------------------- Controller MultimediaBackend ---------------------------

//---------------DIRECTIVAS------------------------------
	BienvenidoApp.directive("fileread", [function () {
			    return {
			        scope: {
			            fileread: "="
			        },
			        link: function (scope, element, attributes) {
			            element.bind("change", function (changeEvent) {
			                scope.$apply(function () {
			                    scope.fileread = changeEvent.target.files[0];
			                   // console.log(scope.fileread)
			                    // or all selected files:
			                    // scope.fileread = changeEvent.target.files;
			                });
			            });
			        }
			    }
			}]);
			
			
			
			
//----------------SERVICES-----------------------------------
BienvenidoApp.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl, obj){
         var data = new FormData();
    		
	data.append('id',obj.id);
	data.append('nombreAudio',obj.nombreAudio);
	data.append('carpetaAudio',obj.carpetaAudio);
	data.append('fechaAudio',obj.fechaAudio);
	data.append('archivo',file);
	
         var settings = {
		  "async": true,
		  "crossDomain": true,
		  "url": uploadUrl,
		  "method": "POST",
		  "headers": {
		    "content-type": "application/x-www-form-urlencoded",
		    "cache-control": "no-cache",
		    "postman-token": "ec2a9923-617a-5d70-a797-55a4fcb5e83d"
		  },
		  "data": {'hola':"Hola"}
		}
		
		$.ajax(settings).done(function (response) {
		  console.log(response);
		});
     }
 }]);
			
			
BienvenidoApp.controller('MultimediaBackend',['$scope','$http','$timeout','fileUpload', function ($scope, $http, $timeout,fileUpload) {


//-------------Variables y objetos creadas.-----------------------
	
	$scope.nombreSerie = "";  //almacena el nombre de la serie.
	$scope.idSerie = 0; 	  //almacena el id de la serie que queremos modificar.
	
	$scope.estadoFormSerieInsert = true ; //indica si el formulario sera de insersion o de modificacion. (bolean).
	
	var rutaRaiz =  location.href;
	
	$scope.audio = {
		id: 0,
        	nombreAudio: "",
        	carpetaAudio: 0, 
        	fechaAudio: "",
        	archivo:"",
        	nombreArchivo:"",
        	tipoArchivo:"",
        	tamanoArchivo:"",
        	tmpArchivo: "",
	}
	
	
//-------------Fin Variables----------------------------


	
	//Obtener Series de Mensajes. 
	  $scope.getSeries = () => {
	        $http.get(rutaRaiz+"/getSerie").then(function (data) {
	            $scope.series = data;	            
	        });
	    }
		
	
	
	$scope.estados = { showInicio: true, showMiembros: false,showFamilias: false,showVisitantes: false,showMinisterios: false,showCrecimiento: false,showFinanzas: false,showEstadisticas: false,showNoticias: false,showMultimedia: false,showEventos: false,showreportes: false,showUsuarios:false, showSingleMiembro: false };
	
	$scope.estados2 = { menuAudio: true, formSeries: false, successMessage: false, dangerMessage: false,mantenimientoAudiosSeries: false,agregarAudio: false};
	
	$scope.estadoModuloMembresia = {bodyParent:true, formularioMembresia:false,Profesional:false};

	$scope.console = () => {	
		//console.log("Estoy en console");
	}
	
	$scope.inicializar = () => {
		// Inicializar la carga de las series.
		//$scope.getSeries();
		$http.post("multimedia/getSerie").then(function (data) {	            
	            $scope.seriesCreadas = data.data.gruposAudio;	            
	        });
	}
	
	$scope.show = (param) => {
    	
	    	switch (param){
	    		//Inicio
	    		case 1:
	    			for (var k in $scope.estados){
					    if ($scope.estados.hasOwnProperty(k)) {
					        $scope.estados[k] = false; 
					    }
					}
	    			$scope.estados.showInicio = true;
	    			//console.log("Si es aqui");
	    		break;
	    		
	    		//Miembros
	    		case 2:
	    			for (var k in $scope.estados){
					    if ($scope.estados.hasOwnProperty(k)) {
					        $scope.estados[k] = false; 
					    }
					}
	    			$scope.estados.showMiembros = true;
	    		break;
	    		
	    		//Familias
	    		case 3:
	    			for (var k in $scope.estados){
				    if ($scope.estados.hasOwnProperty(k)) {
				        $scope.estados[k] = false; 
				    }
				}
	    			//$scope.estados.showfamilias = true;
	    			$scope.estados.showFamilias = true;
	    		break;
	    		
	    		//Visitantes
	    		case 4:
	    			for (var k in $scope.estados){
				    if ($scope.estados.hasOwnProperty(k)) {
				        $scope.estados[k] = false; 
				    }
				}
	    			$scope.estados.showVisitantes = true;
	    		break;
	    		
	    		//Ministerios
	    		case 5:
	    			for (var k in $scope.estados){
				    if ($scope.estados.hasOwnProperty(k)) {
				        $scope.estados[k] = false; 
				    }
				}
	    			$scope.estados.showMinisterios = true;
	    		break;
	    		
	    		//Grupo de crecimiento
	    		case 6:
	    			for (var k in $scope.estados){
				    if ($scope.estados.hasOwnProperty(k)) {
				        $scope.estados[k] = false; 
				    }
				}
	    			$scope.estados.showCrecimiento= true;
	    		break;
	    		
	    		//Finanzas
	    		case 7:
	    			for (var k in $scope.estados){
				    if ($scope.estados.hasOwnProperty(k)) {
				        $scope.estados[k] = false; 
				    }
				}
	    			$scope.estados.showFinanzas= true;
	    		break;
	    		
	    		//Estadisticas
	    		case 8:
	    			for (var k in $scope.estados){
				    if ($scope.estados.hasOwnProperty(k)) {
				        $scope.estados[k] = false; 
				    }
				}
	    			$scope.estados.showFinanzas= true;
	    		break;
	    		
	    		//Noticias
	    		case 9:
	    			for (var k in $scope.estados){
				    if ($scope.estados.hasOwnProperty(k)) {
				        $scope.estados[k] = false; 
				    }
				}
	    			$scope.estados.showNoticias= true;
	    		break;
	    		
	    		//Multimedia
	    		case 10:
	    			for (var k in $scope.estados){
				    if ($scope.estados.hasOwnProperty(k)) {
				        $scope.estados[k] = false; 
				    }
				}
	    			$scope.estados.showMultimedia= true;
	    		break;
	    		
	    		//Eventos
	    		case 11:
	    			for (var k in $scope.estados){
				    if ($scope.estados.hasOwnProperty(k)) {
				        $scope.estados[k] = false; 
				    }
				}
	    			$scope.estados.showEventos= true;
	    		break;
	    		
	    		//Reportes
	    		case 12:
	    			for (var k in $scope.estados){
				    if ($scope.estados.hasOwnProperty(k)) {
				        $scope.estados[k] = false; 
				    }
				}
	    			$scope.estados.showreportes= true;
	    		break;
	    		
	    		//Usuarios
	    		case 13:
	    			for (var k in $scope.estados){
				    if ($scope.estados.hasOwnProperty(k)) {
				        $scope.estados[k] = false; 
				    }
				}
	    			$scope.estados.showUsuarios= true;
	    		break;
	    		default:
	    			console.log("Nada que hacer");
	    		break;
	    	}
    
    	}
    	
    	
    	$scope.catAudioInsert = (param) => {
    		switch (param){
	    		//Inicio
	    		case 1:
	    			$scope.estados2.menuAudio= false;
	    			$scope.estados2.mantenimientoAudiosSeries = true;	    			
	    		break;
	    		
	    		// Formulario Serie
	    		case 2:
	    			$scope.estados2.menuAudio= false;
	    			$scope.estados2.formSeries = true;
	    			$scope.nombreSerie = "";
	    			$scope.idSerie = 0;
	    			$scope.estadoFormSerieInsert = true;
	    			
	    		break;
	    		//Agregar Audios
	    		case 3:
	    			$scope.estados2.menuAudio= false;
	    			$scope.estados2.agregarAudio= true;
	    		break;
	    		
	    		
	    		
	    		default:
	    			console.log("Nada que hacer");
	    		break;
	    	}
    	}
    	
    	$scope.AtrasCatAudio = (param) => {
    		switch (param){
	    		//Menu Inicio MultimediaBackend
	    		case 1:
	    			$scope.estados2.menuAudio= true;
	    			$scope.estados2.mantenimientoAudiosSeries = false;
	    			
	    		break;
	    		
	    		// Formulario Serie
	    		case 2:
	    			$scope.estados2.formSeries = false;
	    			$scope.estados2.menuAudio= true;
	    			//$scope.nombreSerie = "";
	    			//$scope.idSerie = 0;
	    			
	    		break;
	    		
	    		//Agregar Audios
	    		case 3:
	    			$scope.estados2.menuAudio = true;
	    			$scope.estados2.agregarAudio= false;
	    			$scope.nombreSerie = "";
	    		break;
	    		
	    		
	    		default:
	    			console.log("Nada que hacer");
	    		break;
	    	}
    	}
    	
    	var ocultarSuccess = () =>{
    		$scope.estados2.successMessage = false;
    	}
    	
    	var ocultarDanger = () =>{
    		
    	}
    	
    	$scope.guardarSerie = () => {    	
    		console.log($scope.idSerie);
    		
    		$http.post("multimediaBackend/agregarCarpetaAudio",{"nombreCarpeta":$scope.nombreSerie,"id":$scope.idSerie}).then(function (data) {
	            
	            //console.log(data);
	            if(data.status == 200){
	            	$scope.estados2.successMessage = true;
	            	$('.formSeries').css('opacity',0.5);
                	$timeout(function(){
			           $scope.estados2.successMessage = false;
			           $('.formSeries').css('opacity',1);
			           $scope.nombreSerie = "";
			           $scope.idSerie = "";
			           $scope.estadoFormSerieInsert = true;
			           $scope.getSeries();
			        }, 2000);
	            }else{
	            	$scope.estados2.dangerMessage = true;
	            	$timeout(function(){
			           $scope.estados2.dangerMessage = false;
			           $('.formSeries').css('opacity',1);
			           $scope.nombreSerie = "";
			        }, 2000);
	            }
	        });
	        $scope.inicializar(); 
    	}
    	
    	
    	
    	$scope.agregarAudio = () =>{
    		//console.log($scope.audio);
    		//console.log($scope.audio.archivo);
    		//console.log($scope.audio.archivo.name);
    		
    		$scope.audio.nombreArchivo = $scope.audio.archivo.name;
        	$scope.audio.tipoArchivo = $scope.audio.archivo.type;
        	$scope.audio.tamanoArchivo = $scope.audio.archivo.size;
    		
    		var uploadUrl = "MultimediaBackend/agregarAudio";
    		
    		var data = new FormData();
    		
    		data.append('id',$scope.audio.id);
		data.append('nombreAudio',$scope.audio.nombreAudio);
		data.append('carpetaAudio',$scope.audio.carpetaAudio);
		data.append('fechaAudio',$scope.audio.fechaAudio);
		data.append('archivo',$scope.audio.archivo);
		
		/*$.ajax({
			url:"MultimediaBackend/agregarAudio",
			type:'POST',
			crossDomain: true,
			contentType: "application/x-www-form-urlencoded",
			data: {'hola':"Hola"},
			processData:false,
			cache:false,
	
			success: function(data){									
				console.log(data);
		        },
		        error: function(e) { 
		        	console.log(e);
		        }
		});*/
    		
	    	/*$http.post("MultimediaBackend/agregarAudio",{'hola':"Hola"}).then(function (data) {	            
		      console.log(data);      	            
	        });*/
	        
	        /*$http({
		        url: uploadUrl,
		        method: 'POST',
		        data: data ,
		        //assign content-type as undefined, the browser
		        //will assign the correct boundary for us
		        headers: { 'Content-Type': undefined},
		        //prevents serializing payload.  dont do it.
		        transformRequest: angular.identity
		    });*/
	        
	        
	        
	      fileUpload.uploadFileToUrl($scope.audio.archivo, uploadUrl , $scope.audio);
	      
		 
    	
    		
    	}
    	
    	$scope.verSeries = () => {
    		$http.post("multimedia/getSerie").then(function (data) {	            
	            $scope.seriesCreadas = data.data.gruposAudio;	            
	        });
    		
    	}
    	
    	$scope.editSerie = (id) => {
    		
	    	$scope.estados2.mantenimientoAudiosSeries = false;
	    	$scope.estados2.formSeries = true;
	    	$scope.estadoFormSerieInsert = false;
	    	
	    	$scope.idSerie = id;
	    	
	    	
	    	$http.post("multimedia/getSerie",{"id":$scope.idSerie}).then(function (data) {	 
	            $scope.nombreSerie = data.data.gruposAudio[0].nombre           
	        });
    	}
    	
    	$scope.cancelarFormSerie = () => {
    		$scope.estados2.menuAudio = true;
    		$scope.estados2.mantenimientoAudiosSeries = false;
    		$scope.estados2.formSeries = false;
    		$scope.estadoFormSerieInsert = true;
    		$scope.idSerie = 0;
    		$scope.nombreSerie = "";
    	}

    	$scope.btnProfesion = (valor) => {
    		console.log(valor);
    		
	    	if(valor == 4){
				$scope.estadoModuloMembresia.Profesional = true;
	    	}else{
	    		$scope.estadoModuloMembresia.Profesional = false;
	    	}
	    	
    	}
    	

    	$scope.addMiembroBienvenido = () => {
    		$scope.estadoModuloMembresia.bodyParent = false;
    		$scope.estadoModuloMembresia.formularioMembresia = true; 

    		$http.post("miembros/getEstatosMiembros").then(function (data) {
    			//console.log('estoy aqui');
    			//console.log(data.data.estados);	            
	            $scope.estadosMiembros = data.data.estados;	  

	            const mapped = Object.keys($scope.estadosMiembros).map(key => ({type:key, value:$scope.estadosMiembros[key]}));          
	        	console.log(mapped);
	        });
    	}

    	$scope.cancelarFormSerie = () => {
    		$scope.estadoModuloMembresia.bodyParent = true;
    		$scope.estadoModuloMembresia.formularioMembresia = false; 

    	}
    	
    	



    	$scope.guardarMiembro = () => {    	
    		console.log('guardar miembro');
    		
    		/*$http.post("multimediaBackend/agregarCarpetaAudio",{"nombreCarpeta":$scope.nombreSerie,"id":$scope.idSerie}).then(function (data) {
	            
	            //console.log(data);
	            if(data.status == 200){
	            	$scope.estados2.successMessage = true;
	            	$('.formSeries').css('opacity',0.5);
                	$timeout(function(){
			           $scope.estados2.successMessage = false;
			           $('.formSeries').css('opacity',1);
			           $scope.nombreSerie = "";
			           $scope.idSerie = "";
			           $scope.estadoFormSerieInsert = true;
			           $scope.getSeries();
			        }, 2000);
	            }else{
	            	$scope.estados2.dangerMessage = true;
	            	$timeout(function(){
			           $scope.estados2.dangerMessage = false;
			           $('.formSeries').css('opacity',1);
			           $scope.nombreSerie = "";
			        }, 2000);
	            }
	        });*/
	        //$scope.inicializar(); 
    	}	

    	$scope.getMiembros = () => {
    		$http.post("bienvenido/miembros").then(function (data) {	
    			console.log(data.data); 
    			
    			/*for (var k in data.data){
				    if (k.imagen = "") {
				        console.log("vacia");
				    }else{
				        console.log(k);
				        console.log("----------------------");
				        console.log(data);
				        console.log("----------------------");
				    }
    			}*/
    			
    		    angular.forEach(data.data, function(value, key){
                  if(value.imagen == ""){
                     data.data[key].imagen = "sin_imagen.gif";
                  }
                });
	            $scope.miembros = data.data;           
	        });    		
    	}
    	
    	$scope.getSingleMiembro = (id) => {
    	    //console.log(id);
    	    $scope.idMimbro = id;
    	 	$http.post("bienvenido/miembro",{"id":$scope.idMimbro}).then(function (data) {	
    			//console.log(data); 
    			
    		 
                  if(data.data.imagen == ""){
                     data.data.imagen = "sin_imagen.gif";
                  }
                
                
	            $scope.miembro = data.data; 
	            console.log($scope.miembro );
	            $scope.estados.showMiembros = false;
	        });   		
    	}


    	
    	
    	//--------------------Miembros--------------------------\\
    	$scope.holaMundo = "Hola Mundo";
    
    	
    	$scope.console();
    	$scope.inicializar();     
    	$scope.getSeries();
    	$scope.getMiembros();
}]);

