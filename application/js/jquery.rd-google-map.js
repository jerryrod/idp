/*
 *  RD-Google Map - v0.1
 *  Easy as hell Google Map Api Jquery plugin.
 *
 *  Made by Evgeniy Gusarov (Stmechanus || Diversant)
 *
 *  Under MIT License
 */

pathArray = location.href.split('/');
protocol = pathArray[0];
host = pathArray[2];
url = pathArray[0] + '/' + pathArray[1] + '/' + pathArray[2] + '/' + pathArray[3] + '/' + pathArray[4] + '/' + pathArray[5] + '/' + pathArray[6] + '/';
console.log(url);

;
(function ($) {
    'use strict'

    var def_settings = {
            cntClass: 'map',
            mapClass: 'map_model',
            locationsClass: 'map_locations',
            marker: {
                basic: url +'../application/images/gmap_marker.png',
                active: url +'../application/images/gmap_marker_active.png'
            },
            styles: []
        },

        defaults = {
            map: {
                x: -69.8198159,
                y: 18.4667484,
                zoom: 14
            },
            locations: []
        };


        var getLocations = function ($map, settings) {
        var $locations = $map.parent().find('.' + settings.locationsClass).find('li');

        var locations = [];


        if ($locations.length > 0) {
            $locations.each(function (i) {
                var $loc = $(this);

                if ($loc.data('x') && $loc.data('y')) {
                    locations[i] = {
                        x: $loc.data('x'),
                        y: $loc.data('y'),
                        basic: $loc.data('basic') ? $loc.data('basic') : settings.marker.basic,
                        active: $loc.data('active') ? $loc.data('active') : settings.marker.active
                    }

                    if (!$.trim($loc.html())) {
                        locations[i].content = false;
                    } else {
                        locations[i].content = '<div class="iw-content">' + $loc.html() + '</div>';
                    }
                }
            });
        }
        return locations;
    }

    $.fn.googleMap = function (settings) {

        settings = $.extend(true, {}, def_settings, settings);

        $(this).each(function () {
            var $this = $(this);

            var options = $.extend(
                true, {}, defaults,
                {
                    map: {
                        /*x: $this.data('x'),
                        y: $this.data('y'),
                        zoom: $this.data('zoom')*/
                        x: -69.8198159,
                        y: 18.4667484,
                        zoom: 18
                    },
                    locations: getLocations($this, settings)
                }
            );

            var map = new google.maps.Map(this, {
                    center: new google.maps.LatLng(
                        parseFloat(options.map.y),
                        parseFloat(options.map.x)
                    ),
                    scrollwheel: false,
                    styles: settings.styles,
                    zoom: options.map.zoom
                }),
                infowindow = new google.maps.InfoWindow(),
               // markers = [];

               markers = new google.maps.Marker(
                    {
                        position: new google.maps.LatLng(
                            parseFloat(options.locations.y),
                            parseFloat(options.locations.x)),
                        map: map,
                        icon: options.locations.basic,
                        index: 1
                    }
                );

               var latlng = new google.maps.LatLng(18.4665484,-69.8197159);

                function addmarker(latilongi) {
                    var marker = new google.maps.Marker({
                        position: latilongi,
                        title: 'Iglesia de Dios de la Profecia',
                        draggable: true,
                        map: map
                    });
                    map.setCenter(marker.getPosition());
                }

                addmarker(latlng);


            google.maps.event.addDomListener(window, 'resize', function() {
                map.setCenter(new google.maps.LatLng(
                    parseFloat(options.map.y),
                    parseFloat(options.map.x)
                ));
            });
        });
    };


})(jQuery);
