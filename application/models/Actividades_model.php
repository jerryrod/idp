<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Actividades_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_Actividades($id_actividad='',$id_usuario)
	{
		$data = array();	

		$this->db->where('a.id_usuario', 2); 

		if($id_actividad==null){
			$this->db->select('a.*,d.nombreDia');
			$this->db->from('actividadesirregulares a');
			$this->db->join('diassemana d','a.dia=d.id');
			$sql = $this->db->get();
			$data = $sql->result_array();
		}

		if($id_actividad != null){
			$this->db->where('a.id', $id_actividad); 
			$this->db->select('a.*,d.nombreDia');
			$this->db->from('actividadesirregulares a');
			$this->db->join('diassemana d','a.dia=d.id');
			$sql = $this->db->get();
			$data = $sql->row();
		}
		
		return $data;
	}



	public function save_actividad($id='',$id_usuario,$dia,$fecha,$nombre,$hora,$descripcion)
	{
		$data = array(
                  	'id_usuario' => $id_usuario,
                  	'dia' => $dia ,
   					'fecha' => $fecha,
   					'nombre' => $nombre,
   					'hora' => $hora,
   					'Descripcion' => $descripcion   					 
				);
		
		
		if($id > 0){
			$this->db->where('id',$id);
			$this->db->where('id_usuario',$id_usuario);
			$this->db->update('actividadesirregulares',$data);
		}

		if($id == null){
			$this->db->insert('actividadesirregulares',$data);
		}
	}

	public function eliminar_Imagen($id='',$titulo,$descripcion,$fecha,$imagen='',$id_usuario)
	{
		if($imagen == null){
			$data = array(
                  	'imagen' => $imagen
				);
		}
		if($imagen != null){
			$data = array(
   					'imagen' => $imagen
				);
		}
		
		if($id >0){
			$this->db->where('id',$id);
			$this->db->update('noticias',$data);
		}

		if($id == null){
			$this->db->insert('noticias',$data);
		}
	}

	public function removeActividad($id_usuario,$id)
	{
		if($id > 0){
			$this->db->where('id',$id);
			$this->db->where('id_usuario',$id_usuario);
			$this->db->delete('actividadesirregulares');
		}
		
	}


}