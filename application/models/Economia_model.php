<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Economia_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_Conceptos($value='')
	{
		$data = array();		
		
		$sql = $this->db->get('conceptos');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombre'];
            }
		}
		return $data;
		
	}

	public function save_Datos($concepto,$dato2010,$dato2011,$dato2012,$dato2013,$dato2014)
	{
		$data = array(
                  	'id_concepto' => $concepto,
                  	'dato2010' => $dato2010,
   					'dato2011' => $dato2011,
   					'dato2012' => $dato2012,
   					'dato2013' => $dato2013,
   					'dato2014'=> $dato2014
				);

		$this->db->insert('datos',$data);
	}

	public function get_Valores($value='')
	{
		$data = array();

		$this->db->select('c.nombre,d.*');
		$this->db->join('conceptos c','d.id_concepto = c.id');
		$this->db->from('datos d');
		$sql = $this->db->get();
		$data =$sql->result_array();
		return $data;
	}

}