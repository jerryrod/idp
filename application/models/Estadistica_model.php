<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Estadistica_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_ActividadesRegulares($value='')
	{
		$data = array();
		$sql = $this->db->get('actividadesregulares');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombreActividadRegular'];
            }
		}
		return $data;
		
	}

	public function get_PorcentajeAsistenciaEstadistico($sexo,$ministerio,$actividad)
	{

		$data = array();

		if($sexo>1){
			if($sexo==2){
				$sexo = 'M';
			}
			if($sexo==3){
				$sexo = 'F';
			}
			$this->db->where('m.sexo',$sexo);
		}

		$this->db->select('format(((select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario where estado_asistencia="presente" and ac.fecha >= m.fechaConversion )/(select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario where ac.fecha >= m.fechaConversion))*100,2) as porcentaje');
		$this->db->from('asistencia a');
		$this->db->join('miembros m','a.id_miembro = m.id and a.id_usuario = m.usuario');
		$this->db->group_by('a.id_miembro');
		$sql = $this->db->get();
		$data =$sql->result_array();
		return $data;
	}

	public function get_PorcentajeAsistenciaEstadistico2($sexo,$ministerio,$actividad)
	{

		$data = array();

		if($sexo == 1 && $ministerio == 1 && $periodo == 1 && $actividad == 8){
			$this->db->select('(select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario join miembros m on a.id_miembro = m.id and a.id_usuario = m.usuario where estado_asistencia="presente" and ac.fecha >= m.fechaConversion)/(select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario join miembros m on a.id_miembro = m.id and a.id_usuario = m.usuario where ac.fecha >= m.fechaConversion)*100 as porcentaje');
			$sql = $this->db->get();
			$data =$sql->row();
		}

		if($sexo > 1 && $ministerio == 1 && $periodo == 1 && $actividad == 8){
			if($sexo==2){
				$sexo = 'M';
			}
			if($sexo==3){
				$sexo = 'F';
			}
			$this->db->where('m.sexo',$sexo);

			$this->db->select('format(((select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario where estado_asistencia="presente" and ac.fecha >= m.fechaConversion )/(select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario where ac.fecha >= m.fechaConversion))*100,2) as porcentaje');
			$this->db->from('asistencia a');
			$this->db->join('miembros m','a.id_miembro = m.id and a.id_usuario = m.usuario');
			$this->db->group_by('a.id_miembro');
			$sql = $this->db->get();
			$data =$sql->result_array();

		}

		/*$this->db->select('format(((select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario where estado_asistencia="presente" and ac.fecha >= m.fechaConversion )/(select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario where ac.fecha >= m.fechaConversion))*100,2) as porcentaje');
		$this->db->from('asistencia a');
		$this->db->join('miembros m','a.id_miembro = m.id and a.id_usuario = m.usuario');
		$this->db->group_by('a.id_miembro');
		$sql = $this->db->get();
		$data =$sql->result_array();*/
		return $data;
	}


	public function get_PorcentajeAsistenciaEstadistico3($sexo,$ministerio='',$actividad='',$fechaInicial ='',$fechaFinal ='')
	{

		$data = array();
		$this->db->where('m.sexo',$sexo);

		if($fechaInicial != null && $fechaFinal != null){
			$this->db->select('m.sexo,(select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario join miembros m on a.id_miembro = m.id and a.id_usuario = m.usuario where estado_asistencia="presente" and m.sexo = "'.$sexo.'" and ac.fecha >= m.fechaConversion and a.fecha_asistencia >="'.$fechaInicial.'" and a.fecha_asistencia <= "'.$fechaFinal.'")/(select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario join miembros m on a.id_miembro = m.id and a.id_usuario = m.usuario where m.sexo = "'.$sexo.'"  and ac.fecha >= m.fechaConversion and a.fecha_asistencia >="'.$fechaInicial.'" and a.fecha_asistencia <= "'.$fechaFinal.'")*100 as porcentaje');
		}else{
			$this->db->select('m.sexo,(select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario join miembros m on a.id_miembro = m.id and a.id_usuario = m.usuario where estado_asistencia="presente" and m.sexo = "'.$sexo.'" and ac.fecha >= m.fechaConversion)/(select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario join miembros m on a.id_miembro = m.id and a.id_usuario = m.usuario where m.sexo = "'.$sexo.'"  and ac.fecha >= m.fechaConversion)*100 as porcentaje');
		}		
		
		$this->db->from('asistencia a');
		$this->db->join('miembros m','a.id_miembro = m.id and a.id_usuario = m.usuario');
		$this->db->group_by('m.sexo');
		$sql = $this->db->get();
		$data =$sql->row();

		return $data;
		
	}

	public function get_PorcentajeAsistenciaEstadistico4($sexo,$ministerio,$actividad)
	{

		$data = array();

		
		$this->db->where('m.sexo',$sexo);
		$this->db->where('a.evento_asistencia',$actividad);
		$this->db->select('m.sexo,(select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario join miembros m on a.id_miembro = m.id and a.id_usuario = m.usuario where estado_asistencia="presente" and m.sexo = "'.$sexo.'" and a.evento_asistencia = "'.$actividad.'"  and ac.fecha >= m.fechaConversion)/(select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario join miembros m on a.id_miembro = m.id and a.id_usuario = m.usuario where m.sexo = "'.$sexo.'" and a.evento_asistencia = "'.$actividad.'" and ac.fecha >= m.fechaConversion)*100 as porcentaje');
		$this->db->from('asistencia a');
		$this->db->join('miembros m','a.id_miembro = m.id and a.id_usuario = m.usuario');
		$this->db->group_by('m.sexo','a.evento_asistencia');
		$sql = $this->db->get();
		$data =$sql->row();

		return $data;
		
	}

	public function get_Total_Asistencias_Presentes_Agrupadas_Por_Actividad($sexo='',$fechaInicial='',$fechaFinal='')
	{

		$data = array();

		if($sexo != null){
			if($sexo==2){
				$sexo='M';
			}
			if($sexo==3){
				$sexo='F';
			}
			$this->db->where('m.sexo',$sexo);
		}
		if($fechaInicial != null && $fechaFinal != null){
			$this->db->where('a.fecha_asistencia >=',$fechaInicial);
			$this->db->where('a.fecha_asistencia <=',$fechaFinal);
		}


		$this->db->where('estado_asistencia','presente');
		//$this->db->where('ac.fecha >=','m.fechaConversion');

		$this->db->select('ar.nombreActividadRegular,count(*) as TotalAsistenciaPresente');
		$this->db->from('asistencia a');
		$this->db->join('actividades ac','a.idEvento = ac.id and a.id_usuario = ac.id_usuario');
		$this->db->join('miembros m','a.id_miembro = m.id and a.id_usuario = m.usuario');
		$this->db->join('actividadesregulares ar','ar.id = a.evento_asistencia');
		$this->db->group_by('a.evento_asistencia');
		$sql = $this->db->get();
		$data = $sql->result_array();

		return $data;
		
	}

	public function get_Total_Asistencias_Agrupadas_Por_Actividad($sexo = '',$fechaInicial='',$fechaFinal='')
	{

		$data = array();

		if($sexo != null){
			if($sexo==2){
				$sexo='M';
			}
			if($sexo==3){
				$sexo='F';
			}
			$this->db->where('m.sexo',$sexo);
		}

		if($fechaInicial != null && $fechaFinal != null){
			$this->db->where('a.fecha_asistencia >=',$fechaInicial);
			$this->db->where('a.fecha_asistencia <=',$fechaFinal);
		}

		//$this->db->where('ac.fecha >=','m.fechaConversion');

		$this->db->select('ar.nombreActividadRegular,count(*) as TotalAsistencia');
		$this->db->from('asistencia a');
		$this->db->join('actividades ac','a.idEvento = ac.id and a.id_usuario = ac.id_usuario');
		$this->db->join('miembros m','a.id_miembro = m.id and a.id_usuario = m.usuario');
		$this->db->join('actividadesregulares ar','ar.id = a.evento_asistencia');
		$this->db->group_by('a.evento_asistencia');
		$sql = $this->db->get();
		$data = $sql->result_array();

		return $data;
		
	}

	public function get_Total_Asistencias_Presentes_Por_Actividad($sexo = '',$actividad,$fechaInicial='',$fechaFinal='')
	{

		$data = array();

		if($sexo != null){
			if($sexo==2){
				$sexo='M';
			}
			if($sexo==3){
				$sexo='F';
			}
			$this->db->where('m.sexo',$sexo);
		}

		if($fechaInicial != null && $fechaFinal != null){
			$this->db->where('a.fecha_asistencia >=',$fechaInicial);
			$this->db->where('a.fecha_asistencia <=',$fechaFinal);
		}

		$this->db->where('a.evento_asistencia',$actividad);
		$this->db->where('estado_asistencia','presente');
		//$this->db->where('ac.fecha >=','m.fechaConversion');

		$this->db->select('ar.nombreActividadRegular,count(*) as TotalAsistencia');
		$this->db->from('asistencia a');
		$this->db->join('actividades ac','a.idEvento = ac.id and a.id_usuario = ac.id_usuario');
		$this->db->join('miembros m','a.id_miembro = m.id and a.id_usuario = m.usuario');
		$this->db->join('actividadesregulares ar','ar.id = a.evento_asistencia');
		$this->db->group_by('a.evento_asistencia');
		$sql = $this->db->get();
		$data = $sql->result_array();

		return $data;
		
	}

	public function get_Total_Asistencias_Por_Actividad($sexo = '',$actividad,$fechaInicial='',$fechaFinal='')
	{

		$data = array();

		if($sexo != null){
			if($sexo==2){
				$sexo='M';
			}
			if($sexo==3){
				$sexo='F';
			}
			$this->db->where('m.sexo',$sexo);
		}

		if($fechaInicial != null && $fechaFinal != null){
			$this->db->where('a.fecha_asistencia >=',$fechaInicial);
			$this->db->where('a.fecha_asistencia <=',$fechaFinal);
		}

		$this->db->where('a.evento_asistencia',$actividad);

		//$this->db->where('ac.fecha >=','m.fechaConversion');

		$this->db->select('ar.nombreActividadRegular,count(*) as TotalAsistencia');
		$this->db->from('asistencia a');
		$this->db->join('actividades ac','a.idEvento = ac.id and a.id_usuario = ac.id_usuario');
		$this->db->join('miembros m','a.id_miembro = m.id and a.id_usuario = m.usuario');
		$this->db->join('actividadesregulares ar','ar.id = a.evento_asistencia');
		$this->db->group_by('a.evento_asistencia');
		$sql = $this->db->get();
		$data = $sql->result_array();

		return $data;
		
	}



}