<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Evento_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	

	public function save_Evento($id='',$nombre_evento,$fecha,$imagen='',$descripcion,$nombre_responsable,$id_usuario,$ministerio)
	{
		$data = array(
					'nombre'      => $nombre_evento,
					'fecha'      => $fecha,
					'imagen'	=> $imagen,
					'descripcion' => $descripcion,
                  	'responsable' =>  $nombre_responsable,
   					'id_usuario'=> $id_usuario,
   					'ministerio' => $ministerio
				);

		if($id >0){
			$this->db->where('id',$id);
			$this->db->update('eventos',$data);
		}

		if($id == null){
			$this->db->insert('eventos',$data);
		}
	}


	public function get_Eventos($id_usuario,$ministerio='')
	{
		$data = array();	
		$this->db->where('id_usuario', $id_usuario);
		if($ministerio != null){
			$this->db->where('m.ministerio', $ministerio);
		}

		$this->db->select('e.id,e.nombre,e.fecha,e.descripcion,e.responsable,concat(m.nombre, " ",m.apellido) as nombre_completo');
		$this->db->from('eventos e');
		$this->db->join('miembros m', 'm.id = e.responsable','left');
		$sql = $this->db->get();

		$data =$sql->result_array();
		return $data;
		
	}

	
}