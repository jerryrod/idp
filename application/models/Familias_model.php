<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Familias_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function save_Familia($id='',$apellidoPaterno,$apeliidoMatertno,$descripcion)
	{
		$data = array(
                  	'apellidoPaterno' =>  $apellidoPaterno,
                  	'apellidoMaterno' => $apeliidoMatertno ,
   					'descripcion' => $descripcion
				);

		if($id > 0){
			$this->db->where('id',$id);
			$this->db->update('familia',$data);
		}

		if($id == null){
			$this->db->insert('familia',$data);
		}
	}

	public function get_Familias($id='')
	{
		$data = array();	

		if($id > 0){
			$this->db->where('id',$id);
			$this->db->select('*');
			$this->db->from('familia');
			$sql = $this->db->get();
			$data =$sql->row();
		
		}else{
			$this->db->select('*');
			$this->db->from('familia');
			$sql = $this->db->get();			
			$data =$sql->result_array();
		}
				
		return $data;
	}
}