<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Finanzas_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_Actividades($value='')
	{
		$data = array();

		$actividadesConOfrenda = $this->actividadesConOfrendas();

		$this->db->where_not_in('a.id',$actividadesConOfrenda);

		$this->db->select('a.id id,ar.nombreActividadRegular nombreActividadRegular,a.fecha fecha' );
		$this->db->from('actividades a');
		$this->db->join('actividadesRegulares ar','ar.id = a.nombreActividad');
		$sql = $this->db->get();

		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombreActividadRegular']."-".$rs['fecha'];
            }
		}
		return $data;
		
	}

	public function actividadesConOfrendas()
	{
		$data = array();


		$this->db->select('id,actividad');
		$this->db->from('ofrendas');		
		$sql = $this->db->get();

		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['actividad'];
            }
		}

		//$data = $sql->result_array();

		return $data;
	}

	public function get_Ofrendas($id_usuario,$id='')
	{
		$data = array();
		$this->db->where('o.id_usuario',$id_usuario);
		if($id != null){
			$this->db->where('o.id',$id);
			$this->db->select('o.*,ar.nombreActividadRegular');
			$this->db->from('ofrendas o');
			$this->db->join('actividades a','a.id = o.actividad');
			$this->db->join('actividadesregulares ar','ar.id = a.nombreActividad');
			//$this->db->join('gastosofrenda go','o.id=go.id_Ofrenda');
			$sql = $this->db->get();
			$data = $sql->row();
		}
		if($id == null){
			$this->db->select('o.*,ar.nombreActividadRegular');
			$this->db->from('ofrendas o');
			$this->db->join('actividades a','a.id = o.actividad');
			$this->db->join('actividadesregulares ar','ar.id = a.nombreActividad');
			//$this->db->join('gastosofrenda go','o.id=go.id_Ofrenda');
			$sql = $this->db->get();
			$data = $sql->result_array();
		}
		

		return $data;

	}

	public function get_Gastos($id_usuario,$id='')
	{
		$data = array();
		$this->db->where('go.id_usuario',$id_usuario);
		if($id != null){
			$this->db->where('go.id_Ofrenda',$id);
			$this->db->select('go.*');
			$this->db->from('gastosofrenda go');
			$sql = $this->db->get();
			$data = $sql->result_array();
		}
		if($id == null){
			$this->db->select('go.*');
			$this->db->from('gastosofrenda go');
			$sql = $this->db->get();
			$data = $sql->result_array();
		}
		

		return $data;

	}

	public function save_Ofrenda($id,$cantidadNeta,$cantidadBruta,$actividad,$id_usuario,$fechaOfrenda,$gastosTotal)
	{
		$data = array(
                  	'cantidadNeta' =>  $cantidadNeta,
                  	'cantidadBruta' => $cantidadBruta,
   					'actividad' => $actividad,
   					'id_usuario' => $id_usuario,
   					'fechaOfrenda' => $fechaOfrenda,
   					'gastos'=> $gastosTotal
				);
		if($id >0){
			$this->db->where('id',$id);
			$this->db->update('ofrendas',$data);
		}

		if($id == null){
			$this->db->insert('ofrendas',$data);
		}
		return $this->db->insert_id();
	}

	public function save_Gastos($id_Ofrenda,$id_Usuario,$descripcionGasto,$cantidadGasto,$fechaOfrenda)
	{
		$data = array(
                  	'id_Ofrenda' =>  $id_Ofrenda,
                  	'id_Usuario' => $id_Usuario,
   					'descripcionGasto' => $descripcionGasto,
   					'cantidadGasto' => $cantidadGasto,
   					'fechaGasto' => $fechaOfrenda
				);

		$this->db->insert('gastosofrenda',$data);
	}


}