<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Gcrecimiento_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_Celulas($id_usuario)
	{
		$data = array();	
		$this->db->where('g.id_usuario', $id_usuario); 

		$this->db->select('g.*, d.nombreDia,concat(m.nombre," ",m.apellido) as nombre');
		$this->db->from('grupocrecimiento g');
		$this->db->join('diassemana d','g.dia = d.id');
		$this->db->join('miembros m','g.lider = m.id');

		$sql = $this->db->get();
		$data =$sql->result_array();
		return $data;
	}


	public function get_Dias($value='')
	{
		$data = array();
		$sql = $this->db->get('diassemana');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombreDia'];
            }
		}
		return $data;
		
	}

	public function get_Lider($id_usuario,$id)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('g.id', $id); 
		
		$this->db->select('concat(m.nombre," ",m.apellido) as lider');
		$this->db->from('grupocrecimiento g');
		$this->db->join('miembros m','g.lider = m.id');

		$sql = $this->db->get();
		$data =$sql->row();
		return $data;
	}

	public function get_Miembros_Por_Celula_model($id_usuario,$id_celula)
	{
		$data = array();	
		
		$this->db->where('usuario', $id_usuario); 
		$this->db->where('celula', $id_celula); 
		
		$sql = $this->db->get('miembros');
		$data =$sql->result_array();

		return $data;
	}

	public function get_Celula_Editar($id_usuario,$id)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('id', $id); 
		
		$this->db->select('*');
		$this->db->from('grupocrecimiento');

		$sql = $this->db->get();
		$data =$sql->row();
		return $data;
	}

	public function get_Celula_Single($id_usuario,$id)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('g.id_usuario', $id_usuario); 
		$this->db->where('g.id', $id); 
		
		$this->db->select('concat(m.nombre, " ", m.apellido) as nombreCompleto,g.*,d.nombreDia as nombreDia');
		$this->db->from('grupocrecimiento g');
		$this->db->join('miembros m','g.lider = m.id');
		$this->db->join('diassemana d','g.dia = d.id');


		$sql = $this->db->get();
		$data =$sql->row();
		return $data;
	}

	
	public function get_Celulas_Add_Miembro($id_usuario)
	{
		$data = array();
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->select('g.*,m.nombre,m.apellido');
		$this->db->from('grupocrecimiento g');
		$this->db->join('miembros m','g.lider = m.id');
		$sql = $this->db->get();
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombre'].' '.$rs['apellido'];
            }
		}
		return $data;
		
	}

	public function save_Celula($id='',$lider_celula,$anfitrion,$direccion,$dia,$fecha_inicio,$id_usuario)
	{
		$data = array(
					'lider'      => $lider_celula,
					'anfitrion'      => $anfitrion,
					'direccion'	=> $direccion,
					'dia' => $dia,
                  	'fechaInicio' =>  $fecha_inicio,
   					'id_usuario'=> $id_usuario
				);

		if($id >0){
			$this->db->where('id',$id);
			$this->db->update('grupocrecimiento',$data);

		}

		if($id == null){
			$this->db->insert('grupocrecimiento',$data);
		}

		return $this->db->insert_id();		

	}

	public function save_lider_celula($lider,$id_usuario,$id_celula)
	{
		$data = array(
					'id_miembro'  => $lider,
					'id_usuario'  => $id_usuario,
   					'id_celula'=> $id_celula
				);
		
		$this->db->insert('liderescelula',$data);		

	}

	public function save_Cambio_de_lider($id_lider,$descripcion,$id_usuario_cambio,$id_celula,$lider_reemplazo)
	{
		$data = array(
					'fecha_fin' => date('Y-m-d H:i:s'),
					'descripcion_cambio_lider' => $descripcion,
					'id_usuario_cambio' => $id_usuario_cambio,
					'id_miembro_reemplazo' => $lider_reemplazo
				);

		$this->db->where('id_miembro',$id_lider);
		$this->db->where('id_celula',$id_celula);
		
		$this->db->update('liderescelula',$data);		

	}

	public function save_Cambio_de_lider2($id_lider,$descripcion,$id_usuario,$id)
	{
		$data = array(
					'idLider'  => $id_lider,
					'descripcion' =>$descripcion,
					'id_usuario'  => $id_usuario,
   					'id_celula'=> $id
				);
		
		$this->db->insert('pruebatabla',$data);		

	}

	

	
}