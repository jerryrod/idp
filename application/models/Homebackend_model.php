<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class homeBackend_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_AdultosPresentes($id_usuario,$id_evento,$grupo='')
	{
		//var_dump($id);
		$data = array();	

		if($grupo != null){
			if($grupo == 'a'){
				$ministerio = array(5,6);
			}
			if($grupo == 'j'){
				$ministerio = array(7,8,9);				
			}
			$this->db->where_in('m.ministerio',$ministerio);
		}	
		
		$this->db->where('a.id_usuario', $id_usuario); 
		$this->db->where('a.idEvento', $id_evento); 
		$this->db->where('a.estado_asistencia', 'presente');
		
		$this->db->select('count(*) as cantidadTotal');
		$this->db->from('asistencia a');
		$this->db->join('miembros m','a.id_miembro = m.id');
		$sql = $this->db->get();
		$data = $sql->row();
		return $data;
		//return $id_evento;
	}


	//Este metodo permite obtener la ultima actividad del mes realizada, esta podra ser: Escuela Biblica, culto domingo o culto del viernes.
	public function get_UltimaActividad($id_usuario,$actividad)
	{
		$data = array();

		$this->db->where('id_usuario', $id_usuario);
		$this->db->where('nombreActividad',$actividad);

		$this->db->select('id');
		$this->db->from('actividades');
		$this->db->order_by('fecha','desc');
		$sql = $this->db->get();
		$data = $sql->row();
		return $data;
	}

	public function getCantidadMiembroPorGrupo($id_usuario,$id_evento,$grupo)
	{
		//var_dump($id);
		$data = array();
		if($grupo != null){
			if($grupo == 'a'){
				$ministerio = array(5,6);
			}
			if($grupo == 'j'){
				$ministerio = array(7,8,9);
			}
			$this->db->where_in('m.ministerio',$ministerio);
		}	
		
				
		$this->db->where('a.id_usuario', $id_usuario); 
		$this->db->where('a.idEvento', $id_evento); 
		
		
		$this->db->select('count(*) as cantidadTotal');
		$this->db->from('asistencia a');
		$this->db->join('miembros m','a.id_miembro = m.id');
		$sql = $this->db->get();
		$data = $sql->row();
		return $data;
		//return $id_evento;
	}

	//Retorna los datos de la actividad de una semana atras
	public function get_ActividadesSemanal()
	{
		$data1 = array();

		$sql = $this->db->get('asistenciaactsemanal');
		$data1 = $sql->result_array();
		return $data1;
	}

	//Retorna los datos de la actividad de dos semana atras
	public function get_ActividadesSemanal2()
	{
		$data = array();

		$sql = $this->db->get('asistenciasemanal2');
		$data = $sql->result_array();
		return $data;
	}

	//Retorna los datos de la actividad de dos semana atras
	public function get_ActividadesSemanal3()
	{
		$data = array();

		$sql = $this->db->get('asistenciasemanal3');
		$data = $sql->result_array();
		return $data;
	}
}
