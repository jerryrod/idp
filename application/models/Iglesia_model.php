<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Iglesia_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	
	public function add_church($id='',$nombreIglesia,$calleIglesia,$numeroLocalIglesia,$paisIglesia,$provincia,$sectorIglesia)
	{
		$data = array(
                  	'nombre' =>  $nombreIglesia,
                  	'provincia' => $provincia ,
   					'pais' => $paisIglesia,
   					'calle' => $calleIglesia,
   					'numeroLocal' => $numeroLocalIglesia,
   					'sectorIglesia' => $sectorIglesia
   		);

		if($id >0){
			$this->db->where('id',$id);
			$this->db->update('iglesias',$data);
		}

		if($id == null){
			$this->db->insert('iglesias',$data);
		}
		return $this->db->insert_id();
	}	
}