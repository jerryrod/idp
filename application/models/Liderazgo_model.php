<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Liderazgo_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_Lideres($id_lider='',$id_usuario)
	{
		$estado = 0;
		$data = array();	
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('l.estado !=',$estado);
		if($id_lider==null){
			$this->db->select('concat(m.nombre, " ",m.apellido) as nombre_completo,c.nombre, l.*');
			$this->db->join('miembros m','m.id=l.id_miembro and l.id_usuario=m.usuario');
			$this->db->join('cargos c','l.id_cargo = c.id');
			$this->db->from('lidereslocales l');
			$sql = $this->db->get();
			$data =$sql->result_array();
		}
		if($id_lider != null){
			$this->db->where('id', $id_lider); 
			$sql = $this->db->get('lidereslocales');
			$data =$sql->row();
		}
		
		return $data;
	}





	public function get_Lideres_fijos($id_lider='',$id_usuario)
	{
		$estado = 0;
		$cargos = array(1,2,5,6,7,8,9,10,11);
		$data = array();	
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('l.estado !=',$estado);
		$this->db->where_in('l.id_cargo',$cargos);
		$this->db->order_by('l.id_cargo','asc');
		
		if($id_lider==null){
			$this->db->select('concat(m.nombre, " ",m.apellido) as nombre_completo,c.nombre, l.*');
			$this->db->join('miembros m','m.id=l.id_miembro and l.id_usuario=m.usuario');
			$this->db->join('cargos c','l.id_cargo = c.id');
			$this->db->from('lidereslocales l');
			$sql = $this->db->get();
			$data =$sql->result_array();
		}
		if($id_lider != null){
			$this->db->where('id', $id_lider); 
			$sql = $this->db->get('lidereslocales');
			$data =$sql->row();
		}

		
		return $data;
	}

	public function get_Cargos()
	{
		$data = array();
		$sql = $this->db->get('cargos');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombre'];
            }
		}
		return $data;
		
	}

	public function get_Lider($id_lider,$id_usuario)
	{
		
		$data = array();	
		
		$this->db->where('l.id_usuario', 2); 
		$this->db->where('l.id_miembro', $id_lider); 
		$this->db->select('concat(m.nombre, " ",m.apellido) as nombre_completo, l.*');
		$this->db->join('miembros m','m.id=l.id_miembro and l.id_usuario=m.usuario');
		$this->db->from('lidereslocales l');
		$sql = $this->db->get();
		$data = $sql->result_array();
		return $data;
	}

	public function get_Lider_Por_Cargo($id_usuario,$cargo)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('id_cargo', $cargo); 
		
		$sql = $this->db->get('lidereslocales');
		if($sql->row() > 0){
			return 1;
		}else{
			return 0;
		}
	}

	public function get_Lider_Editar($id_usuario,$id)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('id', $id); 
		
		$sql = $this->db->get('lidereslocales');
		$data = $sql->row();
		return $data;
	}


	public function save_Lider($id='',$lider,$cargo,$fechaNombramiento,$imagen='',$id_usuario,$estado,$biografia)
	{
		if($imagen == null){
			$data = array(
                  	'id_miembro' =>  $lider,
                  	'fecha_inicio' => $fechaNombramiento ,
   					'id_cargo' => $cargo,
   					'id_usuario' => $id_usuario,
   					'estado' => $estado,
   					'resenabibliografica' => $biografia

				);
		}
		if($imagen != null){
			$data = array(
                  	'id_miembro' =>  $lider,
   					'id_usuario' => $id_usuario,  
                  	'fecha_inicio' => $fechaNombramiento ,
   					'id_cargo' => $cargo, 					
   					'imagenLider' => $imagen,
   					'estado' => $estado,
   					'resenabibliografica' => $biografia
				);
		}
		
		if($id > 0){
			$this->db->where('id',$id);
			$this->db->where('id_usuario',$id_usuario);
			$this->db->update('lidereslocales',$data);			
		}

		if($id == null){
			$this->db->insert('lidereslocales',$data);
		}
	}

	public function save_Cambio_de_lider($id_lider,$descripcion='',$id_usuario_cambio,$id,$lider_reemplazo,$estado)
	{
		$data = array(
					'fecha_fin' => date('Y-m-d H:i:s'),
					'descripcion_cambio_lider' => $descripcion,
					'id_usuario_cambio' => $id_usuario_cambio,
					'id_miembro_reemplazo' => $lider_reemplazo,
					'estado' => $estado
				);

		$this->db->where('id_miembro',$id_lider);
		$this->db->where('id',$id);
		
		$this->db->update('lidereslocales',$data);		

	}

	public function eliminar_Imagen($id='',$titulo,$descripcion,$fecha,$imagen='',$id_usuario)
	{
		if($imagen == null){
			$data = array(
                  	'imagen' => $imagen
				);
		}
		if($imagen != null){
			$data = array(
   					'imagen' => $imagen
				);
		}
		
		if($id >0){
			$this->db->where('id',$id);
			$this->db->update('noticias',$data);
		}

		if($id == null){
			$this->db->insert('noticias',$data);
		}
	}


}