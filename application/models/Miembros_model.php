<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Miembros_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_Carreras($value='')
	{
		$data = array();
		$sql = $this->db->get('carrerasuniversitarias');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombreCarrera'];
            }
		}
		return $data;
		
	}

	public function get_Responsables($id_usuario)
	{
		$data = array();
		$this->db->where('usuario', $id_usuario); 
		$sql = $this->db->get('miembros');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombre'].' '.$rs['apellido'];
            }
		}
		return $data;
		
	}

	public function get_Estados($value='')
	{
		$data = array();
		$sql = $this->db->get('estados');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombreEstado'];
            }
		}
		return $data;
		
	}

	
	public function get_Actividades($id_usuario)
	{
		$data = array();	
		$this->db->where('a.id_usuario', $id_usuario); 
		$this->db->select('a.*,ar.nombreActividadRegular');
		$this->db->from('actividades a');		
		$this->db->join('actividadesregulares ar','ar.id=a.nombreActividad');
		$sql = $this->db->get();
		$data =$sql->result_array();
		return $data;
		
	}

	public function get_Eventos($id_usuario)
	{
		$data = array();	
		$this->db->where('id_usuario', $id_usuario);

		$this->db->select('e.id,e.nombre,e.fecha,e.descripcion,e.responsable,concat(m.nombre, " ",m.apellido) as nombre_completo');
		$this->db->from('eventos e');
		$this->db->join('miembros m', 'm.id = e.responsable','left');
		$sql = $this->db->get();

		$data =$sql->result_array();
		return $data;
		
	}

	public function get_MiembrosCount($id_usuario)
	{
		$data = array();
		$this->db->select('count(*) as cantidad');	
		$this->db->where('usuario', $id_usuario); 
		$sql = $this->db->get('miembros');
		$data =$sql->result_array();
		return $data;
	}

	public function get_MiembrosCountMinisterio($id_ministerio='')
	{
		$data = array();
		if($id_ministerio != null){
			$this->db->where('m.id',$id_ministerio);
		}	
		$this->db->select('m.nombre as nombreMinisterio ,(select count(*) from miembros where ministerio = m.id) cantidad');	
		$sql = $this->db->get('ministerios m');
		$data =$sql->result_array();
		return $data;
	}

	public function get_MiembrosCountMinisterio2($id_ministerio='')
	{
		$data = array();
		if($id_ministerio != null){
			$this->db->where('m.id',$id_ministerio);
		}	
		$this->db->select('m.nombre as nombreMinisterio ,(select count(*) from miembros where ministerio = m.id) cantidad');	
		$sql = $this->db->get('ministerios m');
		//$data =$sql->result_array();
		$data =$sql->result_object();
		return $data;
	}

	public function get_MiembrosCountHombre($id_usuario)
	{
		$data = array();
		$this->db->select('count(*) as cantidadHombre');	
		$this->db->where('usuario', $id_usuario); 
		$this->db->where('sexo', 'M'); 
		$sql = $this->db->get('miembros');
		$data =$sql->result_array();
		return $data;
	}

	public function get_nombreImagenContenida($id)
	{
		$data = array();
		$this->db->select('imagen');	
		$this->db->where('id', $id); 
		$sql = $this->db->get('miembros');
		$data =$sql->row();
		return $data;
	}



	public function get_MiembrosCountMujer($id_usuario)
	{
		$data = array();
		$this->db->select('count(*) as cantidadMujer');	
		$this->db->where('usuario', $id_usuario);
		$this->db->where('sexo', 'F');  
		$sql = $this->db->get('miembros');
		$data =$sql->result_array();
		return $data;
	}

	public function get_Miembros($id_usuario,$sexo='')
	{
		$data = array();	
		$this->db->where('usuario', $id_usuario); 
		if($sexo != null){
			$this->db->where('sexo',$sexo);
		}
		

		$sql = $this->db->get('miembros');
		$data =$sql->result_array();
		return $data;
	}


	public function get_Miembro_Por_Nombre($id_usuario,$nombre)
	{

		$data = array();
		$nombre2 = $nombre;	
		$this->db->where('usuario', $id_usuario); 
		$where = "nombre like '".$nombre."%'";
		$this->db->where($where);
		//$this->db->like('nombre', $nombre,'after'); 

		$sql = $this->db->get('miembros');
		$data =$sql->result_array();
		return $data;
	}

	public function get_MiembrosActivos($id_usuario)
	{
		$data = array();	
		$this->db->where('usuario', $id_usuario); 
		$this->db->where('estado', 1); 
		$sql = $this->db->get('miembros');
		$data =$sql->result_array();
		return $data;
	}

	public function get_MiembrosInactivos($id_usuario)
	{
		$data = array();	
		$this->db->where('usuario', $id_usuario); 
		$this->db->where('estado', 2); 
		$sql = $this->db->get('miembros');
		$data =$sql->result_array();
		return $data;
	}

	public function get_MiembrosDescarriados($id_usuario)
	{
		$data = array();	
		$this->db->where('usuario', $id_usuario); 
		$this->db->where('estado', 5); 
		$sql = $this->db->get('miembros');
		$data =$sql->result_array();
		return $data;
	}

	public function get_MiembrosTrasladados($id_usuario)
	{
		$data = array();	
		$this->db->where('usuario', $id_usuario); 
		$this->db->where('estado', 3); 
		$sql = $this->db->get('miembros');
		$data =$sql->result_array();
		return $data;
	}

	public function get_MiembrosExterior($id_usuario)
	{
		$data = array();	
		$this->db->where('usuario', $id_usuario); 
		$this->db->where('estado', 4); 
		$sql = $this->db->get('miembros');
		$data =$sql->result_array();
		return $data;
	}

	public function get_MiembrosFallecidos($id_usuario)
	{
		$data = array();	
		$this->db->where('usuario', $id_usuario); 
		$this->db->where('estado', 6); 
		$sql = $this->db->get('miembros');
		$data =$sql->result_array();
		return $data;
	}

	public function get_Miembro($id_usuario,$id='')
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('m.usuario', $id_usuario);
		if($id != null){
			$this->db->where('m.id', $id); 
		} 
		

		
		$this->db->select('m.id,m.nombre,m.apellido,m.email,m.telefono,es.nombreEstado,mm.nombre as nombreMinisterio,ec.nombreCivil,ed.nombreEstadoEducativo,ne.nombreNivelEducativo,el.nombreEstadoLaboral,m.imagen,m.sexo,m.fechaNacimiento,m.fechaConversion,c.nombreCarrera,m.celula,m.Calle,m.numeroCasa,m.localidad,m.municipio,m.codigoPostal');
		$this->db->from('miembros m');
		$this->db->join('estadocivil ec', 'm.estadoCivil = ec.id','left');
		$this->db->join('ministerios mm', 'm.ministerio = mm.id','left');
		$this->db->join('estadoeducativo ed', 'm.estadoEducativo = ed.id','left');
		$this->db->join('niveleducativo ne', 'm.nivelEducativo = ne.id','left');
		$this->db->join('estadolaboral el', 'm.estadoLaboral = el.id','left');
		$this->db->join('carrerasuniversitarias c', 'm.carreraProfesional = c.id','left');
		$this->db->join('estados es', 'm.estado = es.id','left');

		$sql = $this->db->get();
		if($id != null){
			$data =$sql->row();
		}else{
			$data =$sql->result_array();
		}
		return $data;
	}

	public function get_Actividad($id_usuario,$id)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('a.id_usuario', $id_usuario); 
		$this->db->where('a.id', $id); 

		
		$this->db->select('a.*,ar.nombreActividadRegular');
		$this->db->from('actividades a');
		$this->db->join('actividadesRegulares ar','ar.id=a.nombreActividad');
		$sql = $this->db->get();
		$data =$sql->row();
		return $data;
	}

	public function get_Evento($id_usuario,$id)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('e.id', $id); 

		
		$this->db->select('e.id,e.nombre,e.fecha,e.descripcion,e.responsable,concat(m.nombre, " ",m.apellido) as nombre_completo');
		$this->db->from('eventos e');
		$this->db->join('miembros m', 'm.id = e.responsable','left');
		$sql = $this->db->get();
		$data =$sql->row();
		return $data;
	}



	public function get_Miembro_Editar($id_usuario,$id)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('usuario', $id_usuario); 
		$this->db->where('id', $id); 
		
		$this->db->select('*');
		$this->db->from('miembros');

		$sql = $this->db->get();
		$data =$sql->row();
		return $data;
	}

	public function get_Estadistica_Evento($id_usuario,$id)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('idEvento', $id); 

		
		$this->db->select('count(*) as cantidadTotal');
		$this->db->from('asistencia');
		$sql = $this->db->get();
		$data =$sql->row();
		return $data;
	}

	public function get_miembros_Por_Evento($id_usuario,$id_Evento)
	{
		$data = array();	
		$this->db->where('id_usuario', $id_usuario);
		$this->db->where('a.idEvento',$id_Evento);

		$this->db->select('a.estado_asistencia,concat(m.nombre, " ",m.apellido) as nombre_completo');
		$this->db->from('asistencia a');
		$this->db->join('miembros m', 'm.id = a.id_miembro');
		$sql = $this->db->get();

		$data =$sql->result_array();
		return $data;
	}

	public function get_Estadistica_Evento_Presentes($id_usuario,$id)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('idEvento', $id); 
		$this->db->where('estado_asistencia', 'presente');

		
		$this->db->select('count(*) as cantidadTotal');
		$this->db->from('asistencia');
		$sql = $this->db->get();
		$data =$sql->row();
		return $data;
	}

	public function existeAsistencia($id)
	{
		$data = array();
		$this->db->where('id_miembro',$id);
		$sql = $this->db->get('asistencia');
		if($sql->num_rows() > 0){            
            return 1;
		}
		return 0;
		
	}

	public function get_PorcentajeAsistencia($id_usuario,$id)
	{
		$data = array();	
		
		$this->db->where('a.id_usuario', $id_usuario); 
		$this->db->where('a.id_miembro', $id); 

		
		$this->db->select('concat(format(((select count(*) from asistencia a	join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario where estado_asistencia="presente" and id_miembro='.$id.' and ac.fecha >= (select fechaConversion from miembros where id='.$id.'))/(select count(*) from asistencia a join actividades ac on a.idEvento = ac.id and a.id_usuario = ac.id_usuario where id_miembro='.$id.' and ac.fecha >= (select fechaConversion from miembros where id='.$id.')))*100,2),"%") as porcentaje');
		$this->db->from('asistencia a');
		$this->db->group_by('a.id_miembro');
		$sql = $this->db->get();
		$data = $sql->row();
		return $data->porcentaje;		
		
	}

	public function save_Miembro($id='',$nombre,$apellido,$email='',$telefono='',$nombre_archivo='',$calle,$casa,$urbanizacion,$Municipio,$codigoPostal,$estado,$ministerio, $estadoCivil,$estadoEducativo,$nivelEducativo,$estadoLaboral,$sexo,$fechaNacimiento,$fechaConversion,$carrera,$id_usuario,$celula)
	{
		$data = array(
                  	'nombre' =>  $nombre,
                  	'apellido' => $apellido ,
   					'email' => $email,
   					'telefono' => $telefono,
   					'imagen' => $nombre_archivo,
   					'Calle' => $calle,
   					'numeroCasa' => $casa,
   					'localidad' => $urbanizacion,
   					'municipio' => $Municipio,
   					'codigoPostal' => $codigoPostal,
   					'estado' => $estado,
   					'usuario' => $id_usuario,
   					'estadoCivil' => $estadoCivil ,
   					'estadoEducativo' => $estadoEducativo,
   					'nivelEducativo' => $nivelEducativo ,
   					'estadoLaboral' => $estadoLaboral ,
   					'sexo' => $sexo ,
   					'fechaNacimiento' => $fechaNacimiento ,
   					'fechaConversion' => $fechaConversion ,
   					'carreraProfesional' => $carrera,
   					'celula' => $celula,
   					'ministerio' => $ministerio   
				);

		if($id >0){
			$this->db->where('id',$id);
			$this->db->update('miembros',$data);
		}

		if($id == null){
			$this->db->insert('miembros',$data);
		}
	}

	public function eliminar_Imagen($id,$nombreImagen)
	{
		$data = array(					
                  	'imagen' =>  $nombreImagen
				);

		$this->db->where('id',$id);
		$this->db->update('miembros',$data);
		

	}

	public function save_Descripcion_Cambio($idMiembro,$descripcion_cambio,$id_usuario,$antiguoEstado,$actualEstado)
	{
		$data = array(
                  	'id_miembro' =>  $idMiembro,
                  	'descripcion_cambio' => $descripcion_cambio,
                  	'id_usuario' =>  $id_usuario,
                  	'antiguoEstado' => $antiguoEstado,
                  	'actualEstado' => $actualEstado
				);

		$this->db->insert('cambioestado',$data);
		
	}


	public function save_Asistencia($id='',$idMiembro,$id_usuario,$valor,$idEvento,$fecha,$nombre,$excusaPastoral='')
	{
		$data = array(
                  	'id_miembro' =>  $idMiembro,
                  	'id_usuario' =>  $id_usuario,
                  	'estado_asistencia' => $valor ,
                  	'idEvento' => $idEvento ,
   					'fecha_asistencia' => $fecha,
   					'evento_asistencia' => $nombre
				);

		if($id >0){
			$this->db->where('id',$id);
			$this->db->update('asistencia',$data);
		}

		if($id == null){
			$this->db->insert('asistencia',$data);
		}
	}

	public function save_Actividad($id='',$id_usuario,$nombre,$fecha)
	{
		$data = array(
					'id_usuario'      => $id_usuario,
                  	'nombreActividad' =>  $nombre,
   					'fecha'=> $fecha
				);

		if($id >0){
			$this->db->where('id',$id);
			$this->db->update('miembros',$data);
		}

		if($id == null){
			$this->db->insert('actividades',$data);
		}
		return $this->db->insert_id();
	}

	public function save_Evento($id='',$nombre_evento,$fecha,$imagen='',$descripcion,$nombre_responsable,$id_usuario)
	{
		$data = array(
					'nombre'      => $nombre_evento,
					'fecha'      => $fecha,
					'imagen'	=> $imagen,
					'descripcion' => $descripcion,
                  	'responsable' =>  $nombre_responsable,
   					'id_usuario'=> $id_usuario
				);

		if($id >0){
			$this->db->where('id',$id);
			$this->db->update('eventos',$data);
		}

		if($id == null){
			$this->db->insert('eventos',$data);
		}
	}

	public function update_Miembro_Celula($id_usuario,$id_miembro)
	{
		$data = array(
					'celula'      => '0'
				);	
		
		$this->db->where('usuario', $id_usuario); 
		$this->db->where('id', $id_miembro); 

		$this->db->update('miembros',$data);

	}

	public function update_Miembro_Celula2($id_usuario,$id_miembro,$id_celula)
	{
		$data = array(
					'celula'      => $id_celula
				);	
		

		$this->db->where('usuario', $id_usuario); 
		$this->db->where('id', $id_miembro); 

		$this->db->update('miembros',$data);

	}
}