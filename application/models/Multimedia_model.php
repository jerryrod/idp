<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Multimedia_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_Carpetas_Audio($value='')
	{
		$data = array();
		$sql = $this->db->get('carpetaaudio');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombre'];
            }
		}
		return $data;
		
	}

	public function get_Carpetas_Foto($value='')
	{
		$data = array();
		$sql = $this->db->get('carpetafoto');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombre'];
            }
		}
		return $data;
		
	}

	public function get_Audios_por_fecha($id_usuario='')
	{
		$data = array();
		$this->db->group_by('fecha_audio');
		$sql = $this->db->get('audios');
		
		/*if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombre'];
            }
		}*/

		$data = $sql->result_array();
		return $data;
	}

	public function get_Audios($id_usuario,$idCarpeta='',$idAudio='')
	{
		$data = array();
		if($idCarpeta != null){
			$this->db->where('nombreCarpeta', $idCarpeta); 
		}
		if($idAudio != null){
			$this->db->where('a.id', $idAudio); 
		}

		$this->db->where('a.id_usuario', $id_usuario); 
		$this->db->select('a.*,c.nombre as nombreCarpeta');
		$this->db->from('audios a');	
		$this->db->join('carpetaaudio c','a.nombreCarpeta=c.id');
		$sql = $this->db->get();
		
		$data =$sql->result_array();
		return $data;
	}

	

	public function get_Fotos($id_usuario,$idCarpeta='',$idFoto='')
	{
		$data = array();
		if($idCarpeta != null){
			$this->db->where('nombreCarpetaFoto', $idCarpeta); 
		}
		if($idFoto != null){
			$this->db->where('f.id', $idFoto); 
		}

		$this->db->where('f.id_usuario', $id_usuario); 
		$this->db->select('f.*,c.nombre as idCarpeta,c.nombreCarpeta as nombreCarperta');
		$this->db->from('fotos f');	
		$this->db->join('carpetafoto c','f.nombreCarpetaFoto=c.id');
		$sql = $this->db->get();
		
		$data =$sql->result_array();
		return $data;
	}

	

	public function get_Album_Fotos($id_usuario)
	{
		$data = array();	

		$this->db->where('id_usuario', $id_usuario); 
		$sql = $this->db->get('carpetafoto');
		
		$data =$sql->result_array();
		return $data;
	}

	public function get_Audio_Editar($id_usuario,$id)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('id', $id); 
		
		$this->db->select('*');
		$this->db->from('audios');

		$sql = $this->db->get();
		$data =$sql->row();
		return $data;
	}

	public function get_nombre_archivo_audio($id, $id_usuario)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('id', $id); 
		
		$this->db->select('tmp_name');
		$this->db->from('audios');

		$sql = $this->db->get();
		$data =$sql->row();
		return $data;
	}

	

	public function get_Grupos($id_usuario,$idSerie='')
	{
		$data = array();
		
		if($idSerie > 0){					
			$this->db->where('id',$idSerie );
			$this->db->where('id_usuario', $id_usuario); 
			$this->db->select('*');
			$this->db->from('carpetaaudio');	
			$sql = $this->db->get();						
		}

		if($idSerie == null){
			$this->db->where('id_usuario', $id_usuario); 
			$this->db->select('*');
			$this->db->from('carpetaaudio');	
			$sql = $this->db->get();
		}
		
		$data =$sql->result_array();
		return $data;
	}

	public function get_AlbumsFotos($id_usuario)
	{
		$data = array();

		$this->db->where('id_usuario', $id_usuario); 
		$this->db->select('*');
		$this->db->from('carpetafoto');	
		$sql = $this->db->get();
		
		$data =$sql->result_array();
		return $data;
	}
	

	public function get_audio_por_nombre($id_usuario,$nombre_audio)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('tmp_name', $nombre_audio); 
		
		$sql = $this->db->get('audios');
		
		//return $this->db->count_all_results();	
		return $sql->num_rows();		
		
	}

	public function get_foto_por_nombre($id_usuario,$nombre_foto)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('nombreFoto', $nombre_foto); 
		
		$sql = $this->db->get('fotos');
		
		//return $this->db->count_all_results();	
		return $sql->num_rows();		
		
	}

	

	public function add_Carpeta_Audio($id='',$id_usuario,$nombreCarperta,$imagen='')
	{
		$data = array(
				'nombre' => $nombreCarperta,					
				'id_usuario' => $id_usuario,
				'imagen' => $imagen
		);
		
		if($id > 0){	
				
			$this->db->where('id',$id);
			$this->db->update('carpetaaudio',$data);
						
		}
		if($id == null){
			$this->db->insert('carpetaaudio',$data);
		}
	}




	public function save_Audio($id='',$nombreAudio,$carpetaAudio,$fechaAudio,$id_usuario,$nombreArchivo)
	{
		$data = array(
					'nombreAudio' => $nombreAudio,
					'nombreCarpeta' => $carpetaAudio,
					'fecha_audio' => $fechaAudio,
					'id_usuario' => $id_usuario,
					'tmp_name' => $nombreArchivo
			);

		if($id > 0){	
				
			$this->db->where('id',$id);
			$this->db->update('audios',$data);
						
		}

		if($id == null){
			$this->db->insert('audios',$data);
		}

	}

	

	public function removeAudio($id_usuario,$id)
	{
		$this->db->where('id',$id);
		$this->db->where('id_usuario',$id_usuario);
		$this->db->delete('audios'); 
	}

	public function removeAlbumFoto($id_usuario,$id)
	{
		$this->db->where('id',$id);
		$this->db->where('id_usuario',$id_usuario);
		$this->db->delete('carpetafoto'); 
	}

	public function removeFotosDelAlbum($id_usuario,$idCarpeta)
	{
		$this->db->where('nombreCarpetaFoto',$idCarpeta);
		$this->db->where('id_usuario',$id_usuario);
		$this->db->delete('fotos'); 
	}
	

	public function add_Carpeta_Foto($nombre,$tipoActividad,$diaServicio,$id_usuario,$nombreCarpetaGenerado )
	{
		if($diaServicio == 0){
			$data = array(
					'nombre' => $nombre,
					'tipoActividad' => $tipoActividad,
					'id_usuario' => $id_usuario,
					'nombreCarpeta' => $nombreCarpetaGenerado 
			);
		}else{
			$data = array(
					'nombre' => $nombre,
					'tipoActividad' => $tipoActividad,
					'diaServicio' => $diaServicio,
					'id_usuario' => $id_usuario,
					'nombreCarpeta' => $nombreCarpetaGenerado 
			);
		}		

		$this->db->insert('carpetaFoto',$data);
	}

	public function save_Foto($id='',$carpetaFoto,$id_usuario,$nombreGUID)
	{
		$data = array(
					'nombreCarpetaFoto' => $carpetaFoto,
					'id_usuario' => $id_usuario,
					'nombreFotoGUID' => $nombreGUID
			);

		if($id > 0){
			$this->db->where('id',$id);
			$this->db->update('fotos',$data);
		}

		if($id == null){
			$this->db->insert('fotos',$data);
		}

	}

	public function get_nombre_folder_carpeta_foto($id_usuario,$id)
	{
		$data = array();

		$this->db->where('id_usuario', $id_usuario); 
		$this->db->where('id', $id); 
		$this->db->select('nombreCarpeta');
		$this->db->from('carpetafoto');	
		$sql = $this->db->get();
		
		$row = $sql->row();
		return $row->nombreCarpeta;
	}
}
