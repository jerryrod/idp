<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Noticias_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_Noticias($id_noticia='',$id_usuario='')
	{
		$data = array();	
		$this->db->where('id_usuario', 2); 
		if($id_noticia==null){
			$this->db->order_by('id', 'desc');
			$sql = $this->db->get('noticias');
			$data =$sql->result_array();
		}
		if($id_noticia != null){
			$this->db->where('id', $id_noticia); 
			$sql = $this->db->get('noticias');
			$data =$sql->row();
		}
		
		return $data;
	}



	public function save_Noticia($id='',$titulo,$descripcion,$fecha,$imagen='',$id_usuario,$columna )
	{
		if($imagen == null){
			$data = array(
                  	'titulo' =>  $titulo,
                  	'descripcion' => $descripcion ,
   					'fecha' => $fecha,
   					'id_usuario' => $id_usuario ,
   					'columna' => $columna 
				);
		}
		if($imagen != null){
			$data = array(
                  	'titulo' =>  $titulo,
                  	'descripcion' => $descripcion ,
   					'imagen' => $imagen,
   					'fecha' => $fecha,
   					'id_usuario' => $id_usuario, 
   					'columna' => $columna 
				);
		}
		
		if($id > 0){
			$this->db->where('id',$id);
			$this->db->where('id_usuario',$id_usuario);
			$this->db->update('noticias',$data);
		}

		if($id == null){
			$this->db->insert('noticias',$data);
		}
	}

	public function eliminar_Imagen($id='',$titulo,$descripcion,$fecha,$imagen='',$id_usuario)
	{
		if($imagen == null){
			$data = array(
                  	'imagen' => $imagen
				);
		}
		if($imagen != null){
			$data = array(
   					'imagen' => $imagen
				);
		}
		
		if($id >0){
			$this->db->where('id',$id);
			$this->db->update('noticias',$data);
		}

		if($id == null){
			$this->db->insert('noticias',$data);
		}
	}

	public function get_Noticia_Editar($id_usuario='',$id='')
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('usuario', $id_usuario); 
		$this->db->where('id', $id); 
		
		$this->db->select('*');
		$this->db->from('noticias');

		$sql = $this->db->get();
		$data =$sql->row();
		return $data;
	}


}