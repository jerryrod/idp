<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Usuarios_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}


	public function add_user($id='',$nombre,$apellido,$email,$usuario,$pass,$rol,$usuario_sesion,$id_Iglesia='')
	{
		
		if($usuario_sesion == 1){
			if($id > 0 && $pass == null){
				$data = array(
							"nombre" => $nombre,
							"apellido" => $apellido,
							"email" => $email,
							"perfil" => $rol,
							"username" => $usuario,
							"iglesia" => $id_Iglesia							
						);

				$this->db->where('id',$id);
				$this->db->update('users',$data);
			}

			if($id > 0 && $pass != null){
				$data = array(
							"nombre" => $nombre,
							"apellido" => $apellido,
							"email" => $email,
							"perfil" => $rol,
							"username" => $usuario,
							"iglesia" => $id_Iglesia,
							"password" => sha1($pass)
						);

				$this->db->where('id',$id);
				$this->db->update('users',$data);
			}

			if($id == null){
				$data = array(
							"nombre" => $nombre,
							"apellido" => $apellido,
							"email" => $email,
							"perfil" => $rol,
							"username" => $usuario,
							"iglesia" => $id_Iglesia,
							"password" => sha1($pass)
						);
				
				$this->db->insert('users',$data);
			}
			
		}

		
	}

	public function verificarPasswordModel($id, $pass)
	{
		$existe = 0;
		$this->db->where('id',$id);
		$this->db->where('password',sha1($pass));

		$sql = $this->db->get('users');

		if($sql->num_rows() > 0){ 
			$existe =1;
		}

		return $existe;		 
	}

	public function get_roles($value='')
	{
		$data = array();
		$sql = $this->db->get('perfilusuario');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['perfil'];
            }
		}
		return $data;
	}

	public function get_usuarios($value='')
	{
		$this->db->select('u.*,p.perfil,i.nombre nombreIglesia');
		$this->db->from('users u');
		$this->db->join('perfilusuario p','u.perfil = p.id');
		$this->db->join('iglesias i','i.id = u.iglesia','left');
		$sql = $this->db->get();
		$data = $sql->result_array();
		return $data;
	}

	public function get_Usuario($id)
	{
		//var_dump($id);
		$data = array();	
		
		$this->db->where('u.id', $id); 

		$this->db->select('u.*,p.perfil as rol');
		$this->db->from('users u');
		$this->db->join('perfilusuario p','u.perfil = p.id');
		$sql = $this->db->get();

		$data =$sql->row();
		return $data;
	}

	public function delete_user($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('users');
	}

	public function get_Provincias($value='')
	{
		$data = array();
		$sql = $this->db->get('provincia');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombre'];
            }
		}
		return $data;
		
	}

	public function get_paises($value='')
	{
		$data = array();
		$sql = $this->db->get('pais');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombre'];
            }
		}
		return $data;
		
	}

	public function get_Iglesias($value='')
	{
		$data = array();
		$sql = $this->db->get('iglesias');
		if($sql->num_rows() > 0){            
            foreach($sql->result_array() as $rs){
                $data[$rs['id']] = $rs['nombre'];
            }
		}
		return $data;
		
	}

	
	

}