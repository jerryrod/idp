<?php 
 if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 

	//var_dump($actividadesEditar->id);

	$id= '';
	$nombreActividadEditar ='';
	$fechaActividadEditar ='';
	$descripcionActividadEditar='';
	$horaActividadEditar = '';
	$dia= '';

	if(isset($actividadesEditar)){
		$id = $actividadesEditar->id;
		$nombreActividadEditar = $actividadesEditar->nombre;
		$fechaActividadEditar =$actividadesEditar->fecha;
		$horaActividadEditar =$actividadesEditar->hora;
		$descripcionActividadEditar = $actividadesEditar->Descripcion;
		$dia = $actividadesEditar->dia;
	}

 ?>
<div class="container">
	<div class="row">
		<div class="col-md-10 ">
			<div class="row">
				<div class="col-md-12">
					<h1>Mantenimiento de Actividades</h1>
					<center class="cargandoNoticias">
						<img id="cargandoNoticias" src="<?php echo base_url(); ?>/application/img/loading.gif">
					</center>
				</div>
				<div class="col-md-12 errorFormularioNoticias">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1><span>Error!!!!</span> Verifica que los campos esten introducidos de la manera correcta. </h1>
						</div>
					</div>
					
				</div>
				<div class="col-md-12 successFormularioNoticias">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1><span>Usuario!!!!</span> Insertado de manera correcta. </h1>
						</div>
					</div>
					
				</div>
				<div class="col-md-12 cargandoContenedor">
					<div class="row BoxGeneralNoticias">
						<div class="col-md-3 menuLateralNoticias">
							<ul class="nav nav-pills nav-stacked">
								<li><a id="opciA1" href="">Actividades Creadas</a></li>
								<li><a id="opciA2" href="">Agregar Actividad</a></li>
								<li><a id="opciA3" href="">Algo de Noticias</a></li>
							</ul>
						</div>
						<div class="col-md-9 contenidoNoticias">
							<div class="cajaDialogoEliminarActividad">
							</div>
							<div class="opciA1">
								<div class="row" style="margin-top:5%;">
										<div class="col-md-4">
											<strong>Nombre</strong>
										</div>
										<div class="col-md-2">
											<strong>Fecha</strong>
										</div>
										<div class="col-md-2">
											<strong>Hora</strong>
										</div>
									</div>
									<?php 
										//var_dump($actividades);
										foreach ($actividades as $key => $value) {	
											?>
											<div class="row">
												<div class="col-md-4">
													<p><?php echo $value['nombre']; ?></p>
												</div>
												<div class="col-md-2">
													<p><?php echo $value['fecha']; ?></p>
												</div>
												<div class="col-md-2">
													<p><?php echo $value['hora']; ?></p>
												</div>
												<div class="col-md-1">
													<a href="<?php echo base_url(); ?>index.php/actividades?ac=2&id=<?php echo $value['id']; ?>"><p style="font-size:25px" class="glyphicon glyphicon-edit"></p></a>
												</div>
												<div class="col-md-1">
													<a class="removeActividadIrregular" href="<?php echo base_url(); ?>index.php/actividades?id=<?php echo $value['id']; ?>"><p style="font-size:25px;color: #CA2727;" class="glyphicon glyphicon-remove-circle"></p></a>
												</div>
											</div>

											<?php

										}
									 ?>
							</div>
							<div class="opciA2">
								<div class="panel panel-primary">
							    <div class="panel-heading">Formulario de Actividades</div>
							    <div class="panel-body">
							   		<?php 
							   			//var_dump($miembro);
							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('actividades/agregarActividad',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('id',$id);
												 echo form_hidden($datahidden);
											?>
										</div>
										<!--/////////////////  Titulo /////////////////////// -->
										<?php 
										
							   			$nombre = array(
								              'name'        => 'nombreActividad',
								              'id'          => 'nombreActividad',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Actividad',
								              'required'    => 'required',
								              'value'		=> $nombreActividadEditar
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="nombreActividad"><abbr title="Campo Obligatorio">*</abbr> Nombre:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($nombre); ?>
								  				<p class="help-block">Nombre de la Actividad</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  FECHA Noticia /////////////////////// -->

										<?php 

							   			$fechaActividad = array(
								              'name'        => 'fechaActividad',
								              'id'          => 'fechaActividad',
								              'type'		=> 'date',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required',
								              'value'		=> $fechaActividadEditar
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="fechaActividad"><abbr title="Campo Obligatorio">*</abbr> Fecha:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($fechaActividad); ?>
								  				<p class="help-block">Fecha de la actividad</p>
								  			</div>
								  		</div>
								  		
								  		<!--/////////////////  HOra Actividad /////////////////////// -->

										<?php 

							   			$horaActividad = array(
								              'name'        => 'horaActividad',
								              'id'          => 'horaActividad',
								              'type'		=> 'time',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required',
								              'value'		=> $horaActividadEditar
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="horaActividad"><abbr title="Campo Obligatorio">*</abbr> Hora:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($horaActividad); ?>
								  				<p class="help-block">Hora de la actividad</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  Dia /////////////////////// -->

								  		
								  		<?php 
									  		if(isset($dia)){
												$valueDia = $dia;
											}else{
												$valueDia =1;
											}
								  			$atributosDias ='id="inputDias" class="string email required form-control"';
								  		 ?>
								  		

								  		<div class="form-group email required user_horizontal_email">
								  		<label class="email col-sm-3 control-label" for="user_horizontal_email"> Dia: </label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('dias',$dias, $valueDia,$atributosDias) ?>
								  				<p id="labeSeleccioneCarreras" class="help-block">Seleccione el dia</p>
								  			</div>
								  		</div>

								  		<!--//////////////Descripcion ///////////////////////-->

								  		<?php 

											$descripcion = array(
															 'name'        => 'descripcion_actividad',
												              'id'          => 'descripcion_actividad',
												              'class'		=> 'string email required form-control',
												              'required'    => 'required',
												              'maxlength'	=> '150',
												              'value'		=> $descripcionActividadEditar

												  			);

								  		 ?>

								  		 <div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="descripcion_actividad"><abbr title="Campo Obligatorio">*</abbr> Detalle de Actividad</label>
								  			<div class="col-sm-9">
								  				<?php echo form_textarea($descripcion); ?>
								  			</div>
								  		</div>
								  		
								  		
								  		<?php 
								  			$atributosSubmit ='class="btn btn-primary" id="submitAgregarActividad"';
								  			if(isset($_GET['ac']) && !isset($_GET['id'])){ echo form_submit('commit', 'Agregar Noticia',$atributosSubmit);} 
								  			if(isset($_GET['ac']) && isset($_GET['id'])){ echo form_submit('commit', 'Modificar Noticia',$atributosSubmit);} 
								  			/*echo form_submit('commit', 'Agregar Actividad',$atributosSubmit);*/
								  		?>
								</div>
  							</div>
							</div>
							<div class="opciA3">
								<p>3</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>