<?php if(!$this->session->userdata('is_logued_in')){
			redirect(base_url().'index.php/login');
		} 
		
?>
<html lang="es" ng-app="BienvenidoApp">
	<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
		
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title>IDP</title>
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/application/css/style.css">
	    
	    <!-- Bootstrap -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	    
	   <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">-->
	    <!-- Uikit-->
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>application/css/jquery-ui.css">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>application/css/Uikit/css/uikit.min.css">

	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	    <script src="<?php echo base_url(); ?>/application/js/angular.js"></script>
	    <script src="<?php echo base_url(); ?>/application/js/Angular-Animate.js"></script>
	    <script src="<?php echo base_url(); ?>/application/js/Funciones-angular.js"></script>

		
	    <link rel="stylesheet" href="<?php echo base_url(); ?>application/fancyBox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

	    <script type="text/javascript" src="<?php echo base_url(); ?>application/fancyBox/source/jquery.fancybox.pack.js?v=2.1.5" ></script>

	   	        
	    <script type="text/javascript" src="<?php echo base_url(); ?>application/js/JQWidgets/jqwidgets/jqxcore.js"></script>
	    <script type="text/javascript" src="<?php echo base_url(); ?>application/js/JQWidgets/jqwidgets/jqxdraw.js"></script>
	    <script type="text/javascript" src="<?php echo base_url(); ?>application/js/JQWidgets/jqwidgets/jqxchart.core.js"></script>
	    <script type="text/javascript" src="<?php echo base_url(); ?>application/js/JQWidgets/jqwidgets/jqxdata.js"></script>
			
	   
		
		<script type="text/javascript" src="<?php echo base_url(); ?>application/css/Uikit/js/uikit.min.js"></script>
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>application/js/validarCampos.js"></script>


		<script type="text/javascript">
			$(document).ready(function () {
            // prepare chart data as an array

            	$(".fancybox").fancybox();							
				$(".various").fancybox({
					maxWidth	: 1400,
					maxHeight	: 1800,
					fitToView	: false,
					width		: '70%',
					height		: '70%',
					autoSize	: false,
					closeClick	: false,
					openEffect	: 'elastic',
					closeEffect	: 'fade'
				});

				$(".formularioFamilia").fancybox({
					maxWidth	: 800,
					maxHeight	: 1800,
					fitToView	: false,
					width		: '100%',
					height		: '100%',
					autoSize	: true,
					closeClick	: false,
					openEffect	: 'elastic',
					scrolling	:'no',
					closeEffect	: 'fade'
				});

				$(".formularioUsuario").fancybox({
					maxWidth	: 800,
					maxHeight	: 600,
					fitToView	: false,
					width		: '100%',
					height		: '100%',
					autoSize	: true,
					closeClick	: false,
					openEffect	: 'elastic',
					scrolling	:'auto',
					closeEffect	: 'fade'
				});

				

            	grafica();
	            function grafica () {
	            	
	            	var source = {
				        datatype: "array",
				        datafields: [{
				            name: 'Ministerio'
				        }, {
				            name: 'Cantidad'
				        }],
				        <?php 

				        	echo "localdata:[";
				        	foreach ($cantidadMiembrosMinisterios as $key => $value) {
				        		echo "{Ministerio:'".$value['nombreMinisterio']."',Cantidad:".$value['cantidad']."},";
				        	}			        	
				        	echo "]";
				         ?>
				    };

				    var dataAdapter = new $.jqx.dataAdapter(source, {
				        async: false,
				        autoBind: true,
				        loadError: function (xhr, status, error) {
				            		alert('Error loading "' + source.url + '" : ' + error);
				    	}
					});
				    // prepare jqxChart settings
		            var settings = {
		                title: "",
		                description: "",
		                enableAnimations: true,
		                showLegend: false,
		                showBorderLine: true,
		                legendPosition: { left: 520, top: 140, width: 100, height: 100 },
		                padding: { left: 5, top: 5, right: 5, bottom: 5 },
		                titlePadding: { left: 0, top: 0, right: 0, bottom: 10 },
		                source: dataAdapter,
		                colorScheme: 'scheme02',
		                seriesGroups:
		                    [
		                        {
		                            type: 'donut',
		                            showLabels: true,
		                            series:
		                                [
		                                    {
		                                        dataField: 'Cantidad',
		                                        displayText: 'Ministerio',
		                                        labelRadius: 120,
		                                        initialAngle: 15,
		                                        radius: 170,
		                                        innerRadius: 70,
		                                        centerOffset: 0
		                                        //formatSettings: { sufix: '%', decimalPlaces: 1 }
		                                    }
		                                ]
		                        }
		                    ]
		            };

	           	 // setup the chart
	            $('#graficaPorMinisterios').jqxChart(settings);
	         }   

	        $(document.body).on('click','#submitAgregarMiembro', function(e) {
				e.preventDefault();
				console.log('aqui estoy yo');
				$('.PanelMiembros').css('opacity',0.5);
				$('.cargando').fadeIn('fast');

				var id =$("input[name=idMiembro]").val();
				var nombre  = $("input#user_horizontal_name").val();
				var apellido  = $("input#user_horizontal_lastname").val();
				var email  = $("input#user_horizontal_email").val();
				var telefono  = $("input#user_horizontal_telefono").val();
				var inputFileImage = document.getElementById('user_horizontal_imagen_miembro');
				//var file = inputFileImage.files[0];
				var calle  = $("input#user_horizontal_calle").val();
				var casa  = $("input#user_horizontal_casa").val();
				var urbanizacion  = $("input#user_horizontal_urbanizacion").val();
				var Municipio  = $("input#user_horizontal_Municipio").val();
				var codigoPostal  = $("input#user_horizontal_codigop").val();
				var estado =  $("select[name=estados] option:selected").val();
				var ministerio =  $("select[name=ministerios] option:selected").val();
				var estadoCivil  = $("input[name=user_horizontal_estadoCivil]:checked").val();
				var estadoEducativo  = $("input[name=user_horizontal_estadoEducativo]:checked").val();
				var nivelEducativo = $("input[name=user_horizontal_nivelEducativo]:checked").val();
				var estadoLaboral = $("input[name=user_horizontal_estadoLaboral]:checked").val();
				var sexo = $("input[name=user_horizontal_sexo]:checked").val();
				var celula = $("select[name=celula] option:selected").val();
				var fechaNacimiento = $("input#user_horizontal_fecha").val();
				var fechaConversion = $("input#user_horizontal_fechaConversion").val();
				var carrera = $("select[name=carreras] option:selected").val();
				var descripcionCambioEstado = $('#user_horizontal_descripcion_cambio').val();
				var valueAntiguo =  $('#inputEstados').val();

				var data = new FormData();

				data.append('id',id);
				data.append('nombre',nombre);
				data.append('apellido',apellido); 
				data.append('email',email);
				data.append('telefono',telefono);  
				//data.append('archivo',file);
				data.append('calle',calle);  
				data.append('casa',casa);  
				data.append('urbanizacion',urbanizacion);  
				data.append('Municipio',Municipio); 
				data.append('codigoPostal',codigoPostal);  
				data.append('estado',estado); 
				data.append('ministerio',ministerio); 
				data.append('estadoCivil',estadoCivil); 
				data.append('estadoEducativo',estadoEducativo);
				data.append('nivelEducativo',nivelEducativo);
				data.append('estadoLaboral',estadoLaboral); 
				data.append('sexo',sexo);
				data.append('celula',celula);
				data.append('fechaNacimiento',fechaNacimiento);
				data.append('fechaConversion',fechaConversion);
				data.append('carrera',carrera);
				data.append('descripcionCambioEstado',descripcionCambioEstado);
				data.append('valueAntiguo',valueAntiguo);	


				$('#inputCarreras').fadeOut('fast');

				//console.log(nombre);
				var url2 = "<?php echo site_url('miembros/agregarMiembro'); ?>";
				//console.log(url2);

				$.ajax({
			        type: "POST",
			        url: "<?php echo site_url('miembros/agregarMiembro'); ?>",
			        data: data, // <--- THIS IS THE CHANGE
			        contentType:false,
			        processData:false,
			        cache:false,
			        dataType: "json",
			        success: function(data){
			          validado = JSON.parse(data);
			          //validado = 0;
			           console.log('Exito');
			           console.log(validado);


			           $('.cargando').fadeOut('fast');
			           $('.cargandoContenedor2').css('opacity',1);

			           	if(validado['validado']==1){
			           		$("input#user_horizontal_name").val('');
							$("input#user_horizontal_lastname").val('');
							$("input#user_horizontal_email").val('');
							$("input#user_horizontal_telefono").val('');
							$("input[name=user_horizontal_estadoCivil]:checked").prop( "checked", false );
							$("input[name=user_horizontal_estadoEducativo]:checked").prop( "checked", false );
							$("input[name=user_horizontal_nivelEducativo]:checked").prop( "checked", false );
							$("input[name=user_horizontal_estadoLaboral]:checked").prop( "checked", false );
							$("input[name=user_horizontal_sexo]:checked").prop( "checked", false );
							$("input#user_horizontal_fecha").val('');
							$("input#user_horizontal_fechaConversion").val('');


							$('.successFormularioMiembros').fadeIn('fast',function(){
								setTimeout(function(){ 
									$('.successFormularioMiembros').fadeOut('slow');
									$('#welco-miembros2').click();
								}, 500);
							});
							$('.errorFormularioMiembros').fadeOut('fast');

			          	}
			           	if(validado['validado']==2){
			           		$('.errorFormularioMiembros').fadeIn('slow');
			           	}
						$('body').scrollTop(0);
			        },
			        error: function(e) { 
			        	console.log(e);
			        	console.log('fallo2'); 
			        	$('.cargando').fadeOut('fast');
			           	$('.cargandoContenedor').css('opacity',1);
			        }
				});

			});  

				



        });

		</script>	


	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	   		 	
	</head>

	<body ng-controller="MultimediaBackend">
		<div class="container-fluid">
			<div class="row">
			  	<div class="col-sm-2 col-md-2 sidebar-Full">
				    <div class="sidebar-nav">
					     <div class="navbar-default" role="navigation">
					        <div class="navbar-collapse collapse sidebar-navbar-collapse sidebar-Full2">
								<div class="row">
									<div class="col md-12 perfilFoto">
										<img src="<?php echo base_url(); ?>application/images/page-1_img02.jpg" alt="">
									</div>
								</div>
								
								<div class="row">
									 <ul style="width:100%" class="nav navbar-nav">
							           	<li class="activej"><a ng-click="show(1)" href="<?php //echo site_url('Bienvenido/inicio'); ?>"><span class="uk-icon-home"></span>Inicio</a></li>
							           	<?php if($this->session->userdata('iglesia') != 0){ ?>
							           	<li class="dropdown">
								          <a href="#" id="welco-drop-miembros"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="uk-icon-users"></span>Membresia <span class="caret"></span></a>
								          <ul class="dropdown-menu">
								            <li><a ng-click="show(2)" href="#">Miembros</a></li>
								            <li><a ng-click="show(3)" href="#">Familias</a></li>
								            <li><a ng-click="show(4)" href="#">Visitantes</a></li>
								          </ul>
								        </li>
							            <li><a ng-click="show(5)" href="#"><span class="uk-icon-university"></span>Ministerios</a></li>
							            <li><a ng-click="show(6)" href="#"><span class="uk-icon-sun-o"></span>Grupo de Crecimiento</a></li> 
							            <li><a ng-click="show(7)" href="#"><span class="uk-icon-dollar"></span>Finanzas</a></li>
							            <li><a ng-click="show(8)" href="#"><span class="uk-icon-line-chart"></span>Estadistica</a></li>
							            <li><a ng-click="show(9)" href="#"><span class="uk-icon-columns"></span>Noticias</a></li>
							            <li><a ng-click="show(10)" href="#"><span class="uk-icon-youtube-play"></span>Multimedia</a></li>
							            <li><a ng-click="show(11)" href="#"><span class="uk-icon-tasks"></span>Eventos</a></li>
							            <li><a ng-click="show(12)" href="#"><span class="uk-icon-file-text"></span>Reportes</a></li>
							            <?php 
							            	if($this->session->userdata('perfil') != 1){

							            	}else{ ?>
												<li><a ng-click="show(13)" href="#"><span class="uk-icon-users"></span>Usuarios</a></li>	
							            	<?php }

							             ?>
							             <?php } ?>								
							        </ul>
							    </div>
							   
							 </div>
					      </div>
				    </div>
			  </div>
			  <div class="col-sm-10">
			    <div class="row header_bienvenido">
			    	<div class="col-md-4 col-xs-2">
			    		<img src="<?php echo base_url(); ?>application/img/logo.png" alt="">
			    	</div>
			    	<div class="col-md-4 col-xs-4">
			    		<h1>Iglesia de Dios de la Profecia</h1>
			    	</div>
			    	<div class="col-md-4 col-xs-2">
			    		<a id="log" class="btn btn-primary pull-right" href="<?php echo base_url()?>index.php/login/logout_ci">Cerrar Seccion</a>
			    	</div>
			    </div>
			    
			    <div class="row body-backend" ng-show="estados.showInicio">
			    	<div class="col-md-12 header-title">
			    		<h1>Aqui va el nombre de la iglesia</h1>
			    	</div>
			    	<div class="col-md-11 firts-row-inicio" >
			    		<div class="row">
			    			<div class="col-md-4 box-perfil-picture">
			    				<center>
			    					<div class="row box-perfil-picture-child-1">
				    					<h2 class="col-md-12 col-lg-12">Bienvenido <span><?php echo $this->session->userdata('nombre') .' '. $this->session->userdata('apellido') ; ?></span></h2>
										<small class="col-md-12 col-lg-12 box-perfil-cargo"><?php echo $this->session->userdata('perfilNombre') ?></small>
				    				</div>
				    				<div class="row box-perfil-picture-child-2">
				    					<img src="<?php echo base_url()?>application/images/57259be4945d4.jpg" alt="">
				    				</div>
			    				</center>
			    				<div class="row btnEditPerfilBienvenido">
			    					<div class="col-md-5 col-md-offset-7">
			    						<a href="#"><span class="uk-icon-edit"></span>Editar Perfil</a>
			    					</div>			    					
			    				</div>	
			    			</div>
			    			<div class="col-md-8">
			    				<div class="row boxGraficaBienvenido">
			    					<div class="col-md-4 leyendaGrafica">
			    						<div class="row">
			    							<div class="col-md-12 tituloLeyendaGrafica">
			    								<p><span>Miembros</span><br>	    								
	    										Cantidad por ministerio</p>
			    							</div>	    									
	    								</div>
			    						<div class="row">			    							
			    							<div class="col-md-3">
		    									<ul class="cantidadMiembrosMinisteriosLista2">
			    									<li style="background-color:#aeff6a"></li>
				    								<li style="background-color:#ff3493"></li>
				    								<li style="background-color:#ffca48"></li>
				    								<li style="background-color:#24d0ff"></li>
				    								<li style="background-color:#95ace8"></li>
				    								<li style="background-color:#ff8e54"></li>
				    							</ul>
			    							</div>
			    							<div class="col-md-8">
			    								<ul class="cantidadMiembrosMinisteriosLista">				
								    				<?php 
								    					foreach ($cantidadMiembrosMinisterios as $key => $value) {
								    						echo "<li>".$value['nombreMinisterio']."</li>";
								    					}
								    				?>
					    						</ul>
			    							</div>
			    						</div>				    							    						
			    					</div>
			    					<div class="col-md-7 col-md-offset-1">
			    						<div id="graficaPorMinisterios" style="width:100%; height:400px;"></div>			    						
			    					</div>
			    				</div>	
			    			</div>
			    		</div>
			    		<div class="row segundaSeccionBienvenido">
			    			<div class="col-md-6 every-cumpleano">
			    				<div class="row header-Cumpleano">
			    					<div class="col-md-4">
			    						<span class="uk-icon-birthday-cake birthday-cake"></span>
			    					</div>
			    					<div class="col-md-7">
			    						<div class="row textoCumpleano">
			    							<span class="pull-right ">Cumpleaños de <?php
																	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");																	 
																	echo  $meses[date('n')-1];
							    								?>
			    							</span>
			    						</div>
			    						<div class="row">
			    							<span class="pull-right cantidadCumpleanos">5</span>
			    						</div>			    					
			    					</div>
			    				</div>
			    				<div class="row body-cumpleano">
			    					<div class="col-md-12 ">

			    						<div class="row" style="margin-bottom: 2%;">
			    							<hr class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-lg-10 col-lg-offset-1">
			    							<div class="col-md-1 col-md-offset-1 imagenPadreCumple">
			    								<img src="<?php echo base_url(); ?>application/images/page-1_img02.jpg" alt="">
			    							</div>
			    							<div class="col-md-5">
			    								<div class="row">
			    									<span class="nombreCumple">Edwin Encarnación</span>
			    								</div>
			    								<div class="row">
			    									<p>
			    										<span class="uk-icon-envelope-o" id="correoCumpleano"> Emanuel@hotmail.com</span>
			    										<span class="uk-icon-phone" id="telefonoCumpleano"> 8095648546</span>
			    									</p>
			    								</div>
			    							</div>
			    							<div class="col-md-4">
			    								<div class="row">
			    									<span class="fechaCumple">Fecha de nacimiento: 1988-02-12</span>
			    								</div>
			    							</div>
			    						</div>

			    					</div>
			    				</div>
			    			</div>
			    			<div class="col-md-6">
			    				<p>hola</p>
			    				<span></span>
			    			</div>
			    		</div>
			    	</div>
			    	</div>
			    	<!------Miembros------>
			    	<div class="PanelMiembros" ng-show="estados.showMiembros">
			    		<div class="col-md-12 header-title">
							<h1>Membresia</h1>
							<center class="cargando">
								<img id="cargando" src="<?php echo base_url(); ?>/application/img/loading.gif">
							</center>
							<a id="welco-inicio" ng-click="show(1)" href="#">Inicio</a><a id="welco-miembros2" href="#">	/	Miembros</a>
						</div>
						<div class="col-md-11 body-parent" ng-show="estadoModuloMembresia.bodyParent">
							<div class="row body-start">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-2">
											<button ng-click="addMiembroBienvenido()" class="btn btn-info"><span class="uk-icon-user-plus"></span>Agregar Miembro</button>
										</div>
									</div>
									<div class="row input-filter-miembros-bienvenido">
										<div class="col-lg-12 col-md-12">
										    <div class="input-group">
										      <input type="text" id="inputBuscarMiembro" class="form-control" placeholder="Search for...">
										      <span class="input-group-btn">
										        <button id="buscarMiembro" class="btn btn-default" type="button"><span class="uk-icon-search"></span></button>
										      </span>
										    </div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row body-box">
								 <div class="col-md-3 member-box-padre" ng-repeat="miembro in miembros">
									<div class="row">
										<div class="col-md-10 col-md-offset-1 member-box" >
											<div class="row">
												<a class="nombreMiembroBienvenido" href="<?php echo base_url(); ?>application/miembros/ver_MiembroBienbenido/{{miembro.id}}"><img src="<?php echo base_url(); ?>application/miembros/{{miembro.imagen}}" alt="" /></a>
											</div>
											<a class="nombreMiembroBienvenido" ng-click = "getSingleMiembro(miembro.id)" data-id={{miembro.id}} ><span id="nombreMiembroBienvenido">{{miembro.nombre + ' ' +  miembro.apellido}}</span></a>
											<br><adress class="direccionMiembroBienvenido"><span class="uk-icon-map-marker"></span> Calle  {{miembro.Calle}}</adress>
											<br><adress class="direccionMiembroBienvenido">Casa #{{miembro.numeroCasa + ' ' + miembro.localidad}}</adress>
											<br><adress class="direccionMiembroBienvenido">{{miembro.Calle}}</adress>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-10 col-md-offset-1 " ng-show="estadoModuloMembresia.formularioMembresia">
							<div class="row">
				    			<div class="panel panel-primary">
							    <div class="panel-heading">Formulario de Membresia</div>
							    <div class="panel-body">
							   		<?php 
							   			//var_dump($miembro);
							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('miembros/agregarMiembro',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('idMiembro',$id);
												 echo form_hidden($datahidden);
											?>
										</div>
										<!--/////////////////  NOMBRE /////////////////////// -->
										<?php 
										
							   			$nombre = array(
								              'name'        => 'user_horizontal_name',
								              'id'          => 'user_horizontal_name',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Nombre',
								              'required'    => 'required'
								              //'value'		=> $nombreEditar
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_name"><abbr title="Campo Obligatorio">*</abbr> Nombre(s)</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($nombre); ?>
								  				<p class="help-block">Primer y Segundo Nombre</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  APELLIDO  /////////////////////// -->
								  		<?php 

							   			$apellido = array(
								              'name'        => 'user_horizontal_lastname',
								              'id'          => 'user_horizontal_lastname',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Apellido',
								              'required'    => 'required'
								              //'value'		=> $apellidoEditar
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_lastname"><abbr title="Campo Obligatorio">*</abbr> Apellido(s)</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($apellido); ?>
								  				<p class="help-block">Primer y Segundo Apellido</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  EMAIL /////////////////////// -->

								  		<?php 

							   			$email = array(
								              'name'        => 'user_horizontal_email',
								              'id'          => 'user_horizontal_email',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Correo Electronico',
								              'type'		=> 'email'
								              //'value'		=> $emailEditar
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email col-sm-3 control-label" for="user_horizontal_email"> Correo Electronico</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($email); ?>
								  				<p class="help-block">Correo Electronico si lo posee</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  Telefono /////////////////////// -->

								  		<?php 

							   			$telefono = array(
								              'name'        => 'user_horizontal_telefono',
								              'id'          => 'user_horizontal_telefono',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Telefono',
								              'type'		=> 'text'
								              //'value'		=> $telefonoEditar
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email col-sm-3 control-label" for="user_horizontal_email"> Telefono</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($telefono); ?>
								  				<p class="help-block">Telefono si lo posee</p>
								  			</div>
								  		</div>


								  		<!--/////////////////  ESTADO Membresia  /////////////////////// -->
								  		<?php 

											/*if(isset($estadoMiembro)){
												//$valueEstado = $estadoMiembro;
											}else{
												$valueEstado =1;
											}*/
											$valueEstado =1;
								  			$atributosEstados ='id="inputEstados" class="string email required form-control"';

								  			//$estados = array('' => );

								  			 //json_decode({estadosMiembros});
								  		 ?>
								  		

								  		<div class="form-group email required user_horizontal_email">
								  		<label class="email col-sm-3 control-label" for="user_horizontal_email"> Estado: </label>
								  			<div class="col-sm-9">

								  				<?php echo form_dropdown('estados',$estados,  $valueEstado,$atributosEstados) ?>

								  				<p id="labeSeleccioneCarreras" class="help-block">Seleccione el Estado</p>
								  			</div>
								  		</div>

								  		<!--////////////////// Descripcion de Cambio de Estado  //////////////////////-->


								  		<?php 

							   			$DescripcionCambio = array(
								              'name'        => 'user_horizontal_descripcion_cambio',
								              'id'          => 'user_horizontal_descripcion_cambio',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required'
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email" id="descripcionCambio">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_descripcion_cambio"><abbr title="Campo Obligatorio">*</abbr> Descripción del Cambio:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_textarea($DescripcionCambio); ?>
								  				<p class="help-block">¿Por que el Cambio de Estatus?:</p>
								  			</div>
								  		</div>


								  		<!--/////////////////  ESTADO CIVIL  /////////////////////// -->


								  		<?php 

							   			$EstadoCivilSoltero = array(
								              'name'        => 'user_horizontal_estadoCivil',
								              'id'          => 'user_horizontal_choices_soltero',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '1'
								         );

							   			$EstadoCivilCasado = array(
								              'name'        => 'user_horizontal_estadoCivil',
								              'id'          => 'user_horizontal_choices_casado',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '2'
								         );

							   			$EstadoCivilDivorciado = array(
								              'name'        => 'user_horizontal_estadoCivil',
								              'id'          => 'user_horizontal_choices_divorciado',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '3'
								         );

							   			$EstadoCivilViudo = array(
								              'name'        => 'user_horizontal_estadoCivil',
								              'id'          => 'user_horizontal_choices_viudo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '4'
								         );

							   			$EstadoCivilLibre = array(
								              'name'        => 'user_horizontal_estadoCivil',
								              'id'          => 'user_horizontal_choices_libre',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '5'
								         );
							   			?> 
							   			<div class="form-group check_boxes optional user_horizontal_choices">
											<label class="check_boxes optional col-sm-3 control-label"><abbr title="Campo Obligatorio">*</abbr>Estado Civil</label>
											<div class="col-sm-9">

												<span class="radio">
													<label for="user_horizontal_choices_soltero">
													<?php echo form_radio($EstadoCivilSoltero); ?>Soltero(a)</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_casado">
													<?php echo form_radio($EstadoCivilCasado); ?>Casado(a)</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_divorciado">
													<?php echo form_radio($EstadoCivilDivorciado); ?>Divorciado(a)</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_viudo">
													<?php echo form_radio($EstadoCivilViudo); ?>Viudo(a)</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_libre">
													<?php echo form_radio($EstadoCivilLibre); ?>Union Libre</label>
												</span>
												<p class="help-block">Estado Civil</p>

											</div>
										</div>


										<!--/////////////////  ESTADO EDUCATIVO /////////////////////// -->

										<?php 

							   			$EstadoEducativoActivo = array(
								              'name'        => 'user_horizontal_estadoEducativo',
								              'id'          => 'user_horizontal_choices_activo_estadoEducativo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '1'
								         );

							   			$EstadoEducativoInactivo = array(
								              'name'        => 'user_horizontal_estadoEducativo',
								              'id'          => 'user_horizontal_choices_inactivo_estadoEducativo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '2'
								         );
							   			
							   			?> 

										<div class="form-group check_boxes optional user_horizontal_choices">
											<label class="check_boxes optional col-sm-3 control-label"><abbr title="Campo Obligatorio">*</abbr>Estado Educativo</label>
											<div class="col-sm-9">

												<span class="radio">
													<label for="user_horizontal_choices_activo_estadoEducativo">
													<?php echo form_radio($EstadoEducativoActivo); ?>Activo</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_inactivo_estadoEducativo">
													<?php echo form_radio($EstadoEducativoInactivo); ?>Inactivo</label>
												</span>
												<p class="help-block">Estado Educativo</p>

											</div>
										</div>

										<!--/////////////////  NIVEL EDUCATIVO  /////////////////////// -->

										<?php 

							   			$NivelEducativoBasico = array(
								              'name'        => 'user_horizontal_nivelEducativo',
								              'id'          => 'user_horizontal_choices_basico_nivelEducativo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '1',
								              'ng-click'	=>  'btnProfesion(1)'
								         );

							   			$NivelEducativoBachiller = array(
								              'name'        => 'user_horizontal_nivelEducativo',
								              'id'          => 'user_horizontal_choices_bachiller_nivelEducativo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '2',
								              'ng-click'	=>  'btnProfesion(2)'
								         );
							   			
							   			$NivelEducativoUniversitario = array(
								              'name'        => 'user_horizontal_nivelEducativo',
								              'id'          => 'user_horizontal_choices_universitario_nivelEducativo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '3',
								              'ng-click'	=>  'btnProfesion(3)'
								         );

							   			$NivelEducativoProfesional = array(
								              'name'        => 'user_horizontal_nivelEducativo',
								              'id'          => 'user_horizontal_choices_profesional_nivelEducativo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '4',
								              'ng-click'	=>  'btnProfesion(4)'
								         );

							   			$atributosCarreras ='id="inputCarreras" class="string email required form-control" ';


							   			?> 

										<div class="form-group check_boxes optional user_horizontal_choices">
											<label class="check_boxes optional col-sm-3 control-label"><abbr title="Campo Obligatorio">*</abbr>Nivel Educativo</label>
											<div class="col-sm-9 "  >

												<span class="radio">
													<label for="user_horizontal_choices_basico_nivelEducativo">
														<?php echo form_radio($NivelEducativoBasico); ?>Básico</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_bachiller_nivelEducativo">
														<?php echo form_radio($NivelEducativoBachiller); ?>Bachiller</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_universitario_nivelEducativo">
														<?php echo form_radio($NivelEducativoUniversitario); ?>Universitario</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_profesional_nivelEducativo">
														<?php echo form_radio($NivelEducativoProfesional); ?>Profesional</label>
												</span>

												<p class="help-block">Nivel Educativo</p>

												<div class="form-group email required user_horizontal_email" >
										  			<div class="col-sm-9" ng-show="estadoModuloMembresia.Profesional">
										  				<?php echo form_dropdown('carreras',$carreras, 1,$atributosCarreras) ?>
										  				<p id="labeSeleccioneCarreras" class="help-block">Seleccione la Carrera</p>
										  			</div>
								  				</div>

												
											</div>
										</div>

										<!--/////////////////  ESTADO LABORAL  /////////////////////// -->

										<?php 

							   			$EstadoLaboralActivo = array(
								              'name'        => 'user_horizontal_estadoLaboral',
								              'id'          => 'user_horizontal_choices_activo_estadoLaboral',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '1'
								         );

							   			$EstadoLaboralInactivo = array(
								              'name'        => 'user_horizontal_estadoLaboral',
								              'id'          => 'user_horizontal_choices_inactivo_estadoLaboral',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '2'
								         );
							   			
							   			
							   			?> 

										<div class="form-group check_boxes optional user_horizontal_choices">
											<label class="check_boxes optional col-sm-3 control-label"><abbr title="Campo Obligatorio">*</abbr>Estado Laboral</label>
											<div class="col-sm-9">

												<span class="radio">
													<label for="user_horizontal_choices_activo_estadoLaboral">
														<?php echo form_radio($EstadoLaboralActivo); ?>Activo</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_inactivo_estadoLaboral">
														<?php echo form_radio($EstadoLaboralInactivo); ?>Inactivo</label>
												</span>
												<p class="help-block">Estado Laboral</p>
											</div>
										</div>

										<!--/////////////////  SEXO  /////////////////////// -->

										<?php 

							   			$SexoMasculino = array(
								              'name'        => 'user_horizontal_sexo',
								              'id'          => 'user_horizontal_sex_male',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  'M'
								         );

							   			$SexoFemenino = array(
								              'name'        => 'user_horizontal_sexo',
								              'id'          => 'user_horizontal_sex_male',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  'F'
								         );
							   			
							   			
							   			?> 

										<div class="form-group radio_buttons optional user_horizontal_sex">
											<label class="radio_buttons optional col-sm-3 control-label"><abbr title="Campo Obligatorio">*</abbr>Sexo</label>
											<div class="col-sm-9">
												<span class="radio">
													<label for="user_horizontal_sex_male">
														<?php echo form_radio($SexoMasculino); ?>Masculino</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_sex_female">
														<?php echo form_radio($SexoFemenino); ?>Femenino</label>
												</span>

												<p class="help-block">Sexo</p>
											</div>
										</div>

										<!--/////////////////  Lider /////////////////////// -->
										<?php 
										
										if(isset($liderCelula)){
												$valueLider = $liderCelula;
											}else{
												$valueLider =1;
											}

							   			$lider = array(
								              'name'        => 'user_horizontal_celula',
								              'id'          => 'user_horizontal_celula',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required'
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_lider"><abbr title="Campo Obligatorio">*</abbr> Nombre del Lider:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('celula',$celulas, $valueLider,$lider) ?>								  				
								  				<p class="help-block">Lider de la Celula</p>
								  			</div>
								  		</div>

										<!--/////////////////  FECHA NACIMIENTO /////////////////////// -->

										<?php 

							   			$fechaNacimiento = array(
								              'name'        => 'user_horizontal_fecha',
								              'id'          => 'user_horizontal_fecha',
								              'type'		=> 'date',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required'
								              //'value'		=> $fechaNacimientoEditar
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_fecha"><abbr title="Campo Obligatorio">*</abbr> Fecha Nacimiento</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($fechaNacimiento); ?>
								  				<p class="help-block">Fecha Nacimiento</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  FECHA DE CONVERSION /////////////////////// -->

								  		<?php

								  		$fechaConversion = array(
								              'name'        => 'user_horizontal_fechaConversion',
								              'id'          => 'user_horizontal_fechaConversion',
								              'type'		=> 'date',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required'
								              //'value'		=> $fechaConversionEditar
								         );							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_fechaConversion"><abbr title="Campo Obligatorio">*</abbr> Fecha de Conversión</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($fechaConversion); ?>
								  				<p class="help-block">Fecha en la que acepto a jesús</p>
								  			</div>
								  		</div>

								  		<?php 
								  			$atributosSubmit ='class="btn btn-primary" id="submitAgregarMiembro"';
								  			echo form_submit('commit', 'Registrar Miembro',$atributosSubmit); 
								  			echo form_close();
								  		?>

								</div>
  							</div>		
							</div>
						</div>

			    	</div>
			    	
			    	<!------familias------>
			    	<div class="row body-backend" ng-show="estados.showFamilias">
			    		<span>familias</span>
			    	</div>
			    	
			    	<!------visitantes------>
			    	<div class="row body-backend" ng-show="estados.showVisitantes">
			    		<span>visitantes</span>
			    	</div>
			    	
			    	<!------Ministerios------>
			    	<div class="row body-backend" ng-show="estados.showMinisterios">
			    		<span>Ministerios</span>
			    	</div>
			    	
			    	<!------Grupo de Crecimiento------>
			    	<div class="row body-backend" ng-show="estados.showCrecimiento">
			    		<span>Crecimiento</span>
			    	</div>
			    	
			    	<!------Finanzas------>
			    	<div class="row body-backend" ng-show="estados.showFinanzas">
			    		<span>Finanzas</span>
			    	</div>
			    	
			    	<!------Estadisticas------>
			    	<div class="row body-backend" ng-show="estados.showEstadisticas">
			    		<span>Estadisticas</span>
			    	</div>
			    	
			    	<!------Noticias------>
			    	<div class="row body-backend" ng-show="estados.showNoticias">
			    		<span>Noticias</span>
			    	</div>
			    	
			    	<!------Multimedia------>
			    	<div class="row body-backend" ng-show="estados.showMultimedia">
			    		<div class="col-md-12 ">
			    			<div class="row header-title">
			    				<h1>multimedia</h1>
			    			</div>
			    			<div ng-show="estados2.menuAudio">
				    			<div class="row">
				    				<div class="col-md-3 boxCatAudioInsert" ng-click="catAudioInsert(1)">
				    					<div class="boxCatAudioInsertChild">
				    						<span>{{"Ver series y audios existentes" | uppercase}}</span>
				    					</div>
				    				</div>
				    				<div class="col-md-3 boxCatAudioInsert" ng-click="catAudioInsert(2)">
				    					<div class="boxCatAudioInsertChild">
				    						<span>{{"Agregar nueva serie" | uppercase}}</span>
			    						</div>
				    				</div>
				    				<div class="col-md-3 boxCatAudioInsert" ng-click="catAudioInsert(3)">
				    					<div class="boxCatAudioInsertChild">
				    						<span>{{"Agregar Audio a Serie" | uppercase}}</span>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="row">
				    				<div class="col-md-3 boxCatAudioInsert" ng-click="catAudioInsert(4)">
				    					<div class="boxCatAudioInsertChild">
				    						<span>{{"Agregar Audio sin serie" | uppercase}}</span>
				    					</div>
				    				</div>
				    				<div class="col-md-3 boxCatAudioInsert" ng-click="catAudioInsert(5)">
				    					<div class="boxCatAudioInsertChild">
				    						<span>{{"Agregar Audio Conferencia" | uppercase}}</span>
				    					</div>
				    				</div>
				    			</div>
				    		</div>
				    		<div class="row" ng-show="estados2.mantenimientoAudiosSeries">
				    			<div class="col-md-10 col-md-offset-1">
				    				<div class="row">
				    					<div class="col-md-9">
				    						<h1>Mantenimiento de Series</h1>
				    					</div>
				    					<div class="col-md-1">
			    							<span ng-click="AtrasCatAudio(1)" class="fechaAtras glyphicon glyphicon-arrow-left"></span>
			    						</div>
			    					</div>
				    				<div class="row">				    									    					
				    					<div class="col-md-12 ">
				    						<div class="row">
				    							<div class="col-md-3 cajaSeriesAudio" ng-repeat="serie in series.data.gruposAudio ">
												<div class="row row2">
													<center>
														<img src="<?php echo base_url(); ?>/application/images/images2.png"/>
													</center>
												</div>
												<div class="row row2" ng-mouseenter="hoverTituloSerie=true"  ng-mouseleave="hoverTituloSerie=false">
													<div class="tituloSerie" ng-hide="hoverTituloSerie">
														<span >{{serie.nombre | uppercase }}</span>										
													</div>
													<div class="buttonAbrir" ng-click="editSerie(serie.id)" ng-show="hoverTituloSerie">
														<span >{{"editar" | uppercase }}</span>
													</div>													
												</div>
											</div>
				    						</div>
				    					</div>
				    					
				    				</div>
				    				<div class="row">
				    					<h1>Mantenimiento de Audios</h1>
				    				</div>
				    			</div>
				    		</div>
			    			<div class="row" ng-show="estados2.formSeries">
			    				<div class="col-md-10 col-md-offset-1">
			    					<div class="row">
			    						<span ng-click="AtrasCatAudio(2)" ng-show="estadoFormSerieInsert" class="fechaAtras glyphicon glyphicon-arrow-left"></span>			    						
			    						<span ng-click="catAudioInsert(1)" ng-hide="estadoFormSerieInsert" class="fechaAtras glyphicon glyphicon-arrow-left"></span>
			    					</div>			    					
								<div class="row">
									<div class="alert alert-success" ng-show="estados2.successMessage">
									    <strong>Success!</strong> La carpeta fue insertada de manera correcta.
									</div>
									<div class="alert alert-danger" ng-show="estados2.dangerMessage">
									    <strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.
									</div>
								</div>
					
			    					<div class="row">
				    					<div class="panel panel-primary formSeries">
									    <div class="panel-heading">Formulario de Carpeta</div>
									    <div class="panel-body">
									    	
								   			
											<!--/////////////////  NOMBRE /////////////////////// -->
											<?php 
											
								   			$nombre = array(
											              'name'        => 'user_horizontal_name_carpeta',
											              'id'          => 'user_horizontal_name_carpeta',
											              'class'		=> 'string email required form-control',
											              //'placeholder' => 'Nombre Carpeta',
											              'required'    => 'required',
											              'ng-model'    =>  "nombreSerie"
											         );
								   			?> 
	
								   			<div class="form-group email required user_horizontal_email">
									  			<label class="email required col-sm-3 control-label" for="user_horizontal_name_carpeta"><abbr title="Campo Obligatorio">*</abbr> Nombre:</label>
									  			<div class="col-sm-9">
									  				<?php echo form_input($nombre); ?>
									  				<p class="help-block">Escriba el nombre de la carpeta que desea agregar</p>
									  			</div>
									  		</div>
									  		
									  			
									  		<?php 
									  			$atributosSubmit ='class="btn btn-primary" ng-show="estadoFormSerieInsert" ng-click="guardarSerie()"';
									  			$atributosSubmit2 ='class="btn btn-primary" ng-hide="estadoFormSerieInsert" ng-click="guardarSerie()"';
									  			
									  			$atributoBtnEliminar='class="btn btn-danger" ng-hide="estadoFormSerieInsert" ng-click="eliminarSerie()"';
									  			$atributoBtnCancelar='class="btn btn" ng-click="cancelarFormSerie()"';
									  			
									  			
									  			echo form_submit('', 'Agregar Carpeta',$atributosSubmit); 
									  			echo form_submit('', 'Modificar Carpeta',$atributosSubmit2); 
									  			
									  			echo form_button('','Eliminar Serie',$atributoBtnEliminar);
									  			echo form_button('','Cancelar',$atributoBtnCancelar);
									  			
									  		?>
										  	
									    </div>
									</div>
								</div>
							</div>								
			    			</div>
			    			<div class="row" ng-show="estados2.agregarAudio">
			    				<div class="col-md-10 col-md-offset-1">
			    					<div class="row atrasFormSerie">
			    						<span ng-click="AtrasCatAudio(3)" class="fechaAtras glyphicon glyphicon-arrow-left"></span>
			    					</div>	
			    					<div class="row">
			    						<div class="panel panel-primary">
									    <div class="panel-heading">Formulario de Audio</div>
									    <div class="panel-body">		


											<!--/////////////////  NOMBRE /////////////////////// -->
											<?php 
											
								   			$nombre = array(
											              'name'        => 'user_horizontal_name_audio',
											              'id'          => 'user_horizontal_name_audio',
											              'class'		=> 'string email required form-control',
											              'placeholder' => 'Nombre Audio',
											              'required'    => 'required',
											              'ng-model'  => 'audio.nombreAudio'
									         		);
								   			?> 

								   			<div class="form-group email required user_horizontal_email">
									  			<label class="email required col-sm-3 control-label" for="user_horizontal_name_audio"><abbr title="Campo Obligatorio">*</abbr> Nombre:</label>
									  			<div class="col-sm-9">
									  				<?php echo form_input($nombre); ?>
									  				<p class="help-block">Escriba el nombre del Audio</p>
									  			</div>
									  		</div>

											<!--/////////////////  Carpetas Creadas  /////////////////////// -->
									  		<?php
												if(isset($nombreCarpetaEditar)){
													$valueEstado = $nombreCarpetaEditar;
												}else{
													$valueEstado =1;
												}
									  			$atributosCarpetaAudio = 'id = "inputCarpetaAudio" class="string email required form-control" ';
									  		 ?>
									  		

									  		<div class="form-group email required user_horizontal_email">
									  			<label class="email col-sm-3 control-label" for="user_horizontal_email"> Carpeta: </label>
									  			<div class="col-sm-9">
									  				<select class="string email required form-control" name="repeatSelect" id="inputCarpetaAudio" ng-model = "audio.carpetaAudio">
													      <option ng-repeat="option in seriesCreadas" value="{{option.id}}">{{option.nombre}}</option>
													    </select>
									  				<p class="help-block">Seleccione la Carpeta</p>
									  			</div>
									  		</div>									  		
									  											  		
									  		<!--/////////////////  audio /////////////////////// -->

									  		<?php 

								   			$audio = array(
									              'name'        => 'user_horizontal_imagen_lider2',
									              'id'          => 'user_horizontal_imagen_lider2',
									              'class'		=> 'string email required form-control',
									              'type'    => 'file',
									              'accept'  => '.mp3',
									              'fileread'	=> 'audio.archivo'
									         );

								   			?>
								   			
								   			<div style="margin-top:2%;" id="audioInput" class="form-group email required user_horizontal_email">
									  			<label class="email col-sm-3 control-label" for="user_horizontal_imagen_lider2"> Audio:</label>
									  			<div class="col-sm-9">
									  				<?php echo form_input($audio); ?>
									  				<p class="help-block">Seleccione el Audio</p>
									  			</div>
									  		</div>

									  		<!--/////////////////  FECHA /////////////////////// -->

										<?php 

							   			$fechaAudio = array(
								              'name'        => 'fechaAudio',
								              'id'          => 'fechaAudio',
								              'type'		=> 'date',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required',
								              'ng-model'	=> 'audio.fechaAudio'
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="fechaAudio"><abbr title="Campo Obligatorio">*</abbr> Fecha:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($fechaAudio); ?>
								  			</div>
								  		</div>
								  	</div>

								  		<div class="row" style="margin-bottom:3%">

								  		<?php 
									  			$atributosSubmit ='class="btn btn-primary col-md-2  col-md-offset-3" id="agregarAudioBienvenido" ';
									  			echo form_submit('commit', 'Agregar Audio',$atributosSubmit);
									  		?>
									  		<input type="button" value="Cancelar" class="btn btn-danger col-md-offset-1 col-md-2 " id="cancelarAudio">
									  	</div>

									</div>
  								</div>
			    					
			    				</div>
			    			</div>
			    		</div>
			    		
			    	</div>
			    	
			    	<!------eventos------>
			    	<div class="row body-backend" ng-show="estados.showEventos">
			    		<span>eventos</span>
			    	</div>
			    	
			    	<!------reportes------>
			    	<div class="row body-backend" ng-show="estados.showreportes">
			    		<span>reportes</span>
			    	</div>
			    	
			    	<!------Usuarios------>
			    	<div class="row body-backend" ng-show="estados.showUsuarios">
			    		<span>Usuarios</span>
			    	</div>
			    			    
				<div class="alert alert-success col-md-10 col-md-offset-1 SuccessMensajeUsuarioEliminado">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<strong>Success!</strong> Usuario Eliminado.
				</div>
				    <div class="row body-backend2 cargandoContenedor2">
				    		<div class="col-md-12 successFormularioMiembros">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<h1><span>Usuario!!!!</span> Insertado de manera correcta. </h1>
								</div>
							</div>				
						</div>
				    </div>
			  </div>
			</div>
			</div>
			<script src="<?php echo base_url(); ?>application/js/funciones.js"></script>

			<?php

				if(isset($_GET['u'])){?>
						<script type="text/javascript">
							<?php 
							if($_GET['u']=='n'){ ?>
									$('#welco-usuarios').click();

									setTimeout(function () {
										$('.cargandoContenedor2').css('opacity','0.5');
										$('.SuccessMensajeUsuarioEliminado').fadeIn('slow');
									},500);
									setTimeout(function () {										
										$('.SuccessMensajeUsuarioEliminado').fadeOut('slow');
										$('.cargandoContenedor2').css('opacity','1');
									},3000);
							<?php } ?>

							
						</script>

					<?php
				}?>
	</body>
	<script type="text/javascript">
	
	$(document).ready(function() {
	/*-----------------Agregar Archivo de Audio --------------*/					
		$("#agregarAudioBienvenido").click(function(e) {
			e.preventDefault();
			
			//var data = {};


			var id = $("input[name=id]").val();
			var nombreAudio = $("#user_horizontal_name_audio").val();									
			var carpetaAudio = $("select[name=repeatSelect] option:selected").val();	
			var fechaAudio = $("#fechaAudio").val();							
			var inputFileImage = document.getElementById('user_horizontal_imagen_lider2');
			var file = inputFileImage.files[0];
			var data = new FormData();

			data.append('id',id);
			data.append('nombreAudio',nombreAudio);
			data.append('carpetaAudio',carpetaAudio);
			data.append('fechaAudio',fechaAudio);
			data.append('archivo',file);
			
			//console.log("Dentro estoy yo");
			
			

			$.ajax({
				url:"<?php echo site_url('multimediaBackend/agregarAudio'); ?>",
				type:'POST',
				data:data,
				contentType:false,
			        processData:false,
			        cache:false,
			        dataType: "json",

				success: function(data){									
					console.log(data);
		        },
		        error: function(e) { 
		        	console.log(e);
		        }
			});
			

		});
	});



	</script>
</html>