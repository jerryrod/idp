<!--	FORMULARIO DE BOOSTRAP

<form accept-charset="UTF-8" action="http://simple-form-bootstrap.plataformatec.com.br/examples/create_horizontal" class="simple_form form-horizontal" enctype="multipart/form-data" id="new_user_horizontal" method="post" novalidate="novalidate">
								      	<div style="display:none">
								      		<input name="utf8" type="hidden" value="✓">
								      		<input name="authenticity_token" type="hidden" value="3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs=">
								      	</div>

								      	<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_name"><abbr title="required">*</abbr> Nombre(s)</label>
								  			<div class="col-sm-9">
								  				<input class="string email required form-control" id="user_horizontal_name" name="user_horizontal[name]" placeholder="Nombre" type="text">
								  					<p class="help-block">Primer y Segundo Nombre</p>
								  			</div>
								  		</div>

								  		<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_lastname"><abbr title="required">*</abbr> Apellido(s)</label>
								  			<div class="col-sm-9">
								  				<input class="string email required form-control" id="user_horizontal_lastname" name="user_horizontal[lastname]" placeholder="Apellido" type="text">
								  					<p class="help-block">Primer y Segundo Apellido</p>
								  			</div>
								  		</div>

								  		<div class="form-group email required user_horizontal_email">
								  			<label class="email col-sm-3 control-label" for="user_horizontal_email"> Correo Electronico</label>
								  			<div class="col-sm-9">
								  				<input class="string email required form-control" id="user_horizontal_email" name="user_horizontal[email]" placeholder="Correo" type="email">
								  					<p class="help-block">Correo Electronico si lo posee</p>
								  			</div>
								  		</div>
								  		<!--Input Type File-->
								  		<!--<div class="form-group file optional user_horizontal_file">
								  			<label class="file optional col-sm-3 control-label" for="user_horizontal_file">File</label>
								  			<div class="col-sm-9">
								  				<input class="file optional" id="user_horizontal_file" name="user_horizontal[file]" type="file">
								  					<p class="help-block">Example block-level help text here.</p>
								  			</div>
								  		</div>-->
                                       <!--
										<div class="form-group check_boxes optional user_horizontal_choices">
											<label class="check_boxes optional col-sm-3 control-label"><abbr title="required">*</abbr>Estado Civil</label>
											<div class="col-sm-9">

												<span class="radio">
													<label for="user_horizontal_choices_soltero">
													<input class="check_boxes optional" id="user_horizontal_choices_soltero" name="user_horizontal_estadoCivil" type="radio" value="Soltero">Soltero(a)</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_casado">
													<input class="check_boxes optional" id="user_horizontal_choices_casado" name="user_horizontal_estadoCivil" type="radio" value="casado">Casado(a)</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_divorciado">
													<input class="check_boxes optional" id="user_horizontal_choices_divorciado" name="user_horizontal_estadoCivil" type="radio" value="divorciado">Divorciado(a)</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_viudo">
													<input class="check_boxes optional" id="user_horizontal_choices_viudo" name="user_horizontal_estadoCivil" type="radio" value="viudo">Viudo(a)</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_libre">
													<input class="check_boxes optional" id="user_horizontal_choices_libre" name="user_horizontal_estadoCivil" type="radio" value="libre">Union Libre</label>
												</span>

												<input name="user_horizontal[choices][]" type="hidden" value="">	
											</div>
										</div>

										<div class="form-group check_boxes optional user_horizontal_choices">
											<label class="check_boxes optional col-sm-3 control-label"><abbr title="required">*</abbr>Estado Educativo</label>
											<div class="col-sm-9">

												<span class="radio">
													<label for="user_horizontal_choices_activo">
													<input class="check_boxes optional" id="user_horizontal_choices_activo_estadoEducativo" name="user_horizontal_estadoEducativo" type="radio" value="Activo">Activo</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_inactivo">
													<input class="check_boxes optional" id="user_horizontal_choices_inactivo_estadoEducativo" name="user_horizontal_estadoEducativo" type="radio" value="inactivo">Inactivo</label>
												</span>

												<input name="user_horizontal[choices][]" type="hidden" value="">
											</div>
										</div>

										<div class="form-group check_boxes optional user_horizontal_choices">
											<label class="check_boxes optional col-sm-3 control-label"><abbr title="required">*</abbr>Nivel Educativo</label>
											<div class="col-sm-9">

												<span class="radio">
													<label for="user_horizontal_choices_basico">
													<input class="check_boxes optional" id="user_horizontal_choices_basico" name="user_horizontal_nivelEducativo" type="radio" value="basico">Basico</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_bachiller">
													<input class="check_boxes optional" id="user_horizontal_choices_bachiller" name="user_horizontal_nivelEducativo" type="radio" value="bachiller">Bachiller</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_universitario">
													<input class="check_boxes optional" id="user_horizontal_choices_universitario" name="user_horizontal_nivelEducativo" type="radio" value="universitario">Universitario</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_profesional">
													<input class="check_boxes optional" id="user_horizontal_choices_profesional" name="user_horizontal_nivelEducativo" type="radio" value="universitario">Profesional</label>
												</span>
												
												<input name="user_horizontal[choices][]" type="hidden" value="">
											</div>
										</div>

										<div class="form-group check_boxes optional user_horizontal_choices">
											<label class="check_boxes optional col-sm-3 control-label"><abbr title="required">*</abbr>Estado Laboral</label>
											<div class="col-sm-9">

												<span class="radio">
													<label for="user_horizontal_choices_activo">
													<input class="check_boxes optional" id="user_horizontal_choices_activo_estadoLaboral" name="user_horizontal_estadoLaboral" type="radio" value="activo">Activo</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_inactivo">
													<input class="check_boxes optional" id="user_horizontal_choices_inactivo_estadoLaboral" name="user_horizontal_estadoLaboral" type="radio" value="inactivo">Inactivo</label>
												</span>												
												
												<input name="user_horizontal[choices][]" type="hidden" value="">
											</div>
										</div>

										<div class="form-group radio_buttons optional user_horizontal_sex">
											<label class="radio_buttons optional col-sm-3 control-label"><abbr title="required">*</abbr>Sexo</label>
											<div class="col-sm-9">
												<span class="radio">
													<label for="user_horizontal_sex_male">
														<input class="radio_buttons optional" id="user_horizontal_sex_male" name="user_horizontal[sex]" type="radio" value="M">Masculino</label>
												</span>
												<span class="radio">
													<label for="user_horizontal_sex_female">
														<input class="radio_buttons optional" id="user_horizontal_sex_female" name="user_horizontal[sex]" type="radio" value="F">Femenino</label>
												</span>
											</div>
										</div>

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_lastname"><abbr title="required">*</abbr> Fecha Nacimiento</label>
								  			<div class="col-sm-9">
								  				<input class="string email required form-control" id="user_horizontal_lastname" name="user_horizontal[lastname]" placeholder="Fecha Nacimiento" type="date">
								  					<p class="help-block">Fecha Nacimiento</p>
								  			</div>
								  		</div>
										<input class="btn btn-default" name="commit" type="submit" value="Registrar Miembro">
									</form>-->