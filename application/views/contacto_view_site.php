<link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/google-map.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/mailform.css">
<main>
  <section class="map">
    <div id="google-map" class="map_model"></div>
    <ul class="map_locations">
      <li data-x="18.4667484" data-y="-69.8198159">
        <p>Calle Miramar Norte #16, Los Frailes I, Santo Domingo Este <span>800 2345-6789</span></p>
      </li>
    </ul>
  </section>        
  <section class="well1">
    <div class="container">
      <h2>Escribenos</h2>
      <form name="formContact" method="post" action="<?php echo base_url(); ?>application/bat/rd-mailform.php" class="mailform off2">
        <input type="hidden" name="form-type" value="contact">
        <fieldset class="row">
          <label class="grid_4">
            <input type="text" id="name" name="name" placeholder="Tu Nombre Completo:" data-constraints="@LettersOnly @NotEmpty">
          </label>
          <label class="grid_4">
            <input type="text" id="phone" name="phone" placeholder="Telefono:" data-constraints="@Phone">
          </label>
          <label class="grid_4">
            <input type="text" id="email"   name="email" placeholder="Email:" data-constraints="@Email @NotEmpty">
          </label>
          <label class="grid_12">
            <textarea name="message"  id="message" placeholder="Mensaje:" data-constraints="@NotEmpty"></textarea>
          </label>
          <div class="mfControls grid_4">
            <button type="submit" id="submitContact" class="btn">Enviar comentario</button>
          </div>
          <div class="mfControls grid_5 off1 successContacto">
            <p>Mensaje Enviado correctamente</p>
          </div>
        </fieldset>
      </form>
    </div>
  </section>
</main>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- JQuery Validate -->
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">

$('form[name=formContact]').submit(function(e) {
       e.preventDefault();
    }).validate({
            debug: false,
            rules: {
                "name": {
                    required: true
                },
                "phone":{
                  required: true,
                  number: true,
                  minlength: 10,
                  maxlength: 11

                },
                "email":{
                  required: true,
                  email: true
                },
                "message":{
                  required: true
                }
            },
            messages: {
                "name": {
                    required: "Campo Obligatorio"
                },
                "phone":{
                    required: "Campo Obligatorio",
                    number: "Introduzca un numero valido",
                    minlength: "No puede ser menor de 10 digitos",
                    maxlength: "No puede ser mayor de 11 digitos"
                },
                "email":{
                     required: "Campo Obligatorio",
                     email:"Introduzca un email valido"
                },
                "message":{
                    required: "Campo Obligatorio"
                }

            },submitHandler: function(form){
            
            data = {};

            data['name'] = $("input[name=name]").val();
            data['phone'] = $("input[name=phone]").val();
            data['email'] = $("input[name=email]").val();
            data['mensaje'] = $("input[name=message]").val();
            
            url2 = "<?php echo base_url(); ?>application/bat/rd-mailform.php";

            $.ajax({
                  type: "POST",
                  url: url2,
                  data: data, // <--- THIS IS THE CHANGE
                  dataType: "json",
                  success: function(data){
                    console.log('exito');
                    console.log(data);

                      //$('.successContacto').show('Fold',1000);

                      $('.successContacto').fadeIn(3000);
                      setTimeout(function(){ 
                        $('.successContacto').fadeOut('slow');                
                      }, 3500);
                      

                    $('form[name=formContact]').each(function(){
                        this.reset();
                    });
                  },
                  error: function(e) { 
                    console.log('error');
                    console.log(e);
                  }
              });
          }
      });


</script>