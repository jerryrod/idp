<?php //var_dump($actividades); 


 if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 

	$id = '';
	$cantidadBrutaValue ='';
	$fechaActividadEditar= '';


?>
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<div class="col-md-12">
				<h1>Mantenimiento de Economia</h1>
				<center class="cargando">
					<img id="cargando" src="<?php echo base_url(); ?>/application/img/loading.gif">
				</center>
			</div>
			
			<div class="col-md-12 cargandoContenedor">
				<div class="row BoxGeneralMembresia">
					<div class="col-md-2 menuLateralEconomia">
						<ul class="nav nav-pills nav-stacked">
							<li><a id="opcione1" href="">Agregar Valor</a></li>
							<li><a id="opcione2" href="">Ver datos</a></li>
							<li><a id="opcione3" href="">EPP</a></li>
							<li><a id="opcione4" href="">Actividades</a></li>
							<li role="presentation" class="dropdown">
		        				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
		          					Eventos <span class="caret"></span>
		       					 </a>
						        <ul class="dropdown-menu">
						          <li><a id="opcione5" href="">Eventos Creados</a></li>
						          <li><a id="opcione6" href="">Crear Eventos</a></li>
						          <li><a id="opcione7" href="">Otra Cosita</a></li>
						          <li role="separator" class="divider"></li>
						          <li><a href="#">Separated link</a></li>
						        </ul>
							</li>
						</ul>
					</div>

					<div class="col-md-8 contenidoEconomia">
						<div class="opcione1">
							<div class="panel panel-primary">
							    <div class="panel-heading">Formulario de conceptos</div>
							    <div class="panel-body">
							   		<?php 
							   			//var_dump($miembro);
							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_economia', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('finanzas/agregarOfrenda',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('idOfrenda',$id);
												  echo form_hidden($datahidden);
											?>
										</div>


								  		<!--/////////////////  Conceptos  /////////////////////// -->
								  		<?php 

											if(isset($actividadValue)){
												$valueActividad = $actividadValue;
											}else{
												$valueActividad = 1;
											}
								  			$atributosConceptos ='id="inputConceptos" class="string email form-control"';
								  		 ?>
								  		

								  		<div class="form-group email required user_horizontal_email">
								  		<label class="email col-sm-3 control-label" for="inputActividades"> Actividad: </label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('inputConceptos',$actividades, $valueActividad,$atributosConceptos) ?>
								  				<p id="labeSeleccioneCarreras" class="help-block">Seleccione la Actividad</p>
								  			</div>
								  		</div>


										<!--/////////////////  2010 /////////////////////// -->
										<?php 
										
							   			$atributo2010 = array(
								              'name'        => 'user_horizontal_2010',
								              'id'          => 'user_horizontal_2010',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Cantidad ',
								              'required'    => 'required'
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_2010"><abbr title="Campo Obligatorio">*</abbr> 2010</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($atributo2010); ?>
								  			</div>
								  		</div>

								  		<!--/////////////////  2011 /////////////////////// -->
										<?php 
										
							   			$atributo2011 = array(
								              'name'        => 'user_horizontal_2011',
								              'id'          => 'user_horizontal_2011',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Cantidad ',
								              'required'    => 'required'
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_2011"><abbr title="Campo Obligatorio">*</abbr> 2011</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($atributo2011); ?>
								  			</div>
								  		</div>

								  		<!--/////////////////  2012 /////////////////////// -->
										<?php 
										
							   			$atributo2012 = array(
								              'name'        => 'user_horizontal_2012',
								              'id'          => 'user_horizontal_2012',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Cantidad ',
								              'required'    => 'required'
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_2012"><abbr title="Campo Obligatorio">*</abbr> 2012</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($atributo2012); ?>
								  			</div>
								  		</div>

								  		<!--/////////////////  2013 /////////////////////// -->
										<?php 
										
							   			$atributo2013 = array(
								              'name'        => 'user_horizontal_2013',
								              'id'          => 'user_horizontal_2013',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Cantidad ',
								              'required'    => 'required'
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_2013"><abbr title="Campo Obligatorio">*</abbr> 2013</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($atributo2013); ?>
								  			</div>
								  		</div>

								  		<!--/////////////////  2014 /////////////////////// -->
										<?php 
										
							   			$atributo2014 = array(
								              'name'        => 'user_horizontal_2014',
								              'id'          => 'user_horizontal_2014',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Cantidad ',
								              'required'    => 'required'
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_2014"><abbr title="Campo Obligatorio">*</abbr> 2014</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($atributo2014); ?>
								  			</div>
								  		</div>

							   			
								  		<?php 
								  			$atributosSubmitDatos ='class="btn btn-primary" id="submitAgregarDatos"';
								  			echo form_submit('committ', 'Registrar Datos',$atributosSubmitDatos); 
								  			echo form_close();
								  		?>

								</div>
  							</div>

						</div>
						<div class="opcione2">
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-1 BoxEconomia"><h4>0</h4></div>
										<div class="col-md-6 BoxEconomia">
											<h4>Conceptos</h4>
										</div>
										<div class="col-md-1 BoxEconomia"><h4>2010</h4></div>
										<div class="col-md-1 BoxEconomia"><h4>2011</h4></div>
										<div class="col-md-1 BoxEconomia"><h4>2012</h4></div>
										<div class="col-md-1 BoxEconomia"><h4>2013</h4></div>
										<div class="col-md-1 BoxEconomia"><h4>2014</h4></div>
									</div>
									<?php 
											//var_dump($valores);
											$contador =1;
											foreach ($valores as $key => $value) {
												?>
												<div class="row">
													<div class="col-md-1 BoxEconomia"><p><?php echo $contador; ?></p></div>
													<div class="col-md-6 BoxEconomia">
														<p><?php echo $value['nombre']; ?></p>
													</div>
													<div class="col-md-1 BoxEconomia">
														<p><?php echo $value['dato2010']; ?></p>
													</div>
													<div class="col-md-1 BoxEconomia">
														<p><?php echo $value['dato2011']; ?></p>
													</div>
													<div class="col-md-1 BoxEconomia">
														<p><?php echo $value['dato2012']; ?></p>
													</div>
													<div class="col-md-1 BoxEconomia">
														<p><?php echo $value['dato2013']; ?></p>
													</div>
													<div class="col-md-1 BoxEconomia">
														<p><?php echo $value['dato2014']; ?></p>
													</div>
												</div>
												<?php
												$contador++;
											}
										 ?>
								</div>
							</div>
						</div>
						<div class="opcione3">
						</div>
						<div class="opcione4">
							<h1>4</h1>
						</div>
						<div class="opcione5">
							<h1>5</h1>
						</div>
						<div class="opcione6">
							<h1>6</h1>
						</div>
						<div class="opcione7">
							<h1>7</h1>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>