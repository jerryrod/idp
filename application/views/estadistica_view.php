	


	<link rel="stylesheet" href="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxcore.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxdraw.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxchart.core.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxdata.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxbuttons.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxscrollbar.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxmenu.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxlistbox.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxdropdownlist.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxgrid.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxgrid.selection.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxgrid.pager.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxgrid.filter.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxcheckbox.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/scripts/demos.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            // prepare chart data as an array
            var sampleData = [
                    { Day: 'Monday', Keith: 30, Erica: 15, George: 25 },
                    { Day: 'Tuesday', Keith: 25, Erica: 25, George: 30 },
                    { Day: 'Wednesday', Keith: 30, Erica: 20, George: 25 },
                    { Day: 'Thursday', Keith: 35, Erica: 25, George: 45 },
                    { Day: 'Friday', Keith: 20, Erica: 20, George: 25 },
                    { Day: 'Saturday', Keith: 30, Erica: 20, George: 30 },
                    { Day: 'Sunday', Keith: 60, Erica: 45, George: 90 }
            ];

            // prepare jqxChart settings
            var settings = {
                title: "Fitness & exercise weekly scorecard",
                description: "Time spent in vigorous exercise",
                enableAnimations: true,
                showLegend: true,
                padding: { left: 5, top: 5, right: 5, bottom: 5 },
                titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
                source: sampleData,
                xAxis:
                    {
                        dataField: 'Day',
                        gridLines: { visible: true }
                    },
                colorScheme: 'scheme01',
                seriesGroups:
                    [
                        {
                            type: 'column',
                            columnsGapPercent: 50,
                            seriesGapPercent: 0,
                            valueAxis:
                            {
                                visible: true,
                                unitInterval: 10,
                                minValue: 0,
                                maxValue: 100,
                                title: { text: 'Time in minutes' }
                            },
                            series: [
                                    { dataField: 'Keith', displayText: 'Keith' },
                                    { dataField: 'Erica', displayText: 'Erica' },
                                    { dataField: 'George', displayText: 'George' }
                            ]
                        }
                    ]
            };

            // setup the chart
            $('#jqxChart').jqxChart(settings);

            var adapter = new $.jqx.dataAdapter({
                datafields: [
                    { name: "Day", type: "string" },
                    { name: "Keith", type: "number" },
                    { name: "Erica", type: "number" },
                    { name: "George", type: "number" }
                ],
                localdata: sampleData,
                datatype: 'array'
            });

            $("#jqxGrid").jqxGrid({
                width: 848,
                height: 232,
                filterable: true,
                showfilterrow: true,
                source: adapter,
                columns:
                [
                    { text: "Day", width: '40%', datafield: "Day", filteritems: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"], filtertype: "checkedlist" },
                    { text: "Keith", width: '20%', datafield: "Keith" },
                    { text: "Erica", width: '20%', datafield: "Erica" },
                    { text: "George", width: '20%', datafield: "George" }
                ]
            });
            $("#jqxGrid").on('filter', function () {
                var rows = $("#jqxGrid").jqxGrid('getrows');
                var chart = $('#jqxChart').jqxChart('getInstance');
                chart.source = rows;
                chart.update();
            });
        });
    </script>


 
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<h1>Estadisticas</h1>
		</div>
		
		
		<div class="row">
			<div class="col-md-12 cargandoContenedor">
				<div class="row BoxGeneralMembresia">
					<div class="col-md-2 menuLateralEstadistica">
						<ul class="nav nav-pills nav-stacked">
							<li><a id="opcion1" href="">Asistencia General</a></li>
							<li><a id="opcion2" href="">Damas</a></li>
							<li><a id="opcion3" href="">Jovenes</a></li>
							<li><a id="opcion4" href="">Adoslecentes</a></li>
							<li><a id="opcion5" href="">Ninos</a></li>
							<li><a id="opcion6" href="">Evangelismo</a></li>
							<li><a id="opcion7" href="">Oración</a></li>
							<li><a id="opcion8" href="">Adoración</a></li>
							<li role="presentation" class="dropdown">
		        				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
		          					Eventos <span class="caret"></span>
		       					 </a>
						        <ul class="dropdown-menu">					          
						          <li><a id="opcion9" href="">Otra Cosita</a></li>
						          <li role="separator" class="divider"></li>
						          <li><a href="#">Separated link</a></li>
						        </ul>
							</li>
						</ul>
					</div>


					<div class="col-md-10 contenidoEstadistica">						
						<div class="opcion1">
							<ul class="nav nav-tabs" role="tablist" id="TabsId">
								<li id="id-li-select-sexo" style="width:25%;border-left: 1px solid #ddd;">
								    <div class="form-group email required user_horizontal_email">
								  		<label class="email col-sm-3 control-label" for="user_horizontal_email"> Sexo: </label>
							  			<div class="col-sm-8">
							  				<select name="sexoFiltroEstadistica" id="sexoFiltroEstadistica" class="string email required form-control">
												<option value="0" selected="selected">Nulo</option>
												<option value="1" >Todos</option>
												<option value="2">Masculino</option>
												<option value="3">Femenino</option>
											</select>
							  			</div>
								  	</div>
	        					</li>
	        					<li id="id-li-select-ministerio" style="width:25%;border-left: 1px solid #ddd;">
								    <div class="form-group email required user_horizontal_email">
								  		<label class="email col-sm-3 control-label" for="user_horizontal_email"> Ministerio: </label>
							  			<div class="col-sm-8">
							  				<select name="ministerioFiltroEstadistica" id="ministerioFiltroEstadistica" class="string email required form-control">
												<option value="0" selected="selected">Nulo</option>
												<option value="1">Todos</option>
												<option value="2">Caballeros</option>
												<option value="3">Damas</option>
												<option value="4">Jovenes</option>
												<option value="5">Adolescentes</option>
												<option value="6">Ninos</option>
											</select>
							  			</div>
								  	</div>
	        					</li>

	        					<!--/////////////////  FECHA Actividad /////////////////////// -->

								<?php 

					   			$fechaInicial = array(
						              'name'        => 'user_horizontal_fechaInicial',
						              'id'          => 'user_horizontal_fechaInicial',
						              'type'		=> 'date',
						              'class'		=> 'string email required form-control',
						              'required'    => 'required'
						         );

					   			$fechaFinal = array(
						              'name'        => 'user_horizontal_fechaFinal',
						              'id'          => 'user_horizontal_fechaFinal',
						              'type'		=> 'date',
						              'class'		=> 'string email required form-control',
						              'required'    => 'required'
						         );
					   										   			
					   			?> 

	        					<li id="id-li-select-periodo" style="width:50%;border-left: 1px solid #ddd;">
								    <div class="form-group email required user_horizontal_email">
								  		<label class="email col-sm-1 control-label" for="user_horizontal_fechaInicial">Fecha:</label>
							  			<div class="col-sm-5">
							  				<?php echo form_input($fechaInicial); ?>
							  				<p class="help-block">Fecha Inicial</p>
							  			</div>
							  			<div class="col-sm-5">
							  				<?php echo form_input($fechaFinal); ?>
							  				<p class="help-block">Fecha Final</p>
							  			</div>
								  	</div>
	        					</li>

	        					<li id="id-li-select-actividad" style="margin-bottom: 1%;margin-top: 2%;width:50%;border-left: 1px solid #ddd;">

	        						<!--/////////////////  Actividades Regulares  /////////////////////// -->
								  		<?php 
								  			$atributosActividadesRegulares ='id="actividadesRegularesFiltroEstadistica" class="string email required form-control"';
								  		 ?>
								    <div class="form-group email required user_horizontal_email">
								  		<label class="email col-md-1 control-label" for="user_horizontal_email"> Actividad: </label>
							  			<div class="col-md-10">
							  				<?php echo form_dropdown('actividadesRegularesFiltroEstadistica',$actividadesRegulares, 9,$atributosActividadesRegulares) ?>
							  			</div>
								  	</div>
	        					</li>
	        					<li id="id-li-select-boton" style="margin-bottom: 1%;margin-top: 2%;width:40%;border-left: 1px solid #ddd;">
	        						<span class="input-group-btn">
	            							<button class="btn btn-info" id="buscarEstadisticaAsistencia" type="button">Buscar</button>
	          						</span>
	          					</li>

							</ul>
							<h1 id="mujeresPorActividad"> Estadistica de Mujeres</h1>
							<h1 id="hombresPorActividad"> Estadistica de Hombres</h1>
							<div class="row leyendaEstadistica" style="margin-top:2%;">
								<div class="col-md-2 col-md-offset-4">
									<div class="row">
										<div class="col-md-8">
											<strong>Hombres</strong>
										</div>
										<div class="col-md-1" style="background-color:#31b0d5;min-height: 20px;">													
										</div>												
									</div>	
								</div>
								<div class="col-md-2">
									<div class="row">
										<div class="col-md-8">
											<strong>Mujeres</strong>
										</div>
										<div class="col-md-1" style="background-color:#b1ccda;min-height: 20px;">												
										</div>												
									</div>
								</div>										
							</div>
							<div class="row contenedorGraficaEstadistica">
								<canvas id="canvas2" height="100" width="200"></canvas>	
							</div>	
						</div>
					</div>
					<div class="opcion2">
						  
					    <div id="jqxGrid"></div>
						<div id='jqxChart' style="margin-top: 50px; width: 400px; height: 100px; position: relative; left: 0px; top: 0px;"></div>
		
					</div>
					<div class="opcion3">
						<p>3</p>
					</div>
					<div class="opcion4">
						<p>4</p>
					</div>
					<div class="opcion5">
						<p>5</p>
					</div>
					<div class="opcion6">
						<p>6</p>
					</div>
					<div class="opcion7">
						<p>7</p>
					</div>
					<div class="opcion8">
						<p>8</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
