<?php if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 



?>
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<div class="col-md-12">
				<h1>Mantenimiento de Eventos</h1>
				<center class="cargando">
					<img id="cargando" src="<?php echo base_url(); ?>/application/img/loading.gif">
				</center>
			</div>
			<div class="col-md-12 errorFormularioMiembros">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Error!!!!</span> Verifica que los campos esten introducidos de la manera correcta. </h1>
					</div>
				</div>				
			</div>
			
			<div class="col-md-12 successFormularioEvento">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Evento!!!!</span> creado correctamente. </h1>
					</div>
				</div>
				
			</div>
			<div class="col-md-12 cargandoContenedor">
				<div class="row BoxGeneralMembresia">
					<div class="col-md-2 menuLateralMembresia">
						<ul class="nav nav-pills nav-stacked">
							<li><a id="opE1" href="">Eventos Creados</a></li>
							<li><a id="opE2" href="">Crear Eventos</a></li>							
						</ul>
					</div>
					<div class="col-md-8 contenidoMembresia">
						<div class="opE1">
							<div class="row">
								<div class="col-md-12">
									<ul class="nav nav-tabs" role="tablist" id="TabsId2">
									    <li role="presentation" class="active"><a href="#eventos" aria-controls="home" role="tab" data-toggle="tab">Eventos</a></li>
									    <li role="presentation"><a href="#crearEventos" aria-controls="crearEventos" role="tab" data-toggle="tab">Otra Actividad Aqui</a></li>
									</ul>

									<div class="tab-content tab-Contenido">
										<!-- Total Eventos-->
		    							<div role="tabpanel" class="tab-pane fade in active" id="eventos">
		    								<div class="row">
												<div class="col-md-3">
													<strong>Nombre del Evento</strong>
												</div>

												<div class="col-md-3">
													<strong>Encargado</strong>
												</div>

												<div class="col-md-2">
													<strong>Fecha</strong>
												</div>

												<div class="col-md-2">
													<strong>Operaciones</strong>
												</div>
											</div>
												<?php 
													foreach ($eventos as $key => $value) {
														?>
														<div class="row">
															<div class="col-md-3">
																<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/eventos/ver_Evento/<?php echo $value['id']; ?>"><p><?php echo $value['nombre']; ?></p></a>
															</div>															
															<div class="col-md-3">
																<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['responsable']; ?>"><p><?php echo $value['nombre_completo']; ?></p></a>
															</div>
															<div class="col-md-2">
																<p><?php echo $value['fecha']; ?></p>
															</div>
															<div class="col-md-2">
																<a href="<?php echo base_url(); ?>index.php/eventos?e=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
															</div>
														</div>

														<?php

													}
												?>
		    							</div>

		    							<!-- Crear Eventos-->

		    							<div role="tabpanel" class="tab-pane fade " id="crearEventos">
		    								
		    							</div>
									</div>
								</div>								
							</div>
						</div>
						<div class="opE2">
							<div class="panel panel-primary">
							    <div class="panel-heading">Formulario de Evento</div>
							    <div class="panel-body">
							   		<?php 
							   			//var_dump($miembro);
							   			$attributess = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_evento', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('miembros/agregarEvento',$attributess); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   		?>
							   		
							   			<div style="display:none">
											<?php echo form_hidden('idEvento2',$id);
												 echo form_hidden($datahidden);
											?>
										</div>
										<!--/////////////////  NOMBRE /////////////////////// -->
										<?php 
										
							   			$nombreEventoBig = array(
								              'name'        => 'user_horizontal_name_evento',
								              'id'          => 'user_horizontal_name_evento',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Nombre',
								              'required'    => 'required'
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_name_evento"><abbr title="Campo Obligatorio">*</abbr> Nombre del Evento</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($nombreEventoBig); ?>
								  				<p class="help-block">Nombre del evento</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  responsable  /////////////////////// -->

								  		<?php $atributosResponsable ='id="user_horizontal_name_responsable" class="string email required form-control"'; ?>

								  		<div class="form-group email required user_horizontal_email">
								  		<label class="email required col-sm-3 control-label" for="user_horizontal_name_responsable"><abbr title="Campo Obligatorio">*</abbr>Responsable</label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('responsable',$responsable, 1,$atributosResponsable) ?>
								  				<p id="labeSeleccioneResponsable" class="help-block">Seleccione el responsable</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  Ministerio  /////////////////////// -->

								  		<?php $atributosMinisterio ='id="user_horizontal_ministerios" class="string email required form-control"'; ?>

								  		<div class="form-group email required user_horizontal_email">
								  		<label class="email required col-sm-3 control-label" for="user_horizontal_ministerios"><abbr title="Campo Obligatorio">*</abbr>Ministerio</label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('ministerios',$ministerios, 1,$atributosMinisterio) ?>
								  				<!--<p id="labeSeleccioneResponsable" class="help-block">Seleccione el ministerio</p>-->
								  			</div>
								  		</div>

								  		<!--/////////////////  FECHA Evento /////////////////////// -->

										<?php 

							   			$fechaEvento = array(
								              'name'        => 'user_horizontal_fecha2',
								              'id'          => 'user_horizontal_fecha2',
								              'type'		=> 'date',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required'
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_fecha2"><abbr title="Campo Obligatorio">*</abbr>Fecha Evento</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($fechaEvento); ?>
								  				<p class="help-block">Fecha Evento</p>
								  			</div>
								  		</div>


								  		<!--/////////////////  Descripcion del Evento /////////////////////// -->

										<?php 

							   			$Descripcion = array(
								              'name'        => 'user_horizontal_descripcion',
								              'id'          => 'user_horizontal_descripcion',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required'
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_descripcion"><abbr title="Campo Obligatorio">*</abbr> Descripcion</label>
								  			<div class="col-sm-9">
								  				<?php echo form_textarea($Descripcion); ?>
								  				<p class="help-block">Descripcion del evento</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  FECHA Evento /////////////////////// -->

									

								  		 <?php 
								  			$atributosSubmitEvento ='class="btn btn-primary" id="submitAgregarEvento"';
								  			echo form_submit('commit', 'Registrar Evento',$atributosSubmitEvento); 
								  			echo form_close();
								  		?>
								</div>
							</div>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>