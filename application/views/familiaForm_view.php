<?php if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 

?>

		<div class="container padreFamilia">
			<div class="row">
				<div class="col-md-8 col-lg-8">
					<div class="row">
						<div class="alert alert-success SuccessMensajeFamilia">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Success!</strong> Familia agregada.
						 </div>
						 <center class="cargando">
							<img id="cargando" src="<?php echo base_url(); ?>/application/img/loading.gif">
						</center>
					</div>
					<div class="row">
				<div class="panel panel-primary">
							    <div class="panel-heading">Formulario de Familias</div>
							    <div class="panel-body">
							   		<?php 
							   			//var_dump($miembro);
							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'familia_Form', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('familia/agregarFamilia',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('idFamilia',$id);
												 echo form_hidden($datahidden);
											?>
										</div>
										<!--<div class="row">
											<p class="col-md-4 col-md-offset-1 tituloFormularioMembresia"><span class="uk-icon-edit"></span> Datos Generales</p>
										</div>
										<hr>-->
										<!--/////////////////  NOMBRE /////////////////////// -->
										<?php 
										
							   			$apellidoPaterno = array(
								              'name'        => 'apellidoPaterno',
								              'id'          => 'apellidoPaterno',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Apellido del Padre',
								              'required'    => 'required'
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="apellidoPaterno"><abbr title="Campo Obligatorio">*</abbr> Apellido Paterno</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($apellidoPaterno); ?>
								  				<!--<p class="help-block">Primer y Segundo Nombre</p>-->
								  			</div>
								  		</div>

								  		<!--/////////////////  APELLIDO  /////////////////////// -->
								  		<?php 

							   			$apellidoMaterno = array(
								              'name'        => 'apellidoMaterno',
								              'id'          => 'apellidoMaterno',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Apellido de la Madre',
								              'required'    => 'required'
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="apellidoMaterno"><abbr title="Campo Obligatorio">*</abbr> Apellido Materno</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($apellidoMaterno); ?>
								  				<!--<p class="help-block">Apellido de la madre</p>-->
								  			</div>
								  		</div>

								  		<!--////////////////// Descripcion de Cambio de Estado  //////////////////////-->


								  		<?php 

							   			$DescripcionFamilia = array(
								              'name'        => 'user_horizontal_descripcion',
								              'id'          => 'user_horizontal_descripcion',
								              'class'		=> 'string email required form-control'
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_descripcion"><abbr title="Campo Obligatorio">*</abbr> Descripción:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_textarea($DescripcionFamilia); ?>
								  			</div>
								  		</div>								  	

								  		<?php 
								  			$atributosSubmit ='class="btn btn-primary" id="submitAgregarFamilia"';
								  			echo form_submit('commit', 'Guardar',$atributosSubmit); 
								  			echo form_close();
								  		?>

								</div>
  							</div>
					</div>
				</div>
			</div>
			
		</div>
	</body>
</html>