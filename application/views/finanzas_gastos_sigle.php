<?php //var_dump($ofrenda) ?>

<?php //var_dump($gastos) ?>

<?php //var_dump($id); ?>

<div class="container-fluid">
	<div class="row">
		<h1>Descripcion de los gastos</h1>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3">
					<div class="row"><strong>Actividad:</strong></div>
					<div class="row"><strong>Cantidad Bruta:</strong></div>
				</div>
				<div class="col-md-6">
					<div class="row"><?php echo $ofrenda->nombreActividadRegular ?></div>
					<div class="row"><?php echo '$'.$ofrenda->cantidadBruta.'.00' ?></div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3">
					<div class="row"><strong>Cantidad Neta:</strong></div>
					<div class="row"><strong>Fecha:</strong></div>
				</div>
				<div class="col-md-6">
					<div class="row"><?php echo '$'.$ofrenda->cantidadNeta.'.00' ?></div>
					<div class="row"><?php echo $ofrenda->fechaOfrenda ?></div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="row tablaDeGastos">
				<div class="col-md-3"><strong>Cantidad</strong></div>
				<div class="col-md-9"><strong>Descripción</strong></div>
			</div>		
			<?php foreach ($gastos as $key => $value) { ?>
				<div class="row tablaDeGastos">
					<div class="col-md-3"><?php echo '$'.$value['cantidadGasto'].'.00'; ?></div>
					<div class="col-md-9"><?php echo $value['descripcionGasto']; ?></div>					
				</div>
			<?php } ?>
		</div>
	</div>
</div>