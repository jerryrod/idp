<?php //var_dump($actividades); 


 if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 

	$id = '';
	$cantidadBrutaValue ='';
	$fechaActividadEditar= '';


?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<h1>Mantenimiento de Finanzas</h1>
					<center class="cargando">
						<img id="cargando" src="<?php echo base_url(); ?>/application/img/loading.gif">
					</center>
				</div>
				
				<div class="col-md-12 cargandoContenedor">
					<div class="row BoxGeneralMembresia">
						<div class="col-md-2 menuLateralFinanzas">
							<ul class="nav nav-pills nav-stacked">
								<li><a id="opcionf2" href="">Ver ofrendas</a></li>
								<li><a id="opcionf1" href="">Agregar Ofrenda</a></li>							
								<li><a id="opcionf3" href="">Agregar Asistencia</a></li>
								<li><a id="opcionf4" href="">Actividades</a></li>
								<li role="presentation" class="dropdown">
			        				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			          					Eventos <span class="caret"></span>
			       					 </a>
							        <ul class="dropdown-menu">
							          <li><a id="opcionf5" href="">Eventos Creados</a></li>
							          <li><a id="opcionf6" href="">Crear Eventos</a></li>
							          <li><a id="opcionf7" href="">Otra Cosita</a></li>
							          <li role="separator" class="divider"></li>
							          <li><a href="#">Separated link</a></li>
							        </ul>
								</li>
							</ul>
						</div>

						<div class="col-md-10 contenidoFinanzas">
							<div class="opcionf2">
								<div class="row">
									<div class="col-md-3">
										<strong>Nombre Actividad</strong>
									</div>
									<div class="col-md-2">
										<strong>Entrada Bruta</strong>
									</div>
									<div class="col-md-2">
										<strong>Entrada Neta</strong>
									</div>
									<div class="col-md-1">
										<strong>Gasto</strong>
									</div>
									<div class="col-md-2">
										<strong>Fecha</strong>
									</div>
								</div>
								<hr>
								<div class="row tablaDeOfrendas">
									<div class="col-md-12">								
										<?php 
										if($ofrenda != null){
										$contador=1;
										for ($i=0; $i < count($ofrenda); $i++) { ?>
											<div class="row <?php if($contador%2==0){echo "colorLista";} ?>">
												<div class="col-md-3">
													<span><?php echo $contador ."-". $ofrenda[$i]['nombreActividadRegular'] ?></span>
												</div>
												<div class="col-md-2">
													<span><?php echo "$". $ofrenda[$i]['cantidadBruta'] ?></span>
												</div>
												<div class="col-md-2">
													<span><?php echo "$".$ofrenda[$i]['cantidadNeta'] ?></span>
												</div>
												<div class="col-md-1">
													<a class="various fancybox.ajax" href="<?php echo base_url().'index.php/finanzas/ver_gastos/'.$ofrenda[$i]['id'] ?>"><span><?php echo "$".$ofrenda[$i]['gastos'] ?></span></a>
												</div>
												<div class="col-md-2">
													<span><?php $fecha = new DateTime($ofrenda[$i]['fechaOfrenda']); echo $fecha->format('d-m-Y') ?></span>
												</div>
											</div>
										<?php $contador++; } 
									}
									if($ofrenda == null){ ?>
										<h1>No hay data de Ofrendas.</h1>
									<?php }
										//var_dump($ofrenda);
										 ?>
									</div>
								</div>								
							</div>
							<div class="opcionf1">
								<div class="panel panel-primary">
								    <div class="panel-heading">Formulario de Membresia</div>
								    <div class="panel-body">
								   		<?php 
								   			//var_dump($miembro);
								   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_finanzas', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

								   			echo form_open('finanzas/agregarOfrenda',$attributes); 

								   			$datahidden = array(
									              'utf8'  => '✓',
									              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
									            );
								   			?>
								   			<div style="display:none">
												<?php echo form_hidden('idOfrenda',$id);
													 echo form_hidden($datahidden);
												?>
											</div>


									  		<!--/////////////////  Actividad Ofrenda  /////////////////////// -->
									  		<?php 

												if(isset($actividadValue)){
													$valueActividad = $actividadValue;
												}else{
													$valueActividad = 1;
												}
									  			$atributosActividades ='id="inputActividades" class="string email form-control"';
									  		 ?>
									  		

									  		<div class="form-group email required user_horizontal_email">
									  		<label class="email col-sm-3 control-label" for="inputActividades"> Actividad: </label>
									  			<div class="col-sm-9">
									  				<?php echo form_dropdown('inputActividades',$actividades, $valueActividad,$atributosActividades) ?>
									  				<p id="labeSeleccioneCarreras" class="help-block">Seleccione la Actividad</p>
									  			</div>
									  		</div>


											<!--/////////////////  Cantidad Bruta /////////////////////// -->
											<?php 
											
								   			$cantidadBruta = array(
									              'name'        => 'user_horizontal_cantidaBruta',
									              'id'          => 'user_horizontal_cantidaBruta',
									              'class'		=> 'string email required form-control',
									              'placeholder' => 'Cantidad ',
									              'required'    => 'required',
									              'value'		=> $cantidadBrutaValue
									         );
								   			?> 
								   			<div class="form-group email required user_horizontal_email">
									  			<label class="email required col-sm-3 control-label" for="user_horizontal_cantidaBruta"><abbr title="Campo Obligatorio">*</abbr> Cantidad recolectada</label>
									  			<div class="col-sm-9">
									  				<?php echo form_input($cantidadBruta); ?>
									  				<p class="help-block">Cantidad Bruta</p>
									  			</div>
									  		</div>

									  		<!--/////////////////  FECHA Actividad /////////////////////// -->

											<?php 

								   			$fechaActividad = array(
									              'name'        => 'user_horizontal_fechaActividad',
									              'id'          => 'user_horizontal_fechaActividad',
									              'type'		=> 'date',
									              'class'		=> 'string email required form-control',
									              'required'    => 'required',
									              'value'		=> $fechaActividadEditar
									         );
								   										   			
								   			?> 

											<div class="form-group email required user_horizontal_email">
									  			<label class="email required col-sm-3 control-label" for="user_horizontal_fechaActividad"><abbr title="Campo Obligatorio">*</abbr> Fecha Actividad</label>
									  			<div class="col-sm-9">
									  				<?php echo form_input($fechaActividad); ?>
									  				<p class="help-block">Fecha Actividad</p>
									  			</div>
									  		</div>

									  		<div class="row">
									  			<div class="col-md-2 col-md-offset-4">
									  				<h4>Gastos</h4>
									  			</div>
									  			<div class="col-md-2">
									  				<span>Agregar</span>
									  				<span id="agregarInputGasto" class="glyphicon glyphicon-plus-sign iconPlus"></span> 
									  				<span id="cantidadGastos"></span>
									  			</div>								  			
									  		</div>

									  		<br>

									  		<!--/////////////////  Gasto  /////////////////////// -->

									  		<div class="boxInputOfrenda">
									  			
									  		</div>
									  		<div class="row">
									  			<div class="col-md-4 col-md-offset-8">
									  				<p>Cantidad Neta:RD$<span id="cantidadNeta"></span></p>
									  			</div>
									  		</div>
								   			
									  		<?php 
									  			$atributosSubmitOfrenda ='class="btn btn-primary" id="submitAgregarOfrenda"';
									  			echo form_submit('commit', 'Registrar Ofrenda',$atributosSubmitOfrenda); 
									  			echo form_close();
									  		?>

									</div>
	  							</div>

							</div>						
							<div class="opcionf3">
								<h1>3</h1>
							</div>
							<div class="opcionf4">
								<h1>4</h1>
							</div>
							<div class="opcionf5">
								<h1>5</h1>
							</div>
							<div class="opcionf6">
								<h1>6</h1>
							</div>
							<div class="opcionf7">
								<h1>7</h1>
							</div>
						</div>
					</div>			
				</div>
			</div>
		</div>
	</div>		
</div>