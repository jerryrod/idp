<?php 
	$id= '';
	$lider ='';
	$anfitrion ='';
	$direccion='';
	$dia='';
	$fechaInicio='';

	if(isset($celula)){
		$id =$celula->id;
		$lider2 = $celula->lider;
		$anfitrion =$celula->anfitrion;
		$direccion = $celula->direccion;
		$dia =$celula->dia;
		$fechaInicio = $celula->fechaInicio;
		
	}
	//var_dump($celula);
 ?>
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<div class="col-md-12">
				<h1>Mantenimiento de Grupos de Crecimiento</h1>
				<center class="cargando2">
					<img id="cargando2" src="<?php echo base_url(); ?>/application/img/loading.gif">
				</center>
			</div>
			<div class="col-md-12 errorFormularioGCrecimiento">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Error!!!!</span> Verifica que los campos esten introducidos de la manera correcta. </h1>
					</div>
				</div>
				
			</div>
			<div class="col-md-12 successFormularioCelula">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Celula!!!!</span> Insertada de manera correcta. </h1>
					</div>
				</div>
				
			</div>
			
			<div class="col-md-12 cargandoContenedor">
				<div class="row BoxGeneralCelula">
					<div class="col-md-2 menuLateralCelula">
						<ul class="nav nav-pills nav-stacked">
							<li><a id="opc1" href="">Celulas</a></li>
							<li><a id="opc2" href="">Agregar Celula</a></li>
							<li><a id="opc3" href="">Agregar Miembro a Celula</a></li>
							<!--<li><a id="op4" href="">Actividades</a></li>
							<li role="presentation" class="dropdown">
		        				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
		          					Eventos <span class="caret"></span>
		       					 </a>
						        <ul class="dropdown-menu">
						          <li><a id="op5" href="">Eventos Creados</a></li>
						          <li><a id="op6" href="">Crear Eventos</a></li>
						          <li><a id="op7" href="">Otra Cosita</a></li>
						          <li role="separator" class="divider"></li>
						          <li><a href="#">Separated link</a></li>
						        </ul>
							</li>-->
						</ul>
					</div>

					<div class="col-md-8 contenidoCelula">
						<div class="opc1 row">
							<div class="col-md-12" style="margin-top: -5%;">
								<div class="row" style="margin-top:5%;">
									<div class="col-md-4">
										<strong>Lider de Celula</strong>
									</div>
								</div>
								<?php 
									//var_dump($miembros1);
									//$CI =& get_instance();
									foreach ($celulas as $key => $value) {

										//$porcentajeString = $CI->porcentajeIndividualPorMiembrosColor($value['id']);
										//$porcentajeInt = (int)$porcentajeString;
										//echo $porcentajeInt;
										?>
										<div class="row">
											<div class="col-md-4">
												<a class="various fancybox.ajax " href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['lider']; ?>"><p><?php echo $value['nombre']; ?></p></a>
											</div>
											<div class="col-md-2">
												<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/gcrecimiento/ver_Celula/<?php echo $value['id']; ?>"><p>Ver</p></a>
											</div>
											<div class="col-md-2">
												<a href="<?php echo base_url(); ?>index.php/gcrecimiento?b=2&id=<?php echo $value['id']; ?>"><p>Editar</p></a>
											</div>
											
										</div>

										<?php

									}
								 ?>
							</div>
						</div>
						<div class="opc2">
							<div class="panel panel-primary">
							    <div class="panel-heading">Formulario de Grupo de Celula</div>
							    <div class="panel-body">
							   		<?php 
							   			//var_dump($miembro);
							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_celula', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('gcrecimiento/agregarCelula',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('id',$id);
												 echo form_hidden($datahidden);
											?>
										</div>

										<!--/////////////////  Lider /////////////////////// -->

										<?php 
										if(isset($lider2)){
											$valueLider = $lider2;
										}else{
											$valueLider =1;
										}
										
							   			$lider = array(
								              'name'        => 'user_horizontal_lider',
								              'id'          => 'user_horizontal_lider',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Nombre',
								              'required'    => 'required'

								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_lider"><abbr title="Campo Obligatorio">*</abbr> Nombre del Lider:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('lider',$miembros, $valueLider,$lider) ?>								  				
								  				<p class="help-block">Nombre Completo</p>
								  			</div>
								  		</div>

								  		<!--////////////////// Descripcion de Cambio de Lider  //////////////////////-->


								  		<?php 

							   			$DescripcionCambio = array(
								              'name'        => 'user_horizontal_descripcion_cambio_lider',
								              'id'          => 'user_horizontal_descripcion_cambio_lider',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required'
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email" id="descripcionCambioLider">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_descripcion_cambio_lider"><abbr title="Campo Obligatorio">*</abbr> Descripción del Cambio:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_textarea($DescripcionCambio); ?>
								  				<p class="help-block">¿Por que el Cambio de Lider?:</p>
								  			</div>
								  		</div>



								  		<!--/////////////////  Anfitrion  /////////////////////// -->
								  		<?php 

							   			$anfitrion = array(
								              'name'        => 'user_horizontal_anfitrion',
								              'id'          => 'user_horizontal_anfitrion',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Anfitrion',
								              'required'    => 'required',
								              'value'		=>  $anfitrion
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_anfitrion"><abbr title="Campo Obligatorio">*</abbr> Nombre del Anfitrion</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($anfitrion); ?>
								  				<p class="help-block">Nombre Completo</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  Direccion /////////////////////// -->

								  		<?php 

							   			$direccion = array(
								              'name'        => 'user_horizontal_direccion',
								              'id'          => 'user_horizontal_direccion',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Direccion',
								              'required'    => 'required',
								              'value'		=>  $direccion
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email col-sm-3 control-label" for="user_horizontal_direccion"><abbr title="Campo Obligatorio">*</abbr>Dirección</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($direccion); ?>
								  				<p class="help-block">Direccion de la celula</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  Dia /////////////////////// -->

								  		
								  		<?php 
									  		if(isset($dia)){
												$valueDia = $dia;
											}else{
												$valueDia =1;
											}
								  			$atributosDias ='id="inputDias" class="string email required form-control"';
								  		 ?>
								  		

								  		<div class="form-group email required user_horizontal_email">
								  		<label class="email col-sm-3 control-label" for="user_horizontal_email"> Dia: </label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('dias',$dias, $valueDia,$atributosDias) ?>
								  				<p id="labeSeleccioneCarreras" class="help-block">Seleccione el dia</p>
								  			</div>
								  		</div>

										
										<!--/////////////////  FECHA Inicio /////////////////////// -->

										<?php 

							   			$fechaInicio = array(
								              'name'        => 'user_horizontal_fecha',
								              'id'          => 'user_horizontal_fecha',
								              'type'		=> 'date',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required',
								              'value'		=>  $fechaInicio
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_fecha"><abbr title="Campo Obligatorio">*</abbr> Fecha: </label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($fechaInicio); ?>
								  				<p class="help-block">Fecha de Inicio</p>
								  			</div>
								  		</div>	

								  		<div class="row integrantes">
								  			<div class="col-md-10 integrantes2 col-md-offset-1">
								  				<div class="row integrantesFuera">
								  					<div class="col-md-6">
								  						<p id="integrantes">Integrantes</p>
								  					</div>
								  					<div class="col-md-6">
								  						<a class="various fancybox.ajax btn btn-primary" href="<?php echo base_url(); ?>index.php/gcrecimiento/agregarMiembroCelula/<?php if(isset($_GET['id'])){echo $_GET['id'];} ?>" name="addMiembro" id="addMiembro">Agregar Miembro</a>
								  					</div>
								  					
								  				</div>									  			
									  			
									  			<?php 
													//var_dump($miembros1);
													$CI =& get_instance();
													if(isset($_GET['b'])){

														$celulasIntegrantes = $CI->get_Miembros_Por_Celula($_GET['id']);
														foreach ($celulasIntegrantes as $key => $value) {
															?>
															<div class="row integrantesDentro">
																<div class="col-md-4">
																	<p><?php echo $value['nombre']." ". $value['apellido']; ?></p>
																</div>
																<div class="col-md-4">																	
																	<a class="eliminarMiembroCelula" data-celula="<?php echo $value['celula'];?>" href="<?php echo base_url(); ?>index.php/miembros/actualizarCelulaMiembro/<?php echo $value['id']; ?>"><p>Eliminar</p></a>
																</div>
															</div>
															
														<?php }
													}
										 		?>

								  			</div>	
								  		</div>								  		

								  		<?php 
								  			$atributosSubmit ='class="btn btn-primary" id="submitAgregarCelula" style="margin-left: 4%;margin-bottom: 2%;margin-top:3%"';
								  			echo form_submit('Celula', 'Crear Celula',$atributosSubmit); 
								  		?>

								</div>
  							</div>
						</div>
						<div class="opc3">
							<p>hola</p>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>