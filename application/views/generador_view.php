<?php if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 

?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/application/css/style2.css">
<link rel="stylesheet" type="text/css" href="http://jqwidgets.com/public/jqwidgets/styles/jqx.base.css">
<link rel="stylesheet" type="text/css" href="http://jqwidgets.com/public/jqwidgets/styles/jqx.energyblue.css">

<script type="text/javascript">
	$(document).ready(function () {
		
		var employees = [{
	       "EmployeeID": 1,
	           "FirstName": "Nancy",
	           "LastName": "Davolio",
	           "ReportsTo": 2,
	           "Country": "USA",
	           "Title": "Sales Representative",
	           "HireDate": "1992-05-01 00:00:00",
	           "BirthDate": "1948-12-08 00:00:00",
	           "City": "Seattle",
	           "Address": "507 - 20th Ave. E.Apt. 2A"
	   }, {
	       "EmployeeID": 2,
	           "FirstName": "Andrew",
	           "LastName": "Fuller",
	           "ReportsTo": null,
	           "Country": "USA",
	           "Title": "Vice President, Sales",
	           "HireDate": "1992-08-14 00:00:00",
	           "BirthDate": "1952-02-19 00:00:00",
	           "City": "Tacoma",
	           "Address": "908 W. Capital Way"
	   }, {
	       "EmployeeID": 3,
	           "FirstName": "Janet",
	           "LastName": "Leverling",
	           "ReportsTo": 2,
	           "Country": "USA",
	           "Title": "Sales Representative",
	           "HireDate": "1992-04-01 00:00:00",
	           "BirthDate": "1963-08-30 00:00:00",
	           "City": "Kirkland",
	           "Address": "722 Moss Bay Blvd."
	   }, {
	       "EmployeeID": 4,
	           "FirstName": "Margaret",
	           "LastName": "Peacock",
	           "ReportsTo": 2,
	           "Country": "USA",
	           "Title": "Sales Representative",
	           "HireDate": "1993-05-03 00:00:00",
	           "BirthDate": "1937-09-19 00:00:00",
	           "City": "Redmond",
	           "Address": "4110 Old Redmond Rd."
	   }, {
	       "EmployeeID": 5,
	           "FirstName": "Steven",
	           "LastName": "Buchanan",
	           "ReportsTo": 2,
	           "Country": "UK",
	           "Title": "Sales Manager",
	           "HireDate": "1993-10-17 00:00:00",
	           "BirthDate": "1955-03-04 00:00:00",
	           "City": "London",
	           "Address": "14 Garrett Hill"
	   }, {
	       "EmployeeID": 6,
	           "FirstName": "Michael",
	           "LastName": "Suyama",
	           "ReportsTo": 5,
	           "Country": "UK",
	           "Title": "Sales Representative",
	           "HireDate": "1993-10-17 00:00:00",
	           "BirthDate": "1963-07-02 00:00:00",
	           "City": "London",
	           "Address": "Coventry House Miner Rd."
	   }, {
	       "EmployeeID": 7,
	           "FirstName": "Robert",
	           "LastName": "King",
	           "ReportsTo": 5,
	           "Country": "UK",
	           "Title": "Sales Representative",
	           "HireDate": "1994-01-02 00:00:00",
	           "BirthDate": "1960-05-29 00:00:00",
	           "City": "London",
	           "Address": "Edgeham Hollow Winchester Way"
	   }, {
	       "EmployeeID": 8,
	           "FirstName": "Laura",
	           "LastName": "Callahan",
	           "ReportsTo": 2,
	           "Country": "USA",
	           "Title": "Inside Sales Coordinator",
	           "HireDate": "1994-03-05 00:00:00",
	           "BirthDate": "1958-01-09 00:00:00",
	           "City": "Seattle",
	           "Address": "4726 - 11th Ave. N.E."
	   }, {
	       "EmployeeID": 9,
	           "FirstName": "Anne",
	           "LastName": "Dodsworth",
	           "ReportsTo": 5,
	           "Country": "UK",
	           "Title": "Sales Representative",
	           "HireDate": "1994-11-15 00:00:00",
	           "BirthDate": "1966-01-27 00:00:00",
	           "City": "London",
	           "Address": "7 Houndstooth Rd."
	   }];

	   //// prepare the data
	   var source = {
	       dataType: "json",
	       dataFields: [{
	           name: 'EmployeeID',
	           type: 'number'
	       }, {
	           name: 'ReportsTo',
	           type: 'number'
	       }, {
	           name: 'FirstName',
	           type: 'string'
	       }, {
	           name: 'LastName',
	           type: 'string'
	       }, {
	           name: 'Country',
	           type: 'string'
	       }, {
	           name: 'City',
	           type: 'string'
	       }, {
	           name: 'Address',
	           type: 'string'
	       }, {
	           name: 'Title',
	           type: 'string'
	       }, {
	           name: 'HireDate',
	           type: 'date'
	       }, {
	           name: 'BirthDate',
	           type: 'date'
	       }],
	       hierarchy: {
	           keyDataField: {
	               name: 'EmployeeID'
	           },
	           parentDataField: {
	               name: 'ReportsTo'
	           }
	       },
	       id: 'EmployeeID',
	       localData: employees
	   };

	   var dataAdapter = new $.jqx.dataAdapter(source);
	   // create Tree Grid
	   $("#treeGrid").jqxTreeGrid({
	       width: 800,
	       source: dataAdapter,
	       theme: 'energyblue',
	       exportSettings: {
	           columnsHeader: true,
	           collapsedRecords: true,
	           hiddenColumns: false
	       },
	       ready: function () {
	           $("#treeGrid").jqxTreeGrid('expandRow', '2');
	       },
	       columns: [{
	           text: 'First Name',
	           dataField: 'FirstName',
	           width: 150
	       }, {
	           text: 'Last Name',
	           dataField: 'LastName',
	           width: 120
	       }, {
	           text: 'Title',
	           dataField: 'Title',
	           width: 160
	       }, {
	           text: 'Birth Date',
	           dataField: 'BirthDate',
	           cellsFormat: 'd',
	           width: 120
	       }, {
	           text: 'Hire Date',
	           dataField: 'HireDate',
	           cellsFormat: 'd',
	           width: 120
	       }, {
	           text: 'Address',
	           dataField: 'Address',
	           width: 250
	       }, {
	           text: 'City',
	           dataField: 'City',
	           width: 120
	       }, {
	           text: 'Country',
	           dataField: 'Country',
	           width: 120
	       }]
	   });

	   $("#excelExport").jqxButton({
	       theme: 'energyblue'
	   });
	   $("#xmlExport").jqxButton({
	       theme: 'energyblue'
	   });
	   $("#csvExport").jqxButton({
	       theme: 'energyblue'
	   });
	   $("#tsvExport").jqxButton({
	       theme: 'energyblue'
	   });
	   $("#htmlExport").jqxButton({
	       theme: 'energyblue'
	   });
	   $("#jsonExport").jqxButton({
	       theme: 'energyblue'
	   });
	   $("#excelExport").click(function () {
	       $("#treeGrid").jqxTreeGrid('exportData', 'xls');
	   });
	   $("#xmlExport").click(function () {
	       $("#treeGrid").jqxTreeGrid('exportData', 'xml');
	   });
	   $("#csvExport").click(function () {
	       $("#treeGrid").jqxTreeGrid('exportData', 'csv');
	   });
	   $("#tsvExport").click(function () {
	       $("#treeGrid").jqxTreeGrid('exportData', 'tsv');
	   });
	   $("#htmlExport").click(function () {
	       $("#treeGrid").jqxTreeGrid('exportData', 'html');
	   });
	   $("#jsonExport").click(function () {
	       $("#treeGrid").jqxTreeGrid('exportData', 'json');
	   });


	   /*Segunda Tabla*/
	   var data3 = new Array();
	   	/*var data = new Array();
	   	

		var firstNames = [
		    "Andrew", "Nancy", "Shelley", "Regina", "Yoshi", "Antoni", "Mayumi", "Ian", "Peter", "Lars", "Petra", "Martin", "Sven", "Elio", "Beate", "Cheryl", "Michael", "Guylene"];

		var lastNames = [
		    "Fuller", "Davolio", "Burke", "Murphy", "Nagase", "Saavedra", "Ohno", "Devling", "Wilson", "Peterson", "Winkler", "Bein", "Petersen", "Rossi", "Vileid", "Saylor", "Bjorn", "Nodier"];

		var productNames = [
		    "Black Tea", "Green Tea", "Caffe Espresso", "Doubleshot Espresso", "Caffe Latte", "White Chocolate Mocha", "Cramel Latte", "Caffe Americano", "Cappuccino", "Espresso Truffle", "Espresso con Panna", "Peppermint Mocha Twist"];

		var priceValues = [
		    "2.25", "1.5", "3.0", "3.3", "4.5", "3.6", "3.8", "2.5", "5.0", "1.75", "3.25", "4.0"];

		for (var i = 0; i < 200; i++) {
		    var row = {};
		    var productindex = Math.floor(Math.random() * productNames.length);
		    var price = parseFloat(priceValues[productindex]);
		    var quantity = 1 + Math.round(Math.random() * 10);

		    row["firstname"] = firstNames[Math.floor(Math.random() * firstNames.length)];
		    row["lastname"] = lastNames[Math.floor(Math.random() * lastNames.length)];
		    row["productname"] = productNames[productindex];
		    row["price"] = price;
		    row["quantity"] = quantity;
		    row["total"] = price * quantity;

		    data[i] = row;
		}*/


		<?php  
			foreach ($miembros as $key => $value) { 
				//echo $value;
			?>		
			var row = {};
			row['nombre'] = "<?php echo $value['nombre'];?>";
			row['apellido'] = "<?php echo $value['apellido'];?>";
			row['email'] = "<?php echo $value['email'];?>";
			row['telefono'] = "<?php echo $value['telefono'];?>";
			row['estado'] = "<?php echo $value['nombreEstado'];?>";
			row['estadoCivil'] = "<?php echo $value['nombreCivil'];?>";
			row['estadoEducativo'] = "<?php echo $value['nombreEstadoEducativo'];?>";
			row['nivelEducativo'] = "<?php echo $value['nombreNivelEducativo'];?>";
			row['estadoLaboral'] = "<?php echo $value['nombreEstadoLaboral'];?>";
			row['sexo'] = "<?php echo $value['sexo'];?>";
			row['fechaNacimiento'] = "<?php echo $value['fechaNacimiento'];?>";
			row['fechaConversion'] = "<?php echo $value['fechaConversion'];?>";
			row['carreraProfesional'] = "<?php echo $value['nombreCarrera'];?>";
			row['celula'] = "<?php echo $value['celula'];?>";
			row['ministerio'] = "<?php echo $value['nombreMinisterio'];?>";
			data3[<?php echo $key; ?>] = row;
			
		<?php 	
			}
	
		 ?>  

 		var source2 = {
 			localData: data3,
 			dataType: "array",
 			dataFields: [{
 				name: "nombre",
 				type: "string"
 			}, {
 				name: "apellido",
 				type: "string"

 			}, {
 				name: "email",
 				type: "string"
 			}, {
 				name: "telefono",
 				type: "string"
 			}, {
 				name: "estado",
 				type: "string"
 			}, {
 				name: "estadoCivil",
 				type: "string"
 			}, {
 				name: "estadoEducativo",
 				type: "string"
 			}, {
 				name: "nivelEducativo",
 				type: "string"
 			}, {
 				name: "estadoLaboral",
 				type: "string"
 			}, {
 				name: "sexo",
 				type: "string"
 			}, {
 				name: "fechaNacimiento",
 				type: "string"
 			}, {
 				name: "fechaConversion",
 				type: "string"
 			}, {
 				name: "carreraProfesional",
 				type: "string"
 			}, {
 				name: "celula",
 				type: "string"
 			}, {
 				name: "ministerio",
 				type: "string"
 			}]
 		};

		/*var source2 = {
		    localData: data,
		    dataType: "array",
		    dataFields: [{
		        name: 'firstname',
		        type: 'string'
		    }, {
		        name: 'lastname',
		        type: 'string'
		    }, {
		        name: 'productname',
		        type: 'string'
		    }, {
		        name: 'quantity',
		        type: 'number'
		    }, {
		        name: 'price',
		        type: 'number'
		    }, {
		        name: 'total',
		        type: 'number'
		    }]
		};*/

		console.log(data3);

		var dataAdapter2 = new $.jqx.dataAdapter(source2);
		$("#table").jqxDataTable({
		    width: 1000,
		    height: 550,
		    theme: 'energyblue',
		    pageable: true,
		    exportSettings: {
		        columnsHeader: true,
		        hiddenColumns: false,
		        serverURL: null,
		        characterSet: null,
		        collapsedRecords: false,
		        recordsInView: true,
		        fileName: "jqxDataTable"
		    },
		    source: dataAdapter2,
		    columns: [{
		        text: 'Nombre',
		        dataField: 'nombre',
		        width: 100
		    }, {
		        text: 'apellido',
		        dataField: 'apellido',
		        width: 100
		    }, {
		        text: 'Email',
		        editable: false,
		        dataField: 'email',
		        width: 180
		    }, {
		        text: 'Telefono',
		        dataField: 'telefono',
		        width: 80
		        /*cellsAlign: 'right',
		        align: 'right'*/
		    }, {
		        text: 'Estado del Miembro',
		        dataField: 'estado',
		        width: 90
		        /*cellsAlign: 'right',
		        align: 'right',
		        cellsFormat: 'c2'*/
		    }, {
		        text: 'Estado Civil',
		        dataField: 'estadoCivil'
		       /* cellsAlign: 'right',
		        align: 'right',
		        cellsFormat: 'c2'*/
		    },{
		        text: 'Estado Educativo',
		        dataField: 'estadoEducativo',
		        width: 100
		    },{
		        text: 'Nivel Educativo',
		        dataField: 'nivelEducativo',
		        width: 100
		    },{
		        text: 'Estado Laboral',
		        dataField: 'estadoLaboral',
		        width: 100
		    },{
		        text: 'Sexo',
		        dataField: 'sexo',
		        width: 100
		    },{
		        text: 'Fecha Nacimiento',
		        dataField: 'fechaNacimiento',
		        width: 100
		    },{
		        text: 'Fecha Conversion',
		        dataField: 'fechaConversion',
		        width: 100
		    },{
		        text: 'Carrera Profesional',
		        dataField: 'carreraProfesional',
		        width: 200
		    },{
		        text: 'Celula',
		        dataField: 'celula',
		        width: 100
		    },{
		        text: 'ministerio',
		        dataField: 'ministerio',
		        width: 100
		    }]
		});

		$("#jqxbuttonxls").jqxButton({
		    theme: 'energyblue',
		    height: 30
		});

		$("#jqxbuttoncsv").jqxButton({
		    theme: 'energyblue',
		    height: 30
		});

		$("#jqxbuttonhtml").jqxButton({
		    theme: 'energyblue',
		    height: 30
		});

		$('#jqxbuttonxls').click(function () {
		    $("#table").jqxDataTable('exportData', 'xls');
		});

		$('#jqxbuttoncsv').click(function () {
		    $("#table").jqxDataTable('exportData', 'csv');
		});
		$('#jqxbuttonhtml').click(function () {
		    $("#table").jqxDataTable('exportData', 'html');
		});


	});

</script>
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<div class="col-md-12">
				<h1>Mantenimiento de Miembros</h1>
				<center class="cargando">
					<img id="cargando" src="<?php echo base_url(); ?>/application/img/loading.gif">
				</center>
			</div>
			<div class="col-md-12 errorFormularioMiembros">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Error!!!!</span> Verifica que los campos esten introducidos de la manera correcta. </h1>
					</div>
				</div>				
			</div>
			<div class="col-md-12 successFormularioMiembros">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Usuario!!!!</span> Insertado de manera correcta. </h1>
					</div>
				</div>				
			</div>
			<div class="col-md-12 cargandoContenedor">
				<div class="row BoxGeneralMembresia">
					<div class="col-md-2 menuLateralGenerador">
						<ul class="nav nav-pills nav-stacked">
							<li><a id="op1Ge" href="">Membresía Local</a></li>
							<li><a id="op2Ge" href="">Agregar miembro</a></li>
							<li><a id="op3Ge" href="">Agregar Asistencia</a></li>
							<li><a id="op4Ge" href="">Actividades</a></li>
							<li><a id="op5Ge" href="">5</a></li>
							<li><a id="op6Ge" href="">6</a></li>
							<li><a id="op7Ge" href="">7</a></li>
						</ul>
					</div>

					<div class="col-md-8 contenidoGenerador">
						<div class="op1Ge">
							<div id="treeGrid"></div>
							<div style='margin-top: 20px;'>
							    <div style='float: left;'>
							        <input type="button" value="Export to Excel" id='excelExport' />
							        <br />
							        <br />
							        <input type="button" value="Export to XML" id='xmlExport' />
							    </div>
							    <div style='margin-left: 10px; float: left;'>
							        <input type="button" value="Export to CSV" id='csvExport' />
							        <br />
							        <br />
							        <input type="button" value="Export to TSV" id='tsvExport' />
							    </div>
							    <div style='margin-left: 10px; float: left;'>
							        <input type="button" value="Export to HTML" id='htmlExport' />
							        <br />
							        <br />
							        <input type="button" value="Export to JSON" id='jsonExport' />
							    </div>
							</div>
						</div>
						<div class="op2Ge">
							<div id="table"></div>
							<div class="row">
								<div class="col-md-4">
									<input type="button" style="margin: 20px;" id="jqxbuttonxls" value="Export Data to XLS" />
								</div>
								<div class="col-md-4">
									<input type="button" style="margin: 20px;" id="jqxbuttoncsv" value="Export Data to CSV" />
								</div>
								<div class="col-md-4">
									<input type="button" style="margin: 20px;" id="jqxbuttonhtml" value="Export Data to HTML" />
								</div>
							</div>
							
						</div>
						<div class="op3Ge">
							3
						</div>
						<div class="op4Ge">
							4
						</div>
						<div class="op5Ge">
							5
						</div>
						<div class="op6Ge">
							6
						</div>
						<div class="op7Ge">
							7
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>