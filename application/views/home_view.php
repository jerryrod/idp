	<!--------------------------------Carrusel---------------------------->
	<div class="carruselcito">
		<div id="carousel-id" class="carousel slide" data-ride="carousel">
		    <ol class="carousel-indicators">
		        <li data-target="#carousel-id" data-slide-to="0" class=""></li>
		        <li data-target="#carousel-id" data-slide-to="1" class=""></li>
		        <li data-target="#carousel-id" data-slide-to="2" class="active"></li>
		    </ol>
		    <div class="carousel-inner">
		        <div class="item">
		            <img data-src="<?php echo base_url(); ?>application/img/1.jpg" alt="First slide" src="<?php echo base_url(); ?>/application/img/1.jpg">
		            <div class="container">
		                <div class="carousel-caption">
		                    <h1>Servicios</h1>
		                    <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
		                    <p><a class="btn btn-lg btn-primary" href="" role="button">Ver Más</a></p>
		                </div>
		            </div>
		        </div>
		        <div class="item">
		            <img data-src="<?php echo base_url(); ?>application/img/2.jpg" alt="Second slide" src="<?php echo base_url(); ?>/application/img/2.jpg">
		            <div class="container">
		                <div class="carousel-caption">
		                    <h1>Multimedia</h1>
		                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
		                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Ver más</a></p>
		                </div>
		            </div>
		        </div>
		        <div class="item active">
		            <img data-src="<?php echo base_url(); ?>application/img/3.jpg" alt="Third slide" src="<?php echo base_url(); ?>/application/img/3.jpg">
		            <div class="container">
		                <div class="carousel-caption">
		                    <h1>Nosotros</h1>
		                    <p>Creemos que la verdadera felicidad se encuentra finalmente en una relación íntima y personal con Jesucristo.</p>
		                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Ver más</a></p>
		                </div>
		            </div>
		        </div>
		    </div>
		    <a style="display:none" class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
		    <a style="display:none" class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
		</div>
	</div>
</div>

<!--------------------------------   Liderazgo  ---------------------------->

<div class="row liderazgo">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<h1 class="col-md-8">Liderazgo</h1>					
		</div>
		<div class="row">
			<hr class="col-md-11">
		</div>	
		<div class="row" style="margin-top:2%;">
			<div class="col-md-2">
				<img src="<?php echo base_url(); ?>/application/img/logo.png">
			</div>
			<div class="col-md-6">
				<div class="row">
					<p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor 
					(N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen.</p>
				</div>
				<div class="row">
					<a class="col-md-4 pull-right" href="<?php echo base_url(); ?>index.php/liderazgo">Ver Liderazgo Local</a>	
				</div>
			</div>
			<div class="col-md-4">
				<img style="width:100%;margin-top: -12%;" src="<?php echo base_url(); ?>application/img/zapatero.jpg">
			</div>
		</div>
	</div>
</div>

<!--------------------------------   Nosotros    ---------------------------->

<div class="row nosotros">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<h1>Nosotros</h1>
		</div>
		<div class="row">
			<p>La Iglesia de Dios de la Profecía es firme en su compromiso con la fe cristiana ortodoxa. Afirmamos que hay un solo Dios que existe eternamente en tres personas: Padre, Hijo y Espíritu Santo. 
			Creemos en la deidad de Cristo, su nacimiento virginal, Su vida sin pecado, los milagros físicos que realizó,</p>
		</div>
	</div>
</div>

<!--------------------------------   Noticias    ---------------------------->

<div class="row noticias">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<h1 class="col-md-8">Noticias</h1>	
			<a class="col-md-2 pull-right estiloButon" href="<?php echo base_url(); ?>index.php/noticias">Más Noticias</a>		
		</div>
		<div class="row">
			<hr class="col-md-11">
		</div>		
		<div class="row">
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $noticias[0]['id']; ?>"><img style="width:100%;border-radius: 10px;max-height: 190px;" class="row" src="<?php echo base_url(); ?>application/noticias/<?php if(isset($noticias[0]['imagen'])){echo $noticias[0]['imagen'];}else{echo "COGOP_Logo.jpg";} ?>">
				<p class="row"><?php echo $noticias[0]['titulo'] ?></p></a>
			</div>
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $noticias[1]['id']; ?>"><img style="width:100%;border-radius: 10px;max-height: 190px;" class="row" src="<?php echo base_url(); ?>application/noticias/<?php if(isset($noticias[1]['imagen'])){echo $noticias[1]['imagen'];}else{echo "COGOP_Logo.jpg";} ?>">
				<p class="row"><?php echo $noticias[1]['titulo'] ?></p></a>
			</div>
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $noticias[2]['id']; ?>"><img style="width:100%;border-radius: 10px;max-height: 190px;" class="row" src="<?php echo base_url(); ?>application/noticias/<?php if(isset($noticias[2]['imagen'])){echo $noticias[2]['imagen'];}else{echo "COGOP_Logo.jpg";} ?>">
				<p class="row"><?php echo $noticias[2]['titulo'] ?></p></a>
			</div>
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $noticias[3]['id']; ?>"><img style="width:100%;border-radius: 10px;max-height: 190px;" class="row" src="<?php echo base_url(); ?>application/noticias/<?php if(isset($noticias[3]['imagen'])){echo $noticias[3]['imagen'];}else{echo "COGOP_Logo.jpg";} ?>">
				<p class="row"><?php echo $noticias[3]['titulo'] ?></p></a>
			</div>
		</div>
	</div>
</div>
<!--------------------------------   Multimedia    ---------------------------->

<div class="row multimedia">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<h1 class="col-md-8">Multimedia</h1>	
			<a class="col-md-2 pull-right estiloButon" href="<?php echo base_url(); ?>index.php/multimedia">Ir a Multimedia</a>		
		</div>
		<div class="row">
			<hr class="col-md-11">
		</div>
		<div class="row">
			<div class="col-md-4">
				<img src="<?php echo base_url(); ?>application/img/musica.png">
			</div>
			<div class="col-md-4">
				<img src="<?php echo base_url(); ?>application/img/video.png">
			</div>
			<div class="col-md-4">
				<img src="<?php echo base_url(); ?>application/img/fot.png">
			</div>
		</div>
	</div>
</div>

<div class="row redes">
	<div class="col-md-12">
		<div class="row fotosInstagram">
			<div class="col-md-10 col-md-offset-1"  >
				<div class="row">
					<h1>Instragram</h1>
				</div>
				<div class="row" id="fotoIns">
					
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="row videosYoutube">
			<div class="col-md-10 col-md-offset-1"  >
				<div class="row">
					<h1>Youtube</h1>
				</div>
				<div class="row" id="videoYT">
		
				</div>
			</div>
		</div>
	</div>
	<div id="TW">
		
	</div>
	<div class="col-md-12">
		<div class="row fotosFacebook">
			<div class="col-md-10 col-md-offset-1"  >
				<div class="row">
					<h1>Facebook</h1>
				</div>
				<div class="row" id="facebook">
					
				</div>
			</div>
		</div>
	</div>
	
</div>