
<main>
        <section class="camera_container">
          <div id="camera" class="camera_wrap">
            <div data-src="<?php echo base_url(); ?>application/images/banner1.jpg">
              <!--<div class="camera_caption fadeIn">
                <div class="container">
                  <div class="row">
                    <div class="preffix_6 grid_6">Vasos de Honra para bendecir!</div>
                  </div>
                </div>
              </div>-->
            </div>
            <div data-src="<?php echo base_url(); ?>application/images/banner3.png">
              <!-- <div class="camera_caption fadeIn">
                <div class="container">
                  <div class="row">
                   <div class="preffix_6 grid_6">The best strategies to attract new business</div>
                  </div>
                </div>
              </div>-->
            </div>
            <div data-src="<?php echo base_url(); ?>application/images/banner21.jpg">
              <!--<div class="camera_caption fadeIn">
                <div class="container">
                  <div class="row">
                    <div class="preffix_6 grid_6">A wide range of global business information</div>
                  </div>
                </div>
              </div>-->
            </div>
            <div data-src="<?php echo base_url(); ?>application/images/banner4.png">
               <!--<div class="camera_caption fadeIn">
                <div class="container">
                  <div class="row">
                   <div class="preffix_6 grid_6">A wide range of global business information</div>
                  </div>
                </div>
              </div>-->
            </div>
          </div>
        </section>
       
        <section>
          <div class="container banner_wr">
            <ul class="banner">
              <li>
                <!--<div class="fa-globe"></div>-->
                <img src="<?php echo base_url(); ?>/application/images/LogoNacionalIPD.png" alt="">
                <h3>WebSite<br/>COGOP-USA</h3>
                <p>Web oficial que contiene preguntas frecuentes , la doctrina, la historia , el buscador de la iglesia local , y noticias.</p><a target="_blank" href="http://cogop.org/"></a>
              </li>
              <li>
                <!--<div class="fa-lightbulb-o"></div>-->
                <img src="<?php echo base_url(); ?>/application/images/facebook.png" alt="">
                <h3>Campamento<br/>Nacional</h3>
                <p>Fan Page Oficial en FACEBOOK del Campamento nacional de Jóvenes de La Iglesia de Dios de La Profecia.</p><a target="_blank" href="https://es-la.facebook.com/campamentosidp"></a>
              </li>
              <li>
               <!-- <div class="fa-cog"></div>-->
                <img src="<?php echo base_url(); ?>/application/images/LogoNacionalIPD.png" alt="">
                <h3>WebSite<br/>COGOP-RD</h3>
                <p>La Iglesia de Dios de la Profecía, en la República Dominicana, lucha por fortalecer la visión en la misión encomendada por nuestro Señor y Salvador Jesucristo.</p><a target="_blank" href="http://idprofeciard.com/"></a>
              </li>
              <li>
                <!--<div class="fa-briefcase"></div>-->
                <img src="<?php echo base_url(); ?>/application/images/facebook.png" alt="">
                <h3>COGOP<br/>RD</h3>
                <p>La Iglesia de Dios de la Profecia es una de las muchas iglesias que Dios está utilizando para cambiar el mundo aquí en Republica dominicana. </p><a target="_blank" href="https://es-la.facebook.com/Iglesia-de-Dios-de-la-Profecia-en-Republica-Dominicana-131484126893672/"></a>
              </li>
            </ul>
          </div>
        </section>
         <section>
          <div class="container">
            <hr style="margin-top:5%;height:3px;background-color:#62bfe6;">
          </div>
        </section>
        <section class="well ins1">
          <div class="container NoticiasHome hr">
            <div class="row">
              <div class="grid_10">
                <h1>Noticias</h1>
              </div> 
              <div class="grid_2">
                <a href="<?php echo base_url(); ?>index.php/noticias" class="btn" style="float: right;">Ver Todas</a>
              </div>                
            </div>
            <ul class="row product-list">
              <li class="grid_6">
                <div class="box wow fadeInRight">
                  <div class="box_aside">
                    <!--<div class="icon fa-comments"></div>-->
                    <img src="<?php echo base_url(); ?>application/noticias/<?php if(isset($noticias[0]['imagen'])){echo $noticias[0]['imagen'];}else{echo "COGOP_Logo.jpg";} ?>" alt="">
                  </div>
                  <div class="box_cnt__no-flow">
                    <h3><a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $noticias[0]['id']; ?>"><?php echo $noticias[0]['titulo'] ?> </a></h3>
                    <p><?php echo substr($noticias[0]['descripcion'],0,115).'...' ?> </p>
                  </div>
                  <div>
                    <a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $noticias[0]['id']; ?>" class="btn-noticias">Ver Más</a>
                  </div>
                </div>
                <hr>
                <div data-wow-delay="0.2s" class="box wow fadeInRight">
                   <div class="box_aside">
                    <!--<div class="icon fa-comments"></div>-->
                    <img src="<?php echo base_url(); ?>application/noticias/<?php if(isset($noticias[1]['imagen'])){echo $noticias[1]['imagen'];}else{echo "COGOP_Logo.jpg";} ?>" alt="">
                  </div>
                  <div class="box_cnt__no-flow">
                    <h3><a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $noticias[1]['id']; ?>"><?php echo $noticias[1]['titulo'] ?> </a></h3>
                    <p><?php echo substr($noticias[1]['descripcion'],0,115).'...' ?> </p>
                  </div>
                  <div>
                    <a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $noticias[1]['id']; ?>" class="btn-noticias">Ver Más</a>
                  </div>
                </div>
              </li>
              <li class="grid_6">
                <div data-wow-delay="0.3s" class="box wow fadeInRight">
                   <div class="box_aside">
                    <!--<div class="icon fa-comments"></div>-->
                    <img src="<?php echo base_url(); ?>application/noticias/<?php if(isset($noticias[2]['imagen'])){echo $noticias[2]['imagen'];}else{echo "COGOP_Logo.jpg";} ?>" alt="">
                  </div>
                  <div class="box_cnt__no-flow">
                    <h3><a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $noticias[2]['id']; ?>"><?php echo $noticias[2]['titulo'] ?> </a></h3>
                    <p><?php echo substr($noticias[2]['descripcion'],0,115).'...' ?> </p>
                  </div>
                  <div>
                    <a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $noticias[2]['id']; ?>" class="btn-noticias">Ver Más</a>
                  </div>
                </div>
                <hr>
                <div data-wow-delay="0.4s" class="box wow fadeInRight">
                   <div class="box_aside">
                    <!--<div class="icon fa-comments"></div>-->
                    <img src="<?php echo base_url(); ?>application/noticias/<?php if(isset($noticias[3]['imagen'])){echo $noticias[3]['imagen'];}else{echo "COGOP_Logo.jpg";} ?>" alt="">
                  </div>
                  <div class="box_cnt__no-flow">
                    <h3><a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $noticias[3]['id']; ?>"><?php echo $noticias[3]['titulo'] ?> </a></h3>
                    <p><?php echo substr($noticias[3]['descripcion'],0,115).'...' ?> </p>
                  </div>
                  <div>
                    <a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $noticias[3]['id']; ?>" class="btn-noticias">Ver Más</a>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </section>
        <section class="well1">
          <div class="container">
            <div class="row">
              <div class="grid_4">
                <h2 class="sobreNosotros2">Sobre Nosotros</h2><img src="<?php echo base_url(); ?>application/images/page-1_img01.jpg" alt="">
                <p>La Iglesia de Dios de la Profecia es una de las muchas iglesias que Dios está utilizando para cambiar el mundo aquí en Republica dominicana. Estamos profundamente comprometidos a proclamar las buenas nuevas de que Jesús es la respuesta para nuestro mundo de hoy.</p>
                <p>Nuestra visión es enorme y nuestra misión es clara. Queremos marchar en contra de Satanás y recuperar aquellos que han sido atrapados por sus mentiras. Creemos que la verdadera felicidad se encuentra finalmente en una relación íntima y personal con Jesucristo.</p>
                <a href="<?php echo base_url(); ?>index.php/nosotros" class="btn">Ver Más</a>
              </div>
             <!-- <div class="grid_4">
                <h2>Servicios Medicos</h2>
                <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                <ul class="marked-list">
                  <li><a href="#">Lorem ipsum dolor sit amet </a></li>
                  <li><a href="#">Conse ctetur adipisicing</a></li>
                  <li><a href="#">Elit sed do eiusmod tempor</a></li>
                  <li><a href="#">Incididunt ut labore</a></li>
                  <li><a href="#">Et dolore magna aliqua</a></li>
                  <li><a href="#">Ut enim ad minim veniam</a></li>
                  <li><a href="#">Quis nostrud exercitation</a></li>
                  <li><a href="#">Incididunt ut labore</a></li>
                  <li><a href="#">Et dolore magna aliqua</a></li>
                </ul><a href="#" class="btn">Ver Más</a>
              </div>-->
              <div class="grid_4">
                <h2>Ojetivos Especificos Generales</h2>
                <p>Estamos conscientes de que la improvisación es la madre del impedimento del logro de las metas, nuestra intención es utilizar los principios metodológicos en la planeación, organización y ejecución de nuestros proyectos a fin de llegar donde nos hemos propuesto con la ayuda de Dios.</p>
                <br>
                <p>Entre nuestros objetivos especificos se destacan:</p>
                
                <ul class="marked-list">
                  <li><a href="#">El desarrollo integral de cada iglesia local.</a></li>
                  <li><a href="#">Obedeciendo el mandato de nuestro Señor Jesucristo de ”Id y haced Discípulos”.</a></li>
                  <li><a href="#">Fortalecer los niveles educativos.</a></li>
                  <li><a href="#">Desarrollar nuestras infraestructuras nacionales y locales.</a></li>   
                  <li><a href="#">Procurar la santidad del vínculo matrimonial.</a></li> 
                  <li><a href="#">Desarrollar familias cristianas fuertes y amorosas.</a></li>              
                  <li>
                    <p class="grid_2" id="supervisor">Obispo Flavio Rosario</p>
                    <p class="grid_2" id="supervisorCargo">(Supervisor Nacional)</p>
                  </li>
                  
                </ul><a href="#" class="btn">Ver Más</a>
                <script type='text/javascript'>
                  $('.marked-list li>a').click(function (e){
                    e.preventDefault();
                  });
                </script>
              </div>
              <div class="grid_4">
                <div class="info-box">
                  <h2 class="fa-comment">Actividades</h2>
                  <hr>
                  <h3>Regulares:</h3>
                  <dl>
                    <dt>Domingo:</dt>
                    <dd>10am-12:30pm</dd>
                  </dl>
                  <dl>
                    <dt>Miercoles:</dt>
                    <dd>7:30pm-9:30pm</dd>
                  </dl>
                  <dl>
                    <dt>Viernes:</dt>
                    <dd>7pm-9:30pm</dd>
                  </dl>
                  <hr>
                  <!--Acatividades Irregulares -->
                   <h3>Especiales:</h3>
                   <?php if($actividades != null){
                    foreach ($actividades as $key => $value) { 
                      ?>
                      <dl>
                        <h4><?php echo $value['nombre']; ?></h4>                    
                        <dt><?php echo $value['nombreDia'].':'; ?></dt>
                        <dd><?php echo $value['hora']; ?></dd>
                        <dd style="margin-left: 10%;"><a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/actividades/ver_actividad_irregular/<?php echo $value['id']; ?>"><p id='animacionInformacion'>Más Información</p></a></dd>
                      </dl>                     
                    <?php }
                    }else{ ?>
                       <h4>N/A</h4>  
                    <?php } ?>
                  <hr>
                  <h3>Mas Información:</h3>
                  <dl>
                    <dt>800-234-6789</dt>
                  </dl>
                </div>
                <div class="owl-carousel">
                  <div class="item">
                    <blockquote class="box">
                      <div class="box_aside"><img src="<?php echo base_url(); ?>application/images/page-1_img02.jpg" alt=""></div>
                      <div class="box_cnt__no-flow">
                        <p>
                          <q>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</q>
                        </p>
                        <cite><a href="#">Silvia Lake</a></cite>
                      </div>
                    </blockquote>
                  </div>
                  <div class="item">
                    <blockquote class="box">
                      <div class="box_aside"><img src="<?php echo base_url(); ?>application/images/page-1_img03.jpg" alt=""></div>
                      <div class="box_cnt__no-flow">
                        <p>
                          <q>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</q>
                        </p>
                        <cite><a href="#">Santo Catalino</a></cite>
                      </div>
                    </blockquote>
                  </div>
                  <div class="item">
                    <blockquote class="box">
                      <div class="box_aside"><img src="<?php echo base_url(); ?>application/images/page-1_img04.jpg" alt=""></div>
                      <div class="box_cnt__no-flow">
                        <p>
                          <q>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</q>
                        </p>
                        <cite><a href="#">Soleida Beltre</a></cite>
                      </div>
                    </blockquote>
                  </div>
                  <div class="item">
                    <blockquote class="box">
                      <div class="box_aside"><img src="<?php echo base_url(); ?>application/images/page-1_img05.jpg" alt=""></div>
                      <div class="box_cnt__no-flow">
                        <p>
                          <q>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</q>
                        </p>
                        <cite><a href="#">Johanni Saldaña</a></cite>
                      </div>
                    </blockquote>
                  </div>
                </div>
              </div>
            </div>
              <div id="fotoIns" class="row hr">
                <div class="grid_10">
                  <img id="insLogo" src="<?php echo base_url() ?>application/images/Instagram.png" alt=""><br>
                </div>                
              </div>
              <div class="row hr" id="facebook">
                <div class="grid_10">
                  <img id="faceLogo" src="<?php echo base_url() ?>application/images/facebook_prodvijenie_sayta.png" alt=""><br>
                </div>   
              </div>
          </div>
        </section>
      </main>