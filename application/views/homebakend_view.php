	<!--<script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/scripts/jquery-1.11.1.min.js"></script>	-->
  

	<link rel="stylesheet" href="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxdraw.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxchart.core.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxdata.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxtooltip.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>application/js/JQWidgets/jqwidgets/jqxbulletchart.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

        	 function displayClusterMetrics() {
                var metrics =
                [
                    {
                    	//Escuela Biblica Adultos
                        name: 'Escuela Biblica Adultos',
                        value: <?php echo $AdultosPresentes->cantidadTotal; ?>,//Miembros que asistieron
                        max: <?php echo $cantidadMiembrosAdultos->cantidadTotal; ?> //Total de Miembros que tiene la iglesia
                    },
                    {
                    	//Escuela Biblica Jovenes
                        name: 'Escuela Biblica Jovenes',
                        value: <?php echo $JovenesPresentes->cantidadTotal; ?>,//Personas que asistieron
                        max: <?php echo $cantidadMiembrosJovenes->cantidadTotal; ?>//Total de personas
                    },
                    {
                    	//Culto Viernes
                        name: 'Culto Viernes',
                        value: <?php echo $totalPresentesViernes->cantidadTotal; ?>,//Personas que asistieron
                        max: <?php echo $cantidadMiembros->cantidadTotal; ?>//Total de personas
                    },
                    {
                    	//Culto Domingo
                        name: 'Culto Domingo',
                        value: <?php echo $totalPresentesDomingo->cantidadTotal; ?>,//Personas que asistieron
                        max: <?php echo $cantidadMiembros2->cantidadTotal; ?> //Aqui debe ir el total de lo que estoy evaluando arriba por ejemple fueron 47 personas de 200;
                    }
                ];

                for (var i = 0; i < metrics.length; i++) {

                    var data = [];

                    data.push({ text: 'Presentes', value: metrics[i].value }); // current
                    data.push({ text: 'Ausentes', value: metrics[i].max - metrics[i].value }); // remaining

                    var settings = {
                        title: metrics[i].name,
                        description: '',
                        enableAnimations: true,
                        showLegend: false,
                        showBorderLine: true,
                        backgroundColor: '#FAFAFA',
                        padding: { left: 5, top: 5, right: 5, bottom: 5 },
                        titlePadding: { left: 5, top: 5, right: 5, bottom: 5 },
                        source: data,
                        showToolTips: true,
                        seriesGroups:
                        [
                            {
                                type: 'donut',
                                useGradientColors: false,
                                series:
                                    [
                                        {
                                            showLabels: false,
                                            enableSelection: true,
                                            displayText: 'text',
                                            dataField: 'value',
                                            labelRadius: 120,
                                            initialAngle: 90,
                                            radius: 60,
                                            innerRadius: 50,
                                            centerOffset: 0
                                        }
                                    ]
                            }
                        ]
                    };

                    var selector = '#chartContainer' + (i + 1).toString();

                    var valueText = metrics[i].value.toString();

                    settings.drawBefore = function (renderer, rect) {
                        sz = renderer.measureText(valueText, 0, { 'class': 'chart-inner-text' });

                        renderer.text(
                        valueText,
                        rect.x + (rect.width - sz.width) / 2,
                        rect.y + rect.height / 2,
                        0,
                        0,
                        0,
                        { 'class': 'chart-inner-text' }
                        );
                    }

                    $(selector).jqxChart(settings);
                    $(selector).jqxChart('addColorScheme', 'customColorScheme', ['#00BAFF', '#EDE6E7']);
                    $(selector).jqxChart({ colorScheme: 'customColorScheme' });
                }
            }


            

            function asistenciaPorDias () {
            	var sampledata2 = new Array();
            	// prepare chart data
	            var sampleData = [
	                { Country: 'China', Population: 1347350000, Percent: 19.18 },
	                { Country: 'India', Population: 1210193422, Percent: 17.22 },
	                { Country: 'USA', Population: 313912000, Percent: 4.47 },
	                { Country: 'Indonesia', Population: 237641326, Percent: 3.38 },
	                { Country: 'Brazil', Population: 192376496, Percent: 2.74}
	            ];

	            <?php  
					foreach ($actividadesSemanales as $key => $value) { 
						//echo $value;
					?>		
					var row = {};
					row['Cantidad_Presentes'] = "<?php echo $value['Cantidad_Presentes'];?>";
					row['idEvento'] = "<?php echo $value['idEvento'];?>";
					row['total_miembros'] = "<?php echo $value['total_miembros'];?>";
					row['Actividad'] = "<?php echo $value['nombreActividadRegular'];?>";
					row['fecha'] = "<?php echo $value['fecha'];?>";
					sampledata2[<?php echo $key; ?>] = row;					
				<?php 	
					}
			
				 ?>  

				 

	            // prepare jqxChart settings
	            var settings = {
	                title: "Comparacion de Asistencia por actividades",
	                description: "Semanal",
	                showLegend: true,
	                enableAnimations: true,
	                padding: { left: 20, top: 5, right: 20, bottom: 5 },
	                titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
	                source: sampledata2,
	                xAxis:
	                {
	                    //dataField: 'Country',
	                    dataField: 'Actividad',
	                    gridLines: { visible: true },
	                    flip: false,
	                    text:'Actividad'
	                },
	                valueAxis:
	                {
	                    flip: false,
	                    labels: {
	                        visible: true,
	                        //datafields:'Cantidad_Presentes'
	                        /*formatFunction: function (value) {
	                            return parseInt(value / 1);
	                        }*/
	                    },
	                    minValue: 0,
	                    maxValue: "<?php echo $cantidadMiembros->cantidadTotal; ?>"
	                   // description: 'Index Value'
	                   //console.log("<?php echo $cantidadMiembros->cantidadTotal ?>")
	                },
	                colorScheme: 'scheme01',
	                seriesGroups:
	                    [
	                        {
	                            type: 'column',
	                            orientation: 'vertical',
	                            columnsGapPercent: 80,
	                           // toolTipFormatSettings: { thousandsSeparator: ',' },
	                            series: [
	                                    //{ dataField: 'Population', displayText: 'Population (millions)' }
	                                    { dataField: 'Cantidad_Presentes', displayText: 'Presentes' }
	                                ]
	                        }
	                    ]
	            };

	            // setup the chart
            	$('#chartContainer5').jqxChart(settings);
            }


            function ofrendaEscuelaBiblica() {
            	$("#jqxBulletChart").jqxBulletChart({
	                width: 600,
	                height: 160,
	                barSize: "40%",
	                title: "<?php echo $actividadesSemanales[0]['nombreActividadRegular'].' </br> '.$actividadesSemanales[0]['fecha']; ?>",
	                description: "(Ofrenda de la Actividad)",
	                ranges: [
	                    { startValue: 0, endValue: 1000, color: "#FFA81E" /*,opacity: 0.5 */},
	                    { startValue: 1000, endValue: 2000, color: "#b2ea00" /*,opacity: 0.3*/ },
	                    { startValue: 2000, endValue: 4000, color: "#287cdf" /*,opacity: 0.1*/ }
	                ],
	                //         ofrenda            culto que se hizo     
	                pointer: { value: "<?php echo $actividadesSemanales[0]['ofrenda']; ?>", label: "<?php echo $actividadesSemanales[0]['nombreActividadRegular']. ' (Actual)'; ?>", size: "10%", color: "red" },
	                target: { value: "<?php echo $actividadesSemanales2[0]['ofrenda']; ?>", label: "<?php echo $actividadesSemanales2[0]['nombreActividadRegular'] . ' (Anterior)'; ?>", size: 4, color: "#060101" },
	                ticks: { position: "near", interval: 500, size: 10 },
	                labelsFormat: "c",
	                showTooltip: true
	            });
            }

            function ofrendaViernes() {
            	$("#jqxBulletChart1").jqxBulletChart({
	                width: 600,
	                height: 160,
	                barSize: "40%",
	                title: "<?php echo $actividadesSemanales[1]['nombreActividadRegular'].' </br>'.$actividadesSemanales[1]['fecha']; ?>",
	                description: "(Ofrenda de la Actividad)",
	                ranges: [
	                    { startValue:0 , endValue: 1000, color: "#FFA81E" /*,opacity: 0.5*/ },
	                    { startValue: 1000, endValue: 2000, color: "#b2ea00" /*,opacity: 0.3*/ },
	                    { startValue: 2000, endValue: 4000, color: "#287cdf" /*,opacity: 0.1*/ }
	                ],
	                //         ofrenda            culto que se hizo     
	                pointer: { value: "<?php echo $actividadesSemanales[1]['ofrenda']; ?>", label: "<?php echo $actividadesSemanales[1]['nombreActividadRegular']. ' (Actual)'; ?>", size: "10%", color: "red" },
	                target: { value: "<?php echo $actividadesSemanales2[1]['ofrenda'];  ?>", label: "<?php echo $actividadesSemanales2[1]['nombreActividadRegular']. ' (Anterior)'; ?>", size: 4, color: "#060101" },
	                ticks: { position: "near", interval: 500, size: 10 },
	                labelsFormat: "c",
	                showTooltip: true
	            });
            }

            function ofrendaDomingo() {
            	$("#jqxBulletChart2").jqxBulletChart({
	                width: 600,
	                height: 160,
	                barSize: "40%",
	                title: "<?php echo $actividadesSemanales[2]['nombreActividadRegular'].' </br>'.$actividadesSemanales[2]['fecha']; ?>",
	                description: "(Ofrenda de la Actividad)",
	                ranges: [
	                    { startValue: 0, endValue: 1000, color: "#FFA81E" /*,opacity: 0.5*/ },
	                    { startValue: 1000, endValue: 2000, color: "#b2ea00" /*,opacity: 0.3*/ },
	                    { startValue: 2000, endValue: 4000, color: "#287cdf" /*,opacity: 0.1*/ }
	                ],
	                //         ofrenda            culto que se hizo     
	                pointer: { value: "<?php echo $actividadesSemanales[2]['ofrenda']; ?>", label: "<?php echo $actividadesSemanales[2]['nombreActividadRegular']. ' (Actual)'; ?>", size: "10%", color: "red" },
	                target: { value: "<?php echo $actividadesSemanales2[2]['ofrenda']; ?>", label: "<?php echo $actividadesSemanales2[2]['nombreActividadRegular']. ' (Anterior)'; ?>", size: 4, color: "#060101" },
	                ticks: { position: "near", interval: 500, size: 10 },
	                labelsFormat: "c",
	                showTooltip: true
	            });
            }



            displayClusterMetrics();
            asistenciaPorDias();
            ofrendaEscuelaBiblica();
            ofrendaViernes();
            ofrendaDomingo();
            /*

			SELECT count(*),a.idEvento, cm.cantidad as total_miembros, ar.nombreActividadRegular 
			FROM `asistencia`a 
			left join ( select count(*) cantidad,idEvento from asistencia group by idEvento ) cm on a.idEvento = cm.idEvento 
			left join actividades ac on ac.id = a.idEvento 
			left join actividadesRegulares ar on ac.nombreActividad = ar.id 
			WHERE estado_asistencia = 'presente' 
			group by a.idEvento


            */

        });
    </script>
	<div class="container-fluid dashboard">
		<div class="row" style="margin-top:3%;">
			<?php //var_dump($totalPresentesViernes); ?>
			<div class="col-md-3">
				<div id='chartContainer1' style="width: 100% !important; height: 180px;"></div>
			</div>
			<div class="col-md-3">
				<div id='chartContainer2' style="width: 100%; height: 180px;"></div>
			</div>
			<div class="col-md-3">
				<div id='chartContainer3' style="width: 100%; height: 180px;"></div>
			</div>
			<div class="col-md-3">
				<div id='chartContainer4' style="width: 100%; height: 180px;"></div>
			</div>
		</div>
	    <div class="row" style="margin-top:3%;">	    	
	    	<div class="col-md-6 margenRow">
	    		<div class="row">
	    			<div id="jqxBulletChart"></div>
	    		</div>
	    		<div class="row">
	    			<div id="jqxBulletChart1"></div>
	    		</div>
	    		<div class="row">
	    			<div id="jqxBulletChart2"></div>
	    		</div>	    		
	    	</div>
	    	<div class="col-md-6">
	    		<div id="chartContainer5" style="width:100%; height:440px;"></div>
	    	</div>	    	
	    </div>
	</div>

    


<!--<div class="container">
	<div class="row">
	<div class="col-md-10 col-md-offset-1 contenedorHomeBackend">
		<div class="row">
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/miembros">
					<div class="row">
						<img src="<?php echo base_url();?>/application/images/membership.jpg" alt="" class="col-md-12">
					</div>
					<div class="row">
						<span>Membresia</span>
					</div>	
				</a>			
			</div>
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/gcrecimiento">
					<div class="row">
						<img src="<?php echo base_url();?>/application/images/miembrosBackend.jpg" alt="" class="col-md-12">
					</div>
					<div class="row">
						<span>Celula</span>
					</div>	
				</a>			
			</div>
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/noticiasBackend">
					<div class="row">
						<img src="<?php echo base_url();?>/application/images/icono-noticias.png" alt="" class="col-md-12">
					</div>
					<div class="row">
						<span>Noticias</span>
					</div>	
				</a>			
			</div>
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/finanzas">
					<div class="row">
						<img src="<?php echo base_url();?>/application/images/finan.jpg" alt="" class="col-md-12">
					</div>
					<div class="row">
						<span>Finanzas</span>
					</div>	
				</a>			
			</div>			
		</div>
		<div class="row">
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/ministerios">
					<div class="row">
						<img src="<?php echo base_url();?>/application/images/LIDERAZGO_1.jpg" alt="" class="col-md-12">
					</div>
					<div class="row">
						<span>Ministerios</span>
					</div>	
				</a>			
			</div>
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/estadistica">
					<div class="row">
						<img src="<?php echo base_url();?>/application/images/estadistica.jpg" alt="" class="col-md-12">
					</div>
					<div class="row">
						<span>Estadistica</span>
					</div>	
				</a>			
			</div>
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/liderazgoBackend">
					<div class="row">
						<img src="<?php echo base_url();?>/application/images/liderazgo.jpg" alt="" class="col-md-12">
					</div>
					<div class="row">
						<span>Liderazgo</span>
					</div>	
				</a>			
			</div>
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/multimediaBackend">
					<div class="row">
						<img src="<?php echo base_url();?>/application/images/inco-soft-multimedia.png" alt="" class="col-md-12">
					</div>
					<div class="row">
						<span>Multimedia</span>
					</div>	
				</a>			
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/actividades">
					<div class="row">
						<img src="<?php echo base_url();?>/application/images/pack_online.jpg" alt="" class="col-md-12">
					</div>
					<div class="row">
						<span>Actividades</span>
					</div>	
				</a>			
			</div>
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/eventos">
					<div class="row">
						<img src="<?php echo base_url();?>/application/images/eventos.png" alt="" class="col-md-12">
					</div>
					<div class="row">
						<span>Eventos</span>
					</div>	
				</a>			
			</div>
			<div class="col-md-3">
				<a href="<?php echo base_url(); ?>index.php/home">
					<div class="row">
						<img src="<?php echo base_url();?>/application/images/website.jpg" alt="" class="col-md-12">
					</div>
					<div class="row">
						<span>Web Site</span>
					</div>	
				</a>			
			</div>
		</div>
	</div>
</div>
</div>-->

