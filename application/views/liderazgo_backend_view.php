<?php 
 if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 

	$id= '';
	$liderEditar ='';
	$fechaNombramientoEditar ='';
	$cargoEditar='';
	$imagenLiderEditar='';
	$biografiaEditar = '';

	if(isset($lider)){
		//var_dump($miembro);
		$id =$lider->id;
		$liderEditar = $lider->id_miembro;
		$fechaNombramientoEditar =$lider->fecha_inicio;
		$cargoEditar = $lider->id_cargo;
		$imagenLiderEditar =$lider->imagenLider;
		$biografiaEditar = $lider->resenabibliografica;
	}

	//var_dump($lider);
 ?>
<div class="container">
	<div class="row">
		<div class="col-md-10 ">
			<div class="row">
				<div class="col-md-12">
					<h1>Mantenimiento de Liderazgo</h1>
					<center class="cargandoNoticias">
						<img id="cargandoNoticias" src="<?php echo base_url(); ?>/application/img/loading.gif">
					</center>
				</div>
				<div class="col-md-12 errorFormularioNoticias">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1><span>Error!!!!</span> Verifica que los campos esten introducidos de la manera correcta. </h1>
						</div>
					</div>
					
				</div>
				<div class="col-md-12 successFormularioNoticias">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1><span>Usuario!!!!</span> Insertado de manera correcta. </h1>
						</div>
					</div>
					
				</div>
				<div class="col-md-12 cargandoContenedor">
					<div class="row BoxGeneralNoticias">
						<div class="col-md-3 menuLateralLiderazgo">
							<ul class="nav nav-pills nav-stacked">
								<li><a id="opcionLider1" href="">Lideres Creados</a></li>
								<li><a id="opcionLider2" href="">Agregar Lider</a></li>
								<li><a id="opcionLider3" href="">Algo de Noticias</a></li>
								<!--<li role="presentation" class="dropdown">
			        				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			          					Eventos <span class="caret"></span>
			       					 </a>
							        <ul class="dropdown-menu">
							          <li><a id="opci5" href="">Eventos Creados</a></li>
							          <li><a id="opci6" href="">Crear Eventos</a></li>
							          <li><a id="opci7" href="">Otra Cosita</a></li>
							          <li role="separator" class="divider"></li>
							          <li><a href="#">Separated link</a></li>
							        </ul>
								</li>-->
							</ul>
						</div>
						<div class="col-md-9 contenidoLiderazgo">
							<div class="opcionLider1">
								<div class="row">
										<div class="col-md-4">
											<strong>Nombre</strong>
										</div>
										<div class="col-md-2">
											<strong>Cargo</strong>
										</div>
									</div>
									<?php 
										//var_dump($lideres);
										foreach ($lideres as $key => $value) {											
											?>
											<div class="row">
												<div class="col-md-4">
													<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['id_miembro']; ?>"><p><?php echo $value['nombre_completo']; ?></p></a>
												</div>
												<div class="col-md-4">
													<p><?php echo $value['nombre']; ?></p>
												</div>
												<div class="col-md-2">
													<a href="<?php echo base_url(); ?>index.php/liderazgoBackend?l=2&id=<?php echo $value['id']; ?>"><p style="    font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
												</div>
											</div>

											<?php

										}
									 ?>
							</div>
							<div class="opcionLider2">
								<div class="panel panel-primary">
							    <div class="panel-heading">Formulario de Lideres</div>
							    <div class="panel-body">
							   		<?php 
								   		if(isset($lider)){
											//var_dump($lider);
										}
	

							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_liderazgo', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('lideresBackend/agregarLider',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('idLider',$id);
												 echo form_hidden($datahidden);
											?>
										</div>
										<!--/////////////////  Nombre Lider /////////////////////// -->

								  		<?php 
								  			$atributosResponsable ='id="user_horizontal_name_responsable" class="string email required form-control"'; 


											if(isset($lider)){
												$valueLider = $liderEditar;
											}else{
												$valueLider =1;
											}

								  		?>


								  		<div class="form-group email required user_horizontal_email">
								  		<label class="email required col-sm-3 control-label" for="user_horizontal_name_responsable"><abbr title="Campo Obligatorio">*</abbr>Lider</label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('responsable',$responsable, $valueLider,$atributosResponsable) ?>
								  				<p id="labeSeleccioneResponsable" class="help-block">Seleccione el Lider</p>
								  			</div>
								  		</div>

								  		<!--////////////////// Descripcion de Cambio de Lider  //////////////////////-->


								  		<?php 

							   			$DescripcionCambio = array(
								              'name'        => 'user_horizontal_descripcion_cambio_lider',
								              'id'          => 'user_horizontal_descripcion_cambio_lider',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required'
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email" id="descripcionCambioLider">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_descripcion_cambio_lider"><abbr title="Campo Obligatorio">*</abbr> Descripción del Cambio:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_textarea($DescripcionCambio); ?>
								  				<p class="help-block">¿Por que el Cambio de Lider?:</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  FECHA Inicio /////////////////////// -->

										<?php 

							   			$fechaNombramiento = array(
								              'name'        => 'user_horizontal_fechaNombramiento',
								              'id'          => 'user_horizontal_fechaNombramiento',
								              'type'		=> 'date',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required',
								              'value'		=> $fechaNombramientoEditar
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_fechaNombramiento"><abbr title="Campo Obligatorio">*</abbr> Fecha:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($fechaNombramiento); ?>
								  				<p class="help-block">Fecha del Nombramiento</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  Cargo /////////////////////// -->


								  		<?php 
								  			$atributosCargo ='id="user_horizontal_name_responsable" class="string email required form-control"'; 
								  			if(isset($lider)){
												$valueLider = $cargoEditar;
											}else{
												$valueLider =1;
											}
								  		?>

								  		<div class="form-group email required user_horizontal_email">
								  		<label class="email required col-sm-3 control-label" for="user_horizontal_name_responsable"><abbr title="Campo Obligatorio">*</abbr>Cargo</label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('cargo',$cargo, $cargoEditar,$atributosCargo) ?>
								  				<p id="labeSeleccioneResponsable" class="help-block">Seleccione el nombramiento</p>
								  			</div>
								  		</div>
								  		
								  		<!--/////////////////  Imagen /////////////////////// -->

								  		<?php 

							   			$imagen= array(
								              'name'        => 'user_horizontal_imagen_lider',
								              'id'          => 'user_horizontal_imagen_lider',
								              'class'		=> 'string email required form-control',
								              'type'    => 'file'
								         );

							   			?>
							   			<div id="imagenMostrar2" class="row <?php if((!isset($_GET['l']) && !isset($_GET['id'])) || (isset($_GET['l']) && !isset($_GET['id']))){echo "hidden";} ?>">
								  			<div class="col-md-4 col-md-offset-3">
								  				<img style="width:100%;border-radius: 10px;" src="<?php echo base_url(); ?>/application/lideres/<?php echo $imagenLiderEditar ?>">
								  			</div>		
								  			<div class="col-md-2">
								  				<a style="margin-top:135%" href="" name="cambiar" class="btn btn-info" id="cambiarImagenLider">Cambiar Imagen</a>
								  			</div>
								  			<div class="col-md-2">
								  				<a style="margin-top:135%;margin-left:25%" href="1" name="cambiar" class="btn btn-info" id="eliminarImagenLider">Eliminar Imagen</a>
								  			</div>							  				
								  		</div>
							   			<div style="margin-top:2%;" id="imagenLider" class="<?php if(isset($_GET['l']) && isset($_GET['id']) && $imagenLiderEditar != null){echo "hidden";} ?> form-group email required user_horizontal_email">
								  			<label class="email col-sm-3 control-label" for="user_horizontal_imagen_lider"> Imagen:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($imagen); ?>
								  			</div>
								  		</div>

								  		<!--////////////////// Reseña Biografica  //////////////////////-->

								  		<?php 

							   			$biografia = array(
								              'name'        => 'biografia',
								              'id'          => 'biografia',
								              'class'		=> 'string email required form-control',
								              'value'		=> $biografiaEditar
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email" style="margin-top: 3%;">
								  			<label class="email required col-sm-3 control-label" for="biografia">Reseña Biografica:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_textarea($biografia); ?>
								  			</div>
								  		</div>
								  									  		
										
								  		<?php 
								  			$atributosSubmit ='class="btn btn-primary col-md-offset-3" id="submitAgregarLider"';
								  			if((isset($_GET['l']) && !isset($_GET['id'])) || !isset($_GET['l']) && !isset($_GET['id'])){ echo form_submit('commit', 'Agregar Lider',$atributosSubmit);} 
								  			if(isset($_GET['l']) && isset($_GET['id'])){ echo form_submit('commit', 'Modificar Lider',$atributosSubmit);} 
								  		?>
								  		<input type="button" value="Cancelar" class="btn btn-danger " id="cancelar">

								</div>
  							</div>
							</div>
							<div class="opcionLider3">
								<p>3</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>