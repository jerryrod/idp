</div>
	<div class="row liderazgoContenido">
		<div class="col-md-10 col-md-offset-1 controlLideres">
		<!--////////////       Global           //////////-->
			<div class="row">
				<h1 style="text-align:center;margin-bottom:4%">Lideres Globales</h1>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/img/SamClemens.jpg"><br>
							<strong>Sam Clements</strong>
							<p>Supervisor General</p>
						</a>
					</center>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 ">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/img/SamClemens.jpg"><br><br>
							<strong>Benjamín Feliz</strong>
							<p>Previstero de Área</p>

						</a>
					</center>
				</div>
				<div class="col-md-4">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/img/SamClemens.jpg"><br><br>
							<strong>Sam Clements</strong>
							<p>Supervisor General</p>
						</a>
					</center>
				</div>
				<div class="col-md-4">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/img/SamClemens.jpg"><br><br>
							<strong>Sam Clements</strong>
							<p>Supervisor General</p>
						</a>
					</center>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 ">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/img/SamClemens.jpg"><br><br>
							<strong>Sam Clements</strong>
							<p>Supervisor General</p>

						</a>
					</center>
				</div>
				<div class="col-md-4">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/img/SamClemens.jpg"><br><br>
							<strong>Sam Clements</strong>
							<p>Supervisor General</p>
						</a>
					</center>
				</div>
				<div class="col-md-4">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/img/SamClemens.jpg"><br><br>
							<strong>Sam Clements</strong>
							<p>Supervisor General</p>
						</a>
					</center>
				</div>
			</div>
			<!--////////////       Nacional           //////////-->
			<div class="row">
				<h1 style="text-align:center;margin-bottom:4%">Lideres Nacionales</h1>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/img/Flavio.jpg"><br>
							<strong>Flavio Rosario</strong>
							<p>Supervisor Nacional</p>
						</a>
					</center>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 ">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/lideres/joseromero.jpg"><br><br>
							<strong>José Romero</strong>
							<p>Secretario nacional Cosecha Joven</p>
						</a>
					</center>
				</div>
				<div class="col-md-4">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/lideres/antoniodrullart.jpg"><br><br>
							<strong>Antonio Drullard</strong>
							<p>Secretario nacional Educación</p>
						</a>
					</center>
				</div>
				<div class="col-md-4">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/lideres/adalgisasamboy.jpg"><br><br>
							<strong>Adalgisa de Samboy</strong>
							<p>Secretario nacional Damas en la cosecha</p>
						</a>
					</center>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 ">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/lideres/inocensiosamboy.jpg"><br><br>
							<strong>Inocencio Samboy Hijo</strong>
							<p>Secretario nacional Trabajo social</p>
						</a>
					</center>
				</div>
				<div class="col-md-4">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/lideres/cesarinaflores.jpg"><br><br>
							<strong>Cesarina Flores</strong>
							<p>Secretario nacional Campamento</p>
						</a>
					</center>
				</div>
				<div class="col-md-4">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/lideres/salomontaveras.jpg"><br><br>
							<strong>Salomón Taveras</strong>
							<p>Secretario nacional Adolescentes</p>
						</a>
					</center>
				</div>
				<div class="col-md-4">
					<center>
						<a href="">
							<img src="<?php echo base_url(); ?>/application/lideres/martasalas.jpg"><br><br>
							<strong>Marta Salas</strong>
							<p>Secretario nacional Niños</p>
						</a>
					</center>
				</div>
			</div>
			<!--////////////       Local           //////////-->

			
			<div class="row">
				<h1 style="text-align:center;margin-bottom:4%">Lideres Locales</h1>				 
			</div>
			<div class="row">			
				<?php 
					//var_dump($lideres);	
					$contador = -1;
					foreach ($lideres as $key => $value) {
						if($contador%3 == -1){?>							
							<div class="col-md-4 col-md-offset-4">
							<?php 
						}else{?>
							<div class="col-md-4">
						<?php }
							?>
							<center>
								<a href="<?php echo base_url(); ?>index.php/liderazgo/ver_Lider_Single/<?php echo $value['id_miembro']; ?>">
									<img src="<?php echo base_url(); ?>/application/lideres/<?php echo $value['imagenLider']; ?>"><br>
									<strong><?php echo $value['nombre_completo']; ?></strong>
									<p><?php echo $value['nombre']; ?></p>
								</a>
							</center>
						</div>
					<?php
						$contador++;
						if($contador%3 == 0 ){?>
							</div>
								<div class="row">
						<?php
						}
						}
				?>
			</div>	
			
			</div>
		</div>		
	</div>
