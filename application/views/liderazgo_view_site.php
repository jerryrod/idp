<main>
    <section class="well1 ins4 ">
      <div class="container losP centrarTituloxs">
         <h2>Liderazgo Local</h2>
        <div class="row">
          <div class='grid_5'>
            <img src="<?php echo base_url(); ?>/application/images/IMG_1184.JPG">
          </div>
          <div class="grid_7">
             <h2>Pastores Carrascos</h2>
            <p>El Pastor Alvin Carrasco Nacío en la comunidad de Los Blancos, en el municipio de Enriquillo, en la provincia de Barahona en la Republica Dominicana. 
                Sus padres, Julian Carrasco, de origen agricultor, pero se dedicó a  la pesca, su madre Mayra Perez también de origen agricultor se  casaron en septiembre de 1976, un año después,  el 20 de octubre  de 1977 nació su primer hijo al cual llamaron Alvin.
            </p>
              <br>
            <p>Junto a sus tres hermanos menores (Jorge, Sagrario y Luis)  crecieron en el pequeño poblado de Los Blancos, cuya población era de aproximadamente 500 personas. Pasando muchas calamidades y pobreza, 
                pero desde pequeños, sus padres habían decidido que ellos debían estudiar.</p>
                <br>
                <p>
                  A la edad de 13 años (agosto del 1991) Alvin Carrasco, aceptó en la Iglesia de Dios de la Profecía de la comunidad, donde trabajó predicando el evangelio hasta los 18 años. 
                  Sus padres adquirieron una casa en Santo Domingo y se mudamos con el objetivo básico de que sus hijos fueran a la universidad. 
                  Allí me Alvin se congregó durante muchos años en la Iglesia del KM 12 de las Americas, donde trabajó como Líder de Jóvenes local y después como asistente de subsecretario de distrito, 
                  el cual estaba a cargo de 35 Iglesias. 
                  A los 21 años se casó con Daisy Santana (“el mejor regalo que he recibido del cielo” según sus propias palabras) quienes procrearon tres niños hermosos: Dalvin, Samuel y Darlenis.
                </p>
            </div>
            </div>
          <p>En el 2005, se graduó con una licenciatura en Contabilidad y dos años después con un Master in Business Administration. Trabaja, hasta el momento, en una gran empresa de generación eléctrica como Responsable de Tesorería y Finanzas y por la misericordia de Dios. 
            Fue Pastor de jóvenes en la IDP en Los Frailes, Subsecretario de Distrito (con 15 iglesias a mi cargo) del ministerio juvenil, miembro del ministerio de adoración local, maestro de escuela bíblica y junto a su esposa trabajó con células de crecimiento, una de ellas en su propia casa. </p>
          <p>En mayo del 2014, fueron llamados al ministerio y desde entonces Dios les ha bendecido con una maravillosa congregación en el Ensanche Quisqueya en la ciudad de Santo Domingo. En octubre del 2015, 
          fueron enviados a la hermosa congregación de Los Frailes, donde hasta el momento es el pastor principal.</p>
          <br>
          <p>Actualmente sigue cursando una licenciatura en Teología en el INSTITUTO TEOLOGICO INTERNACIONAL VIDA NUEVA PARA EL MUNDO.</p>
        </div>
      </div>
    </section>
     <hr style="width:88%; margin-left:6%">
     <section class="well1 ins2 mobile-center estiloImagenes">
      <div class="container">
        <h2>Ministerios</h2>
        <div class="row off2">
          <div class="grid_4"><img src="<?php echo base_url()?>/application/images/varones.PNG" alt="">
            <!--<h3>Lider Cabelleros</h3>-->
            <p>El Ministerio de Varones de la iglesia de Dios de la Profecía en Los Frailes, está conformado por hombres de testimonio, que buscan un impacto espiritual, social, familiar y empresarial, para todos los hombres que podamos alcanzar.</p>
           	<p style="display:none">Son parte del ministerio de varones de la iglesia, todos aquellos que superen los 25 años de edad y aquellos jóvenes menores de esta edad, que tengan familia.</p>
            <!--<a href="#" class="btn">Ver Más</a>-->
          </div>
          <div class="grid_4"><img src="<?php echo base_url()?>/application/images/damas.PNG" alt="">
           <!-- <h3>Lider Damas</h3>-->
            <p>Está orientado a servir de guía a las mujeres que forman parte la congregación, sean miembros o no, realizando un programa extenso y constante de actividades orientadas al desarrollo de las mujeres y al fortalecimiento del rol de las esposas, madres e hijas, dentro y fuera del hogar, todo esto visto y basado desde la perspectiva de la palabra de Dios.</p>
            <!--<a href="#" class="btn">Ver Más</a>-->
          </div>
          <div class="grid_4"><img src="<?php echo base_url()?>/application/images/jovenes.PNG" alt="">
            <!--<h3>Lider Jovenes</h3>-->
            <p>Aenean ac leo eget nunc fringilla a non nulla! Nunc orci mi, venenatis quis ultrices vitae, congue non nibh. Nulla bibendum, justo eget ultrices vestibulum erat tortor venenatis risus, sit amet cursus dui augue a arcu.</p>
            <!--<a href="#" class="btn">Ver Más</a>-->
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="grid_4"><img src="<?php echo base_url()?>/application/images/adolescentes.PNG" alt="">
           <!-- <h3>Lider Adolescentes</h3>-->
            <p>Nunc orci mi, venenatis quis ultrices vitae, congue non nibh. Nulla bibendum, justo eget ultrices vestibulum erat tortor venenatis risus, sit amet cursus dui augue a arcu. Quisque mauris risus, gravida a molestie eu.</p>
            <!--<a href="#" class="btn">Ver Más</a>-->
          </div>
          <div class="grid_4"><img src="<?php echo base_url()?>/application/images/niños.PNG" alt="">
           <!-- <h3>Lider Niños</h3>-->
            <p>Quisque mauris risus, gravida a molestie eu, dictum ac augue. Integer sodales tempor lectus; sit amet dictum metus pharetra nec. Fusce bibendum dapibus pretium. Nunc eu sem vitae lacus laoreet elementum. Nulla facilisi.</p>
           <!--<a href="#" class="btn">Ver Más</a>-->
          </div>
          <div class="grid_4"><img src="<?php echo base_url()?>/application/images/adoracion.PNG" alt="">
          <!--  <h3>Lider Adoración</h3>-->
            <p>Integer sodales tempor lectus; sit amet dictum metus pharetra nec. Fusce bibendum dapibus pretium. Nunc eu sem vitae lacus laoreet elementum. Nulla facilisi. Phasellus ullamcorper elementum est, id pretium turpis.</p>
            <!--<a href="#" class="btn">Ver Más</a>-->
          </div>
        </div>
      </div>
    </section>
    <!--<section class="well1">
      <div class="container">
        <h2 class="mobile-center">Price list</h2>
        <div class="row">
          <div class="grid_4">
            <table class="wow fadeInUp">
              <tr>
                <td>Suspendisse sollicitudin velit sed leo</td>
                <td>$ 32.00</td>
              </tr>
              <tr>
                <td>Ut pharetra augue nec augue</td>
                <td>$ 27.00</td>
              </tr>
              <tr>
                <td>Nam elit agna endrerit sit amet</td>
                <td>$ 16.00</td>
              </tr>
              <tr>
                <td>Tincidunt ac viverra sed nulla</td>
                <td>$ 42.00</td>
              </tr>
              <tr>
                <td>Donec porta diam eu massa</td>
                <td>$ 50.00</td>
              </tr>
              <tr>
                <td>Quisque diam lorem interdum vitae</td>
                <td>$ 90.00</td>
              </tr>
            </table>
          </div>
          <div class="grid_4">
            <table data-wow-delay="0.2s" class="wow fadeInUp">
              <tr>
                <td>Tincidunt ac viverra sed nulla</td>
                <td>$ 42.00</td>
              </tr>
              <tr>
                <td>Donec porta diam eu massa</td>
                <td>$ 50.00</td>
              </tr>
              <tr>
                <td>Quisque diam lorem interdum vitae</td>
                <td>$ 32.00</td>
              </tr>
              <tr>
                <td>Scelerisque vitae pede</td>
                <td>$ 27.00</td>
              </tr>
              <tr>
                <td>Donec eget tellus non erat</td>
                <td>$ 16.00</td>
              </tr>
              <tr>
                <td>Lacinia fermentum</td>
                <td>$ 90.00</td>
              </tr>
            </table>
          </div>
          <div class="grid_4">
            <table data-wow-delay="0.4s" class="wow fadeInUp">
              <tr>
                <td>Donec in velit vel ipsum pulvinar</td>
                <td>$ 27.00</td>
              </tr>
              <tr>
                <td>Vestibulum iaculis lacinia est</td>
                <td>$ 42.00</td>
              </tr>
              <tr>
                <td>Proin dictum elementum velit</td>
                <td>$ 50.00</td>
              </tr>
              <tr>
                <td>Fusce euismod consequat ante</td>
                <td>$ 32.00</td>
              </tr>
              <tr>
                <td>Lorem ipsum dolor sit amet</td>
                <td>$ 16.00</td>
              </tr>
              <tr>
                <td>Consectetuer adipiscing elit</td>
                <td>FREE</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </section>-->
  </main>