	
	<?php
		$username = array('name' => 'username', 'placeholder' => 'Usuario', 'class' => 'col-md-12 inputLogin');
		$password = array('name' => 'password',	'placeholder' => 'Contraseña','class' => 'col-md-12 inputLogin');
		$submit = array('name' => 'submit', 'value' => 'Iniciar sesión', 'title' => 'Iniciar sesión','class' => 'col-md-4 pull-right btn btn-info inputLogin');
	?>
	<html lang="es">
	<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
		
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title>IDP</title>
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/application/css/style.css">
	    
	    <!-- Bootstrap -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	    <!--<link rel="stylesheet" href="<?php echo base_url(); ?>application/css/bootstrap.min.css">-->

	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>application/css/jquery-ui.css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	    <link rel="stylesheet" href="<?php echo base_url(); ?>application/fancyBox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
		
	    <script src="<?php echo base_url(); ?>application/js/Chart.js-master/Chart.js"></script>
		<script type="text/javascript" src="http://jqwidgets.com/public/jqwidgets/jqx-all.js"></script>


	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    <style>
			body{
			 padding-bottom: 0px !important;
			 overflow: hidden;
			}
	    </style>
		 	
	</head>
	<body class="login-full">
		<div class="container-fluid">
			<div class="row " id="Log">
				<div class="col-md-4 col-md-offset-4">
					<?php $attributes = array('class' => 'form-signin ');?>
						<?=form_open(base_url().'index.php/login/new_user',$attributes)?>
						<div class="row">
							<img class="col-md-8 col-md-offset-2" src="<?php echo base_url(); ?>application/images/logoanglis21.png"/>
						</div>
						
						<div class="row loginText">
							<p>Iniciar Sesion en Anglis Backend CMS </p>
						</div>
						<div class="row login-full2">
							<?=form_input($username)?><p><?=form_error('username')?></p>
							<?=form_password($password)?><p><?=form_error('password')?></p>
							<?=form_hidden('token',$token)?>
							<?=form_submit($submit)?>
							<?=form_close()?>
							<?php 
							if($this->session->flashdata('usuario_incorrecto'))
							{
							?>
							<p><?=$this->session->flashdata('usuario_incorrecto')?></p>
							<?php
								}
							?>
						</div>
				</div>				
			</div>
		</div>
	</body>
</html>