<?php if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 

	$id= '';
	$nombreEditar ='';
	$apellidoEditar ='';
	$emailEditar='';
	$telefonoEditar='';
	$nombreCivilEditar='';
	$nombreEstadoEducativoEditar='';
	$nombreNivelEducativoEditar='';
	$nombreEstadoLaboralEditar='';
	$sexoEditar='';
	$fechaNacimientoEditar ='';
	$fechaConversionEditar='';
	$nombreCarreraEditar='';

	if(isset($miembro)){
		//var_dump($miembro);
		$id =$miembro->id;
		$nombreEditar = $miembro->nombre;
		$apellidoEditar =$miembro->apellido;
		$emailEditar = $miembro->email;
		$telefonoEditar =$miembro->telefono;
		$estadoMiembro = $miembro->estado;
		$nombreCivilEditar = $miembro->estadoCivil;
		$nombreEstadoEducativoEditar =$miembro->estadoEducativo;
		$nombreNivelEducativoEditar = $miembro->nivelEducativo;
		$nombreEstadoLaboralEditar =$miembro->estadoLaboral;
		$sexoEditar = $miembro->sexo;
		$fechaNacimientoEditar =$miembro->fechaNacimiento;
		$fechaConversionEditar =$miembro->fechaConversion;
		$nombreCarreraEditar =$miembro->carreraProfesional;
		$liderCelula = $miembro->celula;
	}
	//var_dump($miembrosTotal[0]['cantidad']);

?>
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<div class="col-md-12">
				<h1>Mantenimiento de Miembros</h1>
				<center class="cargando">
					<img id="cargando" src="<?php echo base_url(); ?>/application/img/loading.gif">
				</center>
			</div>
			<div class="col-md-12 errorFormularioMiembros">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Error!!!!</span> Verifica que los campos esten introducidos de la manera correcta. </h1>
					</div>
				</div>				
			</div>
			<div class="col-md-12 successFormularioMiembros">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Usuario!!!!</span> Insertado de manera correcta. </h1>
					</div>
				</div>				
			</div>
			<div class="col-md-12 successFormularioAsistencia">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Asistencia!!!!</span> Pasada Correctamente. </h1>
					</div>
				</div>				
			</div>
			<div class="col-md-12 successFormularioEvento">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Evento!!!!</span> creado correctamente. </h1>
					</div>
				</div>
				
			</div>
			<div class="col-md-12 cargandoContenedor">
				<div class="row BoxGeneralMembresia">
					<div class="col-md-2 menuLateralMembresia">
						<ul class="nav nav-pills nav-stacked">
							<li><a id="op1" href="">Membresía Local</a></li>
							<li><a id="op2" href="">Agregar miembro</a></li>
							<li><a id="op3" href="">Agregar Asistencia</a></li>
							<li><a id="op4" href="">Actividades</a></li>
							<!--<li role="presentation" class="dropdown">
		        				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
		          					Eventos <span class="caret"></span>
		       					 </a>
						        <ul class="dropdown-menu">
						          <li><a id="op5" href="">Eventos Creados</a></li>
						          <li><a id="op6" href="">Crear Eventos</a></li>
						          <li><a id="op7" href="">Otra Cosita</a></li>
						          <li role="separator" class="divider"></li>
						          <li><a href="#">Separated link</a></li>
						        </ul>
							</li>-->
						</ul>
					</div>

					<div class="col-md-8 contenidoMembresia">
						<div class="op1">
							
							<ul class="nav nav-tabs" role="tablist" id="TabsId">
							    <li role="presentation" class="active"><a class="ponerVisibleElInputBuscar" href="#home" aria-controls="home" role="tab" data-toggle="tab">Membresia Total</a></li>
							    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Miembros Activos</a></li>
							    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Miembros Inactivos</a></li>
							    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Descarriado</a></li>
							    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#trasladados" aria-controls="trastalados" role="tab" data-toggle="tab">Trasladados</a></li>
							    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#exterior" aria-controls="exterior" role="tab" data-toggle="tab">Residentes Exterior</a></li>
							    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#fallecido" aria-controls="fallecido" role="tab" data-toggle="tab">Fallecidos</a></li>
							    <li id="id-li-Input-buscar" style="width:60%;border-left: 1px solid #ddd;padding-left: 10px;">
								    <div class="input-group" style="width:60%">
	          							<input type="text" class="form-control" id="inputBuscarMiembro" placeholder="Primer Nombre">
	          							<span class="input-group-btn">
	            							<button class="btn btn-default" id="buscarMiembro" type="button">Buscar</button>
	          							</span>
	        						</div>
	        					</li>
							</ul>
							<div class="tab-content tab-Contenido">
								<!--Membresia Total-->
    							<div role="tabpanel" class="tab-pane fade in active" id="home">
    								<div class="row">
										<div class="col-md-4">
											<div class="row">
												<div class="col-md-8">
													<strong>Asistencia Alta</strong>
												</div>
												<div class="col-md-1" style='background-color:#337ab7;min-height: 20px;'>													
												</div>												
											</div>	
										</div>
										<div class="col-md-4">
											<div class="row">
												<div class="col-md-8">
													<strong>Asistencia Media</strong>
												</div>
												<div class="col-md-1" style='background-color:#E6A25A;min-height: 20px;'>												
												</div>												
											</div>
										</div>
										<div class="col-md-4">
											<div class="row">
												<div class="col-md-8">
													<strong>Asistencia Baja</strong>
												</div>
												<div class="col-md-1" style='background-color:#FF000A;min-height: 20px;'>												
												</div>												
											</div>
										</div>
									</div>
    								<div class="row" style="margin-top:5%;">
										<div class="col-md-4">
											<strong>Nombre Completo</strong>
										</div>
										<div class="col-md-2">
											<strong>Función</strong>
										</div>
										<div class="col-md-2">
											<strong>Operacion</strong>
										</div>
									</div>
									<div class="boxMiembrosPrincipal">
										<?php 
											//var_dump($miembros1);
											$CI =& get_instance();
											foreach ($miembros as $key => $value) {

												$porcentajeString = $CI->porcentajeIndividualPorMiembrosColor($value['id']);
												$porcentajeInt = (int)$porcentajeString;
												//var_dump($porcentajeInt);
												?>
												<div class="row">
													<div class="col-md-4">
														<a class="various fancybox.ajax <?php if($porcentajeInt<=40){echo 'colorRojo';} if($porcentajeInt>40 && $porcentajeInt<=70) { echo 'colorOrange';}?>" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['id']; ?>"><p><?php echo $value['nombre'].' '. $value['apellido'] ; ?></p></a>
													</div>
													<div class="col-md-2">
														<p><?php echo $value['apellido']; ?></p>
													</div>
													<div class="col-md-2">
														<a href="<?php echo base_url(); ?>index.php/miembros?a=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
													</div>
												</div>

												<?php

											}
										 ?>
									 </div>
    							</div>
    							<!--Membresia Activa-->
							    <div role="tabpanel" class="tab-pane fade" id="profile">
							    	<div class="row">
										<div class="col-md-4">
											<strong>Nombre Completo</strong>
										</div>
										<div class="col-md-2">
											<strong>Función</strong>
										</div>
										<div class="col-md-2">
											<strong>Operacion</strong>
										</div>
									</div>
									<?php 
										
										foreach ($miembrosActivos as $key => $value) {
											?>
											<div class="row">
												<div class="col-md-4">
													<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['id']; ?>"><p><?php echo $value['nombre'].' '. $value['apellido'] ; ?></p></a>
												</div>
												<div class="col-md-2">
													<p><?php echo $value['apellido']; ?></p>
												</div>
												<div class="col-md-2">
													<a href="<?php echo base_url(); ?>index.php/miembros?a=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
												</div>
											</div>

											<?php

										}
									 ?>
							    </div>
							    <!--Membresia Inactiva-->
							    <div role="tabpanel" class="tab-pane fade" id="messages">
							    	<div class="row">
										<div class="col-md-4">
											<strong>Nombre Completo</strong>
										</div>
										<div class="col-md-2">
											<strong>Función</strong>
										</div>
										<div class="col-md-2">
											<strong>Operacion</strong>
										</div>
									</div>
									<?php 
										
										foreach ($miembrosInactivos as $key => $value) {
											?>
											<div class="row">
												<div class="col-md-4">
													<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['id']; ?>"><p><?php echo $value['nombre'].' '. $value['apellido'] ; ?></p></a>
												</div>
												<div class="col-md-2">
													<p><?php echo $value['apellido']; ?></p>
												</div>
												<div class="col-md-2">
													<a href="<?php echo base_url(); ?>index.php/miembros?a=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
												</div>
											</div>

											<?php

										}
									 ?>
							    </div>
							    <!--Membresia Descarriada-->
							    <div role="tabpanel" class="tab-pane fade" id="settings">
							    	<div class="row">
										<div class="col-md-4">
											<strong>Nombre Completo</strong>
										</div>
										<div class="col-md-2">
											<strong>Función</strong>
										</div>
										<div class="col-md-2">
											<strong>Operacion</strong>
										</div>
									</div>
									<?php 
										
										foreach ($miembrosDescarriados as $key => $value) {
											?>
											<div class="row">
												<div class="col-md-4">
													<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['id']; ?>"><p><?php echo $value['nombre'].' '. $value['apellido'] ; ?></p></a>
												</div>
												<div class="col-md-2">
													<p><?php echo $value['apellido']; ?></p>
												</div>
												<div class="col-md-2">
													<a href="<?php echo base_url(); ?>index.php/miembros?a=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
												</div>
											</div>

											<?php

										}
									 ?>
							    </div>
							    <!--Membresia Trasladada-->
							    <div role="tabpanel" class="tab-pane fade" id="trasladados">
							    	<div class="row">
										<div class="col-md-4">
											<strong>Nombre Completo</strong>
										</div>
										<div class="col-md-2">
											<strong>Función</strong>
										</div>
										<div class="col-md-2">
											<strong>Operacion</strong>
										</div>
									</div>
									<?php 
										
										foreach ($miembrosTrasladados as $key => $value) {
											?>
											<div class="row">
												<div class="col-md-4">
													<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['id']; ?>"><p><?php echo $value['nombre'].' '. $value['apellido'] ; ?></p></a>
												</div>
												<div class="col-md-2">
													<p><?php echo $value['apellido']; ?></p>
												</div>
												<div class="col-md-2">
													<a href="<?php echo base_url(); ?>index.php/miembros?a=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
												</div>
											</div>

											<?php

										}
									 ?>


							    </div>

							    <!--Membresia Residencia Exterior-->
							    <div role="tabpanel" class="tab-pane fade" id="exterior">
							    	<div class="row">
										<div class="col-md-4">
											<strong>Nombre Completo</strong>
										</div>
										<div class="col-md-2">
											<strong>Función</strong>
										</div>
										<div class="col-md-2">
											<strong>Operacion</strong>
										</div>
									</div>
									<?php 
										
										foreach ($miembrosExterior as $key => $value) {
											?>
											<div class="row">
												<div class="col-md-4">
													<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['id']; ?>"><p><?php echo $value['nombre'].' '. $value['apellido'] ; ?></p></a>
												</div>
												<div class="col-md-2">
													<p><?php echo $value['apellido']; ?></p>
												</div>
												<div class="col-md-2">
													<a href="<?php echo base_url(); ?>index.php/miembros?a=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
												</div>
											</div>

											<?php

										}
									 ?>

									 
							    </div>


							    <!--Membresia Fallecida-->
							    <div role="tabpanel" class="tab-pane fade" id="fallecido">
							    	<div class="row">
										<div class="col-md-4">
											<strong>Nombre Completo</strong>
										</div>
										<div class="col-md-2">
											<strong>Función</strong>
										</div>
										<div class="col-md-2">
											<strong>Operacion</strong>
										</div>
									</div>
									<?php 
										
										foreach ($miembrosFallecidos as $key => $value) {
											?>
											<div class="row">
												<div class="col-md-4">
													<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['id']; ?>"><p><?php echo $value['nombre'].' '. $value['apellido'] ; ?></p></a>
												</div>
												<div class="col-md-2">
													<p><?php echo $value['apellido']; ?></p>
												</div>
												<div class="col-md-2">
													<a href="<?php echo base_url(); ?>index.php/miembros?a=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
												</div>
											</div>

											<?php

										}
									 ?>

									 
							    </div>
							</div>	
							<div class="row resenaEstadistica">
								<div class="col-md-12">
									<p>Reseña Estadística de la Membresía</p>
								</div>
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-3">
											<strong>Cantidad Total:</strong><span><?php echo $miembrosTotal[0]['cantidad']; ?></span>
										</div>

										<div class="col-md-3">
											<strong>Cantidad Hombre:</strong><span><?php echo $miembrosTotalHombre[0]['cantidadHombre']; ?></span>
										</div>

										<div class="col-md-3">
											<strong>Cantidad Mujeres:</strong><span><?php echo $miembrosTotalMujer[0]['cantidadMujer']; ?></span>
										</div>
									</div>
								</div>
							</div>						
						</div>
						<div class="op2">
							<div class="panel panel-primary">
							    <div class="panel-heading">Formulario de Membresia</div>
							    <div class="panel-body">
							   		<?php 
							   			//var_dump($miembro);
							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('miembros/agregarMiembro',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('idMiembro',$id);
												 echo form_hidden($datahidden);
											?>
										</div>
										<!--/////////////////  NOMBRE /////////////////////// -->
										<?php 
										
							   			$nombre = array(
								              'name'        => 'user_horizontal_name',
								              'id'          => 'user_horizontal_name',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Nombre',
								              'required'    => 'required',
								              'value'		=> $nombreEditar
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_name"><abbr title="Campo Obligatorio">*</abbr> Nombre(s)</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($nombre); ?>
								  				<p class="help-block">Primer y Segundo Nombre</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  APELLIDO  /////////////////////// -->
								  		<?php 

							   			$apellido = array(
								              'name'        => 'user_horizontal_lastname',
								              'id'          => 'user_horizontal_lastname',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Apellido',
								              'required'    => 'required',
								              'value'		=> $apellidoEditar
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_lastname"><abbr title="Campo Obligatorio">*</abbr> Apellido(s)</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($apellido); ?>
								  				<p class="help-block">Primer y Segundo Apellido</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  EMAIL /////////////////////// -->

								  		<?php 

							   			$email = array(
								              'name'        => 'user_horizontal_email',
								              'id'          => 'user_horizontal_email',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Correo Electronico',
								              'type'		=> 'email',
								              'value'		=> $emailEditar
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email col-sm-3 control-label" for="user_horizontal_email"> Correo Electronico</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($email); ?>
								  				<p class="help-block">Correo Electronico si lo posee</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  Telefono /////////////////////// -->

								  		<?php 

							   			$telefono = array(
								              'name'        => 'user_horizontal_telefono',
								              'id'          => 'user_horizontal_telefono',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Telefono',
								              'type'		=> 'text',
								              'value'		=> $telefonoEditar
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email col-sm-3 control-label" for="user_horizontal_email"> Telefono</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($telefono); ?>
								  				<p class="help-block">Telefono si lo posee</p>
								  			</div>
								  		</div>


								  		<!--/////////////////  ESTADO Membresia  /////////////////////// -->
								  		<?php 

											if(isset($estadoMiembro)){
												$valueEstado = $estadoMiembro;
											}else{
												$valueEstado =1;
											}
								  			$atributosEstados ='id="inputEstados" class="string email required form-control"';
								  		 ?>
								  		

								  		<div class="form-group email required user_horizontal_email">
								  		<label class="email col-sm-3 control-label" for="user_horizontal_email"> Estado: </label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('estados',$estados, $valueEstado,$atributosEstados) ?>
								  				<p id="labeSeleccioneCarreras" class="help-block">Seleccione el Estado</p>
								  			</div>
								  		</div>

								  		<!--////////////////// Descripcion de Cambio de Estado  //////////////////////-->


								  		<?php 

							   			$DescripcionCambio = array(
								              'name'        => 'user_horizontal_descripcion_cambio',
								              'id'          => 'user_horizontal_descripcion_cambio',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required'
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email" id="descripcionCambio">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_descripcion_cambio"><abbr title="Campo Obligatorio">*</abbr> Descripción del Cambio:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_textarea($DescripcionCambio); ?>
								  				<p class="help-block">¿Por que el Cambio de Estatus?:</p>
								  			</div>
								  		</div>


								  		<!--/////////////////  ESTADO CIVIL  /////////////////////// -->


								  		<?php 

							   			$EstadoCivilSoltero = array(
								              'name'        => 'user_horizontal_estadoCivil',
								              'id'          => 'user_horizontal_choices_soltero',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '1'
								         );

							   			$EstadoCivilCasado = array(
								              'name'        => 'user_horizontal_estadoCivil',
								              'id'          => 'user_horizontal_choices_casado',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '2'
								         );

							   			$EstadoCivilDivorciado = array(
								              'name'        => 'user_horizontal_estadoCivil',
								              'id'          => 'user_horizontal_choices_divorciado',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '3'
								         );

							   			$EstadoCivilViudo = array(
								              'name'        => 'user_horizontal_estadoCivil',
								              'id'          => 'user_horizontal_choices_viudo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '4'
								         );

							   			$EstadoCivilLibre = array(
								              'name'        => 'user_horizontal_estadoCivil',
								              'id'          => 'user_horizontal_choices_libre',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '5'
								         );
							   			?> 
							   			<div class="form-group check_boxes optional user_horizontal_choices">
											<label class="check_boxes optional col-sm-3 control-label"><abbr title="Campo Obligatorio">*</abbr>Estado Civil</label>
											<div class="col-sm-9">

												<span class="radio">
													<label for="user_horizontal_choices_soltero">
													<?php echo form_radio($EstadoCivilSoltero); ?>Soltero(a)</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_casado">
													<?php echo form_radio($EstadoCivilCasado); ?>Casado(a)</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_divorciado">
													<?php echo form_radio($EstadoCivilDivorciado); ?>Divorciado(a)</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_viudo">
													<?php echo form_radio($EstadoCivilViudo); ?>Viudo(a)</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_libre">
													<?php echo form_radio($EstadoCivilLibre); ?>Union Libre</label>
												</span>
												<p class="help-block">Estado Civil</p>

											</div>
										</div>


										<!--/////////////////  ESTADO EDUCATIVO /////////////////////// -->

										<?php 

							   			$EstadoEducativoActivo = array(
								              'name'        => 'user_horizontal_estadoEducativo',
								              'id'          => 'user_horizontal_choices_activo_estadoEducativo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '1'
								         );

							   			$EstadoEducativoInactivo = array(
								              'name'        => 'user_horizontal_estadoEducativo',
								              'id'          => 'user_horizontal_choices_inactivo_estadoEducativo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '2'
								         );
							   			
							   			?> 

										<div class="form-group check_boxes optional user_horizontal_choices">
											<label class="check_boxes optional col-sm-3 control-label"><abbr title="Campo Obligatorio">*</abbr>Estado Educativo</label>
											<div class="col-sm-9">

												<span class="radio">
													<label for="user_horizontal_choices_activo_estadoEducativo">
													<?php echo form_radio($EstadoEducativoActivo); ?>Activo</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_inactivo_estadoEducativo">
													<?php echo form_radio($EstadoEducativoInactivo); ?>Inactivo</label>
												</span>
												<p class="help-block">Estado Educativo</p>

											</div>
										</div>

										<!--/////////////////  NIVEL EDUCATIVO  /////////////////////// -->

										<?php 

							   			$NivelEducativoBasico = array(
								              'name'        => 'user_horizontal_nivelEducativo',
								              'id'          => 'user_horizontal_choices_basico_nivelEducativo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '1'
								         );

							   			$NivelEducativoBachiller = array(
								              'name'        => 'user_horizontal_nivelEducativo',
								              'id'          => 'user_horizontal_choices_bachiller_nivelEducativo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '2'
								         );
							   			
							   			$NivelEducativoUniversitario = array(
								              'name'        => 'user_horizontal_nivelEducativo',
								              'id'          => 'user_horizontal_choices_universitario_nivelEducativo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '3'
								         );

							   			$NivelEducativoProfesional = array(
								              'name'        => 'user_horizontal_nivelEducativo',
								              'id'          => 'user_horizontal_choices_profesional_nivelEducativo',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '4'
								         );

							   			$atributosCarreras ='id="inputCarreras" class="string email required form-control"';


							   			?> 

										<div class="form-group check_boxes optional user_horizontal_choices">
											<label class="check_boxes optional col-sm-3 control-label"><abbr title="Campo Obligatorio">*</abbr>Nivel Educativo</label>
											<div class="col-sm-9">

												<span class="radio">
													<label for="user_horizontal_choices_basico_nivelEducativo">
														<?php echo form_radio($NivelEducativoBasico); ?>Básico</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_bachiller_nivelEducativo">
														<?php echo form_radio($NivelEducativoBachiller); ?>Bachiller</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_universitario_nivelEducativo">
														<?php echo form_radio($NivelEducativoUniversitario); ?>Universitario</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_profesional_nivelEducativo">
														<?php echo form_radio($NivelEducativoProfesional); ?>Profesional</label>
												</span>

												<p class="help-block">Nivel Educativo</p>

												<div class="form-group email required user_horizontal_email">
										  			<div class="col-sm-9">
										  				<?php echo form_dropdown('carreras',$carreras, 1,$atributosCarreras) ?>
										  				<p id="labeSeleccioneCarreras" class="help-block">Seleccione la Carrera</p>
										  			</div>
								  				</div>

												
											</div>
										</div>

										<!--/////////////////  ESTADO LABORAL  /////////////////////// -->

										<?php 

							   			$EstadoLaboralActivo = array(
								              'name'        => 'user_horizontal_estadoLaboral',
								              'id'          => 'user_horizontal_choices_activo_estadoLaboral',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '1'
								         );

							   			$EstadoLaboralInactivo = array(
								              'name'        => 'user_horizontal_estadoLaboral',
								              'id'          => 'user_horizontal_choices_inactivo_estadoLaboral',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  '2'
								         );
							   			
							   			
							   			?> 

										<div class="form-group check_boxes optional user_horizontal_choices">
											<label class="check_boxes optional col-sm-3 control-label"><abbr title="Campo Obligatorio">*</abbr>Estado Laboral</label>
											<div class="col-sm-9">

												<span class="radio">
													<label for="user_horizontal_choices_activo_estadoLaboral">
														<?php echo form_radio($EstadoLaboralActivo); ?>Activo</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_choices_inactivo_estadoLaboral">
														<?php echo form_radio($EstadoLaboralInactivo); ?>Inactivo</label>
												</span>
												<p class="help-block">Estado Laboral</p>
											</div>
										</div>

										<!--/////////////////  SEXO  /////////////////////// -->

										<?php 

							   			$SexoMasculino = array(
								              'name'        => 'user_horizontal_sexo',
								              'id'          => 'user_horizontal_sex_male',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  'M'
								         );

							   			$SexoFemenino = array(
								              'name'        => 'user_horizontal_sexo',
								              'id'          => 'user_horizontal_sex_male',
								              'class'		=> 'check_boxes optional',
								              'value'       =>  'F'
								         );
							   			
							   			
							   			?> 

										<div class="form-group radio_buttons optional user_horizontal_sex">
											<label class="radio_buttons optional col-sm-3 control-label"><abbr title="Campo Obligatorio">*</abbr>Sexo</label>
											<div class="col-sm-9">
												<span class="radio">
													<label for="user_horizontal_sex_male">
														<?php echo form_radio($SexoMasculino); ?>Masculino</label>
												</span>

												<span class="radio">
													<label for="user_horizontal_sex_female">
														<?php echo form_radio($SexoFemenino); ?>Femenino</label>
												</span>

												<p class="help-block">Sexo</p>
											</div>
										</div>

										<!--/////////////////  Lider /////////////////////// -->
										<?php 
										
										if(isset($liderCelula)){
												$valueLider = $liderCelula;
											}else{
												$valueLider =1;
											}

							   			$lider = array(
								              'name'        => 'user_horizontal_celula',
								              'id'          => 'user_horizontal_celula',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required'
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_lider"><abbr title="Campo Obligatorio">*</abbr> Nombre del Lider:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('celula',$celulas, $valueLider,$lider) ?>								  				
								  				<p class="help-block">Lider de la Celula</p>
								  			</div>
								  		</div>

										<!--/////////////////  FECHA NACIMIENTO /////////////////////// -->

										<?php 

							   			$fechaNacimiento = array(
								              'name'        => 'user_horizontal_fecha',
								              'id'          => 'user_horizontal_fecha',
								              'type'		=> 'date',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required',
								              'value'		=> $fechaNacimientoEditar
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_fecha"><abbr title="Campo Obligatorio">*</abbr> Fecha Nacimiento</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($fechaNacimiento); ?>
								  				<p class="help-block">Fecha Nacimiento</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  FECHA DE CONVERSION /////////////////////// -->

								  		<?php

								  		$fechaConversion = array(
								              'name'        => 'user_horizontal_fechaConversion',
								              'id'          => 'user_horizontal_fechaConversion',
								              'type'		=> 'date',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required',
								              'value'		=> $fechaConversionEditar
								         );							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_fechaConversion"><abbr title="Campo Obligatorio">*</abbr> Fecha de Conversión</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($fechaConversion); ?>
								  				<p class="help-block">Fecha en la que acepto a jesús</p>
								  			</div>
								  		</div>

								  		<?php 
								  			$atributosSubmit ='class="btn btn-primary" id="submitAgregarMiembro"';
								  			echo form_submit('commit', 'Registrar Miembro',$atributosSubmit); 
								  			echo form_close();
								  		?>

								</div>
  							</div>
						</div>
						<div class="op3">
							<div class="row errorFormularioAsistenciaCheckRepetidos">
								<div class="col-md-8 col-md-offset-2">
									<h1><span>Error!!!!</span> Verifica que no halla usuarios con ambas casillas marcadas. </h1>
								</div>
							</div>
							<div class="panel panel-primary">
							    <div class="panel-heading">Formulario de Asistencia</div>
							    <div class="panel-body">
							   		<?php 
							   			//var_dump($miembro);
							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_asistencia', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('miembros/agregarAsistencia',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('idMiembro',$id);
												 echo form_hidden($datahidden);
											?>
										</div>
										<!--/////////////////  NOMBRE /////////////////////// -->
										<?php 
										
							   			$nombreEvento = array(
								              'name'        => 'user_horizontal_nameEvento',
								              'id'          => 'user_horizontal_nameEvento',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required'
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_name"><abbr title="Campo Obligatorio">*</abbr> Actividad</label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('user_horizontal_nameEvento',$actividadesRegulares, 8,$nombreEvento) ?>
								  				<p class="help-block">Nombre de la Actividad</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  FECHA Evento /////////////////////// -->

										<?php 

							   			$fechaAsistencia = array(
								              'name'        => 'user_horizontal_fechaEvento',
								              'id'          => 'user_horizontal_fechaEvento',
								              'type'		=> 'date',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required'
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_fechaEvento"><abbr title="Campo Obligatorio">*</abbr> Fecha Evento</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($fechaAsistencia); ?>
								  				<p class="help-block">Fecha Evento</p>
								  			</div>
								  		</div>



								  		<div class="row">
											<div class="col-md-4">
												<strong>Nombre</strong>
											</div>
											<div class="col-md-2">
												<strong>Presente</strong>
											</div>
											<div class="col-md-2">
												<strong>Ausente</strong>
											</div>
										</div>

										<?php 
										
										foreach ($miembros as $key => $value) {
											?>
											<div class="row">
												<div class="col-md-4">
													<!--<?php echo $value['id']; ?>-->
													<p><?php echo $value['nombre'].' '. $value['apellido'] ; ?></p>
												</div>
												<div class="col-md-2">
													<?php 
														$dataCheckboxAsistenciaPresente = array(
																    'name'        => 'checkboxAsistencia[]',
																    'id'          => $value['id'],
																    'value'       => 'presente'. ' ' .$value['id'],
																    'checked'     => False
																    );
														echo form_checkbox($dataCheckboxAsistenciaPresente); 
													 ?>
												</div>
												<div class="col-md-2">
													<?php 
														$dataCheckboxAsistenciaAusente = array(
																    'name'        => 'checkboxAsistencia[]',
																    'id'          => $value['id'],
																    'value'       => 'ausente'. '  ' .$value['id'],
																    'checked'     => False
																    );
														echo form_checkbox($dataCheckboxAsistenciaAusente); 
													 ?>
												</div>
											</div>

											<?php

										}
									 ?>

									 <?php 
							  			$atributosSubmit ='class="btn btn-primary" id="submitAgregarAsitencia"';
							  			echo form_submit('commit', 'Registrar Asistencia',$atributosSubmit);
							  			echo form_close(); 
								  	?>
								  	</div>
								</div>
						</div>
						<div class="op4">
							<h1>Actividades y Eventos</h1>

							<div class="row">
								<div class="col-md-4">
									<strong>Nombre del Evento</strong>
								</div>

								<div class="col-md-4">
									<strong>Fecha del Evento</strong>
								</div>

								<div class="col-md-4">
									<strong>Estadistica de asistencia</strong>
								</div>
							</div>
								<?php 
								//var_dump($actividades);	
									foreach ($actividades as $key => $value) {
										?>
										<div class="row">
											<div class="col-md-4">
												<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Actividad/<?php echo $value['id']; ?>"><p><?php echo $value['nombreActividadRegular']; ?></p></a>
											</div>
											<div class="col-md-4">
												<p><?php echo $value['fecha']; ?></p>
											</div>
											<div class="col-md-4">
												<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Estadistica/<?php echo $value['id']; ?>"><p>Ver Estadistica</p></a>
											</div>
										</div>

										<?php

									}
								?>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>