<?php if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 

	
// Crear mantenimiento de editar los ministerios creados.

?>
<div class="container-fluid">	

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="row">
				<h1>Mantenimiento de Ministerios</h1>
			</div>

		
			
			<div class="row">
				<div class="col-md-12 cargandoContenedor">
					<div class="row BoxGeneralMembresia">
						<div class="col-md-2 menuLateralMinisterio">
							<ul class="nav nav-pills nav-stacked">
								<li><a id="opcio11" href="#">Ver Ministerios</a></li>
								<li><a id="opcio22" href="#">Agregar Ministerio</a></li>
								<li><a id="opcio1" href="#">Caballeros</a></li>
								<li><a id="opcio2" href="#">Damas</a></li>
								<li><a id="opcio3" href="#">Jovenes</a></li>
								<li><a id="opcio4" href="#">Adoslecentes</a></li>
								<li><a id="opcio5" href="#">Ninos</a></li>
								<li><a id="opcio6" href="#">Evangelismo</a></li>
								<li><a id="opcio7" href="#">Oración</a></li>
								<li><a id="opcio8" href="#">Adoración</a></li>
								<li role="presentation" class="dropdown">
			        				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			          					Eventos <span class="caret"></span>
			       					 </a>
							        <ul class="dropdown-menu">					          
							          <li><a id="opcio7" href="">Otra Cosita</a></li>
							          <li role="separator" class="divider"></li>
							          <li><a href="#">Separated link</a></li>
							        </ul>
								</li>
							</ul>
						</div>


						<div class="col-md-8 contenidoMinisterio">
							

							<div class="opcio11">
								<div class="row">
									<div class="col-md-10 col-md-offset-1">
										<?php 
											//var_dump($ministerios);
											$contador =1;
											for ($i=0; $i < count($ministerios); $i++) {?> 
												<div class="col-md-3">
													<a class="redirectMinisterios" href="ministerios?idMinisterio=<?php echo $ministerios[$i]['id'] ?>">
														<div class="row margenRow">
															<img src="../application/imagesMinisterio/<?php echo $ministerios[$i]['logo'] ?>" alt="">
														</div>
													</a>
													
												</div>	
												<?php if($contador%4==0){ ?>
														</div><div class="row ">
												<?php } ?>										
											<?php }

										 ?>
									</div>									
								</div>									
	    									
							</div>
							<div class="opcio22">
								<div class="panel panel-primary">
							    <div class="panel-heading">Formulario de ministerio</div>
							    <div class="panel-body">
							   		<?php 
							   			//var_dump($miembro);
							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_celula', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('gcrecimiento/agregarCelula',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('id',$id);
												 echo form_hidden($datahidden);
											?>
										</div>

										<!--/////////////////  Nombre Ministerio /////////////////////// -->

								  		<?php 

							   			$nombre = array(
								              'name'        => 'nombre_ministerio',
								              'id'          => 'nombre_ministerio',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Nombre del Ministerio',
								              'required'    => 'required',
								              'value'		=>  $nombre_ministerio
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email col-sm-3 control-label" for="nombre_ministerio"><abbr title="Campo Obligatorio">*</abbr>Nombre Ministerio:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($nombre); ?>
								  				<p class="help-block">Nombre de Ministerio</p>
								  			</div>
								  		</div>		

										<!--/////////////////  Lider /////////////////////// -->

										<?php 
										if(isset($lider2)){
											$valueLider = $lider2;
										}else{
											$valueLider =1;
										}
										
							   			$lider = array(
								              'name'        => 'user_horizontal_lider',
								              'id'          => 'user_horizontal_lider',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Nombre',
								              'required'    => 'required'

								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_lider"><abbr title="Campo Obligatorio">*</abbr> Nombre del Lider:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('lider',$reponsables, $valueLider,$lider) ?>								  				
								  				<p class="help-block">Nombre Completo</p>
								  			</div>
								  		</div>	

								  		<!--/////////////////  Imagen /////////////////////// -->

								  		<?php 

							   			$imagen= array(
								              'name'        => 'user_horizontal_imagen_ministerio',
								              'id'          => 'user_horizontal_imagen_ministerio',
								              'class'		=> 'string email required form-control',
								              'type'    => 'file'
								         );
							   			?>
							   			<!--<div id="imagenMostrar" class="row <?php if((isset($_GET['n']) && isset($_GET['id']) && $imagenEditar == null) || (isset($_GET['n']) && !isset($_GET['id']) )){echo "hidden";} ?>">
								  			<div class="col-md-4 col-md-offset-3">
								  				<img style="width:100%;border-radius: 10px;" src="<?php echo base_url(); ?>/application/noticias/<?php echo $imagenEditar ?>">
								  			</div>		
								  			<div class="col-md-2">
								  				<a style="margin-top:135%" href="" name="cambiar" class="btn btn-info" id="cambiarImagenNoticia">Cambiar Imagen</a>
								  			</div>
								  			<div class="col-md-2">
								  				<a style="margin-top:135%;margin-left:25%" href="1" name="cambiar" class="btn btn-info" id="eliminarImagenNoticia">Eliminar Imagen</a>
								  			</div>							  				
								  		</div>-->
							   			<div style="margin-top:2%;" class="form-group email required user_horizontal_email">
								  			<label class="email col-sm-3 control-label" for="user_horizontal_imagen_ministerio"> Imagen:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($imagen); ?>
								  			</div>
								  		</div>



								  		<?php 
								  			$atributosSubmit ='class="btn btn-primary" id="submitAgregarMinisterio" style="margin-left: 4%;margin-bottom: 2%;margin-top:3%"';
								  			echo form_submit('Celula', 'Crear Ministerio',$atributosSubmit); 
								  		?>

								</div>
  							</div>
							</div>
							<div class="opcio1">

								<ul class="nav nav-tabs" role="tablist" id="TabsId">
								    <li role="presentation" class="active"><a class="ponerVisibleElInputBuscair" href="#primera-seccion" aria-controls="primera-seccion" role="tab" data-toggle="tab">Miembros</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#segunda-seccion" aria-controls="segunda-seccion" role="tab" data-toggle="tab">Eventos</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#tercera-seccion" aria-controls="tercera-seccion" role="tab" data-toggle="tab">Tercera sección</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#cuarta-seccion" aria-controls="cuarta-seccion" role="tab" data-toggle="tab">Cuarta sección</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#quinta-seccion" aria-controls="quinta-seccion" role="tab" data-toggle="tab">Quinta sección</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#sexta-seccion" aria-controls="sexta-seccion" role="tab" data-toggle="tab">Sexta sección</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#septima-seccion" aria-controls="septima-seccion" role="tab" data-toggle="tab">Septima sección</a></li>							    
								</ul>

								<div class="tab-content tab-Contenido">

									<!--Primera seccion-->
	    							<div role="tabpanel" class="tab-pane fade in active" id="primera-seccion">
										<div role="tabpanel" class="tab-pane fade in active" id="home">
	    								<div class="row">
											<div class="col-md-4">
												<div class="row">
													<div class="col-md-8">
														<strong>Asistencia Alta</strong>
													</div>
													<div class="col-md-1" style='background-color:#337ab7;min-height: 20px;'>													
													</div>												
												</div>	
											</div>
											<div class="col-md-4">
												<div class="row">
													<div class="col-md-8">
														<strong>Asistencia Media</strong>
													</div>
													<div class="col-md-1" style='background-color:#E6A25A;min-height: 20px;'>												
													</div>												
												</div>
											</div>
											<div class="col-md-4">
												<div class="row">
													<div class="col-md-8">
														<strong>Asistencia Baja</strong>
													</div>
													<div class="col-md-1" style='background-color:#FF000A;min-height: 20px;'>												
													</div>												
												</div>
											</div>
										</div>
	    								<div class="row" style="margin-top:5%;">
											<div class="col-md-4">
												<strong>Nombre Completo</strong>
											</div>
											<div class="col-md-3">
												<strong>Estadistica</strong>
											</div>
											<div class="col-md-2">
												<strong>Operacion</strong>
											</div>
										</div>
										<div class="boxMiembrosPrincipal">
											<?php 
												//var_dump($miembros1);
												$CI =& get_instance();
												foreach ($miembrosVarones as $key => $value) {

													$porcentajeString = $CI->porcentajeIndividualPorMiembrosColor($value['id']);
													$porcentajeInt = (int)$porcentajeString;
													//var_dump($porcentajeInt);
													?>
													<div class="row">
														<div class="col-md-4">
															<a class="various fancybox.ajax <?php if($porcentajeInt<=40){echo 'colorRojo';} if($porcentajeInt>40 && $porcentajeInt<=70) { echo 'colorOrange';}?>" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['id']; ?>"><p><?php echo $value['nombre'].' '. $value['apellido'] ; ?></p></a>
														</div>
														<div class="col-md-3">
															<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/ministerios/ver_estadisticas_asistencia_caballeros?id=<?php echo $value['id']; ?>">Asistencia a Eventos</a>
														</div>
														<div class="col-md-2">
															<a href="<?php echo base_url(); ?>index.php/miembros?a=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
														</div>
													</div>

													<?php

												}
											 ?>
										 </div>
	    							</div>				
										
	    							</div>
	    							<!--segunda seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="segunda-seccion">
								    	<div class="row">
												<div class="col-md-3">
													<strong>Nombre del Evento</strong>
												</div>

												<div class="col-md-3">
													<strong>Encargado</strong>
												</div>

												<div class="col-md-2">
													<strong>Fecha</strong>
												</div>

												<div class="col-md-2">
													<strong>Operaciones</strong>
												</div>
											</div>
												<?php 
													foreach ($eventosVarones as $key => $value) {
														?>
														<div class="row">
															<div class="col-md-3">
																<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/eventos/ver_Evento/<?php echo $value['id']; ?>"><p><?php echo $value['nombre']; ?></p></a>
															</div>															
															<div class="col-md-3">
																<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['responsable']; ?>"><p><?php echo $value['nombre_completo']; ?></p></a>
															</div>
															<div class="col-md-2">
																<p><?php echo $value['fecha']; ?></p>
															</div>
															<div class="col-md-2">
																<a href="<?php echo base_url(); ?>index.php/eventos?e=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
															</div>
														</div>

														<?php

													}
												?>
								    </div>
								    <!--tercera seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="tercera-seccion">
								    	<p>3</p>
								    </div>
								    <!--cuarta seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="cuarta-seccion">
								    	<p>4</p>
								    </div>
								    <!--quinta seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="quinta-seccion">
								    	<p>5</p>
								    </div>
								    <!--sexta seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="sexta-seccion">
								    	<p>6</p>
								    </div>
								    <!--septima seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="septima-seccion">
								    	<p>7</p>
								    </div>
							</div>
							<div class="row resenaEstadistica">
									<div class="col-md-12">
										<p>Reseña Estadística de la Membresía</p>
									</div>
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-3">
												<strong>Cantidad Total:</strong><span><?php echo $miembrosTotal[0]['cantidad']; ?></span>
											</div>
											<div class="col-md-3">
												<strong>Cantidad Hombre:</strong><span><?php echo $miembrosTotalHombre[0]['cantidadHombre']; ?></span>
											</div>
										</div>
									</div>
								</div>
						</div>
						<div class="opcio2">
							<ul class="nav nav-tabs" role="tablist" id="TabsId">
								    <li role="presentation" class="active"><a class="ponerVisibleElInputBuscair" href="#primera-seccion2" aria-controls="primera-seccion" role="tab" data-toggle="tab">Miembros</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#segunda-seccion2" aria-controls="segunda-seccion" role="tab" data-toggle="tab">Eventos</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#tercera-seccion2" aria-controls="tercera-seccion" role="tab" data-toggle="tab">Tercera sección</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#cuarta-seccion2" aria-controls="cuarta-seccion" role="tab" data-toggle="tab">Cuarta sección</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#quinta-seccion2" aria-controls="quinta-seccion" role="tab" data-toggle="tab">Quinta sección</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#sexta-seccion2" aria-controls="sexta-seccion" role="tab" data-toggle="tab">Sexta sección</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#septima-seccion2" aria-controls="septima-seccion" role="tab" data-toggle="tab">Septima sección</a></li>							    
								</ul>

								<div class="tab-content tab-Contenido">

									<!--Primera seccion-->
	    							<div role="tabpanel" class="tab-pane fade in active" id="primera-seccion2">
										<div role="tabpanel" class="tab-pane fade in active" id="home">
	    								<div class="row">
											<div class="col-md-4">
												<div class="row">
													<div class="col-md-8">
														<strong>Asistencia Alta</strong>
													</div>
													<div class="col-md-1" style='background-color:#337ab7;min-height: 20px;'>													
													</div>												
												</div>	
											</div>
											<div class="col-md-4">
												<div class="row">
													<div class="col-md-8">
														<strong>Asistencia Media</strong>
													</div>
													<div class="col-md-1" style='background-color:#E6A25A;min-height: 20px;'>												
													</div>												
												</div>
											</div>
											<div class="col-md-4">
												<div class="row">
													<div class="col-md-8">
														<strong>Asistencia Baja</strong>
													</div>
													<div class="col-md-1" style='background-color:#FF000A;min-height: 20px;'>												
													</div>												
												</div>
											</div>
										</div>
	    								<div class="row" style="margin-top:5%;">
											<div class="col-md-4">
												<strong>Nombre Completo</strong>
											</div>
											<div class="col-md-3">
												<strong>Estadistica</strong>
											</div>
											<div class="col-md-2">
												<strong>Operacion</strong>
											</div>
										</div>
										<div class="boxMiembrosPrincipal">
											<?php 
												//var_dump($miembros1);
												$CI =& get_instance();
												foreach ($miembrosMujeres as $key => $value) {

													$porcentajeString = $CI->porcentajeIndividualPorMiembrosColor($value['id']);
													$porcentajeInt = (int)$porcentajeString;
													//var_dump($porcentajeInt);
													?>
													<div class="row">
														<div class="col-md-4">
															<a class="various fancybox.ajax <?php if($porcentajeInt<=40){echo 'colorRojo';} if($porcentajeInt>40 && $porcentajeInt<=70) { echo 'colorOrange';}?>" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['id']; ?>"><p><?php echo $value['nombre'].' '. $value['apellido'] ; ?></p></a>
														</div>
														<div class="col-md-3">
															<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/ministerios/ver_estadisticas_asistencia_caballeros?id=<?php echo $value['id']; ?>">Asistencia a Eventos</a>
														</div>
														<div class="col-md-2">
															<a href="<?php echo base_url(); ?>index.php/miembros?a=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
														</div>
													</div>

													<?php

												}
											 ?>
										 </div>
	    							</div>				
										
	    							</div>
	    							<!--segunda seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="segunda-seccion2">
								    	<div class="row">
												<div class="col-md-3">
													<strong>Nombre del Evento</strong>
												</div>

												<div class="col-md-3">
													<strong>Encargado</strong>
												</div>

												<div class="col-md-2">
													<strong>Fecha</strong>
												</div>

												<div class="col-md-2">
													<strong>Operaciones</strong>
												</div>
											</div>
												<?php 
													foreach ($eventosMujeres as $key => $value) {
														?>
														<div class="row">
															<div class="col-md-3">
																<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/eventos/ver_Evento/<?php echo $value['id']; ?>"><p><?php echo $value['nombre']; ?></p></a>
															</div>															
															<div class="col-md-3">
																<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['responsable']; ?>"><p><?php echo $value['nombre_completo']; ?></p></a>
															</div>
															<div class="col-md-2">
																<p><?php echo $value['fecha']; ?></p>
															</div>
															<div class="col-md-2">
																<a href="<?php echo base_url(); ?>index.php/eventos?e=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
															</div>
														</div>

														<?php

													}
												?>
								    </div>
								    <!--tercera seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="tercera-seccion2">
								    	<?php var_dump($jovenes); ?>
								    </div>
								    <!--cuarta seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="cuarta-seccion2">
								    	<p>4</p>
								    </div>
								    <!--quinta seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="quinta-seccion2">
								    	<p>5</p>
								    </div>
								    <!--sexta seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="sexta-seccion2">
								    	<p>6</p>
								    </div>
								    <!--septima seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="septima-seccion2">
								    	<p>7</p>
								    </div>
							</div>
						</div>
						<div class="opcio3">
							<?php
								$jovenes2 = array();
								$date2 = new DateTime("now");
								foreach ($jovenes as $key => $value) {
									$date1 =  new DateTime($value['fechaNacimiento']);									
									$edad = $date1->diff($date2); 

									if($edad->y >= 18  && $edad->y <= 30 ){
										if(count($value) != 0){
											$jovenes2[$key] = $value;	
										}
										
									}
								}									
							?>

							<ul class="nav nav-tabs" role="tablist" id="TabsId">
								    <li role="presentation" class="active"><a class="ponerVisibleElInputBuscair" href="#primera-seccion2" aria-controls="primera-seccion" role="tab" data-toggle="tab">Miembros</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#segunda-seccion2" aria-controls="segunda-seccion" role="tab" data-toggle="tab">Eventos</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#tercera-seccion2" aria-controls="tercera-seccion" role="tab" data-toggle="tab">Tercera sección</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#cuarta-seccion2" aria-controls="cuarta-seccion" role="tab" data-toggle="tab">Cuarta sección</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#quinta-seccion2" aria-controls="quinta-seccion" role="tab" data-toggle="tab">Quinta sección</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#sexta-seccion2" aria-controls="sexta-seccion" role="tab" data-toggle="tab">Sexta sección</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#septima-seccion2" aria-controls="septima-seccion" role="tab" data-toggle="tab">Septima sección</a></li>							    
								</ul>

								<div class="tab-content tab-Contenido">

									<!--Primera seccion-->
	    							<div role="tabpanel" class="tab-pane fade in active" id="primera-seccion2">
										<div role="tabpanel" class="tab-pane fade in active" id="home">
	    								<div class="row">
											<div class="col-md-4">
												<div class="row">
													<div class="col-md-8">
														<strong>Asistencia Alta</strong>
													</div>
													<div class="col-md-1" style='background-color:#337ab7;min-height: 20px;'>													
													</div>												
												</div>	
											</div>
											<div class="col-md-4">
												<div class="row">
													<div class="col-md-8">
														<strong>Asistencia Media</strong>
													</div>
													<div class="col-md-1" style='background-color:#E6A25A;min-height: 20px;'>												
													</div>												
												</div>
											</div>
											<div class="col-md-4">
												<div class="row">
													<div class="col-md-8">
														<strong>Asistencia Baja</strong>
													</div>
													<div class="col-md-1" style='background-color:#FF000A;min-height: 20px;'>												
													</div>												
												</div>
											</div>
										</div>
	    								<div class="row" style="margin-top:5%;">
											<div class="col-md-4">
												<strong>Nombre Completo</strong>
											</div>
											<div class="col-md-3">
												<strong>Estadistica</strong>
											</div>
											<div class="col-md-2">
												<strong>Operacion</strong>
											</div>
										</div>
										<div class="boxMiembrosPrincipal">
											<?php 
												//var_dump($miembros1);
												$CI =& get_instance();
												foreach ($jovenes2 as $key => $value) {

													$porcentajeString = $CI->porcentajeIndividualPorMiembrosColor($value['id']);
													$porcentajeInt = (int)$porcentajeString;
													//var_dump($porcentajeInt);
													?>
													<div class="row">
														<div class="col-md-4">
															<a class="various fancybox.ajax <?php if($porcentajeInt<=40){echo 'colorRojo';} if($porcentajeInt>40 && $porcentajeInt<=70) { echo 'colorOrange';}?>" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['id']; ?>"><p><?php echo $value['nombre'].' '. $value['apellido'] ; ?></p></a>
														</div>
														<div class="col-md-3">
															<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/ministerios/ver_estadisticas_asistencia_caballeros?id=<?php echo $value['id']; ?>">Asistencia a Eventos</a>
														</div>
														<div class="col-md-2">
															<a href="<?php echo base_url(); ?>index.php/miembros?a=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
														</div>
													</div>

													<?php

												}
											 ?>
										 </div>
	    							</div>				
										
	    							</div>
	    							<!--segunda seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="segunda-seccion2">
								    	<div class="row">
												<div class="col-md-3">
													<strong>Nombre del Evento</strong>
												</div>

												<div class="col-md-3">
													<strong>Encargado</strong>
												</div>

												<div class="col-md-2">
													<strong>Fecha</strong>
												</div>

												<div class="col-md-2">
													<strong>Operaciones</strong>
												</div>
											</div>
												<?php 
													foreach ($eventosMujeres as $key => $value) {
														?>
														<div class="row">
															<div class="col-md-3">
																<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/eventos/ver_Evento/<?php echo $value['id']; ?>"><p><?php echo $value['nombre']; ?></p></a>
															</div>															
															<div class="col-md-3">
																<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['responsable']; ?>"><p><?php echo $value['nombre_completo']; ?></p></a>
															</div>
															<div class="col-md-2">
																<p><?php echo $value['fecha']; ?></p>
															</div>
															<div class="col-md-2">
																<a href="<?php echo base_url(); ?>index.php/eventos?e=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
															</div>
														</div>

														<?php

													}
												?>
								    </div>
								    <!--tercera seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="tercera-seccion2">
								    	<?php var_dump($jovenes2); ?>
								    </div>
								    <!--cuarta seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="cuarta-seccion2">
								    	<p>4</p>
								    </div>
								    <!--quinta seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="quinta-seccion2">
								    	<p>5</p>
								    </div>
								    <!--sexta seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="sexta-seccion2">
								    	<p>6</p>
								    </div>
								    <!--septima seccion-->
								    <div role="tabpanel" class="tab-pane fade" id="septima-seccion2">
								    	<p>7</p>
								    </div>
							</div>
						</div>
						<div class="opcio4">
							<p>4</p>
						</div>
						<div class="opcio5">
							<p>5</p>
						</div>
						<div class="opcio6">
							<p>6</p>
						</div>
						<div class="opcio7">
							<p>7</p>
						</div>
						<div class="opcio8">
							<p>8</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>