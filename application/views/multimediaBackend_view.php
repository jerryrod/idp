<?php 
 if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 

	$id = '';
	$nombreAudioEditar ='';
	$nombreCarpetaEditar ='';
	$fechaAudioEditar='';
	$audioEditar='';

	if(isset($audio)){
		//var_dump($audio);
		$id =$audio->id;
		$nombreAudioEditar = $audio->nombreAudio;
		$nombreCarpetaEditar =$audio->nombreCarpeta;
		$fechaAudioEditar = $audio->fecha_audio;
		$audioEditar =$audio->tmp_name;
	}

	//var_dump($lider);
 ?>
<div class="container">
	<div class="row">
		<div class="col-md-10 ">
			<div class="row">
				<div class="col-md-12">
					<h1>Mantenimiento de Multimedia</h1>
					<center class="cargandoMultimedia">
						<img id="cargandoMultimedia" src="<?php echo base_url(); ?>/application/img/loading.gif">
					</center>
				</div>
				<div class="col-md-12 errorFormularioNoticias">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1><span>Error!!!!</span> Verifica que los campos esten introducidos de la manera correcta. </h1>
						</div>
					</div>
					
				</div>
				<div class="col-md-12 successFormularioNoticias">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1><span>Usuario!!!!</span> Insertado de manera correcta. </h1>
						</div>
					</div>
					
				</div>
				<div class="col-md-12 cargandoContenedor">
					<div class="row BoxGeneralNoticias">
						<div class="col-md-3 menuLateralMultimedia">
							<ul class="nav nav-pills nav-stacked">
								<li><a id="opcionMultimedia1" href="">Archivos Creados</a></li>
								<li><a id="opcionMultimedia2" href="">Agregar Carpeta de Audio</a></li>
								<li><a id="opcionMultimedia3" href="">Agregar Audio</a></li>
								<li><a id="opcionMultimedia4" href="">Agregar Carpeta de Foto</a></li>
								<li><a id="opcionMultimedia5" href="">Agregar Fotos</a></li>
							</ul>
						</div>
						<div class="col-md-9 contenidoMultimedia">
							<div class="cajaDialogoEliminarAudio">
							</div>
							<div class="opcionMultimedia1">
								<ul class="nav nav-tabs" role="tablist" id="TabsMultimedia">
								    <li role="presentation" class="active"><a class="ponerVisibleElInputBuscar" href="#audios" aria-controls="home" role="tab" data-toggle="tab">Audios</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#fotos" aria-controls="profile" role="tab" data-toggle="tab">Fotos</a></li>
								    <li role="presentation"><a class="ponerOcultoElInputBuscar" href="#videos" aria-controls="profile" role="tab" data-toggle="tab">Videos</a></li>
								</ul>

								<div class="tab-content tab-Contenido">
									<!--Membresia Total-->
	    							<div role="tabpanel" class="tab-pane fade in active" id="audios">    								
										<div class="row">
											<div class="col-md-4">
												<strong>Nombre Audio</strong>
											</div>
											<div class="col-md-2">
												<strong>Grupo</strong>
											</div>
											<div class="col-md-2">
												<center>
													<strong>Operaciónes</strong>
												</center>												
											</div>
										</div>
										<div class="boxMiembrosPrincipal">
											<?php 
												//var_dump($audios);
												//$CI =& get_instance();
												foreach ($audios as $key => $value) {

													//$porcentajeString = $CI->porcentajeIndividualPorMiembrosColor($value['id']);
													//$porcentajeInt = (int)$porcentajeString;
													//var_dump($porcentajeInt);
													?>
													<div class="row">
														<div class="col-md-4">
															<p><?php echo $value['nombreAudio'] ; ?></p>
														</div>
														<div class="col-md-2">
															<p><?php echo $value['nombreCarpeta']; ?></p>
														</div>
														<div class="col-md-1">
															<center>
																<a href="<?php echo base_url(); ?>index.php/multimediaBackend?m=3&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
															</center>
														</div>
														<!--<div class="col-md-1">
															<center>
																<a id="removerAudio" href="<?php echo base_url(); ?>index.php/multimediaBackend?id=<?php echo $value['id']; ?>"><p style="font-size: 25px;color: #CA2727;" class="glyphicon glyphicon-remove-circle" i></p></a>
															</center>
														</div>-->
													</div>
													<?php
												}
											 ?>
										 </div>
	    							</div>
	    							<!--Galeria-->
								    <div role="tabpanel" class="tab-pane fade" id="fotos">
								    	<div class="row">
											<div class="col-md-4">
												<strong>Nombre del Album</strong>
											</div>
											<div class="col-md-3">
												<strong>Fecha de Creacción</strong>
											</div>
											<div class="col-md-2">
												<center>
													<strong>Operaciónes</strong>
												</center>												
											</div>
										</div>
										<div class="boxMiembrosPrincipal">
											<?php 
												//var_dump($audios);
												//$CI =& get_instance();
												foreach ($albunFotos as $key => $value) {

													//$porcentajeString = $CI->porcentajeIndividualPorMiembrosColor($value['id']);
													//$porcentajeInt = (int)$porcentajeString;
													//var_dump($albunFotos);
													?>
													<div class="row">
														<div class="col-md-4">
															<p><?php echo $value['nombre'] ; ?></p>
														</div>
														<div class="col-md-3">
															<p><?php echo date_format(date_create($value['fechaCreacion']), 'd/m/Y');?></p>
														</div>
														<div class="col-md-1">	
															<center>
																<a href="<?php echo base_url(); ?>index.php/multimediaBackend?m=5&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
															</center>
														</div>
														<div class="col-md-1">
															<center>
																<a class="removerAlbumFoto" href="<?php echo base_url(); ?>index.php/multimediaBackend?id=<?php echo $value['id']; ?>"><p style="font-size: 25px;color: #CA2727;" class="glyphicon glyphicon-remove-circle" i></p></a>
															</center>
														</div>                 
													</div>
													<?php
												}
											 ?>
										 </div>
								    </div>
								    <div role="tabpanel" class="tab-pane fade" id="videos">
								    	<h1>Videos</h1>
								    </div>
										
								</div>
							</div>
							<div class="opcionMultimedia2">
								<div class="panel panel-primary">
								    <div class="panel-heading">Formulario de Carpeta</div>
								    <div class="panel-body">
								    	<?php 
							   			//var_dump($miembro);
							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_audio', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('multimedia/agregarAudio',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('id',$id);
												 echo form_hidden($datahidden);
											?>
										</div>
										<!--/////////////////  NOMBRE /////////////////////// -->
										<?php 
										
							   			$nombre = array(
								              'name'        => 'user_horizontal_name_carpeta',
								              'id'          => 'user_horizontal_name_carpeta',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Nombre Carpeta',
								              'required'    => 'required'
								         );
							   			?> 

							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_name_carpeta"><abbr title="Campo Obligatorio">*</abbr> Nombre:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($nombre); ?>
								  				<p class="help-block">Escriba el nombre de la carpeta que desea agregar</p>
								  			</div>
								  		</div>

								  		<?php 
								  			$atributosSubmit ='class="btn btn-primary" id="submitAgregarCarpeta"';
								  			echo form_submit('commit', 'Agregar Carpeta',$atributosSubmit); 
								  			echo form_close();
								  		?>
								    </div>
								</div>

							</div>
							<div class="opcionMultimedia3">
								<div class="panel panel-primary">
								    <div class="panel-heading">Formulario de Audio</div>
								    <div class="panel-body">
								   		<?php 		

								   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_mp3', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

								   			echo form_open('lideresBackend/agregarLider',$attributes); 

								   			$datahidden = array(
									              'utf8'  => '✓',
									              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
									            );
								   			?>
								   			<div style="display:none">
												<?php echo form_hidden('id',$id);
													 echo form_hidden($datahidden);
												?>
											</div>

											<!--/////////////////  NOMBRE /////////////////////// -->
											<?php 
											
								   			$nombre = array(
									              'name'        => 'user_horizontal_name_audio',
									              'id'          => 'user_horizontal_name_audio',
									              'class'		=> 'string email required form-control',
									              'placeholder' => 'Nombre Audio',
									              'required'    => 'required',
									              'value'		=>  $nombreAudioEditar
									         );
								   			?> 

								   			<div class="form-group email required user_horizontal_email">
									  			<label class="email required col-sm-3 control-label" for="user_horizontal_name_audio"><abbr title="Campo Obligatorio">*</abbr> Nombre:</label>
									  			<div class="col-sm-9">
									  				<?php echo form_input($nombre); ?>
									  				<p class="help-block">Escriba el nombre del Audio</p>
									  			</div>
									  		</div>

											<!--/////////////////  Carpetas Creadas  /////////////////////// -->
									  		<?php
												if(isset($nombreCarpetaEditar)){
													$valueEstado = $nombreCarpetaEditar;
												}else{
													$valueEstado =1;
												}
									  			$atributosCarpetaAudio ='id="inputCarpetaAudio" class="string email required form-control"';
									  		 ?>
									  		

									  		<div class="form-group email required user_horizontal_email">
									  		<label class="email col-sm-3 control-label" for="user_horizontal_email"> Carpeta: </label>
									  			<div class="col-sm-9">
									  				<?php echo form_dropdown('carpetaAudio',$carpetaAudio, $valueEstado,$atributosCarpetaAudio) ?>
									  				<p id="labeSeleccioneCarreras" class="help-block">Seleccione la Carpeta</p>
									  			</div>
									  		</div>									  		
									  											  		
									  		<!--/////////////////  audio /////////////////////// -->

									  		<?php 

								   			$audio= array(
									              'name'        => 'user_horizontal_imagen_lider2',
									              'id'          => 'user_horizontal_imagen_lider2',
									              'class'		=> 'string email required form-control',
									              'type'    => 'file',
									              'accept'  => '.mp3'
									         );

								   			?>
								   			<div class="form-group email required user_horizontal_email">
								   			<div id="AudioMostrar" class="row <?php if((!isset($_GET['l']) && !isset($_GET['id'])) || (isset($_GET['m']) && !isset($_GET['id']))){echo "hidden";} ?>">
									  			<div class="col-md-12">
									  				<div class="row">
									  					<label class="email col-sm-3 col-md-3 control-label" for="user_horizontal_email"> Audio: </label>
									  					<div class="col-md-3">
										  					<audio controls>
																<?php 
																	$nombre = explode('.',$audioEditar);

																 ?>
												  				<source src="<?php echo base_url().'application/audio/'.$audioEditar ?>" type="audio/<?php echo $nombre[1]; ?>">
															</audio>
										  				</div>	
									  				</div>
										  			<div class="row">
										  				<div class="col-md-2 col-md-offset-3">
										  					<a style="margin-top:10%;margin-bottom:20%" href=""  class="btn btn-info" id="cambiarAudio">Cambiar Audio</a>
											  			</div>
										  			</div>											  				
									  			</div>						  				
									  		</div>
								   			<div style="margin-top:2%;" id="audioInput" class="<?php if(isset($_GET['m']) && isset($_GET['id']) && $audioEditar != null){echo "hidden";} ?> form-group email required user_horizontal_email">
									  			<label class="email col-sm-3 control-label" for="user_horizontal_imagen_lider2"> Audio:</label>
									  			<div class="col-sm-9">
									  				<?php echo form_input($audio); ?>
									  			</div>
									  		</div>

									  		<!--/////////////////  FECHA Evento /////////////////////// -->

										<?php 

							   			$fechaAudio = array(
								              'name'        => 'fechaAudio',
								              'id'          => 'fechaAudio',
								              'type'		=> 'date',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required',
								              'value'		=> $fechaAudioEditar
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="fechaAudio"><abbr title="Campo Obligatorio">*</abbr> Fecha:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($fechaAudio); ?>
								  			</div>
								  		</div>

								  		<div class="row">

								  		<?php 
									  			$atributosSubmit ='class="btn btn-primary col-md-2  col-md-offset-3" id="submitAgregarLider2"';
									  			if((isset($_GET['m']) && !isset($_GET['id'])) || !isset($_GET['m']) && !isset($_GET['id'])){ echo form_submit('commit', 'Agregar Audio',$atributosSubmit);} 
									  			if(isset($_GET['m']) && isset($_GET['id'])){ echo form_submit('commit', 'Modificar Audio',$atributosSubmit);} 
									  			echo form_close();
									  		?>
									  		<input type="button" value="Cancelar" class="btn btn-danger col-md-offset-1 col-md-2 " id="cancelarAudio">
									  	</div>

									</div>
  								</div>
							</div>
							<div class="opcionMultimedia4">
								<div class="panel panel-primary">
								    <div class="panel-heading">Formulario de Carpeta de Foto</div>
								    <div class="panel-body">
								    	<?php 

							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_carpeta_foto', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data');

							   			echo form_open('multimediaBackend/agregarCarpetaFoto',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('id',$id);
												 echo form_hidden($datahidden);
											?>
										</div>
										<!--/////////////////  NOMBRE /////////////////////// -->
										<?php 
										
							   			$nombreCarpetaFoto = array(
								              'name'        => 'user_horizontal_name_carpeta_foto',
								              'id'          => 'user_horizontal_name_carpeta_foto',
								              'class'		=> 'string form-control',
								              'placeholder' => 'Nombre Carpeta',
								              'required'    => 'required'
								         );
							   			?> 

							   			<div class="form-group user_horizontal_email">
								  			<label class="col-sm-3 control-label" for="user_horizontal_name_carpeta_foto"><abbr title="Campo Obligatorio">*</abbr> Nombre:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($nombreCarpetaFoto); ?>
								  				<p class="help-block">Escriba el nombre de la carpeta que desea agregar</p>
								  			</div>
								  		</div>

								  		<!--/////////////// Tipo de Actividad \\\\\\\\\\\\\\\-->

								  		<?php 

								  			$options = array(
								  					  	''  => 'Elija una Opción',
									                  '1'  => 'Servicios Regulares',
									                  '2'    => 'Servicios Especiales',
									                  '3'   => 'Otros',
									                );

								  			$js = 'id="tipoCarpetaFoto" class="string form-control" onChange="agregarInputSegunCambio(this.options[this.selectedIndex].value);"';

								  		 ?>

								  		 <div class="form-group user_horizontal_email">
								  			<label class="col-sm-3 control-label" for="user_horizontal_name_carpeta_foto"><abbr title="Campo Obligatorio">*</abbr> Tipo de actividad:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('tipoCarpetaFoto',$options,'',$js); ?>
								  				<p class="help-block">Elija el tipo de actividad</p>
								  			</div>
								  		</div>

								  		<!--/////////////// Dia de Actividad  \\\\\\\\\\\\\\\-->

								  		<?php 

								  			$optionsRegulares = array(
								  					  '0' => 'Seleccione un dia',
									                  '1'  => 'Miercoles',
									                  '2'    => 'Viernes',
									                  '3'   => 'Domingo',
									                );

								  			$jsRegulares = 'id="diaServicio" class="string form-control"';

								  		 ?>

								  		 <div class="form-group user_horizontal_email hidden">
								  			<label class="col-sm-3 control-label" for="user_horizontal_name_carpeta_foto"><abbr title="Campo Obligatorio">*</abbr> Dia de Servicio:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('diaServicios',$optionsRegulares,'',$jsRegulares); ?>
								  				<p class="help-block">Elija el dia de el servicio</p>
								  			</div>
								  		</div>



								  		

								  		<?php 
								  			$atributosSubmitCarpetaFoto ='class="btn btn-primary" id="submitAgregarCarpetaFoto"';
								  			echo form_submit('commitCarpetaAudio', 'Agregar Carpeta',$atributosSubmitCarpetaFoto); 
								  			echo form_close();
								  		?>
								    </div>
								</div>
							</div>
							<div class="opcionMultimedia5">
								<div class="panel panel-primary">
								    <div class="panel-heading">Formulario de Fotos</div>
								    <div class="panel-body">
								   		<?php 		

								   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_foto', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

								   			echo form_open('lideresBackend/agregarLider',$attributes); 

								   			$datahidden = array(
									              'utf8'  => '✓',
									              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
									            );
								   			?>
								   			<div style="display:none">
												<?php echo form_hidden('id',$id);
													 echo form_hidden($datahidden);
												?>
											</div>

											<!--/////////////////  Carpetas Creadas  /////////////////////// -->
									  		<?php
												if(isset($nombreCarpetaEditar)){
													$valueEstado = $nombreCarpetaEditar;
												}else{
													$valueEstado =1;
												}
									  			$atributosCarpetaAudio ='id="inputCarpetaAudio" class="string email required form-control"';
									  		 ?>
									  		

									  		<div class="form-group email required user_horizontal_email">
									  		<label class="email col-sm-3 control-label" for="user_horizontal_email"> Carpeta: </label>
									  			<div class="col-sm-9">
									  				<?php echo form_dropdown('carpetaFoto',$carpetaFoto, $valueEstado,$atributosCarpetaAudio) ?>
									  				<p id="labeSeleccioneCarreras" class="help-block">Seleccione la Carpeta</p>
									  			</div>
									  		</div>									  		
									  											  		
									  		<!--/////////////////  Foto /////////////////////// -->

									  		<?php 

								   			$fotos= array(
									              'name'        => 'user_horizontal_imagen_fotos[]',
									              'id'          => 'user_horizontal_imagen_fotos[]',
									              'class'		=> 'string form-control',
									              'type'    => 'file',
									              ' multiple' => "true",
									              'accept'  => 'image/*'
									         );

								   			?>
								   			
								   			<?php if((isset($_GET['m']) && isset($_GET['id']) && $_GET['m']==5)){ ?>
								   				<center><h1>Fotos</h1></center>
								   				<div class="row " style="margin-left:2%">
													<?php
														foreach ($fotosBD as $key => $value) {			
												 	?>
													<div class="col-md-4 ">
														<div class="row cajaFotos">
															<img src="<?php echo base_url(); ?>application/imagenesAlbum/<?php echo $value['nombreCarperta'].'/'.$value['nombreFotoGUID']; ?>" alt="" /><br>
															
															<center>
																<a class="removerFoto" href="<?php echo base_url(); ?>index.php/multimediaBackend?id=<?php echo $value['id']; ?>"><p style="font-size: 25px;color: #CA2727;" class="glyphicon glyphicon-remove-circle" i></p></a>
															</center>
														
														</div>
													</div>
													<?php 
													} 
													?>														
													
												</div>
								   			<?php } else { ?>
									   		<div class="form-group user_horizontal_email">
									   			<div style="margin-top:2%;" id="audioInput" class="form-group user_horizontal_email">
										  			<label class="email col-sm-3 control-label" for="user_horizontal_imagen_lider2"> Fotos:</label>
										  			<div class="col-sm-9">
										  				<?php echo form_input($fotos); ?>
										  			</div>
										  		</div>
									  		</div>
									  		<?php } ?>

								  		<?php 
									  			$atributosSubmit ='class="btn btn-primary col-md-offset-3" id="submitAgregarFotos"';
									  		    echo form_submit('commit', 'Agregar Fotos',$atributosSubmit);									  			
									  			
									  		?>
									  		<input type="button" value="Cancelar" class="btn btn-danger " id="cancelarFotos">
									  	<?php echo form_close(); ?>
									</div>
  								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>