</div>
	<div class="row multimediaContenido2">
		<div class="col-md-10 col-md-offset-1">
			<ul class="nav nav-tabs" role="tablist" id="TabsMultimediaHome">
			    <li role="presentation" class="active"><a href="#audiosHome" aria-controls="home" role="tab" data-toggle="tab">Audios</a></li>
			    <li role="presentation"><a href="#fotosHome" aria-controls="profile" role="tab" data-toggle="tab">Galeria</a></li>
			    <li role="presentation"><a href="#videosHome" aria-controls="profile" role="tab" data-toggle="tab">Videos</a></li>
			</ul>

			<div class="tab-content tab-ContenidoMultimedia">
				<!--Membresia Total-->
				<div role="tabpanel" class="tab-pane fade in active" id="audiosHome">    	
					<div class="row">
						<?php 
							//var_dump($grupos);
							foreach ($gruposAudio as $key => $value) {
					 	?>
								<div class="col-md-3">
									<div class="row cajaGrupoAudio">
										<a href="<?php echo base_url(); ?>index.php/multimedia/ver_grupo_single/<?php echo $value['id'] ; ?>">
											<center>
												<img src="<?php echo base_url(); ?>application/img/Folder-Music-icon.png">
												<p><?php echo $value['nombre'] ; ?></p>
											</center>	
										</a>															
									</div>
								</div>
						<?php 
							} 
						?>
					</div>
				</div>
				<!--Galeria-->
			    <div role="tabpanel" class="tab-pane fade" id="fotosHome">
			    	<div class="row">
						<?php 
							//var_dump($grupos);
							foreach ($albumsFotos as $key => $value) {
					 	?>
								<div class="col-md-3">
									<div class="row cajaGrupoAudio">
										<a href="<?php echo base_url(); ?>index.php/multimedia/ver_album_single/<?php echo $value['id'] ; ?>">
											<center>
												<img src="<?php echo base_url(); ?>application/img/folder_256.png">
												<p><?php echo $value['nombre'] ; ?></p>
											</center>	
										</a>															
									</div>
								</div>
						<?php 
							} 
						?>
					</div>
			    </div>
			    <div role="tabpanel" class="tab-pane fade" id="videosHome">
			    	<h1>Videos</h1>
			    </div>
					
			</div>
		</div>		
	</div>


