</div>
	<!-- Bootstrap-->
	<link rel="stylesheet"  title="Estilo alternativo" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> 
	<div class="container-fluid" ng-controller="Multimedia">
		<div class="row multimediaContenido2">
			<center class="cargandoMultimedia">
				<img id="cargandoMultimedia" src="<?php echo base_url(); ?>/application/img/loading.gif">
			</center>
			<ul class="nav nav-tabs" role="tablist" id="TabsMultimediaHome">
			    <li role="presentation" class="active"><a id="audioReturn" href="#audiosHome"  aria-controls="home" role="tab" data-toggle="tab">Audios</a></li>
			    <li role="presentation"><a id="galeriaReturn"  href="#fotosHome" aria-controls="profile" role="tab" data-toggle="tab">Galeria</a></li>
			    <li role="presentation"><a id="videosReturn" href="#videosHome" aria-controls="profile" role="tab" data-toggle="tab">Videos</a></li>
			</ul>

			<div class="tab-content tab-ContenidoMultimedia">
				<!--Audios-->
				<div role="tabpanel" class="tab-pane fade in active" id="audiosHome"> 	
					<div class="container anulacion">			 	
						<div class="row">
							<div class="col-md-4 col-xs-6"><h3>Seccion de Audios</h3></div>
							<div class="col-md-3 pull-right col-xs-6"><button class="btn btn-primary" ng-click="btnAtras(parametroAtras)" ng-show="estados.buttonAtras" >Atras</button></div>
						</div>
						<div class="col-md-3 cajaSeriesAudio" ng-show="estados.Audio">
							<div class="row row2">
								<center>
									<img src="<?php echo base_url(); ?>/application/images/images2.png"/>
								</center>
							</div>
							<div class="row row2" ng-mouseenter="hoverTituloSerie=true"  ng-mouseleave="hoverTituloSerie=false">
								<div class="tituloSerie" ng-hide="hoverTituloSerie">
									<span >{{"Series de Mensajes" | uppercase }}</span>										
								</div>
								<div class="buttonAbrir" ng-click="getCategoriaAudio(1)" ng-show="hoverTituloSerie">
									<span >{{"abrir" | uppercase }}</span>
								</div>
								
							</div>
						</div>
						
						
						<div class="row" ng-show="estados.series" >
							<div class="col-md-3 cajaSeriesAudio" ng-repeat="serie in series.data.gruposAudio " ng-show="estados.RepeatSerie ">
								<div class="row row2">
									<center>
										<img src="<?php echo base_url(); ?>/application/images/images2.png"/>
									</center>
								</div>
								<div class="row row2" ng-mouseenter="hoverTituloSerie=true"  ng-mouseleave="hoverTituloSerie=false">
									<div class="tituloSerie" ng-hide="hoverTituloSerie">
										<span >{{serie.nombre | uppercase }}</span>										
									</div>
									<div class="buttonAbrir" ng-click="getAudiosSerie(serie.id)" ng-show="hoverTituloSerie">
										<span >{{"abrir" | uppercase }}</span>
									</div>
									
								</div>
							</div>
							<div class="col-md-3 cajaSeriesAudio" ng-repeat="audio in audios.data.data " ng-show="estados.RepeatAudio ">
								<div class="row row2">
									<center>
										<img src="<?php echo base_url(); ?>/application/images/images2.png"/>
									</center>
								</div>
								<div class="row row2">
									<div class="tituloSerie">
										<span >{{audio.nombreAudio | uppercase }}</span>
									</div>
								</div>
								<div class="row row2">
									<audio controls class="tagAudio" ng-click="changeImageAudio()">
										<source ng-src="{{'../application/audio/'+audio.tmp_name}}" type="audio/mp3">																
									</audio> 
								</div>
							</div>
							
						</div>
					</div>	
				</div>
				
				<!--Fotos-->
				<div role="tabpanel" class="tab-pane fade" id="fotosHome">
					<div class="container anulacion">
						<div class="row">
							<h3>Seccion de Fotos</h3>
						</div>
				    		<div class="row">
							<?php 
								//var_dump($grupos);
								foreach ($albumsFotos as $key => $value) {
						 	?>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="row cajaGrupoFoto">
											<a href="<?php echo base_url(); ?>index.php/multimedia/ver_album_single/<?php echo $value['id'] ; ?>">
												<center>
													<img src="<?php echo base_url(); ?>application/img/folder_256.png">
													<p><?php echo $value['nombre'] ; ?></p>
												</center>	
											</a>															
										</div>
									</div>
							<?php 
								} 
							?>
						</div>
					</div>	
			    </div>
			    <div role="tabpanel" class="tab-pane fade" id="videosHome">
			    	<div class="container anulacion">
					<div class="row">
						<h3>Seccion de Videos</h3>
					</div>
			    	</div>
			    </div>
					
			</div>
			<?php if(isset($seccion)){ ?>
				<script type="text/javascript">
					var seccion = "<?php echo $seccion ?>";		
					$('#'+seccion).click();					
				</script>
			<?php } ?>
		</div>	
	</div>
	