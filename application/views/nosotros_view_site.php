<main class="mobile-center">
        <section>
          <div class="container hr well1 ins2">
            <div class="row">
              <div class="grid_6">
                <div class="video">
                  <img src="<?php echo base_url(); ?>/application/images/IMAG1517.jpg">
                </div>
              </div>
              <div class="grid_6">
                <h2>Datos Principales</h2>
                <div class="row">
                  <div class="grid_3">
                    <dl class="info">
                      <dt>Nombre</dt>
                      <dd>Iglesia de Dios de la Profecía</dd>
                      <dt>Fundada</dt>
                      <dd>Junio 23, 1987</dd>
                      <dt>Lugar</dt>
                      <dd>Los Frailes I, Av. Las Américas</dd>
                    </dl>
                  </div>
                  <div class="grid_3">
                    <dl class="info">
                      <dt>Historia</dt>
                      <dd>
                        <ul>
                          <li>Lorem ipsum dolor sit 1997-1999 adipis</li>
                          <li>Pellentesque sed dolor  1995-1999</li>
                          <li>Aliquam congue nisl 2001-2005</li>
                          <li>Mauris accumsa vel diam 2006-2008</li>
                          <li>Sed in lacus ut 2008-2010 enim adipiscing </li>
                        </ul>
                      </dd>
                    </dl>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="well1 ins3">
          <div class="container">
            <h2>Quienes Somos?</h2>
            <div class="row off1">
              <div class="grid_6">
                <h3>Visión Institucional</h3>
                <p>Exaltar a Cristo, a su santidad, que todas las naciones sean llenas del Espíritu Santo, hacer discípulos, sembrar en las Iglesias con la pasión por la unidad Cristiana.</p>
                <br>
                <h3>Misión Institucional</h3>
                <p>Empoderados por el Espíritu Santo a través de la oración estableceremos iglesias y equipar  líderes para cumplir con el mandato bíblico de hacer discípulos genuinos de todas las gentes alrededor del mundo, y glorificar a Cristo nuestro señor, quien es la Cabeza de la iglesia.</p>
                
                <hr>
               <h3>Cual es nuestro Objetivo?</h3>
                <div class="row">
                  <div data-wow-delay="0.2s" class="grid_3 wow fadeInLeft"><img src="<?php echo base_url() ?>/application/images/page-2_img01.jpg" alt=""></div>
                  <div class="grid_3 wow fadeInLeft"><img src="<?php echo base_url() ?>/application/images/page-2_img02.jpg" alt=""></div>
                </div>
                <p class="justificado">Nuestro objetivo es exaltar a Cristo, señor y salvador nuestro, quien también es nuestro redentor y dueño, apegados a los principios de las bases cristianas (sola scritura, sola fide, sola gratia, solus cristo, solis deu gloria). También es nuestra meta, vivir en santidad como verdaderos hijos de Dios, predicar el evangelio a toda criatura y hacer discípulos, sembrar la palabra de Dios con pasión por la unidad Cristiana.
                  Empoderados por el Espíritu Santo a través de la oración, preparar y equipar  líderes para cumplir con el mandato bíblico de hacer discípulos verdaderos de todos aquellos que podamos alcanzar, y glorificar a Cristo señor nuestro, siempre brindando alabanza y adoración con nuestras propias vidas, el cual es la Cabeza de la iglesia.</p>
                </div>
              <div class="grid_6">
                <h3>Nuestros Lideres</h3>
                <div class="row">
                  <div class="grid_2 wow fadeInRight">
                    <img src="<?php echo base_url() ?>/application/images/page-2_img03.jpg" alt="">
                    <img src="<?php echo base_url() ?>/application/images/page-2_img06.jpg" alt="">
                  </div>
                  <div data-wow-delay="0.2s" class="grid_2 wow fadeInRight">
                    <img src="<?php echo base_url() ?>/application/images/page-2_img04.jpg" alt="">
                    <img src="<?php echo base_url() ?>/application/images/page-2_img07.jpg" alt="">
                  </div>
                  <div data-wow-delay="0.4s" class="grid_2 wow fadeInRight">
                    <img src="<?php echo base_url() ?>/application/images/page-2_img05.jpg" alt="">
                    <img src="<?php echo base_url() ?>/application/images/page-2_img08.jpg" alt="">
                  </div>
                </div>
                <p>Curabitur facilisis pellentesque pharetra. Donec justo urna, malesuada a viverra ac, pellentesque vitae nunc. Aenean ac leo eget nunc fringilla a non nulla! Nunc orci mi, venenatis quis ultrices vitae, congue non nibh. Nulla bibendum, justo eget ultrices.</p>
                <hr>
                <h3>Compromiso</h3>
                <p>Estamos comprometidos con la santidad del vínculo matrimonial y la importancia de las familias cristianas fuertes y amorosas.</p>
              </div>
            </div>
          </div>
        </section>
        <!--<section class="well1 ins3 bg-primary">
          <div class="container">
            <h2>Nuestros Logros</h2>
            <ul class="product-list row off1">
              <li class="grid_6">
                <div class="box">
                  <div class="box_aside">
                    <div class="icon fa-asterisk"></div>
                  </div>
                  <div class="box_cnt__no-flow">
                    <h3>Vestibulum elementum tempus eleifend</h3>
                    <p>Sed do eiusmod tempor incididunt ut labore et dolore magna. Suspendisse commodo tempor sagittis! In justo est sollicitudin.</p>
                  </div>
                </div>
                <hr>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon fa-asterisk"></div>
                  </div>
                  <div class="box_cnt__no-flow">
                    <h3>Congue dui ut porta aenean laoreet</h3>
                    <p>Pellentesque vitae tortor id neque fermentum pretium. Maecenas ac lacus ut neque rhoncus laoreet sed id tellus.</p>
                  </div>
                </div>
                <hr>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon fa-asterisk"></div>
                  </div>
                  <div class="box_cnt__no-flow">
                    <h3>Aenean laoreet viverra turpis a com</h3>
                    <p>Maecenas ac lacus ut neque rhoncus laoreet sed id tellus. Donec justo tellus, tincidunt vitae pellentesque nec, pharetra a orci. Praesent</p>
                  </div>
                </div>
              </li>
              <li class="grid_6">
                <div class="box">
                  <div class="box_aside">
                    <div class="icon fa-asterisk"></div>
                  </div>
                  <div class="box_cnt__no-flow">
                    <h3>Tempus eleifend cum sociis natoque</h3>
                    <p>Labore et dolore magna. Suspendisse commodo tempor sagittis! In justo est sollicitudin eu scelerisque pretium, placerat eget elit.</p>
                  </div>
                </div>
                <hr>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon fa-trophy"></div>
                  </div>
                  <div class="box_cnt__no-flow">
                    <h3>Sociis natoque penatibus vestibulum</h3>
                    <p>Suspendisse commodo tempor sagittis! In justo est sollicitudin eu scelerisque pretium, placerat eget elit. Praesent faucibus rutrum.</p>
                  </div>
                </div>
                <hr>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon fa-trophy"></div>
                  </div>
                  <div class="box_cnt__no-flow">
                    <h3>Penatibus vestibulum congue dui ut</h3>
                    <p>In justo est sollicitudin eu scelerisque pretium, placerat eget elit. Praesent faucibus rutrum odio at rhoncus.</p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </section>-->
        <section class="well1 bg-primary">
          <div class="container" style="text-align: justify;">
            <div class="row">
              <center>
                <h1>Declaración de Fé</h1>
              </center>              
            </div>
            <div class="row">
              <div class="grid_6">
                <p>Creemos que la Biblia —que consiste del Antiguo y el Nuevo Testamento— es la Palabra inspirada de Dios. Él nos ha hablado por medio de hombres a quienes escogió, los cuales fueron «inspirados por el Espíritu Santo». La Biblia revela el carácter y la voluntad de Dios para la humanidad; y es suficiente para instruir en la salvación y la vida cristiana diaria. La Biblia es la regla de fe y conducta del cristiano.</p><br>
                <p>Creemos en la Santísima Trinidad, un solo Dios que existe eternamente en tres personas: el Padre, el Hijo y el Espíritu Santo.</p><br>
                <p>Creemos en un solo Dios Padre, Creador del cielo y de la tierra, de todo lo visible y lo invisible.</p><br>
                <p>Creemos en un solo Señor, Jesucristo, el Hijo unigénito de Dios, eternamente engendrado por el Padre. Todo fue creado por medio de Él y para Él. Él es Dios verdadero y hombre verdadero. Fue concebido por el poder del Espíritu Santo, y nació de la virgen María. Padeció, murió y fue sepultado, y al tercer día resucitó de entre los muertos. Ascendió a la diestra del Padre, y volverá para juzgar a los vivos y a los muertos. Su reino no tendrá fin.</p><br>
                <p>Creemos en el Espíritu Santo, el Señor y Dador de la vida, quien procede eternamente del Padre. Él es Maestro, Consolador, Ayudador y Dador de los dones espirituales. Por medio de Él se aplica la obra salvífca y santifcadora de Jesucristo a la vida del creyente. Él es la empoderadora presencia de Dios en la vida del cristiano y de la Iglesia. El Padre ha enviado a Su Hijo a bautizar con el Espíritu Santo. Hablar en lenguas y llevar el fruto del Espíritu son las señales neotestamentarias del ser llenos del Espíritu Santo.</p><br>
                <p>Creemos que la salvación es por gracia por medio de la fe en la muerte expiatoria de Jesucristo en la cruz. Él murió en lugar nuestro. Los pecados del creyente son perdonados por el derramamiento de la sangre de Jesucristo. Creemos que hay sanidad para la mente, el cuerpo, el alma y el espíritu del creyente por medio de la sangre de Jesucristo y el poder del Espíritu Santo.</p><br>
               
              </div>
              <div class="grid_6">
                <p>Creemos que la gracia de Dios trae perdón y reconciliación a los que se arrepienten, además de la santificación, la cual los capacita para vivir a la manera de Cristo. La santificación es tanto una obra definitiva de la gracia como un proceso de transformación constante en el creyente efectuada por la sangre de Jesucristo, la Palabra de Dios y el poder del Espíritu Santo.</p> <br>
                <p>Creemos en una Iglesia santa y universal, que se compone de todos los verdaderos creyentes en Jesucristo, la cual ofrece confraternidad y llamamiento al servicio para los hombres y las mujeres de todas las razas, naciones, culturas y lenguas. Creemos en la unidad espiritual y visible de la Iglesia.</p><br>
                <p>Creemos que Dios creó al ser humano —al hombre y a la mujer— a Su imagen y semejanza, y que, por ende, toda vida humana es sagrada.</p><br>
                <p>Creemos en la santidad del matrimonio, que es una unión santa, legal y para toda la vida entre un hombre y una mujer, según dispuesto en el orden divino de la creación. Reconocemos y aceptamos a aquellos que, tras un divorcio, han hallado la gracia, la estabilidad matrimonial, la estabilidad familiar y el contentamiento en un segundo matrimonio que sea aceptable según las normas bíblicas.</p><br>
                <p>Creemos en la santidad de la familia —tradicionalmente, un padre, una madre y sus hijos— como el fundamento de la sociedad y de la iglesia. Afirmamos a las familias monoparentales y a los matrimonios que no tienen hijos o que no pueden tenerlos. Las familias son una parte integrante de la familia de la fe, donde cada una tiene la oportunidad de participar en la vida plena y el ministerio de la iglesia. Afirmamos, además, a los cristianos que, por decisión propia o por llamamiento, viven en soltería y castidad para servir a Dios.</p><br>
                <p>Creemos que Dios reconciliará, en Cristo, todas las cosas en el cielo y en la tierra. Por lo tanto, esperamos un cielo nuevo y una tierra nueva donde mora la justicia.</p><br>

              </div>
            </div>
          </div>
        </section>
        <section class="well1">
          <div class="container" style="text-align: justify;">
            <div class="row">
            <center>
              <h2>Deberes y Proposito de la Iglesia</h2>
            </center>              
            </div>
            <div class="row">
              <div class="grid_2">
                
              </div>
              <div class="grid_8">
                <center><p style="font-weight: 500;font-style: italic;">“Id, y haced discípulos a todas las naciones. . . enseñándoles que guarden todas las cosas que os he mandado; y he aquí yo estoy con vosotros todos los días, hasta el fin del mundo” (Mateo 28:19-20).</p></center>
              </div>
            </div>
            <div class="row">
              <div class="grid_4">                
               <p>El señor formo la iglesia, como su propio cuerpo de creyentes espiritualmente transformados, cuya responsabilidad de predicar el evangelio del Reino de Dios y de hacer discípulos en todo el mundo, enseñándoles exactamente lo que él había enseñado (Mateo 24:14; 28:19-20).</p><br>
               <p>Esa responsabilidad no terminó cuando murieron los primeros discípulos, sino que es continua desde su formación hasta que Cristo venga. La misión esencial de Iglesia, dada primeramente a los apóstoles, ha ido pasando de generación en generación, y Jesucristo prometió estar con todos sus fieles seguidores continuamente hasta el día en que él retorne (Mateo 28:20).</p><br>
                <p>La iglesia no está para salvar al mundo, sino para cumplir  “el ministerio de la reconciliación”, porque “Dios estaba en Cristo reconciliando consigo al mundo, no tomándoles en cuenta a los hombres sus pecados, y nos encargó a nosotros la palabra de la reconciliación” (2 Corintios 5:18-19).</p>
              </div>
              <div class="grid_4">
                <p>El propósito principal de Dios es reconciliar a toda la humanidad con él. Dios le ha encargado que predique cómo ocurrirá la reconciliación, y debe bautizar a los que crean este mensaje. El ministerio de la reconciliación de la Iglesia es sólo el principio de una fase mucho más amplia en el plan de Dios para reconciliar consigo al mundo por medio de Jesucristo.</p>  
                <p>La Iglesia de Dios constituye sólo la primera parte de la gran cosecha que él llevará a cabo para dar la vida eterna a los seres humanos. Aquellos a quienes Dios llame en este tiempo tomarán parte en la labor de salvar al mundo, pero no lo harán en el tiempo presente ni como seres humanos. Al retorno de Jesucristo serán resucitados o transformados en seres espirituales.</p>
                <p>Jesucristo es quien dirige el funcionamiento de la Iglesia (Colosenses 1:18). Para hacer hincapié en lo mucho que la Iglesia necesita su guía, Jesús se comparó a sí mismo con una vid: “Yo soy la vid, vosotros los pámpanos; el que permanece en mí, y yo en él, éste lleva mucho fruto; porque separados de mí nada podéis hacer” (Juan 15:5). </p>

              </div>
              <div class="grid_4">
                <p>La vida y el éxito de los cristianos dependen del poder y la inspiración que reciben de Jesucristo.</p><br>
               <p>Entre los dones espirituales que Jesucristo da a los miembros de su Iglesia están los del liderazgo espiritual: apóstoles, profetas, evangelistas, pastores y maestros (Efesios 4:11). A ellos se les ha confiado la responsabilidad de enseñar, nutrir, proteger y edificar a los miembros de la Iglesia. Los requisitos o cualidades espirituales que deben tener las personas a quienes se les ha confiado esta responsabilidad se encuentran claramente enunciados en 1 Timoteo 3:1-10 y Tito 1:5-9.</p><br>
               <p>Estos individuos deben cuidar amorosamente del rebaño de Dios (Juan 21:15-17; 1 Pedro 5:1-4) de manera que todos los miembros de este cuerpo espiritual puedan llegar “a la unidad de la fe y del conocimiento del Hijo de Dios, a un varón perfecto, a la medida de la estatura de la plenitud de Cristo” (Efesios 4:13).</p><br>  

              </div>
            </div>
          </div>
        </section>
        <!--<section class="well1">
          <div class="container">
            <div class="row">
              <div class="grid_4">
                <h2>Deberes</h2>
                <p>Aenean ac leo eget nunc fringilla a non nulla! Nunc orci mi, venenatis quis ultrices vitae, congue non nibh. Nulla bibendum, justo eget ultrices vestibulum, erat tortor venenatis risus, sit amet cursus dui augue a arcu.</p>
              </div>
              <div class="grid_4">
                <h2>Habilidades</h2>
                <p>Nunc orci mi, venenatis quis ultrices vitae, congue non nibh. Nulla bibendum, justo eget ultrices vestibulum, erat tortor venenatis risus, sit amet cursus dui augue a arcu. Quisque mauris risus, gravida a molestie eu, dictum.</p>
              </div>
              <div class="grid_4">
                <h2>Oportunidades</h2>
                <p>Quisque mauris risus, gravida a molestie eu, dictum ac augue. Integer sodales tempor lectus; sit amet dictum metus pharetra nec. Fusce bibendum dapibus pretium. Nunc eu sem vitae lacus laoreet elementum.</p>
              </div>
            </div>
          </div>
        </section>-->
      </main>