<?php 
 if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 

	$id= '';
	$tituloNoticiaEditar ='';
	$fechaNoticiaEditar ='';
	$descripcionNoticiaEditar='';
	$imagenEditar='';
	$columnaNoticia = '';

	if(isset($noticia)){
		//var_dump($miembro);
		$id =$noticia->id;
		$tituloNoticiaEditar = $noticia->titulo;
		$fechaNoticiaEditar =$noticia->fecha;
		$descripcionNoticiaEditar = $noticia->descripcion;
		$imagenEditar =$noticia->imagen;
	}

	//var_dump($columnas);

 ?>
<div class="container">
	<div class="row">
		<div class="col-md-10 ">
			<div class="row">
				<div class="col-md-12">
					<h1>Mantenimiento de Noticias</h1>
					<center class="cargandoNoticias">
						<img id="cargandoNoticias" src="<?php echo base_url(); ?>/application/img/loading.gif">
					</center>
				</div>
				<div class="col-md-12 errorFormularioNoticias">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1><span>Error!!!!</span> Verifica que los campos esten introducidos de la manera correcta. </h1>
						</div>
					</div>
					
				</div>
				<div class="col-md-12 successFormularioNoticias">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1><span>Usuario!!!!</span> Insertado de manera correcta. </h1>
						</div>
					</div>
					
				</div>
				<div class="col-md-12 cargandoContenedor">
					<div class="row BoxGeneralNoticias">
						<div class="col-md-3 menuLateralNoticias">
							<ul class="nav nav-pills nav-stacked">
								<li><a id="opci1" href="">Noticias Creadas</a></li>
								<li><a id="opci2" href="">Agregar Noticias</a></li>
								<li><a id="opci3" href="">Algo de Noticias</a></li>
								<!--<li role="presentation" class="dropdown">
			        				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			          					Eventos <span class="caret"></span>
			       					 </a>
							        <ul class="dropdown-menu">
							          <li><a id="opci5" href="">Eventos Creados</a></li>
							          <li><a id="opci6" href="">Crear Eventos</a></li>
							          <li><a id="opci7" href="">Otra Cosita</a></li>
							          <li role="separator" class="divider"></li>
							          <li><a href="#">Separated link</a></li>
							        </ul>
								</li>-->
							</ul>
						</div>
						<div class="col-md-9 contenidoNoticias">
							<div class="opci1">
								<div class="row" style="margin-top:5%;">
										<div class="col-md-4">
											<strong>Titulo</strong>
										</div>
										<div class="col-md-2">
											<strong>Fecha</strong>
										</div>
									</div>
									<?php 
										foreach ($noticias as $key => $value) {											
											?>
											<div class="row">
												<div class="col-md-4">
													<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/noticiasBackend/ver_Noticia/<?php echo $value['id']; ?>"><p><?php echo $value['titulo']; ?></p></a>
												</div>
												<div class="col-md-2">
													<p><?php echo $value['fecha']; ?></p>
												</div>
												<div class="col-md-2">
													<a href="<?php echo base_url(); ?>index.php/noticiasBackend?n=2&id=<?php echo $value['id']; ?>"><p>Editar</p></a>
												</div>
											</div>

											<?php

										}
									 ?>
							</div>
							<div class="opci2">
								<div class="panel panel-primary">
							    <div class="panel-heading">Formulario de Noticias</div>
							    <div class="panel-body">
							   		<?php 
							   			//var_dump($miembro);
							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('miembros/agregarMiembro',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('idNoticia',$id);
												 echo form_hidden($datahidden);
											?>
										</div>
										<!--/////////////////  Titulo /////////////////////// -->
										<?php 
										
							   			$titulo = array(
								              'name'        => 'user_horizontal_titulo',
								              'id'          => 'user_horizontal_titulo',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Titulo',
								              'required'    => 'required',
								              'value'		=> $tituloNoticiaEditar
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_titulo"><abbr title="Campo Obligatorio">*</abbr> Titulo:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($titulo); ?>
								  				<p class="help-block">Titulo de la Noticia</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  FECHA Noticia /////////////////////// -->

										<?php 

							   			$fechaNoticia = array(
								              'name'        => 'user_horizontal_fechaNoticia',
								              'id'          => 'user_horizontal_fechaNoticia',
								              'type'		=> 'date',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required',
								              'value'		=> $fechaNoticiaEditar
								         );
							   			
							   			
							   			?> 

										<div class="form-group email required user_horizontal_email">
								  			<label class="email required col-sm-3 control-label" for="user_horizontal_fechaNoticia"><abbr title="Campo Obligatorio">*</abbr> Fecha:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($fechaNoticia); ?>
								  				<p class="help-block">Fecha de creación de la noticia</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  Columna /////////////////////// -->
								  		<?php 

											if(isset($columnaNoticia)){
												$valueColumna = $columnaNoticia;
											}else{
												$valueColumna =1;
											}
								  			$atributosColumna ='id="inputColumnas" class="string email required form-control"';
								  		 ?>
								  		

								  		<div class="form-group email required user_horizontal_email">
								  		<label class="email col-sm-3 control-label" for="user_horizontal_email"> Columna: </label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('columnas',$columnas, $valueColumna,$atributosColumna) ?>
								  				<p id="labeSeleccioneCarreras" class="help-block">Seleccione la columna</p>
								  			</div>
								  		</div>
								  		

								  		<!--/////////////////  Descripcion /////////////////////// -->

								  		<?php 

							   			$descripcionNoticia = array(
								              'name'        => 'user_horizontal_descripcion_noticia',
								              'id'          => 'user_horizontal_descripcion_noticia',
								              'class'		=> 'string email required form-control',
								              'required'    => 'required',
								               'value'		=> $descripcionNoticiaEditar
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email col-sm-3 control-label" for="user_horizontal_descripcion_noticia"> Descripción:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_textarea($descripcionNoticia); ?>
								  			</div>
								  		</div>

								  		<!--/////////////////  Imagen /////////////////////// -->

								  		<?php 

							   			$imagen= array(
								              'name'        => 'user_horizontal_imagen_noticia',
								              'id'          => 'user_horizontal_imagen_noticia',
								              'class'		=> 'string email required form-control',
								              'type'    => 'file'
								         );
							   			?>
							   			<div id="imagenMostrar" class="row <?php if((isset($_GET['n']) && isset($_GET['id']) && $imagenEditar == null) || (isset($_GET['n']) && !isset($_GET['id']) )){echo "hidden";} ?>">
								  			<div class="col-md-4 col-md-offset-3">
								  				<img style="width:100%;border-radius: 10px;" src="<?php echo base_url(); ?>/application/noticias/<?php echo $imagenEditar ?>">
								  			</div>		
								  			<div class="col-md-2">
								  				<a style="margin-top:135%" href="" name="cambiar" class="btn btn-info" id="cambiarImagenNoticia">Cambiar Imagen</a>
								  			</div>
								  			<div class="col-md-2">
								  				<a style="margin-top:135%;margin-left:25%" href="1" name="cambiar" class="btn btn-info" id="eliminarImagenNoticia">Eliminar Imagen</a>
								  			</div>							  				
								  		</div>
							   			<div style="margin-top:2%;" id="imagenNoticias" class="<?php if(isset($_GET['n']) && isset($_GET['id']) && $imagenEditar != null){echo "hidden";} ?> form-group email required user_horizontal_email">
								  			<label class="email col-sm-3 control-label" for="user_horizontal_imagen_noticia"> Imagen:</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($imagen); ?>
								  			</div>
								  		</div>
								  									  		

								  		<?php 
								  			$atributosSubmit ='class="btn btn-primary" id="submitAgregarNoticia"';
								  			if(isset($_GET['n']) && !isset($_GET['id'])){ echo form_submit('commit', 'Agregar Noticia',$atributosSubmit);} 
								  			if(isset($_GET['n']) && isset($_GET['id'])){ echo form_submit('commit', 'Modificar Noticia',$atributosSubmit);} 
								  		?>
								</div>
  							</div>
							</div>
							<div class="opci3">
								<p>3</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>