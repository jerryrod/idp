<!-- Bootstrap-->
	 <link rel="stylesheet"  title="Estilo alternativo" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> 
	 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<div class="row">
	<div class="col-md-10 col-md-offset-1 centrarTituloxs">
		<h1 style="margin-top:3%;">Noticias Publicadas</h1>
	</div>	
</div>
<div class="row noticiasView">
	<div class="col-md-4 col-md-offset-1 columna">
		<!--<div class="row itemNoticia">
		<?php 
			$contador = 0;
			foreach ($noticias as $key => $value) {	
				if($contador%2 == 0){ ?>
					</div>
					<div class="row">
				<?php } ?>			
					<div class="col-md-6 col-xs-12">
						<div class="row itemNoticia">
							<div class="col-md-6 col-xs-12">
								<img src="<?php echo base_url(); ?>application/noticias/<?php if(isset($value['imagen'])){echo $value['imagen'];}else{echo "COGOP_Logo.jpg";} ?>">
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="row">
									<div style="margin-top: 5%;" class="col-md-12 centrarTituloxs">
										<a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $value['id']; ?>">
											<h3><?php echo $value['titulo'] ;?></h3>
											<h3><?php echo $value['fecha']; ?></h3>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>				
				
			<?php 
			$contador++; }

		 ?>
		 </div>-->

		
		 <?php 
		 	$contador = 1;
			foreach ($noticias as $key => $value) {		
				if($value['columna'] == 1){ ?>
					 		<div class="row">
			 					<?php 
			 						$fecha = new DateTime($value['fecha']);
			 						//echo $fecha->format('d M Y'); 

			 					?>
			 					<span><?php echo $fecha->format('d M Y') ?></span>
			 				</div>
			 				<div class="row">
			 					<a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $value['id']; ?>">
			 						<h3><?php echo $value['titulo'] ;?></h3>
			 					</a>
			 					
			 				</div>
			 				<div class="row">
			 					<div class="col-md-4"><img src="<?php echo base_url(); ?>application/noticias/<?php if(isset($value['imagen'])){echo $value['imagen'];}else{echo "COGOP_Logo.jpg";} ?>"></div>
			 					<div class="col-md-8">
									<div class="row">
										<?php 
			 									$textoCorto = substr($value['descripcion'],0,150);
												$texto = explode(':', $textoCorto);
												/*foreach ($texto as $key => $value) {
														echo $value;
												}*/ ?>
												<p><?php echo $textoCorto.'...'; ?></p>
									</div>
									<div class="row">
										<a class="verMas" href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $value['id']; ?>">
											Ver más
										</a>
									</div>
			 						
												
								</div>
			 				</div>		 		
					 		<hr>				
						
					<?php }	
				 }
		 	?>	 	
	</div>
	<div class="col-md-4 columna2">
		<?php  
			foreach ($noticias as $key => $value) {		
				if($value['columna'] == 2){?>					
					 		<div class="row">
					 			<img src="<?php echo base_url(); ?>application/noticias/<?php if(isset($value['imagen'])){echo $value['imagen'];}else{echo "COGOP_Logo.jpg";} ?>">
					 		</div>
					 		<div class="row">
					 			<?php 
					 				$fecha = new DateTime($value['fecha']);
			 						echo $fecha->format('d M Y');
			 					?>	
					 		</div>
					 		<div class="row">
					 			<a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $value['id']; ?>">
			 						<h3><?php echo $value['titulo'] ;?></h3>
			 					</a>
					 		</div>
					 		<div class="row">
					 			<?php 
									$textoCorto = substr($value['descripcion'],0,150);
									$texto = explode(':', $textoCorto);
									/*foreach ($texto as $key => $value) {
											echo $value;
									}*/ ?>
									<p><?php echo $textoCorto.'...'; ?></p>
					 		</div>
					 		<div class="row">
										<a class="verMas" href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $value['id']; ?>">
											Ver más
										</a>
									</div>
				 			<hr>	 	
		 		<?php 
		 	}
		 }

		 ?>
	</div>
	<div class="col-md-2  columna3">
		<?php  
			foreach ($noticias as $key => $value) {		
					
		 	if($value['columna'] == 3 ){ ?>				
						<div class="row"><img src="<?php echo base_url(); ?>application/noticias/<?php if(isset($value['imagen'])){echo $value['imagen'];}else{echo "COGOP_Logo.jpg";} ?>">
					 		</div>
						<div class="row">
							<?php 
								$fecha = new DateTime($value['fecha']);
		 						echo $fecha->format('d M Y');
							 ?>
	
						</div>
						<div class="row">
					 			<a href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $value['id']; ?>">
			 						<h3><?php echo $value['titulo'] ;?></h3>
			 					</a>
					 	</div>
						<div class="row">
							<?php 
								$textoCorto = substr($value['descripcion'],0,150);
							$texto = explode(':', $textoCorto);
							/*foreach ($texto as $key => $value) {
									echo $value;
							}*/ ?>
							<p><?php echo $textoCorto.'...'; ?></p>
						</div>
						<div class="row">
										<a class="verMas" href="<?php echo base_url(); ?>index.php/noticias/ver_noticia/<?php echo $value['id']; ?>">
											Ver más
										</a>
									</div>
						<hr>
		 		<?php 
		 	}
		 }

		 ?>
	</div>
</div>
	
