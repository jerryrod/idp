<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 sigle_actividad_textos">
			<div class="row">
				<h1>Actividad Irregular</h1>
			</div>
			<div class="row">
				<div class="col-md-6">
					<span>Nombre de Actividad: </span><?php echo $actividad->nombre ?>
				</div>
				<div class="col-md-6">
					<span>Día: </span><?php echo $actividad->nombreDia ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<span>Fecha de Actividad: </span><?php echo $actividad->fecha ?>
				</div>
				<div class="col-md-6">
					<span>Hora de la Actividad: </span><?php echo $actividad->hora ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<span>Detalle de la Actividad: </span><p><?php echo $actividad->Descripcion ?></p>
				</div>
			</div>
		</div>		
	</div>
</div>