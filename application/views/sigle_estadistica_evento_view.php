<div class="container">
	<div class="row">
		<div class="col-md-1">
			<a href="<?php echo base_url(); ?>index.php/home"><img src="<?php echo base_url(); ?>/application/img/logo.png"></a>
		</div>
		<div class="col-md-6">
			<h1 style="margin-top: 9%;">Estadistica del Evento</h1>
		</div>
	</div>
	<div class="row contenedorSingleMiembro">
		<div class="col-md-10">
			<div class="row">
				<div class="col-md-6">
					<p>Cantidad de Membrisia Total en ese periodo: <span><?php echo $cantidadAsistencia->cantidadTotal ?></span></p>
				</div>
				<div class="col-md-6">
					<a href="#" id="ver_miembros_sigle_view">Ver Miembros</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>Cantidad de Asistencia: <span><?php echo($cantidadAsistenciaPresente->cantidadTotal); ?></span></p>
				</div>
				<div class="col-md-4">
					<p>Porcentaje de asistencia: <span><?php echo $estadisticaAsistecia; ?>%</span></p>
				</div>
			</div>
			<div class="row ver_total_miembros_sigle_view">
				<div class="col-md-12">
					<?php 
						$contador=1;
						foreach ($miembrosTotal as $key => $value) { ?>
							<div class="row">
								<div class="col-md-6">
									<div class="row <?php if($contador%2==0){echo "colorLista";} ?>">
										<div class="col-md-6">
											<span><?php echo $contador ."-". $value['nombre_completo']; ?></span> 
										</div>
										<div class="col-md-2 ">
											<span class="<?php echo $value['estado_asistencia']; ?>"><?php echo $value['estado_asistencia']; ?></span>
										</div>
									</div>
								</div>									
							</div>												
						<?php
						$contador++; }
					 ?>
				</div>
				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
		$('#ver_miembros_sigle_view').click(function(e){
			e.preventDefault();
			$('.ver_total_miembros_sigle_view').fadeIn('slow');
		});
	
</script>