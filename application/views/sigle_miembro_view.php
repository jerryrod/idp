
<div class="container">
	<div class="row">
		<div class="col-md-1">
			<a href="<?php echo base_url(); ?>index.php/home"><img src="<?php echo base_url(); ?>/application/img/logo.png"></a>
		</div>
		<div class="col-md-6">
			<h1 style="margin-top: 9%;"><?php echo($miembro->nombre .' '.$miembro->apellido); ?></h1>
		</div>
	</div>
	<div class="row contenedorSingleMiembro">
		<div class="col-md-10">
			<div class="row">
				<div class="col-md-4">
					<p>Nombre: <span><?php echo($miembro->nombre); ?></span></p>
				</div>
				<div class="col-md-4">
					<p>Apellido: <span><?php echo($miembro->apellido); ?></span></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>Email: <span><?php echo($miembro->email); ?></span></p>
				</div>
				<div class="col-md-4">
					<p>Telefono: <span><?php echo($miembro->telefono); ?></span></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>Estado Civil: <span><?php echo($miembro->nombreCivil); ?></span></p>
				</div>
				<div class="col-md-4">
					<p>Estado Educativo: <span><?php echo($miembro->nombreEstadoEducativo); ?></span></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>Estado Laboral: <span><?php echo($miembro->nombreEstadoLaboral); ?></span></p>
				</div>
				<div class="col-md-4">
					<p>Sexo: <span><?php echo($miembro->sexo); ?></span></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>Fecha Nacimiento: <span><?php echo($miembro->fechaNacimiento); ?></span></p>
				</div>
				<div class="col-md-4">
					<p>Fecha Conversion: <span><?php echo($miembro->fechaConversion); ?></span></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>Profesion: <span><?php echo($miembro->nombreCarrera); ?></span></p>
				</div>
				<div class="col-md-4">
					<p>Estado: <span><?php echo($miembro->nombreEstado); ?></span></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>Porcentaje de asistencia: <span><?php echo($porcentajeAsistencia); ?></span></p>
				</div>
				<div class="col-md-4">
					<p>Lider de Celula: <span><?php if(isset($lider)){echo($lider->lider);} ?></span></p>
				</div>
			</div>
		</div>
	</div>
</div>
