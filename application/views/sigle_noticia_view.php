
<div class="container">
	<div class="row">
		<div class="col-md-1">
			<a href="<?php echo base_url(); ?>index.php/home"><img src="<?php echo base_url(); ?>/application/img/logo.png"></a>
		</div>
		<div class="col-md-6">
			<h1 style="margin-top: 9%;"><?php echo($noticia->titulo); ?></h1>
		</div>
	</div>
	<div class="row contenedorSingleMiembro">
		<div class="col-md-10">
			<div class="row">
				<div class="col-md-2 col-md-offset-7">
					<p><span><?php echo($noticia->fecha); ?></span></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9">
					<div class="row">
						<?php if($noticia->imagen != null){ ?>
							<div class="col-md-5">
								<img style="width:100%;border-radius: 6px;" src="<?php echo base_url(); ?>application/noticias/<?php echo($noticia->imagen); ?>">
								
							</div>
						<?php } ?>
						<p><span><?php echo($noticia->descripcion); ?></span></p>
					</div>
					
					
				</div>
			</div>			
		</div>
	</div>	
</div>
