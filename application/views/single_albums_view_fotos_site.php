	<!-- Bootstrap -->
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	  <!--Archivos de Uikit Locales -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>application/css/Uikit/css/uikit.min.css" />
<div class="row cajaAudios">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<!--<div class="col-md-6 col-xs-6">
				<h1><?php if($fotos != null){ echo $fotos[0]['idCarpeta']; ?></h1>
			</div>-->
			<div class="col-md-12 col-xs-12">
				<?php echo anchor('multimedia?s=galeriaReturn', 'Volver Atras', 'class="btn-atras btn"') ?>
			</div>
		</div>
		<div class="row">
			<?php
				//var_dump($fotos);
			
				foreach ($fotos as $key => $value) {			
		 	?>
					<div class="col-md-2 cajaFotos  uk-panel uk-panel-box uk-scrollspy-init-inview" data-uk-scrollspy="{cls:'uk-animation-scale-up', repeat: true}">
						<div class="rowuk-scrollspy-init-inview uk-scrollspy-inview uk-animation-slide-top" data-uk-scrollspy="{cls:'uk-animation-scale-up', repeat: true}">
							<!--<a target="_blank" href="<?php echo base_url(); ?>index.php/multimedia/ver_audio_single/<?php echo $value['id'] ; ?>">
								<center>
									<img src="<?php echo base_url(); ?>application/imagenesAlbum/<?php echo $value['nombreCarperta'].'/'.$value['nombreFotoGUID']; ?>">
								</center>	
							</a>-->
							<a class="fancybox2" rel="gallery1" href="<?php echo base_url(); ?>application/imagenesAlbum/<?php echo $value['nombreCarperta'].'/'.$value['nombreFotoGUID']; ?>" title="<?php echo $value['idCarpeta'];?>">
								<img src="<?php echo base_url(); ?>application/imagenesAlbum/<?php echo $value['nombreCarperta'].'/'.$value['nombreFotoGUID']; ?>" alt="" />
							</a>															
						</div>
					</div>
			<?php 
				}
			} 
			else{
				?>
				
				
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<h1>Esta Carpeta no contiene archivos</h1>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12 ">
						<?php echo anchor('multimedia?s=galeriaReturn', 'Volver Atras', 'class="pull-right"') ?>
					</div>
				</div>
				<?php
			}
			?>
			
		</div>
		
	</div>
</div>