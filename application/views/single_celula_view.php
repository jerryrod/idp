
<div class="container">
	<div class="row">
		<div class="col-md-1">
			<a href="<?php echo base_url(); ?>index.php/home"><img src="<?php echo base_url(); ?>/application/img/logo.png"></a>
		</div>
		<div class="col-md-6">
			<h1 style="margin-top: 9%;"> Lider: <?php echo($celula->nombreCompleto); ?></h1>
		</div>
	</div>
	<div class="row contenedorSingleMiembro">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-8">
					<p>Anfitrión: <span><?php echo($celula->anfitrion); ?></span></p>
				</div>				
			</div>
			<div class="row">
				<div class="col-md-8">
					<p>Dia: <span><?php echo($celula->nombreDia); ?></span></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<p>Dirección: <span><?php echo($celula->direccion); ?></span></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<p>Fecha de Inicio: <span><?php echo($celula->fechaInicio); ?></span></p>
				</div>
				
			</div>
			
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-8">
					<p id="integrantes">Integrantes</p>
				</div>				
			</div>
			<?php 
			//var_dump($miembros1);
			$CI =& get_instance();
			$contador =1;
			$celulasIntegrantes = $CI->get_Miembros_Por_Celula($celula->id);
			foreach ($celulasIntegrantes as $key => $value) {
				?>
				<div class="row ">
					<div class="col-md-5 integrantesDentro">
						<span><?php echo $contador."-".$value['nombre']." ". $value['apellido']; ?></span>
					</div>
				</div>
				
			<?php $contador++;}
			
 		?>
		</div>
	</div>
</div>
