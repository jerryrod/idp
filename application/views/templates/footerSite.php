 <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/grid.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/style2.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/camera.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/owl-carousel.css">

 <footer>
        <section class="well3">
          <div class="container">
            <ul class="row contact-list">
              <li class="grid_4 col-md-4">
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-map-marker"></div>
                  </div>
                  <div class="box_cnt__no-flow">
                    <address>C/ Miramar Norte #16 Km.10<br/> Los Frailes I</address>
                  </div>
                </div>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-envelope"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a href="mailto:idplosfrailes@gmail.com">idplosfrailes@gmail.com</a></div>
                </div>
              </li>
              <li class="grid_4 col-md-4" >
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-phone"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a href="callto:#">809-599-1234</a></div>
                </div>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-instagram"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a target='_blank' href="https://www.instagram.com/idplosfrailes/">idplosfrailes</a></div>
                </div>
              </li>
              <li class="grid_4 col-md-4">
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-facebook"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a href="#">Siguenos en facebook</a></div>
                </div>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-twitter"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a target='_blank' href="https://twitter.com/IdpLos">Siguenos en Twitter</a></div>
                </div>
              </li>
            </ul>
          </div>
        </section>
        <section>
          <div class="container">
            <div class="copyright">RodSoft © <span id="copyright-year"></span> derechos reservados.
            </div>
          </div>
        </section>
      </footer>
    </div>
    <script src="<?php echo base_url(); ?>/application/js/script.js"></script>


   <script src="<?php echo base_url(); ?>application/js/redes.js"></script>
            
    <script type="text/javascript" src="<?php echo base_url(); ?>application/fancyBox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

            <!--<script src="<?php echo base_url(); ?>application/css/Uikit/js/uikit.min.js"></script>-->
            <script type="text/javascript">
            $(document).ready(function() {
              
              $(".fancybox").fancybox();
              
              $(".various").fancybox({
                maxWidth  : 600,
                maxHeight : 400,
                fitToView : false,
                width   : '50%',
                height    : '50%',
                autoSize  : false,
                closeClick  : false,
                openEffect  : 'none',
                closeEffect : 'none'
              });

              $(".fancybox2").fancybox({
                openEffect  : 'none',
                closeEffect : 'none'
              });

                $(".form-signin:input[type=submit]").addClass("btn-primary");

                cargarFotosInstagram();
                //cargarYoutube();
                //--cargarTweets();--Todavia Con error. 
                //cargarFacebook();              
            });
        </script>
  </body>
</html>