      <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/grid.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/style2.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/camera.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/owl-carousel.css">
 <footer>
        <section class="well3">
          <div class="container">
            <ul class="row footerEmergencia">
                  <li class="col-md-4 col-xs-12">
                    <div class="box">
                      <div class="box_aside">
                        <div class="icon2 fa-map-marker"></div>
                      </div>
                      <div class="box_cnt__no-flow ">
                        <address >C/ Miramar Norte #16 Km.10<br/> Los Frailes I</address>
                      </div>
                    </div>                    
                  </li>
                  <li class="col-md-4 col-xs-12">
                   <div class="box">
                      <div class="box_aside">
                        <div class="icon2 fa-phone"></div>
                      </div>
                      <div class="box_cnt__no-flow box_jerry"><a style="font-size:24px;" href="callto:#">809-599-1234</a></div>
                    </div>
                  </li>
                  <li class="col-md-4 col-xs-12">
                    <div class="box">
                      <div class="box_aside">
                        <div class="icon2 fa-facebook"></div>
                      </div>
                      <div class="box_cnt__no-flow box_jerry"><a href="#">Siguenos en facebook</a></div>
                    </div>                    
                  </li>
                  <li class="col-md-4 col-xs-12">
                     <div class="box">
                      <div class="box_aside">
                        <div class="icon2 fa-envelope"></div>
                      </div>
                      <div class="box_cnt__no-flow box_jerry"><a href="mailto:idplosfrailes@gmail.com">idplosfrailes@gmail.com</a></div>
                    </div>
                  </li>
                  <li class="col-md-4 col-xs-12">
                    <div class="box">
                      <div class="box_aside">
                        <div class="icon2 fa-instagram"></div>
                      </div>
                      <div class="box_cnt__no-flow box_jerry"><a target='_blank' href="https://www.instagram.com/idplosfrailes/">idplosfrailes</a></div>
                    </div>
                  </li>
                  <li class="col-md-4 col-xs-12">
                    <div class="box">
                      <div class="box_aside">
                        <div class="icon2 fa-twitter"></div>
                      </div>
                      <div class="box_cnt__no-flow box_jerry"><a target='_blank' href="https://twitter.com/IdpLos">Siguenos en Twitter</a></div>
                    </div>                    
                  </li>
            </ul>
          </div>
        </section>
        <section>
          <div class="container">
            <div class="copyright">RodSoft © <span id="copyright-year"></span> derechos reservados.
            </div>
          </div>
        </section>
      </footer>
    </div>
            
    <script src="<?php echo base_url(); ?>/application/js/script.js"></script>

    <script src="<?php echo base_url(); ?>application/js/redes.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
     
    <script type="text/javascript" src="<?php echo base_url(); ?>application/fancyBox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

        <!--<script src="<?php echo base_url(); ?>application/css/Uikit/js/uikit.min.js"></script>-->
        <script type="text/javascript">
            $(document).ready(function() {                        
               

                  
              $(".fancybox").fancybox();
              
              $(".various").fancybox({
                maxWidth  : 600,
                maxHeight : 400,
                fitToView : false,
                width   : '50%',
                height    : '50%',
                autoSize  : false,
                closeClick  : false,
                openEffect  : 'none',
                closeEffect : 'none'
              });

              $(".fancybox2").fancybox({
                openEffect  : 'none',
                closeEffect : 'none'
              });

              /*Script para el manejo de los audios de multimedia*/

              $('.TabsAudios li>a').click(function (e){
                e.preventDefault();
                $('.AudiosFullHijo > .row').children().fadeOut('slow');
                $('.'+$(this).attr('id')).fadeIn('slow');
                 $('.addReproductorItem').remove();
                 $('.addReproductor').children().fadeIn('fast');
                //console.log($(this).attr('id'));
              });

              $('.atrasSeries').click(function (e) {
                e.preventDefault();
                 $('.AudiosFullHijo > .row').children().fadeOut('slow');
                 $('.TabsAudios').fadeIn('slow');
                 $('.addReproductorItem').remove();
                 $('.addReproductor').children().fadeIn('fast');
              });

              $('.atrasSeriesUno').click(function (e) {
                e.preventDefault();
                 $('.seriesDos').fadeOut('slow');  
                 $('.seriesUno').fadeIn('slow');
                 $('.addReproductor').children().fadeIn('fast');
              });

              $(".form-signin:input[type=submit]").addClass("btn-primary");   


              /*Esta funcion es la que nos permite obtener los audios del las series, del mismo modo introduce el html en el elemento.*/
              $('.cajaGrupoAudio a').on("click",function (e) {
                e.preventDefault();

                data = {};
                var ide = $(this).attr('href').split('=');
                data['id'] = ide[1];
                console.log(data['id']);
                  $.ajax({
                      url:"<?php echo site_url('multimedia/getAudiosSerie'); ?>",
                      type:'POST',
                      dataType: "json",
                      data:data,
                      cache:false,

                      success: function(data){
                        console.log(data.data);
                        console.log('Exito');

                        $('.seriesUno').fadeOut('fast');
                          
                        $('.seriesDos').append('<div class="row">\
                                                  <h2 class="col-md-6" id="totar">Series</h2>\
                                                  <a class="atrasSeriesUno pull-right btn col-md-2 col-md-offset-1" href="<?php echo base_url(); ?>index.php/multimedia?serie=2">Atras</a>\
                                                </div>\
                                               <div class="row">\
                                                <div class="col-md-2">\
                                                  <strong>Fecha</strong>\
                                                </div>\
                                                <div class="col-md-5">\
                                                  <strong>Nombre</strong>\
                                                </div>\
                                                <div class="col-md-3">\
                                                  <strong>Audio</strong>\
                                                </div>\
                                              </div>\
                                                ');  
                        
                        $.each(data.data, function(key, value) {
                          $('.seriesDos').append('<div class="row">\
                                                    <div class="col-md-2">\
                                                      '+value["fecha_audio"]+'\
                                                    </div>\
                                                    <div class="col-md-5">\
                                                     '+value["nombreAudio"]+'\
                                                    </div>\
                                                    <div class="col-md-3">\
                                                     <audio controls>\
                                                      <source src=\'<?php echo base_url(); ?>application/audio/<?php echo "'+value['tmp_name']+'" ?>\' type="audio/mp3">\
                                                      Tu Navegador no soporta este elemento.\
                                                    </audio>\
                                                    </div>\
                                                  </div>\
                                                    ');
                          });                             
                        },
                      error: function(e) { 
                            console.log(e);
                            console.log('fallo');
                        }
                    });

                });

                
                /*Esta funcion la cree por un conflicto que tenia chrome, esta clickian el boton de reproducir oculta el mismo boton y crea un tag audio con el item correspondiente*/
                $('.reproducir').click(function (e) {
                  e.preventDefault();
                  $(this).fadeOut('fast');

                  var nombreAudioFile = $(this).attr('href');
                  var current = $(this);
                  setTimeout(function(){
                  $(current).parent().append('<audio autoplay class="addReproductorItem" controls>\
                                                <source src=\'<?php echo base_url(); ?>application/audio/<?php echo "'+nombreAudioFile+'" ?>\' type="audio/mp3">\
                                                Tu Navegador no soporta este elemento.\
                                              </audio>\
                                              ');
                  
                 },800);

                });                


                <?php 
                    if(isset($_GET['serie'])){ ?>                  
                      <?php if($_GET['serie']==2){ ?>
                        $('#seriesReturn').click();
                      <?php } 
                    }
                    if(isset($_GET['s'])){
                      if($_GET['s']=='galeriaReturn'){?>
                        $('#galeriaReturn').click();
                     <?php }
                    }
                ?>

                //cargarFotosInstagram();
                //cargarYoutube();
                //--cargarTweets();--Todavia Con error. 
                //cargarFacebook();
               
              
            });
    </script>
       
  </body>
</html>