<!DOCTYPE html>
	<html lang="es">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title>IDP</title>
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/application/css/style.css">
	    <!-- Bootstrap -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

	    <link rel="stylesheet" href="<?php echo base_url(); ?>application/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

	    <!--Archivos de Uikit Locales -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>application/css/Uikit/css/uikit.min.css" />
        


	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
      
		 	
	</head>
	<body>
	<div class="container-fluid ponerFull">
		<div class="row">
			<nav class="navS col-md-12">
			    <ul>
			    	<li><a href="<?php echo base_url(); ?>index.php/home"><img src="<?php echo base_url(); ?>/application/img/logo.png"></a></li>
			    	<li><a id="inicio" href="<?php echo base_url(); ?>index.php/home">Inicio</a></li>
			    	<li><a id="Noticias" href="<?php echo base_url(); ?>index.php/liderazgo">Liderazgo</a></li>
			    	<li><a id="hacemos" href="<?php echo base_url(); ?>index.php/multimedia">Multimedia</a></li>
			    	<li><a id="nosotros" href="<?php echo base_url(); ?>index.php/nosotros">Nosotros</a></li>
			    	<li><a id="Contáctanos" href="<?php echo base_url(); ?>index.php/contactos">Contáctanos</a></li>
			    	<?php if($this->session->userdata('is_logued_in')) {?>
			    	<li><a id="administracion" href="<?php echo base_url(); ?>index.php/homeBackend">Administración</a></li>
			    	<li><a id="log" href="<?php echo base_url()?>index.php/login/logout_ci">Cerrar Seccion</a></li>
			    	<li><p id="usuario">Hola <?php echo $this->session->userdata('username'); ?></p></li>
			    	<?php }else{ ?>
			    	<li><a id="log" href="<?php echo base_url(); ?>index.php/login/login_view">Iniciar Seccion</a></li>
			    	<?php } ?>
			    	<!--Para usar el fancyBox una clase para llamarla desde jquery y cargar el fancy y un ID que es el html o archivo que mostrara dentro del fancy-->
			    	<!--<li><a class="various" href="#Log">Iniciar Seccion</a></li>-->
			    </ul>
			</nav>
		</div>

			