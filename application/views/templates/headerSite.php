<!DOCTYPE html>
	<html lang="es" ng-app="AlbergueApp">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="format-detection" content="telephone=no">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title>IDP</title>
	    <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/style.css">
	    <!-- Bootstrap
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">--> 
	    <!-- FancyBox -->
	    <link rel="stylesheet" href="<?php echo base_url(); ?>application/fancyBox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	    <!--Archivos de Uikit Locales -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>application/css/Uikit/css/uikit.min.css" />

		<!-- Archivos de plantilla Site-->
            <link rel="icon" href="<?php echo base_url(); ?>" type="image/x-icon">
	    <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/grid.css">
	    <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/style2.css">
	    <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/camera.css">
	    <link rel="stylesheet" href="<?php echo base_url(); ?>/application/css/owl-carousel.css">
	    

	    <!--<script src="<?php echo base_url(); ?>/application/js/jquery.js"></script>-->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<?php echo base_url(); ?>/application/js/angular.js"></script>
	    <script src="<?php echo base_url(); ?>/application/js/Angular-Animate.js"></script>
	    <script src="<?php echo base_url(); ?>/application/js/Funciones-angular.js"></script>
    	<script src="<?php echo base_url(); ?>/application/js/jquery-migrate-1.2.1.js"></script>
	    <!--[if lt IE 9]>
	    <html class="lt-ie9">
	      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/..">
	      <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
	    </html>
	    <script src="js/html5shiv.js"></script><![endif]-->
	    <script src="<?php echo base_url(); ?>/application/js/device.min.js"></script>


	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
      
		 	
	</head>
	<body>
	<div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
           
      -->
      <header>
        <div class="container">
          <div class="brand">
            <!--<h1 class="brand_name"><a href="./">Business</a></h1>-->
            <a href="<?php echo base_url(); ?>index.php/home"><img style='width:19% !important' src="<?php echo base_url(); ?>/application/img/logo.png"></a>
            <p class="brand_slogan">Los Frailes I</p>
          </div><a href="callto:#" class="fa-phone">809-599-1234</a>
          <p>Estamos para predicar las buenas nuevas de salvacion y para servir a los demas. Llamanos si necesitas ayuda.</p>
        </div>
        <div id="stuck_container" class="stuck_container">
          <div class="container">
            <nav class="nav">
              <ul data-type="navbar" class="sf-menu todosLi">
                <li class="homeActive"><a href="<?php echo base_url(); ?>index.php/home">Inicio</a></li>
                <li class="liderazgoActive"><a  href="<?php echo base_url(); ?>index.php/liderazgo">Liderazgo</a>
                  <!--<ul>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li><a href="#">Conse ctetur adipisicing</a></li>
                    <li><a href="#">Elit sed do eiusmod
                        <ul>
                          <li><a href="#">Lorem ipsum</a></li>
                          <li><a href="#">Conse adipisicing</a></li>
                          <li><a href="#">Sit amet dolore</a></li>
                        </ul></a></li>
                    <li><a href="#">Incididunt ut labore</a></li>
                    <li><a href="#">Et dolore magna</a></li>
                    <li><a href="#">Ut enim ad minim</a></li>
                  </ul>-->
                </li>
                <li class="multimediaActive"><a href="<?php echo base_url(); ?>index.php/multimedia">Multimedia</a>
                </li>
                <li class="noticiaActive"><a href="<?php echo base_url(); ?>index.php/noticias">Noticias</a>
                </li>
                <li class="nosotrosActive"><a href="<?php echo base_url(); ?>index.php/nosotros">Nosotros</a>
                </li>
                <li class="contactoActive"><a href="<?php echo base_url(); ?>index.php/contactos">Contactos</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </header>
      <?php if (isset($active)) { ?>
        <script type="text/javascript">
            var activo = "<?php echo $active; ?>";

            $('.todosLi li').removeClass('active');
            $('.'+activo).addClass('active'); 
           
             

        </script>
      <?php } ?>
      
     