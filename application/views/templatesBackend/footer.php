				<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
				    
				    <!-- JQuery Validate -->
				    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js" type="text/javascript"></script>
				    <!-- JQuery UI -->
				    <script src="<?php echo base_url(); ?>application/js/jquery-ui.js" type="text/javascript"></script>
				    <!-- Include all compiled plugins (below), or include individual files as needed -->
				    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
				    <script type="text/javascript" src="<?php echo base_url(); ?>application/fancyBox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
				    <script src="<?php echo base_url(); ?>application/js/funciones.js"></script>
				    <script src="<?php echo base_url(); ?>application/js/validarCampos.js"></script>


				     <!--/////Script para hacer el canvas de las graficas estadisticas.///////////-->
				     
				    <script type="text/javascript">
						$(document).ready(function() {
				 					   							
							$(".fancybox").fancybox();							
							$(".various").fancybox({
								maxWidth	: 1400,
								maxHeight	: 1800,
								fitToView	: false,
								width		: '70%',
								height		: '70%',
								autoSize	: false,
								closeClick	: false,
								openEffect	: 'none',
								closeEffect	: 'none'
							});

							$(".fancybox2").fancybox({
								openEffect	: 'none',
								closeEffect	: 'none'
							});

							/*-------------------- Validar Campos---------------------*/

							//Campos solo letras

							$('#user_horizontal_name').validCampoFranz(' abcdefghijklmnñopqrstuvwxyzáéiou');
							$('#user_horizontal_lastname').validCampoFranz(' abcdefghijklmnñopqrstuvwxyzáéiou');

							//Campos solo Numeros
							$('#user_horizontal_telefono').validCampoFranz('0123456789 ');


							var valueEstado = $('#inputEstados').val();
							var valueLiderCelula = $('#user_horizontal_lider').val();
							var valueLiderLiderazgo = $("select[name=responsable] option:selected").val();
							/*---------------------------------------------------------*/


				    		$(".form-signin:input[type=submit]").addClass("btn-primary");

				    		/*--- Formulario de membresia -----*/


				    		$("#submitAgregarMiembro").click(function(e) {
								e.preventDefault();
								$('.cargandoContenedor').css('opacity',0.5);
								$('.cargando').fadeIn('fast');


								var data = {};
								data['id'] =$("input[name=idMiembro]").val();
								data['nombre']  = $("input#user_horizontal_name").val();
								data['apellido']  = $("input#user_horizontal_lastname").val();
								data['email']  = $("input#user_horizontal_email").val();
								data['telefono']  = $("input#user_horizontal_telefono").val();
								data['estado'] =  $("select[name=estados] option:selected").val();
								data['estadoCivil']  = $("input[name=user_horizontal_estadoCivil]:checked").val();
								data['estadoEducativo']  = $("input[name=user_horizontal_estadoEducativo]:checked").val();
								data['nivelEducativo'] = $("input[name=user_horizontal_nivelEducativo]:checked").val();
								data['estadoLaboral'] = $("input[name=user_horizontal_estadoLaboral]:checked").val();
								data['sexo'] = $("input[name=user_horizontal_sexo]:checked").val();
								data['celula'] = $("select[name=celula] option:selected").val();
								data['fechaNacimiento'] = $("input#user_horizontal_fecha").val();
								data['fechaConversion'] = $("input#user_horizontal_fechaConversion").val();
								data['carrera'] = $("select[name=carreras] option:selected").val();
								data['descripcionCambioEstado'] = $('#user_horizontal_descripcion_cambio').val();
								data['valueAntiguo'] = valueEstado;
								$('#inputCarreras').fadeOut('fast');

								console.log(data);
								var url2 = "<?php echo site_url('miembros/agregarMiembro'); ?>";
								console.log(url2);

								$.ajax({
							        type: "POST",
							        url: "<?php echo site_url('miembros/agregarMiembro'); ?>",
							        data: data, // <--- THIS IS THE CHANGE
							        dataType: "json",
							        success: function(data){
							           validado = JSON.parse(data);

							           console.log('Exito');
							           console.log(validado['validado']);

							           $('.cargando').fadeOut('fast');
							           $('.cargandoContenedor').css('opacity',1);

							           	if(validado['validado']==1){
							           		$("input#user_horizontal_name").val('');
											$("input#user_horizontal_lastname").val('');
											$("input#user_horizontal_email").val('');
											$("input#user_horizontal_telefono").val('');
											$("input[name=user_horizontal_estadoCivil]:checked").prop( "checked", false );
											$("input[name=user_horizontal_estadoEducativo]:checked").prop( "checked", false );
											$("input[name=user_horizontal_nivelEducativo]:checked").prop( "checked", false );
											$("input[name=user_horizontal_estadoLaboral]:checked").prop( "checked", false );
											$("input[name=user_horizontal_sexo]:checked").prop( "checked", false );
											$("input#user_horizontal_fecha").val('');
											$("input#user_horizontal_fechaConversion").val('');

											$('.successFormularioMiembros').fadeIn('fast',function(){
												setTimeout(function(){ 
													$('.successFormularioMiembros').fadeOut('slow');
													$(location).attr('href',window.location.href.replace(window.location.search,'?a=2'));
												}, 500);
											});
											$('.errorFormularioMiembros').fadeOut('fast');

							          	}
							           	if(validado['validado']==2){
							           		$('.errorFormularioMiembros').fadeIn('slow');
							           	}
										$('body').scrollTop(0);
							        },
							        error: function(e) { 
							        	console.log(e);
							        	console.log('fallo'); 
							        	$('.cargando').fadeOut('fast');
							           	$('.cargandoContenedor').css('opacity',1);
							        }
   								});

							});

							var primerParametro = location.search.substring(1,2);
							var segundoParametro = location.search.substring(7,5);
							
							if(primerParametro=='a' &&  segundoParametro=='id'){
								$('#inputEstados').change(function(){
									if(valueEstado == $('#inputEstados').val()){
										$('#descripcionCambio').fadeOut('slow');
									}else{
										$('#descripcionCambio').fadeIn('slow');

									}
								});
							}
							

							/* Ver Total de Miembros en el Sigle View*/

							$('#ver_miembros_sigle_view').click(function(e){
								e.preventDefault();
								$('.ver_total_miembros_sigle_view').fadeIn('slow');
							});
							/*-----Formulario Asistencia --------------------*/


							$("#submitAgregarAsitencia").click(function(e) {
								e.preventDefault();
								$('.cargandoContenedor').css('opacity',0.5);
								$('.cargando').fadeIn('fast');
								
								var data = {};

								var valor =[];
								var id =[];


								data['fecha'] = $("input#user_horizontal_fechaEvento").val();
								data['nombre']  = $("select[name=user_horizontal_nameEvento] option:selected").val();

								idEntrante = 0;
								pase = 1;
								$(':checkbox:checked').each(function(i){									

									if(idEntrante != $(this).val().substring(9)){
										valor[i] = $(this).val().substring(0,8);;
          								id[i] = $(this).val().substring(9);
          								idEntrante = $(this).val().substring(9);
									}else{
										$('.cargando').fadeOut('fast');
							            $('.cargandoContenedor').css('opacity',1);
							            $('body').scrollTop(0);
							            $('.errorFormularioAsistenciaCheckRepetidos').fadeIn('fast');

							            setTimeout(function() {
							            	$('.errorFormularioAsistenciaCheckRepetidos').fadeOut('slow');

							            },5000);
							            pase = 0;
							           return false;
							        }
          							
          							//console.log(i);
       							});

								if(pase > 0){
									data['valor'] = valor;
	       							data['id']=id;

									$.ajax({
								        type: "POST",
								        url: "<?php echo site_url('miembros/agregarAsistencia'); ?>",
								        data: data, // <--- THIS IS THE CHANGE
								        dataType: "json",
								        success: function(data){
								           validado = JSON.parse(data);

								           $('.cargando').fadeOut('fast');
								           $('.cargandoContenedor').css('opacity',1);

								           if(validado['validado']==1){
												$("input#user_horizontal_fechaEvento").val('');
												$(':checkbox').prop('checked',false);
												
												$('body').scrollTop(0);
												$('.successFormularioAsistencia').fadeIn('fast',function(){
													setTimeout(function(){ 
														$('.successFormularioAsistencia').fadeOut('slow');
														//$(location).attr('href',window.location.href.replace(window.location.search,''));
													}, 1000);
												});
											}

								           console.log('Exito');
								           console.log(validado['validado']);
								        },
								        error: function(e) { 
								        	console.log(e);
								        	console.log('fallo');
								        }
	   								});

								}       							

								
							});

							/*----------Formulario de Eventos--------------*/

							$("#submitAgregarEvento").click(function(e) {
								e.preventDefault();
								$('.cargandoContenedor').css('opacity',0.5);
								$('.cargando').fadeIn('fast');
								
								var data = {};

								data['nombre_evento'] = $("input#user_horizontal_name_evento").val();
								data['responsable_evento']  = $("select[name=responsable] option:selected").val();
								data['ministerio']  = $("select[name=ministerios] option:selected").val();
								data['fecha_evento'] = $("#user_horizontal_fecha2").val();
								data['descripcion_evento'] = $("#user_horizontal_descripcion").val();
								

								console.log(data);

								$.ajax({
							        type: "POST",
							        url: "<?php echo site_url('eventos/agregarEvento'); ?>",
							        data: data, // <--- THIS IS THE CHANGE
							        dataType: "json",
							        success: function(data){
							           validado = JSON.parse(data);

							           $('.cargando').fadeOut('fast');
							           $('.cargandoContenedor').css('opacity',1);

							           if(validado['validado']==1){
							           		$("input#user_horizontal_name_evento").val('');
											$("input#user_horizontal_name_responsable").val('');
											$("input#user_horizontal_ministerios").val('');
											$("#user_horizontal_fecha2").val('');
											$("#user_horizontal_descripcion").val('');
											

											$('.successFormularioEvento').fadeIn('fast',function(){
												setTimeout(function(){ 
													$('.successFormularioEvento').fadeOut('slow');
												}, 500);
											});
										}

							           console.log('Exito');
							           console.log(validado['validado']);
							        },
							        error: function(e) { 
							        	console.log(e);
							        	console.log('fallo');
							        }
   								});


							});

							/*----------------- Formulario de Crear Celula------------------*/

							$("#submitAgregarCelula").click(function(e) {
								e.preventDefault();

								$('.cargandoContenedor').css('opacity',0.5);
								$('.cargando2').fadeIn('fast');
								
								var data = {};

								data['id'] = $("input[name=id]").val();
								data['lider_celula'] = $("select[name=lider] option:selected").val();
								data['anfitrion'] = $("input#user_horizontal_anfitrion").val();
								data['direccion'] = $("#user_horizontal_direccion").val();
								data['dia']  = $("select[name=dias] option:selected").val();
								data['fecha_inicio'] = $("#user_horizontal_fecha").val();
								data['descripcion_cambio_lider'] = $('#user_horizontal_descripcion_cambio_lider').val();
															

								console.log(data);		

								$.ajax({
							        type: "POST",
							        url: "<?php echo site_url('gcrecimiento/agregarCelula'); ?>",
							        data: data, // <--- THIS IS THE CHANGE
							        dataType: "json",
							        success: function(data){
							           validado = JSON.parse(data);

							           $('.cargando2').fadeOut('fast');
							           $('.cargandoContenedor').css('opacity',1);

							           if(validado['validado']==1){
							           		$("input#user_horizontal_lider").val('');
											$("input#user_horizontal_anfitrion").val('');
											$("#user_horizontal_direccion").val('');
											$("#user_horizontal_fecha").val('');
											

											$('.successFormularioCelula').fadeIn('fast',function(){
												setTimeout(function(){ 
													$('.successFormularioCelula').fadeOut('slow');
													$(location).attr('href',window.location.href.replace(window.location.search,'?b=1'));
												}, 500);
											});
										}

							           console.log('Exito');
							           console.log(validado['validado']);
							        },
							        error: function(e) { 
							        	console.log(e);
							        	console.log('fallo');
							        }
   								});				


							});

							var primerParametroCelula = location.search.substring(1,2);
							var segundoParametroCelula = location.search.substring(7,5);


							if(primerParametroCelula=='b' &&  segundoParametroCelula=='id'){
								$('#user_horizontal_lider').change(function(){
									if(valueLiderCelula == $('#user_horizontal_lider').val()){
										$('#descripcionCambioLider').fadeOut('slow');
									}else{
										$('#descripcionCambioLider').fadeIn('slow');

									}
								});
							}

							/* ------------------Actualizar Miembros Celulas-----------------------*/

							$('.eliminarMiembroCelula').click(function(e){
								e.preventDefault();
								ruta = $(this).attr('href');

								current = $(this).parent().parent();
								console.log(current);
								var data = {};

								data['celula'] = $(this).data('celula');

								//console.log(data['celula']);

								$.ajax({
							        type: "POST",
							        url: ruta,
							        data: data, // <--- THIS IS THE CHANGE
							        dataType: "json",
							        success: function(data){
							        	current.remove();	
							        	//console.log($(this));							        	
							           console.log('Exito');
							        },
							        error: function(e) { 
							        	console.log(e);
							        	console.log('fallo');
							        }
   								});


							});

							/*------- Agregar Miembros a las Celulas--------------*/


							$('#addMiembroFull').click(function(e){
								e.preventDefault();
								var data = {};

								var valor =[];

								data['celula'] = $('input[name=chkAddMiembroCelula]:checked').data('celula');
								
								$('input[name=chkAddMiembroCelula]:checked').each(function(i){
          							valor[i] = $(this).val();
          							//console.log(i);
       							});

       							data['valor'] = valor;

								$.ajax({
							        type: "POST",
							        url: "<?php echo site_url('gcrecimiento/actualizarMiembroCelula'); ?>",
							        data: data, // <--- THIS IS THE CHANGE
							        dataType: "json",
							        success: function(data){							        								        	
							        	console.log('Exito');
							        },
							        error: function(e) { 
							        	console.log(e);
							        	console.log('fallo');
							        }
   								});							
								
							});

	
							/*-----------------Agregar Noticia--------------*/
							$('#submitAgregarNoticia').click(function(e){
								e.preventDefault();

								$('.cargandoContenedor').css('opacity',0.5);
								$('.cargandoNoticias').fadeIn('fast');



								var id = $("input[name=idNoticia]").val();
								var titulo = $("input[name=user_horizontal_titulo]").val();
								var descripcion_noticia = $("#user_horizontal_descripcion_noticia").val();
								var fecha_noticia = $("#user_horizontal_fechaNoticia").val();
								var columna = $("select[name=columnas] option:selected").val();
   								var inputFileImage = document.getElementById('user_horizontal_imagen_noticia');
								var file = inputFileImage.files[0];
								var data = new FormData();
								
								data.append('id',id);
								data.append('titulo',titulo);
								data.append('descripcion_noticia',descripcion_noticia);
								data.append('fecha_noticia',fecha_noticia);
								data.append('columna',columna);
								data.append('archivo',file);


								//console.log(file);
								//console.log(titulo);
								//console.log(descripcion_noticia);
								///console.log(fecha_noticia);
								//console.log(id);								

								$.ajax({
									url:"<?php echo site_url('NoticiasBackend/agregarNoticia'); ?>",
									type:'POST',
									contentType:false,
									data:data,
									processData:false,
									cache:false,

									success: function(data){

										$('#new_user_horizontal_form').each(function(){
										    this.reset();
										});


										$('.cargandoContenedor').css('opacity',1);
										$('.cargandoNoticias').fadeOut('fast',function(){
											setTimeout(function(){ 
												$(location).attr('href',window.location.href.replace(window.location.search,'?n=2'));
											}, 500);
										});

										console.log('Exito');
							        },
							        error: function(e) { 
							        	console.log(e);
							        	console.log('fallo');
							        }
								});	
							});

						$('#cambiarImagenNoticia').click(function(e){
													e.preventDefault();
													$('#imagenMostrar').addClass('hidden');
													$('#imagenNoticias').removeClass('hidden');
												});


						$('#eliminarImagenNoticia').click(function(e){
							e.preventDefault();
							$('#imagenMostrar').addClass('hidden');


								var id = $("input[name=idNoticia]").val();
								var titulo = $("input[name=user_horizontal_titulo]").val();
								var descripcion_noticia = $("#user_horizontal_descripcion_noticia").val();
								var fecha_noticia = $("#user_horizontal_fechaNoticia").val();
   								var inputFileImage = document.getElementById('user_horizontal_imagen_noticia');
								var file = inputFileImage.files[0];
								var data = new FormData();
								data.append('id',id);
								data.append('titulo',titulo);
								data.append('descripcion_noticia',descripcion_noticia);
								data.append('fecha_noticia',fecha_noticia);
								data.append('archivo',file);




								$.ajax({
										url:"<?php echo site_url('NoticiasBackend/eliminarImagenNoticia'); ?>",
										type:'POST',
										contentType:false,
										data:data,
										processData:false,
										cache:false,

										success: function(data){									
								        	console.log('Exito eliminar NOticia');
								        },
								        error: function(e) { 
								        	console.log(e);
								        	console.log('fallo');
								        }
								});	


							});

								$('#opci2').click(function(){
									$('#new_user_horizontal_form').each(function(){
											this.reset();
											$(location).attr('href',window.location.href.replace(window.location.search,'?n=2'));

									});
							});

						/*----Configuracion Para los Tabs de el Miembro_view*/

							$('#myTabs a').click(function (e) {
	  							e.preventDefault();
	  							$(this).tab('show');
							});

							$('#TabsId2 a').click(function (e) {
	  							e.preventDefault();
	  							$(this).tab('show');
							});

							$('#TabsMultimedia a').click(function (e) {
	  							e.preventDefault();
	  							$(this).tab('show');
							});

							$('#TabsMultimediaHome a').click(function (e) {
	  							e.preventDefault();
	  							$(this).tab('show');
							});

							

							$('#TabsId a').click(function (e) {
	  							e.preventDefault();
	  							if($(this).attr('class')=='ponerOcultoElInputBuscar'){
	  								$('#id-li-Input-buscar').fadeOut('slow');
	  							}
	  							if($(this).attr('class')=='ponerVisibleElInputBuscar'){
	  								$('#id-li-Input-buscar').fadeIn('slow');
	  							}
	  							
	  							$(this).tab('show');

							});


	/*-----------------Buscardor Por Nombre -----------------------*/
							$('#buscarMiembro').click(function(){
							//$('#inputBuscarMiembro').change(function(){

								//if($('#inputBuscarMiembro').val().length != 0){
									$('.cargandoContenedor').css('opacity',0.5);
									$('.cargando').fadeIn('fast');

									$nombre = $('#inputBuscarMiembro').val();
									console.log($nombre);

									ruta = "<?php echo site_url('miembros/getMiembroPorNombre?nombre='); ?>";
									$.ajax({
										url:ruta+$nombre,
										type:'GET',
										data:$('#inputBuscarMiembro').val(),
										dataType: "json",
										cache:false,

										success:function(data){
											console.log('exito');
											console.log(Object.keys(data.miembrosN).length);
											if(data.miembrosN.length > 0){
												
												$contador = 0;
												$('.boxMiembrosPrincipal').empty();	
												$.each(data.miembrosN,function(i,key){													 
											    	$('.boxMiembrosPrincipal').append("<div class=\"row\"><div class=\"col-md-4\"><a class=\"various fancybox.ajax\" href=\"<?php echo base_url(); ?>index.php/miembros/ver_Miembro/"+data.miembrosN[i].id+"\"><p>"+data.miembrosN[i].nombre+" "+data.miembrosN[i].apellido+"</p></a></div><div class=\"col-md-2\"><p>"+data.miembrosN[i].apellido+"</p></div><div class=\"col-md-2\"><a href=\"<?php echo base_url(); ?>index.php/miembros?a=2&id="+data.miembrosN[i].id+"\"><p>Editar</p></a></div></div>");
										    	});								
												$('.cargandoContenedor').css('opacity',1);
												$('.cargando').fadeOut('fast');
												
											}else{
												
												$('.boxMiembrosPrincipal').empty();	
												$('.boxMiembrosPrincipal').append("<div class=\"row\"><div class=\"col-md-12\"><h1>No hay datos que coinciden con ese nombre</h1></div></div>");
												$('.cargandoContenedor').css('opacity',1);
												$('.cargando').fadeOut('fast');
											}

											//console.log(data);
										},
										error:function(e){
											console.log(e);
											console.log('fallo');
										}
										
									});	
																	
								/*}else{
									$('.boxMiembrosPrincipal').empty();	
									// tengo $johan en vez de $miembros tengo que corrergir eso. se pone blanca la pantalla de celulas cuando lo cambio a miembros.
									$('.boxMiembrosPrincipal').html("<?php if(isset($johan)){$CI =& get_instance();	foreach($johan as $key => $value){ $porcentajeString=$CI->porcentajeIndividualPorMiembrosColor($value['id']); $porcentajeInt=(int)$porcentajeString; ?><div class=\"row\"><div class=\"col-md-4\"><a class=\"various fancybox.ajax <?php if($porcentajeInt<=40){echo 'colorRojo';} if($porcentajeInt>40 && $porcentajeInt<=70) { echo 'colorOrange';}?>\" href=\"<?php echo base_url(); ?>index.php/miembros/ver_Miembro/<?php echo $value['id']; ?>\"><p><?php echo $value['nombre'].' '. $value['apellido'] ; ?></p></a></div><div class=\"col-md-2\"><p><?php echo $value['apellido']; ?></p></div><div class=\"col-md-2\"><a href=\"<?php echo base_url(); ?>index.php/miembros?a=2&id=<?php echo $value['id']; ?>\"><p>Editar</p></a></div></div> <?php	}} ?>");
									    	
								}*/
							});

	/*---------- Buscar la estadistica------------------*/

							$('#buscarEstadisticaAsistencia').click(function() {
					
								var data = {};

								data['sexo']=$("select[name=sexoFiltroEstadistica] option:selected").val();
								data['ministerio']=$("select[name=ministerioFiltroEstadistica] option:selected").val();
								data['fechaInicial']=$("#user_horizontal_fechaInicial").val();
								data['fechaFinal']=$("#user_horizontal_fechaFinal").val();
								data['actividad']=$("select[name=actividadesRegularesFiltroEstadistica] option:selected").val();

								console.log(data);

								ruta = "<?php echo site_url('estadistica/getEstadisticaAsistencia'); ?>";
								$.ajax({
									url:ruta,
									type:'POST',
									data:data,
									dataType: "json",
									cache:false,

									success:function(data){
										console.log(data['grafica']);
										console.log(data['porcentajeActividad']);
										$('.contenedorGraficaEstadistica').children().remove();
										$('.contenidoEstadistica>h1').fadeOut('fast');
										$('.contenedorGraficaEstadistica').append('<canvas id="canvas2" height="100" width="200"></canvas>');

										
										if(data['grafica']==1){
											$('.opcion1>h1').fadeOut('fast');
											$('.leyendaEstadistica').fadeOut('fast');
											generarGraficaMasculino(data['porcentajeEstadistica']);
										}
										if(data['grafica']==2){
											$('.opcion1>h1').fadeOut('fast');
											$('.leyendaEstadistica').fadeOut('fast');
											generarGraficaFemenino(data['porcentajeEstadistica']);
										}
										if(data['grafica']==3){
											$('.opcion1>h1').fadeOut('fast');
											$('.leyendaEstadistica').fadeOut('fast');
											generarGraficaAmbosSexos(data['porcentajeEstadistica'].porcentajeEstadisticaHombres,data['porcentajeEstadistica'].porcentajeEstadisticaMujeres);
										}	
										if(data['grafica']==4){
											$('.opcion1>h1').fadeOut('fast');
											$('.leyendaEstadistica').fadeIn('slow');
											generarGraficaPorActividad(data['nombreActividad'],data['porcentajeActividad'],data['nombreActividadF'],data['porcentajeActividadF']);
										}
										if(data['grafica']==5){

											console.log(data);
											console.log('arriba');
											if(data['sexo']==2){
												$('.opcion1>h1').fadeOut('fast');
												$('#hombresPorActividad').fadeIn('slow');
											}
											if(data['sexo']==3){
												$('.opcion1>h1').fadeOut('fast');
												$('#mujeresPorActividad').fadeIn('slow');
											}
											$('.leyendaEstadistica').fadeOut('fast');
											generarGraficaPorActividad2(data['nombreActividad'],data['porcentajeActividad'],null,null);
										}
										if(data['grafica']==6){

											console.log(data);
											console.log('arriba');
											if(data['sexo']==2){
												$('.opcion1>h1').fadeOut('fast');
												$('#hombresPorActividad').fadeIn('slow');
											}
											if(data['sexo']==3){
												$('.opcion1>h1').fadeOut('fast');
												$('#mujeresPorActividad').fadeIn('slow');
											}
											$('.leyendaEstadistica').fadeOut('fast');
											generarGraficaPorActividad2(data['nombreActividad'],data['porcentajeActividad'],null,null);
										}
										if(data['grafica']==7){
											//console.log(data);
											$('.leyendaEstadistica').fadeOut('fast');
											if(data['sexo']==2){
												$('.opcion1>h1').fadeOut('fast');
												$('#hombresPorActividad').fadeIn('slow');
											}
											if(data['sexo']==3){
												$('.opcion1>h1').fadeOut('fast');
												$('#mujeresPorActividad').fadeIn('slow');
											}
											generarGraficaPorActividadYSexo(data);
										}	

										if(data['grafica']==8){
											console.log(data);
											$('.leyendaEstadistica').fadeOut('fast');
											$('.opcion1>h1').fadeOut('fast');
											generarGraficaPorActividad4(data['nombreActividad'],data['porcentajeActividad']);
										}
										if(data['grafica'] ==9){
											console.log(data);
											$('.opcion1>h1').fadeOut('fast');
											$('.leyendaEstadistica').fadeIn('slow');
											generarGraficaPorActividad3(data);
										}
										console.log('arribita');								
										//generarGrafica(data['porcentajeEstadistica']);
										console.log('exito');
										//console.log(data['porcentajeEstadistica'][1].porcentaje);
										
									},
									error:function(e){
										console.log(e);
										console.log('fallo');
									}
									
								});


							});
						
						$('#user_horizontal_cantidaBruta').change(function(e) {
							e.preventDefault();

							var restante = 0;
							var gastosTotal =0;


							restante = restante + $('#user_horizontal_cantidaBruta').val();

							$('.boxInputOfrenda').find(':input[name=user_horizontal_cantidadGasto]').each(function() {
									var elemento = this;
									//alert('elemento.id='+ elemento.id + ",elemento.value"+elemento.value);
									restante = parseInt(restante) - parseInt(elemento.value);
									gastosTotal = parseInt(gastosTotal) + parseInt(elemento.value);
							});
							$('#cantidadNeta').empty();
							$('#cantidadNeta').append(restante);
							$('#cantidadGastos').empty();
							$('#cantidadGastos').append(gastosTotal);
						});


						$('#submitAgregarOfrenda').click(function(e){
							e.preventDefault();
							$('.cargandoContenedor').css('opacity',0.5);
							$('.cargando').fadeIn('fast');
							var data = {};
							var dato1 =[];
							var dato2 = [];
							var dato = [];
							var contador = 0;

							data['actividad'] = $("select[name=inputActividades] option:selected").val();
							data['cantidadBruta'] = $('#user_horizontal_cantidaBruta').val();
							data['fecha'] = $("#user_horizontal_fechaActividad").val();
							$('.boxInputOfrenda').find(':input[name=user_horizontal_cantidadGasto]').each(function() {									
									var elemento = this;
									dato1[contador] = elemento.value;
									contador++;
							});
							contador = 0;
							$('.boxInputOfrenda').find(':input[name=user_horizontal_descripcionGasto]').each(function() {									
									var elemento = this;
									dato2[contador] = elemento.value;	
									contador++;		
							});
							data['gastos'] = dato1;
							data['descripcionGastos'] = dato2;
							data['cantidadNeta'] = parseInt($('#cantidadNeta').text());
							data['gastosTotal'] = parseInt($('#cantidadGastos').text());
							console.log(data);

							$.ajax({
								url:"<?php echo site_url('finanzas/agregarOfrenda'); ?>",
								type:"post",
								data:data,
								dataType: "json",

								success:function(data){
									console.log('exito');
									console.log(data);

									$('#cantidadNeta').text('');
									$('#cantidadGastos').text('');
									$('#user_horizontal_cantidaBruta').val('');
									$('.boxInputOfrenda').empty();
									$("#user_horizontal_fechaActividad").val('');

									$('.cargandoContenedor').css('opacity',1);
									$('.cargando').fadeOut('fast');
								},
								error:function(e){
									console.log(e);
									console.log('fallo');
									$('.cargandoContenedor').css('opacity',1);
									$('.cargando').fadeOut('fast');
								}
								
							});	
						});

						$('#submitAgregarDatos').click(function(e){
							e.preventDefault();
							$('.cargandoContenedor').css('opacity',0.5);
							$('.cargando').fadeIn('fast');
							var data = {};
							var dato1 =[];
							var dato2 = [];
							var dato = [];
							var contador = 0;

							data['concepto'] = $("select[name=inputConceptos] option:selected").val();
							data['2010'] = parseFloat($('#user_horizontal_2010').val());
							data['2011'] = parseFloat($('#user_horizontal_2011').val());
							data['2012'] = parseFloat($('#user_horizontal_2012').val());
							data['2013'] = parseFloat($('#user_horizontal_2013').val());
							data['2014'] = parseFloat($('#user_horizontal_2014').val());


							
							console.log(data);

							$.ajax({
								url:"<?php echo site_url('economia/agregarDatos'); ?>",
								type:"post",
								data:data,
								dataType: "json",

								success:function(data){
									console.log('exito');
									console.log(data);

									$('#new_user_horizontal_form_economia').each(function(){
											    this.reset();
									});


									$('.cargandoContenedor').css('opacity',1);
									$('.cargando').fadeOut('fast');
								},
								error:function(e){
									console.log(e);
									console.log('fallo');
									$('.cargandoContenedor').css('opacity',1);
									$('.cargando').fadeOut('fast');
								}
								
							});	
						});

						/*-----------------Agregar Lider--------------*/
							$('#submitAgregarLider').click(function(e){
								e.preventDefault();

								$('.cargandoContenedor').css('opacity',0.5);
								$('.cargandoNoticias').fadeIn('fast');


								var id = $("input[name=idLider]").val();								
								var lider = $("select[name=responsable] option:selected").val();
								var descripcion_cambio_liderazgo = $("#user_horizontal_descripcion_cambio_lider").val();
								var biografia = $("#biografia").val();
								var fecha_nombramiento = $("#user_horizontal_fechaNombramiento").val();
								var cargo = $("select[name=cargo] option:selected").val();
   								var inputFileImage = document.getElementById('user_horizontal_imagen_lider');
								var file = inputFileImage.files[0];
								var data = new FormData();
								data.append('id',id);
								data.append('lider',lider);
								data.append('biografia',biografia);
								data.append('descripcion_cambio_liderazgo',descripcion_cambio_liderazgo);
								data.append('fecha_nombramiento',fecha_nombramiento);
								data.append('cargo',cargo);
								data.append('archivo',file);

								//console.log(lider);
								//console.log(descripcion_cambio_liderazgo);
								//console.log(id);
								console.log(file);
								//console.log(id);
								//console.log(biografia);

								

								$.ajax({
										url:"<?php echo site_url('liderazgoBackend/agregarLider'); ?>",
										type:'POST',
										contentType:false,
										data:data,
										processData:false,
										cache:false,

										success: function(data){

											$('#new_user_horizontal_form_liderazgo').each(function(){
											    this.reset();
											});


											$('.cargandoContenedor').css('opacity',1);
											$('.cargandoNoticias').fadeOut('fast',function(){
												setTimeout(function(){ 
													$(location).attr('href',window.location.href.replace(window.location.search,'?l=2'));
												}, 500);
											});											
																					
								        	console.log('Exito');
								        },
								        error: function(e) { 
								        	console.log(e);
								        	console.log('fallo');
								        }
								});
							});

							var primerParametroLiderazgo = location.search.substring(1,2);
							var segundoParametroLiderazgo = location.search.substring(7,5);


							if(primerParametroLiderazgo=='l' &&  segundoParametroLiderazgo=='id'){
								$("select[name=responsable]").change(function(){
									if(valueLiderLiderazgo == $("select[name=responsable] option:selected").val()){
										$('#descripcionCambioLider').fadeOut('slow');
									}else{
										$('#descripcionCambioLider').fadeIn('slow');
									}
								});
							}

							$('#cambiarImagenLider').click(function(e){
								e.preventDefault();
								$('#imagenMostrar2').addClass('hidden');
								$('#imagenLider').removeClass('hidden');
							});


							$('#eliminarImagenLider').click(function(e){
								e.preventDefault();
								$('#imagenMostrar2').addClass('hidden');
							});

							$('#cancelar').click(function() {
								//$('#opcionLider1').click();
								$(location).attr('href',window.location.href.replace(window.location.search,'?l=n'));							
								
							});


						 	/*-----------------Agregar Carpeta Audio --------------*/
							$('#submitAgregarCarpeta').click(function(e){
								e.preventDefault();

								$('.cargandoContenedor').css('opacity',0.5);
								$('.cargandoMultimedia').fadeIn('fast');
								var data = {};

								data['nombreCarpeta'] = $('#user_horizontal_name_carpeta').val();

								
								console.log(data);
								//console.log(descripcion_cambio_liderazgo);
								
								$.ajax({
									url:"<?php echo site_url('multimediaBackend/agregarCarpetaAudio'); ?>",
									type:"POST",
									data:data,

									success:function(data){
										console.log('exito');
										console.log(data);

										$('#new_user_horizontal_form_audio').each(function(){
												    this.reset();
										});


										$('.cargandoContenedor').css('opacity',1);
										$('.cargandoMultimedia').fadeOut('fast',function(){
												setTimeout(function(){ 
													$(location).attr('href',window.location.href.replace(window.location.search,'?m=2'));
												}, 100);
										});
										console.log('si soy yo');
									},
									error:function(e){
										console.log(e);
										console.log('fallo');
										$('.cargandoContenedor').css('opacity',1);
										$('.cargando').fadeOut('fast');
									}
									
								});	
							});
							
							/*-----------------Agregar Archivo de Audio --------------*/					
							//$("#submitAgregarLider2").click(function(e) {
							$("#agregarAudioBienvenido").click(function(e) {
								e.preventDefault();
								//$('.cargandoContenedor').css('opacity',0.5);
								//$('.cargando').fadeIn('fast');
								
								//var data = {};

								//e.preventDefault();

								//$('.cargandoContenedor').css('opacity',0.5);
								//$('.cargandoNoticias').fadeIn('fast');


								var id = $("input[name=id]").val();
								var nombreAudio = $("#user_horizontal_name_audio").val();									
								var carpetaAudio = $("select[name=carpetaAudio] option:selected").val();	
								var fechaAudio = $("#fechaAudio").val();							
   								var inputFileImage = document.getElementById('user_horizontal_imagen_lider2');
								var file = inputFileImage.files[0];
								var data = new FormData();

								data.append('id',id);
								data.append('nombreAudio',nombreAudio);
								data.append('carpetaAudio',carpetaAudio);
								data.append('fechaAudio',fechaAudio);
								data.append('archivo',file);

								console.log(nombreAudio);
								console.log(carpetaAudio);
								console.log(id);
								console.log(file);
								console.log(fechaAudio);
								
								

								/*$.ajax({
									url:"<?php echo site_url('multimediaBackend/agregarAudio3'); ?>",
									type:'POST',
									contentType:false,
									data:data,
									processData:false,
									cache:false,

									success: function(data){

										$('#new_user_horizontal_form_mp3').each(function(){
										    this.reset();
										});

										$('.cargandoContenedor').css('opacity',1);
										$('.cargandoNoticias').fadeOut('fast');										
										console.log(data);										
							        	console.log('Exito');
							        },
							        error: function(e) { 
							        	console.log(e);
							        	console.log('fallo');
							        }
								});*/
								

							});

							$('#cambiarAudio').click(function(e){
								e.preventDefault();
								$('#AudioMostrar').addClass('hidden');
								$('#audioInput').removeClass('hidden');
							});


							$('#eliminarAudio').click(function(e){
								e.preventDefault();
								$('#AudioMostrar').addClass('hidden');
							});

							$('#cancelarAudio').click(function() {
								//$('#opcionLider1').click();
								$(location).attr('href',window.location.href.replace(window.location.search,'?m=n'));							
								
							});

							

							$('#removerAudio').click(function(e) {	
								e.preventDefault();
								removerAudio = $(this);
								//console.log(removerAudio);
								
								$('.cajaDialogoEliminarAudio').dialog({ 
									title:'Confirmar',
									open: function() {
									      var markup = 'Desea eliminar este audio?';
									      $(this).html(markup);
									    },
									buttons: [
									    {
									      text: "Aceptar",
									      click: function() {
									      	    ruta = $('#removerAudio').attr('href');
												rutaDividida = ruta.split('=');
												console.log(rutaDividida[1]);

												var data={};
												data['id'] = rutaDividida[1];
												$(this).parent().parent().parent().remove();

												$.ajax({
													url:"<?php echo site_url('MultimediaBackend/eliminarAudio'); ?>",
													type:'POST',
													dataType: "json",
													data:data,
													cache:false,

													success: function(data){	

														console.log(data);
											        	console.log('Exito eliminar Audio');

											        	console.log('entro');
											        	console.log(removerAudio); 

											        	//removerAudio.fadeOut('fast');

											        	setTimeout(function(){											        	 
															$(location).attr('href',window.location.href.replace(window.location.search,'?m=n'));
														}, 100);
											        },
											        error: function(e) { 
											        	console.log(e);
											        	console.log('fallo');
											        }
												});
									      }
									    },
									    {
									      text: "Cancelar",
									      click: function() {
									        $( this ).dialog( "close");
									      }
									    }
									  ]
								});	
							});

							
							/*---------Agregar Carpeta de foto-----------------*/


							$('#new_user_horizontal_form_carpeta_foto').submit(function(e) {
								 e.preventDefault();
							}).validate({
					            debug: false,
					            rules: {
					                "user_horizontal_name_carpeta_foto": {
					                    required: true
					                },
					                "tipoCarpetaFoto":{
					                	required: true
					                }
					            },
					            messages: {
					                "user_horizontal_name_carpeta_foto": {
					                    required: "Introduce el nombre de la carpeta."
					                },
					                "tipoCarpetaFoto":{
					                	required:"ELija una Opcion.",
					                }

					            },submitHandler: function(form){

									$('.cargandoContenedor').css('opacity',0.5);
									$('.cargandoMultimedia').fadeIn('fast');
									var data = {};

									data['nombreCarpetaFoto'] = $('#user_horizontal_name_carpeta_foto').val();
									data['tipoActividad'] = $("select[name=tipoCarpetaFoto] option:selected").val();

									if($("select[name=diaServicios] option:selected").val() == "0"){
										data['diaServicio'] = 0;
									}else{
										data['diaServicio'] = $("select[name=diaServicios] option:selected").val();
									}
									
									console.log(data);
								
									$.ajax({
										url:"<?php echo site_url('multimediaBackend/agregarCarpetaFoto'); ?>",
										type:"POST",
										data:data,

										success:function(data){
											console.log('exito');
											console.log(data);

											$('#new_user_horizontal_form_carpeta_foto').each(function(){
													    this.reset();
											});


											$('.cargandoContenedor').css('opacity',1);
											$('.cargandoMultimedia').fadeOut('fast',function(){
													setTimeout(function(){ 
														$(location).attr('href',window.location.href.replace(window.location.search,'?m=4'));
													}, 100);
											});
											console.log('si soy yo');
										},
										error:function(e){
											console.log(e);
											console.log('fallo');
											$('.cargandoContenedor').css('opacity',1);
											$('.cargando').fadeOut('fast');
										}
										
									});	
								}	
							});


							
							$('.removerAlbumFoto').click(function(e) {	
								e.preventDefault();
								//alert('hola');
								removerAlbumFoto = $(this);
								//console.log(removerAudio);
								
								$('.cajaDialogoEliminarAudio').dialog({ 
									title:'Confirmar',
									open: function() {
									      var markup = 'Desea eliminar este Album Fotografico?';
									      $(this).html(markup);
									    },
									buttons: [
									    {
									      text: "Aceptar",
									      click: function() {
									      		$('.cargandoContenedor').css('opacity',0.5);
									            $('.cargandoMultimedia').fadeIn('fast');

									      	    ruta = $('.removerAlbumFoto').attr('href');
												rutaDividida = ruta.split('=');
												console.log(rutaDividida[1]);

												var data={};
												data['id'] = rutaDividida[1];
												$(this).parent().parent().parent().remove();

												$.ajax({
													url:"<?php echo site_url('MultimediaBackend/eliminarAlbumFoto'); ?>",
													type:'POST',
													dataType: "json",
													data:data,
													cache:false,

													success: function(data){	

														$('.cargandoContenedor').css('opacity',1);
														$('.cargandoMultimedia').fadeOut('fast');
														console.log(data);
											        	console.log('Exito eliminar Audio');

											        	console.log('entro');
											        	console.log(removerAlbumFoto); 

											        	//removerAudio.fadeOut('fast');

											        	setTimeout(function(){											        	 
															$(location).attr('href',window.location.href.replace(window.location.search,'?m=n'));
														}, 100);
											        },
											        error: function(e) { 
											        	//Por alguna razon el esta funcionando pero esta llegando con un error. Entonces el codigo lo pongo aqui en el error.
											        	//El esta borrando los archivos en la bd y los de la carpeta del proyecto local. 
											        	setTimeout(function(){											        	 
															$(location).attr('href',window.location.href.replace(window.location.search,'?m=n'));
														}, 100);

											        	console.log(e);
											        	console.log('fallo');


											        }
												});
									      }
									    },
									    {
									      text: "Cancelar",
									      click: function() {
									        $( this ).dialog( "close");
									      }
									    }
									  ]
								});	
							});

							/*-----------------Agregar Archivo de Fotos --------------*/					
							$("#submitAgregarFotos").click(function(e) {
								e.preventDefault();
								$('.cargandoContenedor').css('opacity',0.5);
								$('.cargandoMultimedia').fadeIn('fast');
								
								var data = {};

								e.preventDefault();

								$('.cargandoContenedor').css('opacity',0.5);
								$('.cargandoNoticias').fadeIn('fast');


								var id = $("input[name=id]").val();								
								var carpetaFoto = $("select[name=carpetaFoto] option:selected").val();								
   								var inputFileImage = document.getElementById('user_horizontal_imagen_fotos[]');
								var file = inputFileImage.files;
								var data = new FormData();

								data.append('id',id);
								data.append('carpetaFoto',carpetaFoto);
								console.log(file.length);
								for (var i = 0;  i < file.length; i++) {
									//console.log(file[i]);
									data.append('archivo[]',file[i]);
								};
								

								//console.log(id);
								//console.log(carpetaFoto);								
								//console.log(file);

								//console.log(file[2]);								

								$.ajax({
									url:"<?php echo site_url('multimediaBackend/agregarFotos'); ?>",
									type:'POST',
									contentType:false,
									data:data,
									processData:false,
									cache:false,

									success: function(data){

										$('#new_user_horizontal_form_mp3').each(function(){
										    this.reset();
										});

										$('.cargandoContenedor').css('opacity',1);
										$('.cargandoMultimedia').fadeOut('fast');										
										console.log(data);										
							        	console.log('Exito');
							        },
							        error: function(e) { 
							        	console.log(e);
							        	console.log('fallo');
							        }
								});
								

							});

								
							$('.removerFoto').click(function(e) {
								e.preventDefault();

								href = $(this).attr('href');

								
								id = href.split('=');
								data['id'] = id[1];


								console.log(id[1]);

								$.ajax({
									url:"<?php echo site_url('multimediaBackend/eliminarFoto'); ?>",
									type:'POST',
									contentType:false,
									data:data,
									processData:false,
									cache:false,

									success: function(data){

										$('#new_user_horizontal_form_mp3').each(function(){
										    this.reset();
										});

										$('.cargandoContenedor').css('opacity',1);
										$('.cargandoMultimedia').fadeOut('fast');										
										console.log(data);										
							        	console.log('Exito');
							        },
							        error: function(e) { 
							        	console.log(e);
							        	console.log('fallo');
							        }
								});
							});


							$('#new_user_horizontal_form_usuario').submit(function(e) {
					            e.preventDefault();
					        }).validate({
					            debug: false,
					            rules: {
					                "user_horizontal_name": {
					                    required: true

					                },
					                "user_horizontal_lastname": {
					                    required: true
					                },
					                "user_horizontal_usuario": {
					                    required: true
					                },
					                "user_horizontal_email": {
					                    required: true,
					                    email: true
					                },
					                "user_horizontal_pass": {
					                    required: true,
					                    /*number:true,
					                    minlength: 5,
					                    maxlength: 5*/
					                }
					            },
					            messages: {
					                "user_horizontal_name": {
					                    required: "Introduce tu nombre."
					                },
					                "user_horizontal_lastname": {
					                    required: "Apellido obligatorio."
					                },
					                "user_horizontal_usuario": {
					                    required: "Usuario obligatorio."
					                },
					                "user_horizontal_email": {
					                    required: "Introduce tu correo.",
					                    email: "Este no es un correo valido"
					                },
					                "user_horizontal_pass": {
					                    required: "Introduce tu contraseña.",
					                    /*number: "Introduce un código postal válido.",
					                    maxlength: "Debe contener 5 dígitos.",
					                    minlength: "Debe contener 5 dígitos."*/
					                }
					            },submitHandler: function(form){

					            	/*$('.cargandoContenedorUsuarios').css('opacity',0.5);
									$('.cargandoGifUsuario').fadeIn('fast');

					            	var data = {};

									var pass1 = $('#user_horizontal_pass').val();
									var pass2 = $('#user_horizontal_pass2').val();

									/*console.log(pass1);
									console.log(pass2)
									console.log('------');

									var tieneClase = $('#user_horizontal_pass_anterior').parent().parent().attr('class').split(' ');

									if(tieneClase[2] == 'hidden'){
										if(pass1 === pass2){
											data['id'] = $('input[name=id]').val();									
											data['nombre'] = $('#user_horizontal_name').val()
											data['apellido'] = $('#user_horizontal_lastname').val()
											data['email'] = $('#user_horizontal_email').val()
											data['usuario'] = $('#user_horizontal_usuario').val()
											data['pass'] = pass1;
											data['rol'] = $("select[name=rol] option:selected").val();


											$.ajax({
												url:"<?php echo site_url('usuarios/guardarUsuario'); ?>",
												type:'POST',
												dataType: "json",
												data:data,
												cache:false,

												success: function(data){	

													$('.cargandoContenedorUsuarios').css('opacity',1);
													$('.cargandoGifUsuario').fadeOut('fast');

													$('#new_user_horizontal_form_usuario').each(function(){
													    this.reset();
													});

													console.log(data);	
										        	console.log('Exito');

										        },
										        error: function(e) { 
										        	console.log(e);
										        	console.log('fallo');
										        }
											});	

											console.log(data);
										}else{
											$('.cargandoContenedorUsuarios').css('opacity',1);
											$('.cargandoGifUsuario').fadeOut('fast');

											$('.labelPass').fadeOut('fast');
											$('.labelPassError').append('<p class="help-block labelPassError">Las Contraseñas no coinciden</p>');
										}
									}else{

										data['id'] = $('input[name=id]').val();	
										data['passAnterior'] =  $('#user_horizontal_pass_anterior').val();

										console.log(data);

										$.ajax({
												url:"<?php echo site_url('usuarios/verificarPassword'); ?>",
												type:'POST',
												dataType: "json",
												data:data,
												cache:false,

												success: function(data){	

													validado = JSON.parse(data);

													if(validado['validado'] == 0){
														$('.cargandoContenedorUsuarios').css('opacity',1);
														$('.cargandoGifUsuario').fadeOut('fast');
														console.log('Contrase;a incorrecta');
														$('.labelPassAnterior').fadeOut('fast');
														$('.labelPassErrorAnterior').append('<p class="help-block labelPassError">Las Contraseña es incorrecta</p>');
													}else{		
														var data2 = {};
														if(pass1 === pass2){
															data2['id'] = $('input[name=id]').val();									
															data2['nombre'] = $('#user_horizontal_name').val()
															data2['apellido'] = $('#user_horizontal_lastname').val()
															data2['email'] = $('#user_horizontal_email').val()
															data2['usuario'] = $('#user_horizontal_usuario').val()
															data2['pass'] = pass1;
															data2['rol'] = $("select[name=rol] option:selected").val();

															console.log('entre al fondo');
															console.log(data2);

															$.ajax({
																url:"<?php echo site_url('usuarios/guardarUsuario'); ?>",
																type:'POST',
																dataType: "json",
																data:data2,
																cache:false,

																success: function(data){	

																	$('.cargandoContenedorUsuarios').css('opacity',1);
																	$('.cargandoGifUsuario').fadeOut('fast');

																	$('#new_user_horizontal_form_usuario').each(function(){
																	    this.reset();
																	});

																	console.log(data);	
														        	console.log('Exito');

														        },
														        error: function(e) { 
														        	console.log(e);
														        	console.log('fallo');
														        }
															});	

															console.log(data);
														}else{
															$('.cargandoContenedorUsuarios').css('opacity',1);
															$('.cargandoGifUsuario').fadeOut('fast');

															$('.labelPass').fadeOut('fast');
															$('.labelPassError').append('<p class="help-block labelPassError">Las Contraseñas no coinciden</p>');
														}
														console.log('contrase;a correcta');
													}
										        },
										        error: function(e) { 
										        	console.log(e);
										        	console.log('fallo');
										        }
											});	
									}	*/
					            }
					            
					        });

							$('#cambiarPasswordUsuario').click(function(e) {
								e.preventDefault();
								$('#user_horizontal_pass').parent().parent().removeClass('hidden');
								$('#user_horizontal_pass2').parent().parent().removeClass('hidden');
								$('#cambiarPasswordUsuario').parent().parent().addClass('hidden');
								$('#user_horizontal_pass_anterior').parent().parent().removeClass('hidden');
							});

							
							$('#cancelarModificacionUsuario').click(function() {
								//$('#opcionLider1').click();
								$(location).attr('href',window.location.href.replace(window.location.search,'?u=n'));							
								
							});

							$('#eliminarUsuario').click(function () {
								$('.cajaDialogoEliminar').dialog({ 
									title:'Confirmar',
									open: function() {
									      var markup = 'Desea eliminar este usuario?';
									      $(this).html(markup);
									    },
									buttons: [
									    {
									      text: "Aceptar",
									      click: function() {

									      	$('.cargandoContenedorUsuarios').css('opacity',0.5);
											$('.cargandoGifUsuario').fadeIn('fast');

											var data = {};
											data['id'] = $('input[name=id]').val();	
											data['id2'] = 

											console.log(data);	

											$.ajax({
												url:"<?php echo site_url('usuarios/eliminarUsuario'); ?>",
												type:'POST',
												dataType: "json",
												data:data,
												cache:false,

												success: function(data){	

													$('.cargandoContenedorUsuarios').css('opacity',1);
													$('.cargandoGifUsuario').fadeOut('fast');

													setTimeout(function(){ 
														$(location).attr('href',window.location.href.replace(window.location.search,'?u=n'));
													}, 100);

													console.log(data);	
										        	console.log('Exito');

										        },
										        error: function(e) { 
										        	console.log(e);
										        	console.log('fallo');
										        }
											});

									        $(this).dialog( "close" );
									      }
									    },
									    {
									      text: "Cancelar",
									      click: function() {
									        $( this ).dialog( "close");
									      }
									    }
									  ]
								});	
							});							
							
							$('#submitAgregarActividad').click(function(e){
								
								$('.cargandoContenedorUsuarios').css('opacity',0.5);
								$('.cargandoGifUsuario').fadeIn('fast');

								e.preventDefault();
								var data = {};



								data['id'] = $('input[name=id]').val();
								data['nombreActividad'] = $('input[name=nombreActividad]').val();
								data['fechaActividad'] = $('input[name=fechaActividad]').val();
								data['horaActividad'] = $('input[name=horaActividad]').val();
								data['diaActividad'] = $("select[name=dias] option:selected").val();
								data['descripcion_actividad'] =$('#descripcion_actividad').val();

								console.log(data);																

								$.ajax({
							        type: "POST",
							        url: "<?php echo site_url('actividades/agregarActividad'); ?>",
							        data: data, // <--- THIS IS THE CHANGE
							        dataType: "json",
							        success: function(data){
							        $('.cargandoContenedorUsuarios').css('opacity',1);
													$('.cargandoGifUsuario').fadeOut('fast');							        								        	
							        	console.log('Exito');
							        	console.log(data);
							        	setTimeout(function(){											        	 
											$(location).attr('href',window.location.href.replace(window.location.search,'?ac=2'));
										}, 100);
							        },
							        error: function(e) { 
							        	$('.cargandoContenedorUsuarios').css('opacity',1);
													$('.cargandoGifUsuario').fadeOut('fast');
							        	console.log(e);
							        	console.log('fallo');
							        }
   								});				
								
							});

							$('.removeActividadIrregular').click(function(e) {	
								e.preventDefault();
								removerAudio = $(this);
								//console.log(removerAudio);
								
								$('.cajaDialogoEliminarActividad').dialog({ 
									title:'Confirmar',
									open: function() {
									      var markup = 'Desea eliminar esta Actividad?';
									      $(this).html(markup);
									    },
									buttons: [
									    {
									      text: "Aceptar",
									      click: function() {
									      	    ruta = $('.removeActividadIrregular').attr('href');
												rutaDividida = ruta.split('=');
												console.log(rutaDividida[1]);

												var data={};
												data['id'] = rutaDividida[1];
												$(this).parent().parent().parent().remove();

												$.ajax({
													url:"<?php echo site_url('actividades/eliminarActividad'); ?>",
													type:'POST',
													dataType: "json",
													data:data,
													cache:false,

													success: function(data){
														console.log(data);
											        	console.log('Exito eliminar Actividad');

											        	setTimeout(function(){											        	 
															$(location).attr('href',window.location.href.replace(window.location.search,'?ac=1'));
														}, 100);
											        },
											        error: function(e) { 
											        	console.log(e);
											        	console.log('fallo');
											        }
												});
									      }
									    },
									    {
									      text: "Cancelar",
									      click: function() {
									        $( this ).dialog( "close");
									      }
									    }
									  ]
								});	
							});


						$('#submitAgregarMinisterio').click(function(e){
								e.preventDefault();

								$('.cargandoContenedorUsuarios').css('opacity',0.5);
								$('.cargandoGifUsuario').fadeIn('fast');

								var id = $('input[name=id]').val();
								var nombreMinisterio =  $('input[name=nombre_ministerio]').val();
								var lider = $("select[name=lider] option:selected").val();
   								var inputFileImage = document.getElementById('user_horizontal_imagen_ministerio');
								var file = inputFileImage.files[0];
								var data = new FormData();
								data.append('id',id);
								data.append('nombreMinisterio',nombreMinisterio);
								data.append('lider',lider);
								data.append('archivo',file);	


								console.log(data);														

								$.ajax({
							        type: "POST",
							        url: "<?php echo site_url('Ministerios/agregarMinisterio'); ?>",	
									contentType:false,
									data:data,
									processData:false,
									cache:false,
							        success: function(data){
							        	$('.cargandoContenedorUsuarios').css('opacity',1);
										$('.cargandoGifUsuario').fadeOut('fast');							        								        	
							        	console.log('Exito');
							        	console.log(data);
							        	setTimeout(function(){											        	 
											$(location).attr('href',window.location.href.replace(window.location.search,'?ac=2'));
										}, 100);
							        },
							        error: function(e) { 
							        	$('.cargandoContenedorUsuarios').css('opacity',1);
										$('.cargandoGifUsuario').fadeOut('fast');
							        	console.log(e);
							        	console.log('fallo');
							        }
   								});		
								
							});


							$('.redirectMinisterios').click(function(e) {
								e.preventDefault();
								$('.contenidoMinisterio > .opcio11').fadeOut('fast');

								var href = $(this).attr('href');
								var hrefDividiva = href.split('=');

								switch(parseInt(hrefDividiva[1])){
									case 5:
										console.log('estoy');										
										//$('#opcio1').click();
										$('.opcio1').fadeIn('slow');
										break;
									case 6:
										$('#opcio2').click();
										break;
									case 7:
										$('#opcio3').click();
										break;$('#opcio1').click();
									case 8:
										$('#opcio4').click();
										break;
									case 9:
										$('#opcio5').click();
										break;
									case 10:
										$('#opcio6').click();
										break;
									default:
										break;
								}	

								console.log(hrefDividiva);	
							});


							

						});


	
				</script>
				<?php 

				//--------VALIDAR LOS INPUT TYPE RADIO DEL FORMULARIO DE MIEMBROS CUANDO RECIBA UN MIEMBRO PARA EDITAR.----------------	
				if(isset($_GET['a'])){?>
						<script type="text/javascript">
							<?php if($_GET['a']==2){ ?>
								$('#op2').click();
							<?php } ?>
							<?php if($_GET['a']==6){ ?>
								$('#op6').click();
							<?php } ?>

							
						</script>	

					<?php
				}

				if(isset($_GET['e'])){?>
						<script type="text/javascript">
							<?php if($_GET['e']==2){ ?>
								$('#opE2').click();
							<?php } ?>
							<?php if($_GET['e']==6){ ?>
								$('#opE6').click();
							<?php } ?>
							
						</script>	

					<?php
				}

				if(isset($_GET['b'])){?>
						<script type="text/javascript">
							<?php if($_GET['b']==2){ ?>
									$('#opc2').click();
									$('.integrantes').fadeIn('slow');
							<?php }
							if($_GET['b']==1){ ?>
									$('#opc1').click();
							<?php }	?>

						</script>

					<?php
				}

				if(isset($_GET['n'])){?>
						<script type="text/javascript">
							<?php 
							if($_GET['n']==2){ ?>
									$('#opci2').click();
							<?php }	?>
						</script>

					<?php
				}

				if(isset($_GET['l'])){?>
						<script type="text/javascript">
							<?php 
							if($_GET['l']==2){ ?>
									$('#opcionLider2').click();
							<?php 
								}
							
							?>
							<?php 
							if($_GET['l']=='n'){ ?>
									$('#opcionLider1').click();
							<?php 
								}
							
							?>
							
						</script>

					<?php
				}

				if(isset($_GET['m'])){?>
						<script type="text/javascript">
							<?php 
							if($_GET['m']=='n'){ ?>
									$('#opcionMultimedia1').click();
							<?php } ?>
							<?php 
							if($_GET['m']==2){ ?>
									$('#opcionMultimedia2').click();
							<?php } ?>

							<?php 
							if($_GET['m']==3){ ?>
									$('#opcionMultimedia3').click();
							<?php } ?>

							<?php 
							if($_GET['m']==5){ ?>
									$('#opcionMultimedia5').click();
							<?php } ?>
						</script>

					<?php
				}

				if(isset($_GET['u'])){?>
						<script type="text/javascript">
							<?php 
							if($_GET['u']=='2'){ ?>
									$('#op2').click();
							<?php } ?>

							
						</script>

					<?php
				}

					/*Redireccionar en la seccion de actividades cuando haga el submit*/
				
				if(isset($_GET['ac'])){?>
						<script type="text/javascript">
							<?php if($_GET['ac']==1){ ?>
								$('#opciA1').click();
							<?php } ?>
							<?php if($_GET['ac']==2){ ?>
								$('#opciA2').click();
							<?php } ?>

							
						</script>	

					<?php
				}


			

				if(isset($miembro)){ ?>
						<script type="text/javascript">
							$('input[name=user_horizontal_sexo][value=<?php echo $miembro->sexo;?>]').attr('checked',true);
							$('input[name=user_horizontal_estadoCivil][value=<?php echo $miembro->estadoCivil;?>]').attr('checked',true);
							$('input[name=user_horizontal_estadoEducativo][value=<?php echo $miembro->estadoEducativo;?>]').attr('checked',true);
							$('input[name=user_horizontal_nivelEducativo][value=<?php echo $miembro->nivelEducativo;?>]').attr('checked',true);
							$('input[name=user_horizontal_estadoLaboral][value=<?php echo $miembro->estadoLaboral;?>]').attr('checked',true);
				
						</script>
				<?php
					}
				?>


	
				
	</body>
</html>