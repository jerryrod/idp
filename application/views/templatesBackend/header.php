<?php 
	if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 
?>


<!DOCTYPE html>
	<html lang="es">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title>IDP</title>
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/application/css/style.css">
	    
	    <!-- Bootstrap -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	    <!--<link rel="stylesheet" href="<?php echo base_url(); ?>application/css/bootstrap.min.css">-->

	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>application/css/jquery-ui.css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	    <link rel="stylesheet" href="<?php echo base_url(); ?>application/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
		
	    <script src="<?php echo base_url(); ?>application/js/Chart.js-master/Chart.js"></script>
		<script type="text/javascript" src="http://jqwidgets.com/public/jqwidgets/jqx-all.js"></script>


	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
		 	
	</head>
	<body>
	<div class="container-fluid">
		<div class="row">
			<nav class="navSBackend col-md-12">
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/homeBackend"><img src="<?php echo base_url(); ?>/application/img/logo.png"></a></li>
			    	<li><a id="Noticias" href="<?php echo base_url(); ?>index.php/homeBackend">Pagina Principal</a></li>
			    	<li role="presentation" class="dropdown">
        				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          					Secciones <span class="caret"></span>
       					 </a>
				        <ul class="dropdown-menu">
				          <li><a href="<?php echo base_url(); ?>index.php/miembros">Miembros</a></li>
				          <li><a href="<?php echo base_url(); ?>index.php/gcrecimiento">Grupos de Crecimiento</a></li>
				          <li><a href="<?php echo base_url(); ?>index.php/noticiasBackend">Noticias</a></li>
				          <li role="separator" class="divider"></li>
				          <li><a href="<?php echo base_url(); ?>index.php/finanzas">Finanzas</a></li>
				          <li><a href="<?php echo base_url(); ?>index.php/ministerios">Ministerios</a></li>
				          <li><a href="<?php echo base_url(); ?>index.php/estadistica">Estadistica</a></li>
				          <li><a href="<?php echo base_url(); ?>index.php/liderazgoBackend">Liderazgo</a></li>
				          <li><a href="<?php echo base_url(); ?>index.php/multimediaBackend">Multimedia</a></li>
				          <li><a href="<?php echo base_url(); ?>index.php/actividades">Activiades</a></li>
				          <li><a href="<?php echo base_url(); ?>index.php/eventos">Eventos</a></li>
				          <li><a href="<?php echo base_url(); ?>index.php/webservices">WEbservice</a></li>
				          <li><a href="<?php echo base_url(); ?>index.php/generador">Generar Archivos</a></li>
				        </ul>
      				</li>
			    	<li><a id="usuarios" href="<?php echo base_url(); ?>index.php/usuarios">Usuarios</a></li>
			    	<li><a id="log" href="<?php echo base_url()?>index.php/login/logout_ci">Cerrar Seccion</a></li>
			    	<li><p id="usuario">Hola <?php echo $this->session->userdata('username'); ?></p></li>

			    	<!--Para usar el fancyBox una clase para llamarla desde jquery y cargar el fancy y un ID que es el html o archivo que mostrara dentro del fancy-->
			    	<!--<li><a class="various" href="#Log">Iniciar Seccion</a></li>-->
			    </ul>
			</nav>
		</div>	
</div>

			
