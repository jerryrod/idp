<?php if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 

?>
<style>
	.fancybox-inner{
		    overflow-x: hidden !important;
	}
</style>
<div style="width:800px">
	<div class="row">
		<div class="col-md-11 formularioUsuarioPapa" style="margin-left:4%;">
			<div class="row">
				<div class="col-md-12">
					<center class="cargandoGifUsuario">
						<img id="cargandoGifUsuario" src="<?php echo base_url(); ?>/application/img/loading.gif">
					</center>
				</div>
				<div class="alert alert-success SuccessMensajeUsuario">
				    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				    <strong>Success!</strong> Usuario agregado.
				 </div>
				 <div class="alert alert-danger dangerMensajeContrasenaUsuario" role="alert">
					  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					  <span class="sr-only">Error:</span>
					  La contraseña no coincide.
				</div>			

				<div class="panel panel-primary">
				    <div class="panel-heading">Formulario de Usuarios</div>
				    <div class="panel-body">
				   		<?php 
				   			//var_dump($miembro);
				   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_usuario', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

				   			echo form_open('usuarios/guardarUsuario',$attributes); 

				   			$datahidden = array(
					              'utf8'  => '✓',
					              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
					            );
				   			?>
				   			<div style="display:none">
								<?php echo form_hidden('idIglesia',$idIglesia);
									 echo form_hidden($datahidden);
								?>
							</div>
							<!--/////////////////  NOMBRE /////////////////////// -->
							<?php 
							
				   			$nombre = array(
					              'name'        => 'user_horizontal_name',
					              'id'          => 'user_horizontal_name',
					              'class'		=> 'string form-control',
					              'placeholder' => 'Nombre',
					              'required'    => 'required'
					         );
				   			?> 
				   			<div class="form-group  required user_horizontal_email">
					  			<label class="required col-sm-3 control-label" for="user_horizontal_name"><abbr title="Campo Obligatorio">*</abbr> Nombre(s)</label>
					  			<div class="col-sm-9">
					  				<?php echo form_input($nombre); ?>
					  				<p class="help-block">Primer y Segundo Nombre</p>
					  			</div>
					  		</div>

					  		<!--/////////////////  APELLIDO  /////////////////////// -->
					  		<?php 

				   			$apellido = array(
					              'name'        => 'user_horizontal_lastname',
					              'id'          => 'user_horizontal_lastname',
					              'class'		=> 'string form-control',
					              'placeholder' => 'Apellido',
					              'required'    => 'required'
					         );
				   			?> 
				   			<div class="form-group required user_horizontal_email">
					  			<label class="col-sm-3 control-label" for="user_horizontal_lastname"><abbr title="Campo Obligatorio">*</abbr> Apellido(s)</label>
					  			<div class="col-sm-9">
					  				<?php echo form_input($apellido); ?>
					  				<p class="help-block">Primer y Segundo Apellido</p>
					  			</div>
					  		</div>

					  		<!--/////////////////  EMAIL /////////////////////// -->

					  		<?php 

				   			$email = array(
					              'name'        => 'user_horizontal_email',
					              'id'          => 'user_horizontal_email',
					              'class'		=> 'string email required form-control',
					              'placeholder' => 'Correo Electronico',
					              'type'		=> 'email'
					         );
				   			?> 
				   			<div class="form-group email required user_horizontal_email">
					  			<label class="email col-sm-3 control-label" for="user_horizontal_email"> Correo Electronico</label>
					  			<div class="col-sm-9">
					  				<?php echo form_input($email); ?>
					  				<p class="help-block">Correo Electronico si lo posee</p>
					  			</div>
					  		</div>	


					  		<!--/////////////////  Usuario  /////////////////////// -->
					  		<?php 

						  		if(isset($usernameEditar) && $usernameEditar != ''){
						  			$usuario = array(
						              'name'        => 'user_horizontal_usuario',
						              'id'          => 'user_horizontal_usuario',
						              'class'		=> 'string form-control',
						              'placeholder' => 'Ej: Maria22',
						              'required'    => 'required',
						              'readonly'	=> 'readonly'
						         
						         );
						  		}else{
						  			$usuario = array(
						              'name'        => 'user_horizontal_usuario',
						              'id'          => 'user_horizontal_usuario',
						              'class'		=> 'string form-control',
						              'placeholder' => 'Ej: Maria22',
						              'required'    => 'required'
						         );
						  		}

				   			?> 
				   			<div class="form-group user_horizontal_email">
					  			<label class="col-sm-3 control-label" for="user_horizontal_usuario"><abbr title="Campo Obligatorio">*</abbr> Usuario</label>
					  			<div class="col-sm-9">
					  				<?php echo form_input($usuario); ?>
					  			</div>
					  		</div>	


					  		<!--/////////////////  Contrase;a Anterior  /////////////////////// -->
					  		<?php								  		

				   			$passAnterior = array(
					              'name'        => 'pass_anterior',
					              'id'          => 'pass_anterior',
					              'class'		=> 'string form-control',
					              'placeholder' => 'Contraseña',
					              'type'		=>'password'
					         );
				   			?> 
				   			<div class="form-group user_horizontal_email hidden">
					  			<label class=" col-sm-3 control-label" for="pass_anterior"><abbr title="Campo Obligatorio">*</abbr> Contraseña anterior</label>
					  			<div class="col-sm-9 labelPassErrorAnterior">
					  				<?php echo form_input($passAnterior); ?>
					  				<p class="help-block labelPassAnterior">Ingrese la contraseña</p>
					  			</div>
					  		</div>	



					  		<!--/////////////////  Contrase;a  /////////////////////// -->
					  		<?php								  		

				   			$pass = array(
					              'name'        => 'user_horizontal_pass',
					              'id'          => 'user_horizontal_pass',
					              'class'		=> 'string form-control',
					              'placeholder' => 'Contraseña',
					              'required'    => 'required',
					              'type'		=>'password'
					         );
				   			?> 
				   			<div class="form-group user_horizontal_email <?php if(isset($_GET['u']) && isset($_GET['id'])){ echo "hidden"; } ?>">
					  			<label class=" col-sm-3 control-label" for="user_horizontal_pass"><abbr title="Campo Obligatorio">*</abbr> Contraseña</label>
					  			<div class="col-sm-9 labelPassError">
					  				<?php echo form_input($pass); ?>
					  				<p class="help-block labelPass">Ingrese la contraseña</p>
					  			</div>
					  		</div>	

					  		<?php 

				   			$pass2 = array(
					              'name'        => 'user_horizontal_pass2',
					              'id'          => 'user_horizontal_pass2',
					              'class'		=> 'string  form-control',
					              'placeholder' => 'Contraseña',
					              'required'    => 'required',
					              'type'		=>'password'
					         );
				   			?> 
				   			<div class="form-group user_horizontal_email <?php if(isset($_GET['u']) && isset($_GET['id'])){ echo "hidden"; } ?>">
					  			<div class="col-sm-9 col-sm-offset-3 labelPassError">
					  				<?php echo form_input($pass2); ?>
					  				<p class="help-block labelPass">Confirmar la contraseña</p>
					  			</div>
					  		</div>	

				  			<div class="form-group user_horizontal_email  <?php if((isset($_GET['u']) && !isset($_GET['id'])) || !isset($_GET['u']) && !isset($_GET['id'])){ echo "hidden"; } ?>">
					  			<div class="col-md-2 col-md-offset-9">
					  				<a href="" name="cambiar" class="btn btn-info" id="cambiarPasswordUsuario">Cambiar Contraseña</a>
					  			</div>
					  		</div>

					  		<!--/////////////////////////    Rol    \\\\\\\\\\\\\\\\\\\\\\\\\\-->

					  		<?php 
					  			$rolEditar = 1;
					  			$atributosRol ='id="user_horizontal_name_rol" class="string form-control"'; 
					  			

					  		?>

					  		<div class="form-group user_horizontal_email">
					  		<label class="col-sm-3 control-label" for="user_horizontal_name_rol"><abbr title="Campo Obligatorio">*</abbr>Rol</label>
					  			<div class="col-sm-9">
					  				<?php echo form_dropdown('rol',$rol, $rolEditar ,$atributosRol) ?>
					  				<p id="labeSeleccioneRol " class="help-block">Seleccione el rol</p>
					  			</div>
					  		</div>	


					  		<!--/////////////////  Iglesia /////////////////////// -->

							<?php 

				   			$nueva = array(
					              'name'        => 'iglesia',
					              'id'          => 'iglesiaNueva',
					              'class'		=> 'check_boxes optional',
					              'value'       =>  '1'
					         );

				   			$existente = array(
					              'name'        => 'iglesia',
					              'id'          => 'iglesiaExistente',
					              'class'		=> 'check_boxes optional',
					              'value'       =>  '2'
					         );

				   			$ninguna = array(
					              'name'        => 'iglesia',
					              'id'          => 'ningunaIglesia',
					              'class'		=> 'check_boxes optional',
					              'value'       =>  '3'
					         );
				   			
				   			?> 

							<div class="form-group check_boxes optional user_horizontal_choices">
								<label class="check_boxes optional col-sm-3 control-label"><abbr title="Campo Obligatorio">*</abbr>Asignar a una Iglesia: </label>
								<div class="col-sm-9">
									<span class="radio">
										<label for="iglesiaNueva">
										<?php echo form_radio($nueva); ?>Nueva</label>
									</span>
									<span class="radio">
										<label for="iglesiaExistente">
										<?php echo form_radio($existente); ?>Existente</label>
									</span>
									<span class="radio">
										<label for="ningunaIglesia">
										<?php echo form_radio($ninguna); ?>Ningunas</label>
									</span>
									<p class="help-block">Iglesia Nueva - Iglesia Existente - Sin Iglesia Asignada.</p>
								</div>
							</div>

							<!--/////////// Si la iglesia es nueva llenara todos los campos siguientes /////////// -->

							<div class="iglesiaNueva hidden">								
							
								<!--/////////////////  NOMBRE de iglesia /////////////////////// -->
								<?php 
								
					   			$nombreIglesia = array(
						              'name'        => 'user_horizontal_nameIglesia',
						              'id'          => 'user_horizontal_nameIglesia',
						              'class'		=> 'string form-control',
						              'placeholder' => 'Ej. IDP Los Frailes I'
						         );
					   			?> 
					   			<div class="form-group  required user_horizontal_email">
						  			<label class="required col-sm-3 control-label" for="user_horizontal_nameIglesia"><abbr title="Campo Obligatorio">*</abbr> Nombre de la Iglesia</label>
						  			<div class="col-sm-9">
						  				<?php echo form_input($nombreIglesia); ?>
						  				<p class="help-block">Especificar el nombre de la Iglesia </p>
						  			</div>
						  		</div>

						  		<!--/////////////////  Urbanizacion de iglesia /////////////////////// -->
								<?php 
								
					   			$sectorIglesia = array(
						              'name'        => 'sectorIglesia',
						              'id'          => 'sectorIglesia',
						              'class'		=> 'string form-control',
						              'placeholder' => 'Ej. Miramar Norte'
						         );
					   			?> 
					   			<div class="form-group  required user_horizontal_email">
						  			<label class="required col-sm-3 control-label" for="sectorIglesia"><abbr title="Campo Obligatorio">*</abbr> Sector o Urbanización:</label>
						  			<div class="col-sm-9">
						  				<?php echo form_input($sectorIglesia); ?>
						  				<p class="help-block">Especificar el nombre del sector de la Iglesia </p>
						  			</div>
						  		</div>

						  		<!--/////////////////  Calle de iglesia /////////////////////// -->
								<?php 
								
					   			$calleIglesia = array(
						              'name'        => 'user_horizontal_calleIglesia',
						              'id'          => 'user_horizontal_calleIglesia',
						              'class'		=> 'string form-control',
						              'placeholder' => 'Ej. Miramar Norte'
						         );
					   			?> 
					   			<div class="form-group  required user_horizontal_email">
						  			<label class="required col-sm-3 control-label" for="user_horizontal_calleIglesia"><abbr title="Campo Obligatorio">*</abbr> Calle:</label>
						  			<div class="col-sm-9">
						  				<?php echo form_input($calleIglesia); ?>
						  				<p class="help-block">Especificar el nombre de la calle de la Iglesia </p>
						  			</div>
						  		</div>

						  		<!--/////////////////  Nuemero de local de la iglesia /////////////////////// -->
								<?php 
								
					   			$numeroLocalIglesia = array(
						              'name'        => 'numeroLocalIglesia',
						              'id'          => 'numeroLocalIglesia',
						              'class'		=> 'string form-control',
						              'placeholder' => 'Ej. 16'
						         );
					   			?> 
					   			<div class="form-group  required user_horizontal_email">
						  			<label class="required col-sm-3 control-label" for="numeroLocalIglesia"><abbr title="Campo Obligatorio">*</abbr> Numero Local:</label>
						  			<div class="col-sm-9">
						  				<?php echo form_input($numeroLocalIglesia); ?>
						  				<p class="help-block">Especificar el numero del local de la Iglesia </p>
						  			</div>
						  		</div>

						  		<!--/////////////////  Pais /////////////////////// -->
								<?php 
								
								
								$valuePais=166;
									

					   			$paisIglesiaAtributos = array(
						              'id'          => 'paisIglesia',
						              'class'		=> 'string email required form-control'
						         );
					   			?> 
					   			<div class="form-group email required user_horizontal_email">
						  			<label class="email required col-sm-3 control-label" for="paisIglesia">Pais</label>
						  			<div class="col-sm-9">
						  				<?php echo form_dropdown('paisIglesia',$pais, $valuePais,$paisIglesiaAtributos) ?>								  				
						  				<p class="help-block">Seleccione la provincia</p>
						  			</div>
						  		</div>

						  		<!--/////////////////  Provincia /////////////////////// -->
								<?php 
								
								
								$valueProvincia=1;
									

					   			$provinciaIglesiaAtributos = array(
						              'id'          => 'provincia',
						              'class'		=> 'string email required form-control'
						         );
					   			?> 
					   			<div class="form-group email required user_horizontal_email">
						  			<label class="email required col-sm-3 control-label" for="provincia">Provincia</label>
						  			<div class="col-sm-9">
						  				<?php echo form_dropdown('provincia',$provincias, $valueProvincia,$provinciaIglesiaAtributos) ?>								  				
						  				<p class="help-block">Seleccione la provincia</p>
						  			</div>
						  		</div>

						  	</div>

						  	<div class="iglesiaExistente hidden">

						  	<!--/////////////////  Lista de Iglesias Agregadas /////////////////////// -->
								
								<?php 
								
								
								$valueIglesiasAgregadas=1;
									

					   			$iglesiasAgregadasAtributos = array(
						              'id'          => 'iglesiasAgregadas',
						              'class'		=> 'string email required form-control'
						         );
					   			?> 
					   			<div class="form-group email required user_horizontal_email">
						  			<label class="email required col-sm-3 control-label" for="iglesiasAgregadas">Iglesias: </label>
						  			<div class="col-sm-9">
						  				<?php echo form_dropdown('iglesiasAgregadas',$iglesias, $valueIglesiasAgregadas,$iglesiasAgregadasAtributos) ?>								  				
						  				<p class="help-block">Seleccione la iglesia</p>
						  			</div>
						  		</div>

						  	</div>


					  		<?php 
					  			$atributosSubmit ='class="btn btn-primary" id="submitAgregarUsuario2"';
					  			if((isset($_GET['u']) && !isset($_GET['id'])) || !isset($_GET['u']) && !isset($_GET['id'])){ echo form_submit('commit', 'Registrar Usuario',$atributosSubmit); }
					  			if(isset($_GET['u']) && isset($_GET['id'])){ 
					  				echo form_submit('commit', 'Modificar Usuario',$atributosSubmit);
					  		?>
					  		<input type="button" value="Eliminar Usuario" class="btn btn-danger " id="eliminarUsuario">

					  		<?php 

					  			} 
					  			echo form_close();
					  		?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js" type="text/javascript"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>application/js/funciones.js"></script>

<script type="text/javascript">
		$(document).ready(function () {
			$('input[name=iglesia]').change(function  () {
				if($(this).val() == 1){
					$('.iglesiaExistente').addClass('hidden');
					$('.iglesiaNueva').removeClass('hidden');
				}
				if($(this).val() == 2){
					$('.iglesiaNueva').addClass('hidden');
					$('.iglesiaExistente').removeClass('hidden');
				}
				if($(this).val() == 3){
					$('.iglesiaNueva').addClass('hidden');
					$('.iglesiaExistente').addClass('hidden');
				}
			});

		});

</script>
</body>
</html>