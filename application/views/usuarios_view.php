<?php if(!$this->session->userdata('is_logued_in')){
		redirect(base_url().'index.php/login');
	} 

	$id= '';
	$nombreEditar ='';
	$apellidoEditar ='';
	$emailEditar='';
	$perfilEditar='';
	$usernameEditar='';
	$passwordEditar='';

	if(isset($usuario)){
		//var_dump($miembro);
		$id =$usuario->id;
		$nombreEditar = $usuario->nombre;
		$apellidoEditar =$usuario->apellido;
		$emailEditar = $usuario->email;
		$perfilEditar =$usuario->perfil;
		$usernameEditar = $usuario->username;
		$passwordEditar = $usuario->password;
	}

	//var_dump($usuario);

	
    


?>
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<div class="col-md-12">
				<h1>Mantenimiento de Usuarios</h1>
				<center class="cargandoGifUsuario">
					<img id="cargandoGifUsuario" src="<?php echo base_url(); ?>/application/img/loading.gif">
				</center>
			</div>
			<div class="col-md-12 errorFormularioMiembros">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Error!!!!</span> Verifica que los campos esten introducidos de la manera correcta. </h1>
					</div>
				</div>				
			</div>
			<div class="col-md-12 successFormularioMiembros">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Usuario!!!!</span> Insertado de manera correcta. </h1>
					</div>
				</div>				
			</div>
			<div class="col-md-12 successFormularioAsistencia">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Asistencia!!!!</span> Pasada Correctamente. </h1>
					</div>
				</div>				
			</div>
			<div class="col-md-12 successFormularioEvento">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><span>Evento!!!!</span> creado correctamente. </h1>
					</div>
				</div>
				
			</div>
			<div class="col-md-12 cargandoContenedorUsuarios">
				<div class="row BoxGeneralMembresia">
					<div class="col-md-2 menuLateralMembresia">
						<ul class="nav nav-pills nav-stacked">
							<li><a id="op1" href="">Usuarios Creados</a></li>
							<li><a id="op2" href="">Agregar Usuario</a></li>
						</ul>
					</div>

					<div class="col-md-8 contenidoMembresia">
						<div class="op1">
							<div class="boxMiembrosPrincipal">
									<div class="row">
										<div class="col-md-3">
											<p>Usuario</p>
										</div>
										<div class="col-md-2">
											<p>Rol</p>
										</div>
										<div class="col-md-2">
											<p>Operacion</p>
										</div>
									</div>
										<?php 
											//var_dump($usuarios);
											//$CI =& get_instance();
											foreach ($usuarios as $key => $value) {
												?>
												<div class="row">
													<div class="col-md-3">
														<a class="various fancybox.ajax" href="<?php echo base_url(); ?>index.php/usuarios/ver_Usuario/<?php echo $value['id']; ?>"><p><?php echo $value['username']; ?></p></a>
													</div>
													<div class="col-md-2">
														<p><?php echo $value['perfil']; ?></p>
													</div>
													<div class="col-md-2">
														<a href="<?php echo base_url(); ?>index.php/usuarios?u=2&id=<?php echo $value['id']; ?>"><p style="font-size: 25px;" class="glyphicon glyphicon-edit"></p></a>
													</div>
												</div>

												<?php
											}
										 ?>
									 </div>
						</div>
						<div class="op2">
							<div class="panel panel-primary">
								<div class="cajaDialogoEliminar" >
								</div>
							    <div class="panel-heading">Formulario de Usuarios</div>
							    <div class="panel-body">
							   		<?php 
							   			//var_dump($miembro);
							   			$attributes = array('class' => 'simple_form form-horizontal', 'id' => 'new_user_horizontal_form_usuario', 'accept-charset'=>'UTF-8','enctype'=>'multipart/form-data' );

							   			echo form_open('usuarios/guardarUsuario',$attributes); 

							   			$datahidden = array(
								              'utf8'  => '✓',
								              'authenticity_token' => '3cR7td0aw4iR0f7dbX0IJ3p0Zo+9yKqAuA41y71S3Hs='
								            );
							   			?>
							   			<div style="display:none">
											<?php echo form_hidden('id',$id);
												 echo form_hidden($datahidden);
											?>
										</div>
										<!--/////////////////  NOMBRE /////////////////////// -->
										<?php 
										
							   			$nombre = array(
								              'name'        => 'user_horizontal_name',
								              'id'          => 'user_horizontal_name',
								              'class'		=> 'string form-control',
								              'placeholder' => 'Nombre',
								              'required'    => 'required',
								              'value'		=> $nombreEditar
								         );
							   			?> 
							   			<div class="form-group  required user_horizontal_email">
								  			<label class="required col-sm-3 control-label" for="user_horizontal_name"><abbr title="Campo Obligatorio">*</abbr> Nombre(s)</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($nombre); ?>
								  				<p class="help-block">Primer y Segundo Nombre</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  APELLIDO  /////////////////////// -->
								  		<?php 

							   			$apellido = array(
								              'name'        => 'user_horizontal_lastname',
								              'id'          => 'user_horizontal_lastname',
								              'class'		=> 'string form-control',
								              'placeholder' => 'Apellido',
								              'required'    => 'required',
								              'value'		=> $apellidoEditar
								         );
							   			?> 
							   			<div class="form-group required user_horizontal_email">
								  			<label class="col-sm-3 control-label" for="user_horizontal_lastname"><abbr title="Campo Obligatorio">*</abbr> Apellido(s)</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($apellido); ?>
								  				<p class="help-block">Primer y Segundo Apellido</p>
								  			</div>
								  		</div>

								  		<!--/////////////////  EMAIL /////////////////////// -->

								  		<?php 

							   			$email = array(
								              'name'        => 'user_horizontal_email',
								              'id'          => 'user_horizontal_email',
								              'class'		=> 'string email required form-control',
								              'placeholder' => 'Correo Electronico',
								              'type'		=> 'email',
								              'value'		=> $emailEditar
								         );
							   			?> 
							   			<div class="form-group email required user_horizontal_email">
								  			<label class="email col-sm-3 control-label" for="user_horizontal_email"> Correo Electronico</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($email); ?>
								  				<p class="help-block">Correo Electronico si lo posee</p>
								  			</div>
								  		</div>	


								  		<!--/////////////////  Usuario  /////////////////////// -->
								  		<?php 

									  		if(isset($usernameEditar) && $usernameEditar != ''){
									  			$usuario = array(
									              'name'        => 'user_horizontal_usuario',
									              'id'          => 'user_horizontal_usuario',
									              'class'		=> 'string form-control',
									              'placeholder' => 'Ej: Maria22',
									              'required'    => 'required',
									              'readonly'	=> 'readonly',
									              'value'		=> $usernameEditar
									         
									         );
									  		}else{
									  			$usuario = array(
									              'name'        => 'user_horizontal_usuario',
									              'id'          => 'user_horizontal_usuario',
									              'class'		=> 'string form-control',
									              'placeholder' => 'Ej: Maria22',
									              'required'    => 'required',
									              'value'		=> $usernameEditar
									         );
									  		}

							   			?> 
							   			<div class="form-group user_horizontal_email">
								  			<label class="col-sm-3 control-label" for="user_horizontal_usuario"><abbr title="Campo Obligatorio">*</abbr> Usuario</label>
								  			<div class="col-sm-9">
								  				<?php echo form_input($usuario); ?>
								  				<?php if(isset($usernameEditar) && $usernameEditar == ''){ ?><p class="help-block">Ingrese el nombre de usuario</p> <?php } ?>
								  			</div>
								  		</div>	


								  		<!--/////////////////  Contrase;a Anterios  /////////////////////// -->
								  		<?php								  		

							   			$passAnterior = array(
								              'name'        => 'user_horizontal_pass_anterior',
								              'id'          => 'user_horizontal_pass_anterior',
								              'class'		=> 'string form-control',
								              'placeholder' => 'Contraseña',
								              'required'    => 'required',
								              'type'		=>'password'
								         );
							   			?> 
							   			<div class="form-group user_horizontal_email hidden">
								  			<label class=" col-sm-3 control-label" for="user_horizontal_pass_anterior"><abbr title="Campo Obligatorio">*</abbr> Contraseña anterior</label>
								  			<div class="col-sm-9 labelPassErrorAnterior">
								  				<?php echo form_input($passAnterior); ?>
								  				<p class="help-block labelPassAnterior">Ingrese la contraseña</p>
								  			</div>
								  		</div>	



								  		<!--/////////////////  Contrase;a  /////////////////////// -->
								  		<?php								  		

							   			$pass = array(
								              'name'        => 'user_horizontal_pass',
								              'id'          => 'user_horizontal_pass',
								              'class'		=> 'string form-control',
								              'placeholder' => 'Contraseña',
								              'required'    => 'required',
								              'type'		=>'password'
								         );
							   			?> 
							   			<div class="form-group user_horizontal_email <?php if(isset($_GET['u']) && isset($_GET['id'])){ echo "hidden"; } ?>">
								  			<label class=" col-sm-3 control-label" for="user_horizontal_pass"><abbr title="Campo Obligatorio">*</abbr> Contraseña</label>
								  			<div class="col-sm-9 labelPassError">
								  				<?php echo form_input($pass); ?>
								  				<p class="help-block labelPass">Ingrese la contraseña</p>
								  			</div>
								  		</div>	

								  		<?php 

							   			$pass2 = array(
								              'name'        => 'user_horizontal_pass2',
								              'id'          => 'user_horizontal_pass2',
								              'class'		=> 'string  form-control',
								              'placeholder' => 'Contraseña',
								              'required'    => 'required',
								              'type'		=>'password'
								         );
							   			?> 
							   			<div class="form-group user_horizontal_email <?php if(isset($_GET['u']) && isset($_GET['id'])){ echo "hidden"; } ?>">
								  			<div class="col-sm-9 col-sm-offset-3 labelPassError">
								  				<?php echo form_input($pass2); ?>
								  				<p class="help-block labelPass">Confirmar la contraseña</p>
								  			</div>
								  		</div>	

							  			<div class="form-group user_horizontal_email  <?php if((isset($_GET['u']) && !isset($_GET['id'])) || !isset($_GET['u']) && !isset($_GET['id'])){ echo "hidden"; } ?>">
								  			<div class="col-md-2 col-md-offset-9">
								  				<a href="" name="cambiar" class="btn btn-info" id="cambiarPasswordUsuario">Cambiar Contraseña</a>
								  			</div>
								  		</div>

								  		<!--/////////////////////////    Rol    \\\\\\\\\\\\\\\\\\\\\\\\\\-->

								  		<?php 
								  			$rolEditar = 1;
								  			$atributosRol ='id="user_horizontal_name_rol" class="string form-control"'; 
								  			if(isset($perfilEditar)){
								  				$rolEditar = $perfilEditar;
								  			}

								  		?>

								  		<div class="form-group user_horizontal_email">
								  		<label class="col-sm-3 control-label" for="user_horizontal_name_rol"><abbr title="Campo Obligatorio">*</abbr>Rol</label>
								  			<div class="col-sm-9">
								  				<?php echo form_dropdown('rol',$rol, $rolEditar ,$atributosRol) ?>
								  				<p id="labeSeleccioneRol " class="help-block">Seleccione el rol</p>
								  			</div>
								  		</div>										

								  		<?php 
								  			$atributosSubmit ='class="btn btn-primary" id="submitAgregarUsuario"';
								  			if((isset($_GET['u']) && !isset($_GET['id'])) || !isset($_GET['u']) && !isset($_GET['id'])){ echo form_submit('commit', 'Registrar Usuario',$atributosSubmit); }
								  			if(isset($_GET['u']) && isset($_GET['id'])){ 
								  				echo form_submit('commit', 'Modificar Usuario',$atributosSubmit);
								  		?>	
								  		<input type="button" value="Cancelar Modificacion" class="btn btn-warning " id="cancelarModificacionUsuario">
								  		<input type="button" value="Eliminar Usuario" class="btn btn-danger " id="eliminarUsuario">

								  		<?php 

								  			} 
								  			echo form_close();
								  		?>

								</div>
  							</div>
						</div>					
					
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>